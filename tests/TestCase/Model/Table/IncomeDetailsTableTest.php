<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IncomeDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IncomeDetailsTable Test Case
 */
class IncomeDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IncomeDetailsTable
     */
    public $IncomeDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.income_details',
        'app.period_incomes',
        'app.employees',
        'app.off_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.on_days',
        'app.period_discounts',
        'app.income_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IncomeDetails') ? [] : ['className' => 'App\Model\Table\IncomeDetailsTable'];
        $this->IncomeDetails = TableRegistry::get('IncomeDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IncomeDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
