<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PeriodSummariesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PeriodSummariesTable Test Case
 */
class PeriodSummariesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PeriodSummariesTable
     */
    public $PeriodSummaries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.period_summaries',
        'app.employees',
        'app.off_days',
        'app.period_summary_employees',
        'app.on_days',
        'app.period_discounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PeriodSummaries') ? [] : ['className' => 'App\Model\Table\PeriodSummariesTable'];
        $this->PeriodSummaries = TableRegistry::get('PeriodSummaries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PeriodSummaries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
