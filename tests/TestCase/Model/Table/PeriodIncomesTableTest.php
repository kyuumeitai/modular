<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PeriodIncomesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PeriodIncomesTable Test Case
 */
class PeriodIncomesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\PeriodIncomesTable
     */
    public $PeriodIncomes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.period_incomes',
        'app.employees',
        'app.off_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.on_days',
        'app.period_discounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('PeriodIncomes') ? [] : ['className' => 'App\Model\Table\PeriodIncomesTable'];
        $this->PeriodIncomes = TableRegistry::get('PeriodIncomes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PeriodIncomes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
