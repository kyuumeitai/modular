<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OnDaysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OnDaysTable Test Case
 */
class OnDaysTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OnDaysTable
     */
    public $OnDays;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.on_days',
        'app.employees',
        'app.off_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.period_discounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OnDays') ? [] : ['className' => 'App\Model\Table\OnDaysTable'];
        $this->OnDays = TableRegistry::get('OnDays', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OnDays);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
