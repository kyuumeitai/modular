<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OutcomeDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OutcomeDetailsTable Test Case
 */
class OutcomeDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OutcomeDetailsTable
     */
    public $OutcomeDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.outcome_details',
        'app.outcomes',
        'app.documents',
        'app.bos',
        'app.providers',
        'app.cities',
        'app.regions',
        'app.countries',
        'app.cost_centers',
        'app.budgets',
        'app.buy_orders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OutcomeDetails') ? [] : ['className' => 'App\Model\Table\OutcomeDetailsTable'];
        $this->OutcomeDetails = TableRegistry::get('OutcomeDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OutcomeDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
