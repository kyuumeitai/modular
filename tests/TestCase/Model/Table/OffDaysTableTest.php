<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OffDaysTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OffDaysTable Test Case
 */
class OffDaysTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OffDaysTable
     */
    public $OffDays;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.off_days',
        'app.employees',
        'app.on_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.period_discounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OffDays') ? [] : ['className' => 'App\Model\Table\OffDaysTable'];
        $this->OffDays = TableRegistry::get('OffDays', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OffDays);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
