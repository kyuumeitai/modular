<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\IncomeTypesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\IncomeTypesTable Test Case
 */
class IncomeTypesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\IncomeTypesTable
     */
    public $IncomeTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.income_types',
        'app.employees',
        'app.off_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.on_days',
        'app.period_discounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('IncomeTypes') ? [] : ['className' => 'App\Model\Table\IncomeTypesTable'];
        $this->IncomeTypes = TableRegistry::get('IncomeTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->IncomeTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
