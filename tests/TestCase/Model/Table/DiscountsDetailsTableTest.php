<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\DiscountsDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\DiscountsDetailsTable Test Case
 */
class DiscountsDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\DiscountsDetailsTable
     */
    public $DiscountsDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.discounts_details',
        'app.legal_discounts',
        'app.discount_types'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('DiscountsDetails') ? [] : ['className' => 'App\Model\Table\DiscountsDetailsTable'];
        $this->DiscountsDetails = TableRegistry::get('DiscountsDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->DiscountsDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
