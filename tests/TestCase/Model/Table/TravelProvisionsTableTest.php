<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TravelProvisionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TravelProvisionsTable Test Case
 */
class TravelProvisionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TravelProvisionsTable
     */
    public $TravelProvisions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.travel_provisions',
        'app.employees',
        'app.off_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.on_days',
        'app.period_discounts',
        'app.budgets',
        'app.buy_orders',
        'app.providers',
        'app.cities',
        'app.regions',
        'app.countries',
        'app.cost_centers',
        'app.outcome_details',
        'app.outcomes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('TravelProvisions') ? [] : ['className' => 'App\Model\Table\TravelProvisionsTable'];
        $this->TravelProvisions = TableRegistry::get('TravelProvisions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TravelProvisions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
