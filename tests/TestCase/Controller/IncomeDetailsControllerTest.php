<?php
namespace App\Test\TestCase\Controller;

use App\Controller\IncomeDetailsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\IncomeDetailsController Test Case
 */
class IncomeDetailsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.income_details',
        'app.period_incomes',
        'app.employees',
        'app.off_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.on_days',
        'app.period_discounts',
        'app.income_types'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
