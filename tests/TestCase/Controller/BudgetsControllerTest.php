<?php
namespace App\Test\TestCase\Controller;

use App\Controller\BudgetsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\BudgetsController Test Case
 */
class BudgetsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.budgets',
        'app.buy_orders',
        'app.providers',
        'app.cities',
        'app.regions',
        'app.countries',
        'app.cost_centers',
        'app.outcome_details',
        'app.outcomes',
        'app.documents',
        'app.bos'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
