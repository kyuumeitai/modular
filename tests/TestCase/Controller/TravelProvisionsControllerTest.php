<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TravelProvisionsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TravelProvisionsController Test Case
 */
class TravelProvisionsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.travel_provisions',
        'app.employees',
        'app.off_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.on_days',
        'app.period_discounts',
        'app.budgets',
        'app.buy_orders',
        'app.providers',
        'app.cities',
        'app.regions',
        'app.countries',
        'app.cost_centers',
        'app.outcome_details',
        'app.outcomes'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
