<?php
namespace App\Test\TestCase\Controller;

use App\Controller\OutcomeReleasesController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\OutcomeReleasesController Test Case
 */
class OutcomeReleasesControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.outcome_releases',
        'app.employees',
        'app.off_days',
        'app.on_days',
        'app.period_summaries',
        'app.period_summary_employees',
        'app.period_discounts',
        'app.cost_centers',
        'app.budgets',
        'app.buy_orders',
        'app.providers',
        'app.cities',
        'app.regions',
        'app.countries',
        'app.contacts',
        'app.buy_orders_details',
        'app.outcomes',
        'app.outcome_details'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
