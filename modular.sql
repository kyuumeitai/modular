-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-02-2019 a las 15:49:44
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `modular`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `budgets`
--

CREATE TABLE `budgets` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `buy_order_id` int(11) NOT NULL,
  `cost_center_id` int(11) NOT NULL,
  `outcome_id` int(11) NOT NULL,
  `period_month` int(2) NOT NULL,
  `period_year` int(4) NOT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buy_orders`
--

CREATE TABLE `buy_orders` (
  `id` int(11) NOT NULL,
  `cotizacion` int(11) DEFAULT NULL,
  `create_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `buy_date` date DEFAULT NULL,
  `provider_id` int(11) NOT NULL,
  `name_buyer` text NOT NULL,
  `rut_buyer` varchar(15) NOT NULL,
  `cost_center_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `buy_orders`
--

INSERT INTO `buy_orders` (`id`, `cotizacion`, `create_date`, `buy_date`, `provider_id`, `name_buyer`, `rut_buyer`, `cost_center_id`) VALUES
(3, 2323, '2016-12-12 00:57:56', '2016-12-12', 3, 'Alejandro Sanchez', '16894888-3', 4),
(4, 2323, '2016-12-12 02:43:25', '2016-12-12', 3, 'Alejandro Sanchez', '16894888-3', 4),
(5, 2323, '2016-12-12 03:09:28', '2016-12-12', 3, 'Alejandro Sanchez', '16894888-3', 4),
(6, 132, '2016-12-22 16:16:51', '1970-01-01', 2, 'Felipe Acuña', '17651442-6', 1),
(7, 132, '2016-12-22 16:17:24', '1970-01-01', 2, 'asdf', '123-12', 1),
(8, 3423432, '2016-12-26 20:20:11', '2016-01-12', 3, 'Alejandro', '16894888-3', 1),
(9, 3423432, '2016-12-26 20:24:50', '2016-01-12', 3, 'Alejandro', '16894888-3', 1),
(10, 3423432, '2016-12-28 19:50:58', '2016-01-12', 3, 'Alejandro', '16894888-3', 4),
(11, 3423432, '2016-12-28 19:55:08', '2016-01-12', 3, 'Alejandro', '16894888-3', 4),
(12, 123, '2016-12-28 23:32:20', '1970-01-01', 2, 'Felipe Acuña', '123-23', 5),
(13, 232, '2016-12-29 00:32:12', '1970-01-01', 2, '21', '123-13', 2),
(14, 121, '2016-12-29 17:10:34', '1970-01-01', 2, '232', '123-121', 1),
(15, 121, '2016-12-29 17:26:03', '1970-01-01', 2, '121', '123-232', 3),
(16, 12, '2016-12-29 17:28:11', '1970-01-01', 2, '121', '123-12', 3),
(17, 132, '2016-12-29 17:41:46', '1970-01-01', 2, 'Felipe Acuña', '17651442-3', 1),
(18, 232, '2016-12-29 18:04:38', '1970-01-01', 2, '121', '1123-2', 1),
(19, 12, '2016-12-29 18:10:01', '1970-01-01', 2, '23', '123-23', 1),
(20, 1, '2016-12-29 18:20:52', '1970-01-01', 2, '12', '123-121', 1),
(21, 12, '2016-12-29 20:16:34', '1970-01-01', 2, '12', '17651442-4', 2),
(22, 1245, '2018-03-20 12:59:09', '1970-01-01', 2, 'Raúl Orellana', '15524322-8', 1),
(23, 1, '2019-02-11 12:33:51', '1970-01-01', 2, 'Felipe Acuña Viera', '17651442-6', 1),
(24, 1, '2019-02-11 13:26:49', '1970-01-01', 2, 'Felipe Acuña Viera', '17651442-6', 2),
(25, 1, '2019-02-11 13:47:49', '1970-01-01', 2, 'Felipe Acuña Viera', '17651442-6', 2),
(26, 12, '2019-02-11 13:50:20', '2019-06-02', 2, 'Felipe Acuña Viera', '17651442-1', 2),
(27, 1, '2019-02-11 13:55:57', '1970-01-01', 2, 'Felipe Acuña Viera', '17651442-6', 2),
(28, 12, '2019-02-11 14:05:08', '1970-01-01', 2, 'Felipe Acuña Viera', '17651442-6', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `buy_orders_details`
--

CREATE TABLE `buy_orders_details` (
  `id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `item` text NOT NULL,
  `detail` text NOT NULL,
  `product_code` varchar(255) NOT NULL,
  `unit_price` double NOT NULL,
  `total_price` double NOT NULL,
  `buy_order_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `buy_orders_details`
--

INSERT INTO `buy_orders_details` (`id`, `quantity`, `item`, `detail`, `product_code`, `unit_price`, `total_price`, `buy_order_id`, `date_created`) VALUES
(1, 5, 'asdasd', 'adasdasdasd', '4443', 2500, 12500, 11, '2016-12-28 19:55:08'),
(2, 10, 'asdalsdn', 'asdmlakskd', '223', 50000, 500000, 11, '2016-12-28 19:55:08'),
(3, 1, '4', '6', '9', 1, 1, 12, '2016-12-28 23:32:20'),
(4, 2, '5', '7', '0', 2, 4, 12, '2016-12-28 23:32:20'),
(5, 3, '6', '8', '''', 3, 9, 12, '2016-12-28 23:32:20'),
(6, 32, '2', '3', '25', 21, 672, 13, '2016-12-29 00:32:12'),
(7, 12, '23', '23', '23', 23, 276, 14, '2016-12-29 17:10:34'),
(8, 232, '23', '23', '23', 23, 5336, 14, '2016-12-29 17:10:34'),
(9, 23, '23', '23', '23', 23, 529, 14, '2016-12-29 17:10:34'),
(10, 121, '23', '23', '2', 223, 26983, 15, '2016-12-29 17:26:03'),
(11, 12, '232', '232', '32', 32, 384, 15, '2016-12-29 17:26:03'),
(12, 12, '23', '23', '12', 12, 144, 16, '2016-12-29 17:28:11'),
(13, 1, '232', '121', '12', 23, 23, 17, '2016-12-29 17:41:46'),
(14, 1, '12', '12', '21', 12, 12, 18, '2016-12-29 18:04:38'),
(15, 1, '23', '232', '12', 12, 12, 19, '2016-12-29 18:10:01'),
(16, 1, 'asdf', '24', '12', 23, 23, 20, '2016-12-29 18:20:52'),
(17, 1, '2', '34', '2', 2, 2, 21, '2016-12-29 20:16:34'),
(18, 1, 'Mdjfañlk', 'Impresora', 'DDFAderueu445', 150000, 150000, 22, '2018-03-20 12:59:09'),
(19, 0, '', '', '', 0, 0, 22, '2018-03-20 12:59:09'),
(20, 0, '', '', '', 0, 0, 22, '2018-03-20 12:59:09'),
(21, 0, '', '', '', 0, 0, 22, '2018-03-20 12:59:09'),
(22, 0, '', '', '', 0, 0, 22, '2018-03-20 12:59:09'),
(23, 1, 'Test', 'Tester', '12', 132, 132, 23, '2019-02-11 12:33:51'),
(24, 1, 'Test', 'Asdf', '12', 12, 12, 24, '2019-02-11 13:26:49'),
(25, 1, '132', '123', '1', 1, 1, 25, '2019-02-11 13:47:49'),
(26, 1, '12', '12312', '131', 12, 12, 26, '2019-02-11 13:50:20'),
(27, 1, '132', '123', '1', 1, 1, 27, '2019-02-11 13:55:57'),
(28, 1, '12', '232', '123', 32, 32, 28, '2019-02-11 14:05:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `region_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cities`
--

INSERT INTO `cities` (`id`, `name`, `region_id`) VALUES
(1, 'Rancagua', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `names` varchar(45) DEFAULT NULL,
  `lastnames` varchar(45) DEFAULT NULL,
  `rut` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `provider_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `contacts`
--

INSERT INTO `contacts` (`id`, `names`, `lastnames`, `rut`, `address`, `provider_id`) VALUES
(2, 'Pepito', 'Gonzalez', '1322', 'Alameda #1052', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cost_centers`
--

CREATE TABLE `cost_centers` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `center_code` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cost_centers`
--

INSERT INTO `cost_centers` (`id`, `title`, `description`, `center_code`) VALUES
(1, 'Gestión', '', 'GSTN01'),
(2, 'Mejoramiento Genético', NULL, 'MJGN02'),
(3, 'Genómica', NULL, 'GNMC03'),
(4, 'Fisiología del Estrés', NULL, 'FIST04'),
(5, 'Agronomía', NULL, 'AGRMIA05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `iso` char(2) NOT NULL,
  `name` varchar(80) NOT NULL,
  `nicename` varchar(80) NOT NULL,
  `iso3` char(3) DEFAULT NULL,
  `numcode` smallint(6) DEFAULT NULL,
  `phonecode` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `countries`
--

INSERT INTO `countries` (`id`, `iso`, `name`, `nicename`, `iso3`, `numcode`, `phonecode`) VALUES
(1, 'AF', 'AFGHANISTAN', 'Afghanistan', 'AFG', 4, 93),
(2, 'AL', 'ALBANIA', 'Albania', 'ALB', 8, 355),
(3, 'DZ', 'ALGERIA', 'Algeria', 'DZA', 12, 213),
(4, 'AS', 'AMERICAN SAMOA', 'American Samoa', 'ASM', 16, 1684),
(5, 'AD', 'ANDORRA', 'Andorra', 'AND', 20, 376),
(6, 'AO', 'ANGOLA', 'Angola', 'AGO', 24, 244),
(7, 'AI', 'ANGUILLA', 'Anguilla', 'AIA', 660, 1264),
(8, 'AQ', 'ANTARCTICA', 'Antarctica', NULL, NULL, 0),
(9, 'AG', 'ANTIGUA AND BARBUDA', 'Antigua and Barbuda', 'ATG', 28, 1268),
(10, 'AR', 'ARGENTINA', 'Argentina', 'ARG', 32, 54),
(11, 'AM', 'ARMENIA', 'Armenia', 'ARM', 51, 374),
(12, 'AW', 'ARUBA', 'Aruba', 'ABW', 533, 297),
(13, 'AU', 'AUSTRALIA', 'Australia', 'AUS', 36, 61),
(14, 'AT', 'AUSTRIA', 'Austria', 'AUT', 40, 43),
(15, 'AZ', 'AZERBAIJAN', 'Azerbaijan', 'AZE', 31, 994),
(16, 'BS', 'BAHAMAS', 'Bahamas', 'BHS', 44, 1242),
(17, 'BH', 'BAHRAIN', 'Bahrain', 'BHR', 48, 973),
(18, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', 50, 880),
(19, 'BB', 'BARBADOS', 'Barbados', 'BRB', 52, 1246),
(20, 'BY', 'BELARUS', 'Belarus', 'BLR', 112, 375),
(21, 'BE', 'BELGIUM', 'Belgium', 'BEL', 56, 32),
(22, 'BZ', 'BELIZE', 'Belize', 'BLZ', 84, 501),
(23, 'BJ', 'BENIN', 'Benin', 'BEN', 204, 229),
(24, 'BM', 'BERMUDA', 'Bermuda', 'BMU', 60, 1441),
(25, 'BT', 'BHUTAN', 'Bhutan', 'BTN', 64, 975),
(26, 'BO', 'BOLIVIA', 'Bolivia', 'BOL', 68, 591),
(27, 'BA', 'BOSNIA AND HERZEGOVINA', 'Bosnia and Herzegovina', 'BIH', 70, 387),
(28, 'BW', 'BOTSWANA', 'Botswana', 'BWA', 72, 267),
(29, 'BV', 'BOUVET ISLAND', 'Bouvet Island', NULL, NULL, 0),
(30, 'BR', 'BRAZIL', 'Brazil', 'BRA', 76, 55),
(31, 'IO', 'BRITISH INDIAN OCEAN TERRITORY', 'British Indian Ocean Territory', NULL, NULL, 246),
(32, 'BN', 'BRUNEI DARUSSALAM', 'Brunei Darussalam', 'BRN', 96, 673),
(33, 'BG', 'BULGARIA', 'Bulgaria', 'BGR', 100, 359),
(34, 'BF', 'BURKINA FASO', 'Burkina Faso', 'BFA', 854, 226),
(35, 'BI', 'BURUNDI', 'Burundi', 'BDI', 108, 257),
(36, 'KH', 'CAMBODIA', 'Cambodia', 'KHM', 116, 855),
(37, 'CM', 'CAMEROON', 'Cameroon', 'CMR', 120, 237),
(38, 'CA', 'CANADA', 'Canada', 'CAN', 124, 1),
(39, 'CV', 'CAPE VERDE', 'Cape Verde', 'CPV', 132, 238),
(40, 'KY', 'CAYMAN ISLANDS', 'Cayman Islands', 'CYM', 136, 1345),
(41, 'CF', 'CENTRAL AFRICAN REPUBLIC', 'Central African Republic', 'CAF', 140, 236),
(42, 'TD', 'CHAD', 'Chad', 'TCD', 148, 235),
(43, 'CL', 'CHILE', 'Chile', 'CHL', 152, 56),
(44, 'CN', 'CHINA', 'China', 'CHN', 156, 86),
(45, 'CX', 'CHRISTMAS ISLAND', 'Christmas Island', NULL, NULL, 61),
(46, 'CC', 'COCOS (KEELING) ISLANDS', 'Cocos (Keeling) Islands', NULL, NULL, 672),
(47, 'CO', 'COLOMBIA', 'Colombia', 'COL', 170, 57),
(48, 'KM', 'COMOROS', 'Comoros', 'COM', 174, 269),
(49, 'CG', 'CONGO', 'Congo', 'COG', 178, 242),
(50, 'CD', 'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'Congo, the Democratic Republic of the', 'COD', 180, 242),
(51, 'CK', 'COOK ISLANDS', 'Cook Islands', 'COK', 184, 682),
(52, 'CR', 'COSTA RICA', 'Costa Rica', 'CRI', 188, 506),
(53, 'CI', 'COTE D''IVOIRE', 'Cote D''Ivoire', 'CIV', 384, 225),
(54, 'HR', 'CROATIA', 'Croatia', 'HRV', 191, 385),
(55, 'CU', 'CUBA', 'Cuba', 'CUB', 192, 53),
(56, 'CY', 'CYPRUS', 'Cyprus', 'CYP', 196, 357),
(57, 'CZ', 'CZECH REPUBLIC', 'Czech Republic', 'CZE', 203, 420),
(58, 'DK', 'DENMARK', 'Denmark', 'DNK', 208, 45),
(59, 'DJ', 'DJIBOUTI', 'Djibouti', 'DJI', 262, 253),
(60, 'DM', 'DOMINICA', 'Dominica', 'DMA', 212, 1767),
(61, 'DO', 'DOMINICAN REPUBLIC', 'Dominican Republic', 'DOM', 214, 1809),
(62, 'EC', 'ECUADOR', 'Ecuador', 'ECU', 218, 593),
(63, 'EG', 'EGYPT', 'Egypt', 'EGY', 818, 20),
(64, 'SV', 'EL SALVADOR', 'El Salvador', 'SLV', 222, 503),
(65, 'GQ', 'EQUATORIAL GUINEA', 'Equatorial Guinea', 'GNQ', 226, 240),
(66, 'ER', 'ERITREA', 'Eritrea', 'ERI', 232, 291),
(67, 'EE', 'ESTONIA', 'Estonia', 'EST', 233, 372),
(68, 'ET', 'ETHIOPIA', 'Ethiopia', 'ETH', 231, 251),
(69, 'FK', 'FALKLAND ISLANDS (MALVINAS)', 'Falkland Islands (Malvinas)', 'FLK', 238, 500),
(70, 'FO', 'FAROE ISLANDS', 'Faroe Islands', 'FRO', 234, 298),
(71, 'FJ', 'FIJI', 'Fiji', 'FJI', 242, 679),
(72, 'FI', 'FINLAND', 'Finland', 'FIN', 246, 358),
(73, 'FR', 'FRANCE', 'France', 'FRA', 250, 33),
(74, 'GF', 'FRENCH GUIANA', 'French Guiana', 'GUF', 254, 594),
(75, 'PF', 'FRENCH POLYNESIA', 'French Polynesia', 'PYF', 258, 689),
(76, 'TF', 'FRENCH SOUTHERN TERRITORIES', 'French Southern Territories', NULL, NULL, 0),
(77, 'GA', 'GABON', 'Gabon', 'GAB', 266, 241),
(78, 'GM', 'GAMBIA', 'Gambia', 'GMB', 270, 220),
(79, 'GE', 'GEORGIA', 'Georgia', 'GEO', 268, 995),
(80, 'DE', 'GERMANY', 'Germany', 'DEU', 276, 49),
(81, 'GH', 'GHANA', 'Ghana', 'GHA', 288, 233),
(82, 'GI', 'GIBRALTAR', 'Gibraltar', 'GIB', 292, 350),
(83, 'GR', 'GREECE', 'Greece', 'GRC', 300, 30),
(84, 'GL', 'GREENLAND', 'Greenland', 'GRL', 304, 299),
(85, 'GD', 'GRENADA', 'Grenada', 'GRD', 308, 1473),
(86, 'GP', 'GUADELOUPE', 'Guadeloupe', 'GLP', 312, 590),
(87, 'GU', 'GUAM', 'Guam', 'GUM', 316, 1671),
(88, 'GT', 'GUATEMALA', 'Guatemala', 'GTM', 320, 502),
(89, 'GN', 'GUINEA', 'Guinea', 'GIN', 324, 224),
(90, 'GW', 'GUINEA-BISSAU', 'Guinea-Bissau', 'GNB', 624, 245),
(91, 'GY', 'GUYANA', 'Guyana', 'GUY', 328, 592),
(92, 'HT', 'HAITI', 'Haiti', 'HTI', 332, 509),
(93, 'HM', 'HEARD ISLAND AND MCDONALD ISLANDS', 'Heard Island and Mcdonald Islands', NULL, NULL, 0),
(94, 'VA', 'HOLY SEE (VATICAN CITY STATE)', 'Holy See (Vatican City State)', 'VAT', 336, 39),
(95, 'HN', 'HONDURAS', 'Honduras', 'HND', 340, 504),
(96, 'HK', 'HONG KONG', 'Hong Kong', 'HKG', 344, 852),
(97, 'HU', 'HUNGARY', 'Hungary', 'HUN', 348, 36),
(98, 'IS', 'ICELAND', 'Iceland', 'ISL', 352, 354),
(99, 'IN', 'INDIA', 'India', 'IND', 356, 91),
(100, 'ID', 'INDONESIA', 'Indonesia', 'IDN', 360, 62),
(101, 'IR', 'IRAN, ISLAMIC REPUBLIC OF', 'Iran, Islamic Republic of', 'IRN', 364, 98),
(102, 'IQ', 'IRAQ', 'Iraq', 'IRQ', 368, 964),
(103, 'IE', 'IRELAND', 'Ireland', 'IRL', 372, 353),
(104, 'IL', 'ISRAEL', 'Israel', 'ISR', 376, 972),
(105, 'IT', 'ITALY', 'Italy', 'ITA', 380, 39),
(106, 'JM', 'JAMAICA', 'Jamaica', 'JAM', 388, 1876),
(107, 'JP', 'JAPAN', 'Japan', 'JPN', 392, 81),
(108, 'JO', 'JORDAN', 'Jordan', 'JOR', 400, 962),
(109, 'KZ', 'KAZAKHSTAN', 'Kazakhstan', 'KAZ', 398, 7),
(110, 'KE', 'KENYA', 'Kenya', 'KEN', 404, 254),
(111, 'KI', 'KIRIBATI', 'Kiribati', 'KIR', 296, 686),
(112, 'KP', 'KOREA, DEMOCRATIC PEOPLE''S REPUBLIC OF', 'Korea, Democratic People''s Republic of', 'PRK', 408, 850),
(113, 'KR', 'KOREA, REPUBLIC OF', 'Korea, Republic of', 'KOR', 410, 82),
(114, 'KW', 'KUWAIT', 'Kuwait', 'KWT', 414, 965),
(115, 'KG', 'KYRGYZSTAN', 'Kyrgyzstan', 'KGZ', 417, 996),
(116, 'LA', 'LAO PEOPLE''S DEMOCRATIC REPUBLIC', 'Lao People''s Democratic Republic', 'LAO', 418, 856),
(117, 'LV', 'LATVIA', 'Latvia', 'LVA', 428, 371),
(118, 'LB', 'LEBANON', 'Lebanon', 'LBN', 422, 961),
(119, 'LS', 'LESOTHO', 'Lesotho', 'LSO', 426, 266),
(120, 'LR', 'LIBERIA', 'Liberia', 'LBR', 430, 231),
(121, 'LY', 'LIBYAN ARAB JAMAHIRIYA', 'Libyan Arab Jamahiriya', 'LBY', 434, 218),
(122, 'LI', 'LIECHTENSTEIN', 'Liechtenstein', 'LIE', 438, 423),
(123, 'LT', 'LITHUANIA', 'Lithuania', 'LTU', 440, 370),
(124, 'LU', 'LUXEMBOURG', 'Luxembourg', 'LUX', 442, 352),
(125, 'MO', 'MACAO', 'Macao', 'MAC', 446, 853),
(126, 'MK', 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'Macedonia, the Former Yugoslav Republic of', 'MKD', 807, 389),
(127, 'MG', 'MADAGASCAR', 'Madagascar', 'MDG', 450, 261),
(128, 'MW', 'MALAWI', 'Malawi', 'MWI', 454, 265),
(129, 'MY', 'MALAYSIA', 'Malaysia', 'MYS', 458, 60),
(130, 'MV', 'MALDIVES', 'Maldives', 'MDV', 462, 960),
(131, 'ML', 'MALI', 'Mali', 'MLI', 466, 223),
(132, 'MT', 'MALTA', 'Malta', 'MLT', 470, 356),
(133, 'MH', 'MARSHALL ISLANDS', 'Marshall Islands', 'MHL', 584, 692),
(134, 'MQ', 'MARTINIQUE', 'Martinique', 'MTQ', 474, 596),
(135, 'MR', 'MAURITANIA', 'Mauritania', 'MRT', 478, 222),
(136, 'MU', 'MAURITIUS', 'Mauritius', 'MUS', 480, 230),
(137, 'YT', 'MAYOTTE', 'Mayotte', NULL, NULL, 269),
(138, 'MX', 'MEXICO', 'Mexico', 'MEX', 484, 52),
(139, 'FM', 'MICRONESIA, FEDERATED STATES OF', 'Micronesia, Federated States of', 'FSM', 583, 691),
(140, 'MD', 'MOLDOVA, REPUBLIC OF', 'Moldova, Republic of', 'MDA', 498, 373),
(141, 'MC', 'MONACO', 'Monaco', 'MCO', 492, 377),
(142, 'MN', 'MONGOLIA', 'Mongolia', 'MNG', 496, 976),
(143, 'MS', 'MONTSERRAT', 'Montserrat', 'MSR', 500, 1664),
(144, 'MA', 'MOROCCO', 'Morocco', 'MAR', 504, 212),
(145, 'MZ', 'MOZAMBIQUE', 'Mozambique', 'MOZ', 508, 258),
(146, 'MM', 'MYANMAR', 'Myanmar', 'MMR', 104, 95),
(147, 'NA', 'NAMIBIA', 'Namibia', 'NAM', 516, 264),
(148, 'NR', 'NAURU', 'Nauru', 'NRU', 520, 674),
(149, 'NP', 'NEPAL', 'Nepal', 'NPL', 524, 977),
(150, 'NL', 'NETHERLANDS', 'Netherlands', 'NLD', 528, 31),
(151, 'AN', 'NETHERLANDS ANTILLES', 'Netherlands Antilles', 'ANT', 530, 599),
(152, 'NC', 'NEW CALEDONIA', 'New Caledonia', 'NCL', 540, 687),
(153, 'NZ', 'NEW ZEALAND', 'New Zealand', 'NZL', 554, 64),
(154, 'NI', 'NICARAGUA', 'Nicaragua', 'NIC', 558, 505),
(155, 'NE', 'NIGER', 'Niger', 'NER', 562, 227),
(156, 'NG', 'NIGERIA', 'Nigeria', 'NGA', 566, 234),
(157, 'NU', 'NIUE', 'Niue', 'NIU', 570, 683),
(158, 'NF', 'NORFOLK ISLAND', 'Norfolk Island', 'NFK', 574, 672),
(159, 'MP', 'NORTHERN MARIANA ISLANDS', 'Northern Mariana Islands', 'MNP', 580, 1670),
(160, 'NO', 'NORWAY', 'Norway', 'NOR', 578, 47),
(161, 'OM', 'OMAN', 'Oman', 'OMN', 512, 968),
(162, 'PK', 'PAKISTAN', 'Pakistan', 'PAK', 586, 92),
(163, 'PW', 'PALAU', 'Palau', 'PLW', 585, 680),
(164, 'PS', 'PALESTINIAN TERRITORY, OCCUPIED', 'Palestinian Territory, Occupied', NULL, NULL, 970),
(165, 'PA', 'PANAMA', 'Panama', 'PAN', 591, 507),
(166, 'PG', 'PAPUA NEW GUINEA', 'Papua New Guinea', 'PNG', 598, 675),
(167, 'PY', 'PARAGUAY', 'Paraguay', 'PRY', 600, 595),
(168, 'PE', 'PERU', 'Peru', 'PER', 604, 51),
(169, 'PH', 'PHILIPPINES', 'Philippines', 'PHL', 608, 63),
(170, 'PN', 'PITCAIRN', 'Pitcairn', 'PCN', 612, 0),
(171, 'PL', 'POLAND', 'Poland', 'POL', 616, 48),
(172, 'PT', 'PORTUGAL', 'Portugal', 'PRT', 620, 351),
(173, 'PR', 'PUERTO RICO', 'Puerto Rico', 'PRI', 630, 1787),
(174, 'QA', 'QATAR', 'Qatar', 'QAT', 634, 974),
(175, 'RE', 'REUNION', 'Reunion', 'REU', 638, 262),
(176, 'RO', 'ROMANIA', 'Romania', 'ROM', 642, 40),
(177, 'RU', 'RUSSIAN FEDERATION', 'Russian Federation', 'RUS', 643, 70),
(178, 'RW', 'RWANDA', 'Rwanda', 'RWA', 646, 250),
(179, 'SH', 'SAINT HELENA', 'Saint Helena', 'SHN', 654, 290),
(180, 'KN', 'SAINT KITTS AND NEVIS', 'Saint Kitts and Nevis', 'KNA', 659, 1869),
(181, 'LC', 'SAINT LUCIA', 'Saint Lucia', 'LCA', 662, 1758),
(182, 'PM', 'SAINT PIERRE AND MIQUELON', 'Saint Pierre and Miquelon', 'SPM', 666, 508),
(183, 'VC', 'SAINT VINCENT AND THE GRENADINES', 'Saint Vincent and the Grenadines', 'VCT', 670, 1784),
(184, 'WS', 'SAMOA', 'Samoa', 'WSM', 882, 684),
(185, 'SM', 'SAN MARINO', 'San Marino', 'SMR', 674, 378),
(186, 'ST', 'SAO TOME AND PRINCIPE', 'Sao Tome and Principe', 'STP', 678, 239),
(187, 'SA', 'SAUDI ARABIA', 'Saudi Arabia', 'SAU', 682, 966),
(188, 'SN', 'SENEGAL', 'Senegal', 'SEN', 686, 221),
(189, 'CS', 'SERBIA AND MONTENEGRO', 'Serbia and Montenegro', NULL, NULL, 381),
(190, 'SC', 'SEYCHELLES', 'Seychelles', 'SYC', 690, 248),
(191, 'SL', 'SIERRA LEONE', 'Sierra Leone', 'SLE', 694, 232),
(192, 'SG', 'SINGAPORE', 'Singapore', 'SGP', 702, 65),
(193, 'SK', 'SLOVAKIA', 'Slovakia', 'SVK', 703, 421),
(194, 'SI', 'SLOVENIA', 'Slovenia', 'SVN', 705, 386),
(195, 'SB', 'SOLOMON ISLANDS', 'Solomon Islands', 'SLB', 90, 677),
(196, 'SO', 'SOMALIA', 'Somalia', 'SOM', 706, 252),
(197, 'ZA', 'SOUTH AFRICA', 'South Africa', 'ZAF', 710, 27),
(198, 'GS', 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'South Georgia and the South Sandwich Islands', NULL, NULL, 0),
(199, 'ES', 'SPAIN', 'Spain', 'ESP', 724, 34),
(200, 'LK', 'SRI LANKA', 'Sri Lanka', 'LKA', 144, 94),
(201, 'SD', 'SUDAN', 'Sudan', 'SDN', 736, 249),
(202, 'SR', 'SURINAME', 'Suriname', 'SUR', 740, 597),
(203, 'SJ', 'SVALBARD AND JAN MAYEN', 'Svalbard and Jan Mayen', 'SJM', 744, 47),
(204, 'SZ', 'SWAZILAND', 'Swaziland', 'SWZ', 748, 268),
(205, 'SE', 'SWEDEN', 'Sweden', 'SWE', 752, 46),
(206, 'CH', 'SWITZERLAND', 'Switzerland', 'CHE', 756, 41),
(207, 'SY', 'SYRIAN ARAB REPUBLIC', 'Syrian Arab Republic', 'SYR', 760, 963),
(208, 'TW', 'TAIWAN, PROVINCE OF CHINA', 'Taiwan, Province of China', 'TWN', 158, 886),
(209, 'TJ', 'TAJIKISTAN', 'Tajikistan', 'TJK', 762, 992),
(210, 'TZ', 'TANZANIA, UNITED REPUBLIC OF', 'Tanzania, United Republic of', 'TZA', 834, 255),
(211, 'TH', 'THAILAND', 'Thailand', 'THA', 764, 66),
(212, 'TL', 'TIMOR-LESTE', 'Timor-Leste', NULL, NULL, 670),
(213, 'TG', 'TOGO', 'Togo', 'TGO', 768, 228),
(214, 'TK', 'TOKELAU', 'Tokelau', 'TKL', 772, 690),
(215, 'TO', 'TONGA', 'Tonga', 'TON', 776, 676),
(216, 'TT', 'TRINIDAD AND TOBAGO', 'Trinidad and Tobago', 'TTO', 780, 1868),
(217, 'TN', 'TUNISIA', 'Tunisia', 'TUN', 788, 216),
(218, 'TR', 'TURKEY', 'Turkey', 'TUR', 792, 90),
(219, 'TM', 'TURKMENISTAN', 'Turkmenistan', 'TKM', 795, 7370),
(220, 'TC', 'TURKS AND CAICOS ISLANDS', 'Turks and Caicos Islands', 'TCA', 796, 1649),
(221, 'TV', 'TUVALU', 'Tuvalu', 'TUV', 798, 688),
(222, 'UG', 'UGANDA', 'Uganda', 'UGA', 800, 256),
(223, 'UA', 'UKRAINE', 'Ukraine', 'UKR', 804, 380),
(224, 'AE', 'UNITED ARAB EMIRATES', 'United Arab Emirates', 'ARE', 784, 971),
(225, 'GB', 'UNITED KINGDOM', 'United Kingdom', 'GBR', 826, 44),
(226, 'US', 'UNITED STATES', 'United States', 'USA', 840, 1),
(227, 'UM', 'UNITED STATES MINOR OUTLYING ISLANDS', 'United States Minor Outlying Islands', NULL, NULL, 1),
(228, 'UY', 'URUGUAY', 'Uruguay', 'URY', 858, 598),
(229, 'UZ', 'UZBEKISTAN', 'Uzbekistan', 'UZB', 860, 998),
(230, 'VU', 'VANUATU', 'Vanuatu', 'VUT', 548, 678),
(231, 'VE', 'VENEZUELA', 'Venezuela', 'VEN', 862, 58),
(232, 'VN', 'VIET NAM', 'Viet Nam', 'VNM', 704, 84),
(233, 'VG', 'VIRGIN ISLANDS, BRITISH', 'Virgin Islands, British', 'VGB', 92, 1284),
(234, 'VI', 'VIRGIN ISLANDS, U.S.', 'Virgin Islands, U.s.', 'VIR', 850, 1340),
(235, 'WF', 'WALLIS AND FUTUNA', 'Wallis and Futuna', 'WLF', 876, 681),
(236, 'EH', 'WESTERN SAHARA', 'Western Sahara', 'ESH', 732, 212),
(237, 'YE', 'YEMEN', 'Yemen', 'YEM', 887, 967),
(238, 'ZM', 'ZAMBIA', 'Zambia', 'ZMB', 894, 260),
(239, 'ZW', 'ZIMBABWE', 'Zimbabwe', 'ZWE', 716, 263);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `countriesasdasd`
--

CREATE TABLE `countriesasdasd` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discounts_details`
--

CREATE TABLE `discounts_details` (
  `id` int(11) NOT NULL,
  `legal_discount_id` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `discount_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `discount_types`
--

CREATE TABLE `discount_types` (
  `id` int(11) NOT NULL,
  `title` varchar(45) DEFAULT NULL,
  `discount_value` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) DEFAULT NULL,
  `rut` varchar(20) DEFAULT NULL,
  `position` varchar(45) DEFAULT NULL,
  `role` enum('ADMINISTRADOR','USUARIO') NOT NULL,
  `section` varchar(45) DEFAULT NULL,
  `telephone` varchar(45) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `vacations` int(3) DEFAULT NULL,
  `address` text,
  `emergency_contact` varchar(255) NOT NULL,
  `emergency_relation` varchar(255) NOT NULL,
  `emergency_tel` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `employees`
--

INSERT INTO `employees` (`id`, `firstname`, `lastname`, `rut`, `position`, `role`, `section`, `telephone`, `email`, `username`, `password`, `vacations`, `address`, `emergency_contact`, `emergency_relation`, `emergency_tel`) VALUES
(1, 'Felipe', 'Acuña', '1234567-9', 'Diseñador', 'ADMINISTRADOR', 'Gestión', '+569 51000071', 'Felipe@acunaviera.com', 'felipeacuna', '$2y$10$Ymj4kfvIlSNudiVygX6leukBCd3bAcan7PFpnxlrJ8XCubr/VaBji', 999, 'Pedro Matus #1038 Pobl. José Olivares', 'Casa', 'Padres', '722750398'),
(4, 'Raúl', 'Orellana', '15524322-8', 'Profesional de Apoyo', 'ADMINISTRADOR', 'Gestión', '', '', 'rorellana', '$2y$10$5gKvbxew/xC5Xo3sQhmsMeHEYs9tStHiCuVynB5GCzW7J9AWpFL0C', NULL, NULL, '', '', ''),
(5, 'Esteban Antonio', 'Valenzuela Cornejo', '17687384-1', 'Técnico de Apoyo', 'USUARIO', 'Mejoramiento Genético', '', 'evalenzuela@ceaf.cl', 'evalenzuelac', '$2y$10$SKOdY24iUIzi1q.nkSVv8uppRWvBIIPANcdZXE6lGUqeUZFwP6T/.', NULL, NULL, '', '', ''),
(6, 'Raúl Patricio', 'Riquieros Reyes', '15731279-0', 'Técnico de Apoyo', 'USUARIO', 'Mejoramiento Genético', '', 'rriquieros@ceaf.cl', 'rriquierosr', '$2y$10$xmARAmMBO1cAfPdX1G1uL.t784FHU6SQjnYLPFAeJ1q2dA4gp6r/i', NULL, NULL, '', '', ''),
(7, 'Rubén Darío', 'Almada', '21570391-6', 'Líder de Línea', 'USUARIO', 'Genómica', '', 'ralmada@ceaf.cl', 'ralmada', 'ralmada', NULL, NULL, '', '', ''),
(8, 'Ariel Rienso', 'Salvatierra Castro', '13755890-4', 'Investigador', 'USUARIO', 'Genómica', '', 'asalvatierra@ceaf.cl', 'asalvatierrac', '$2y$10$T3NpXxh3PAVmIHyTanZJFeJVlSvOROYFZ0wQRZy4q2uEbnEBcIe0K', NULL, NULL, '', '', ''),
(9, 'Ixia Paulina', 'Lienqueo Pino', '15503669-9', 'Asistente de Investigación', 'USUARIO', 'Genómica', '', 'ilienqueo@ceaf.cl', 'ilienqueop', '$2y$10$tsy./glEQuUGMhDZufQTyetLq7hoDWgr4k6UIBdep7vYNAe6UXgGi', NULL, NULL, '', '', ''),
(10, 'Paula Andrea', 'Pimentel Pimentel', '12853409-1', 'Líder de Línea', 'USUARIO', 'Fisiología del Estrés', '', 'ppimentel@ceaf.cl', 'ppimentelp', '$2y$10$vI2/AFwj3g5T/MtCEe8SmuzxgaCX/WkTP7vH9L4NyRKMlXuyRdmm.', NULL, NULL, '', '', ''),
(11, 'Marcia Daniela', 'Bravo Lara', '16253985-K', 'Técnico de Apoyo', 'USUARIO', 'Fisiología del Estrés', '', 'mbravo@ceaf.cl', 'mbravol', '$2y$10$htwjmKoAOGoD3C.96DzCaeMWcJFcE5PwViR6DwQ8m1BgJ6ZWDuLVK', NULL, NULL, '', '', ''),
(12, 'Mauricio Felipe', 'Ortiz Lizana', '12862516-K', 'Director / Líder de Línea', '', 'Agronomía', '', 'mortiz@ceaf.cl', 'mortizl', '$2y$10$/BScU3AOx/iaNs4LUmxrhuySBDJORRBWKIIzd5PflMtwT4PF7Nhy.', NULL, NULL, '', '', ''),
(13, 'Marcelo Andrés', 'Rivera Acuña', '17504378-0', 'Técnico de Apoyo', 'USUARIO', 'Agronomía', '', 'mrivera@ceaf.cl', 'mriveraa', '$2y$10$xNu/XplW9HS9kkqO8eg1TutOQfVq8n6dnPoP40dwlfqvRQXuW9WfK', NULL, NULL, '', '', ''),
(14, 'Myrthon ARturo', 'Naour Pérez', '6035293-3', 'Líder de Línea', 'ADMINISTRADOR', 'Gestión', '', 'mnaour@ceaf.cl', 'mnaourp', '$2y$10$4PpdAhPe4ZY.uADE8IITQO8YtI1.EO0Qn6sIGvrQSfMAkXMD0PWOy', NULL, NULL, '', '', ''),
(15, 'Carolina Andrea', 'Rojas Lobos', '13939543-3', 'Técnico de Apoyo', 'USUARIO', 'Gestión', '', 'crojas@ceaf.cl', 'crojasl', '$2y$10$YsgatczxuQOcl2F2EoXW7.owdz/TkIZBm2NV0ImXsNxyWHSQFuRW2', NULL, NULL, '', '', ''),
(16, 'Verónica de las Mercedes', 'Contalba Araya', '13349074-4', 'Secretaria', 'USUARIO', 'Gestión', '', 'vcontalba@ceaf.cl', 'vcontalbaa', '$2y$10$WzMykvE4zHz6jGDZBAR2gemogG2MPiY4h73FjWo.2zrVt43wZ1k0G', NULL, NULL, '', '', ''),
(17, 'Marta Soledad', 'Suárez Valencia', '11784632-6', 'Auxiliar de Aseo', 'USUARIO', 'Gestión', '', 'msuarez@ceaf.cl', 'msuarezv', '$2y$10$LA/uS7PKxp9KNo3/uNwF5exVnnaN/kwtcxaau8mZeq8zjyOAPp8eS', NULL, NULL, '', '', ''),
(18, 'Angelo Paolo', 'Roco Álvarez', '19210626-5', 'Operario', 'USUARIO', 'Fisiología del Estrés', '', 'aroco@ceaf.cl', 'arocoa', '$2y$10$kaqRQc9ctwx8btjWDWoNKOa/2OiMuvD1Jh6NOj4lUxhbQlHvIm8Ui', NULL, NULL, '', '', ''),
(19, 'Mario Andrés', 'Espinoza González', '18558556-5', 'Operario', 'USUARIO', 'Agronomía', '', 'mespinoza@ceaf.cl', 'mespinozag', '$2y$10$9ZugF7kJDVYMZOEVtew0rOc.foAici7T8HpXbnmsV.WzMUyoWH/Fy', NULL, NULL, '', '', ''),
(20, 'María Josefina', 'Mujica Castiglioni', '17476885-4', 'Asistente de Investigación', 'USUARIO', 'Fisiología del Estrés', '', 'mmujica@ceaf.cl', 'mmujicac', '$2y$10$5D9OaNRfiKijuZjLn78AkeGPSuRfUUjFsIHuAOnVA2ntvQPLzOtNK', NULL, NULL, '', '', ''),
(21, 'Catalina Amelia', 'Álvarez Rojas', '8958096-k', 'Técnico de Apoyo', 'USUARIO', 'Mejoramiento Genético', '', 'calvarez@ceaf.cl', 'calvarezr', '$2y$10$BOG9xn4eBwi2mtLFY9n/hOdzJQ6CmjekKbMMmYIMFC4FOri/jeLpK', NULL, NULL, '', '', ''),
(22, 'Ismael Constantino', 'Opazo Palma', '16653015-6', 'Investigador Doctorando', 'USUARIO', 'Mejoramiento Genético', '', 'iopazo@ceaf.cl', 'iopazop', '$2y$10$LSC.pzNcHIFZD9W2dG9Wx.8eFPFhaRY9jtZg2od.LYzgvNO.GEHvu', NULL, NULL, '', '', ''),
(23, 'Patricio Humberto', 'Mateluna Valdivia', '17139008-7', 'Asistente de Investigación', 'USUARIO', 'Fisiología del Estrés', '', 'pmateluna@ceaf.cl', 'pmatelunav', '$2y$10$ZF4LrFT/UyEnwg91HgRUFuJEO1peQQp2YPqfM.1oxpMX14qiBh7y2', NULL, NULL, '', '', ''),
(24, 'Carolina Andrea', 'Muñoz Castro', '16843326-3', 'Técnico de Apoyo', 'USUARIO', 'Fortalecimiento VII', '', 'cmunoz@ceaf.cl', 'cmuñozc', '$2y$10$Vvbr6uzh88XeKJGcDiX1DOJU9MOBA9vFw0SN0y1QS755Uty/mYbha', NULL, NULL, '', '', ''),
(25, 'José Alexis', 'Neira Román', '14136222-4', 'Investigador Post-doc', 'USUARIO', 'Agronomía', '', 'jneira@ceaf.cl', 'jneirar', '$2y$10$AcIf.GPdP32zk4zsUOTex.z9DVivCF2ijLFJniSe8EH.SpdF.Ntre', NULL, NULL, '', '', ''),
(26, 'Michelle Grace', 'Morales Olmedo', '16116649-9', 'Investigador post-doc', 'USUARIO', 'Agronomía', '', 'mmorales@ceaf.cl', 'mmoraleso', '$2y$10$qEDCR2Qn7of5vV4H5k0Ogu26QCOlfr3MUWH97J1BSy4oJXUXf7Kwe', NULL, NULL, '', '', ''),
(27, 'Francisco Javier', 'Correa Soto', '16492277-4', 'Asistente de Investigación', 'USUARIO', 'Genómica', '', 'fcorrea@ceaf.cl', 'fcorreas', '$2y$10$5eBFQq1dlGBdqzaPP.09E.NoJdKh5CJXm5YK/dIBXIdMhV.ZTOy7K', NULL, NULL, '', '', ''),
(28, 'Franco Alexis', 'Soto Ramírez', '18040010-9', 'Operario', 'USUARIO', 'Genómica', '', 'fsoto@ceaf.cl', 'fsotor', '$2y$10$Z5R0no1Y4NHp8e2WS8LuieEwEUAF6Z1Oiy/IQabiNOpM4vMmS/XMW', NULL, NULL, '', '', ''),
(29, 'Guillermo Andrés', 'Toro Aburto', '14168644-5', 'Investigador Post-doc', 'USUARIO', 'Fisiología del Estrés', '', 'gtoro@ceaf.cl', 'gtoroa', '$2y$10$Lb6wOJzdY94.hZyT4IelLOIsvS4ya1Alae1k4mt5zryIVcqhik242', NULL, NULL, '', '', ''),
(30, 'Verónica Ximena', 'Guajardo Fernández', '13778486-6', 'Investigador Post-doc', 'USUARIO', 'Mejoramiento Genético', '', 'vguajardo@ceaf.cl', 'vguajardof', '$2y$10$080IjwY3JGT3CaLXA4fGH.b1x68zne2DZPfMvFOsOScJOs9WFVDaG', NULL, NULL, '', '', ''),
(31, 'Simón Juan', 'Solís Kunzler', '17176282-0', 'Asistente de Investigación', 'USUARIO', 'Fisiología del Estrés', '', 'ssolis@ceaf.cl', 'ssolisk', '$2y$10$5lm8VfYOVoPz35y1o/.yHOD5QAaYpiwj.n2uJ./ruCCY5LQaEOQm.', NULL, NULL, '', '', ''),
(32, 'Tester', 'tester', NULL, 'Testeador', 'ADMINISTRADOR', 'Gestion', '75123456', 'sopaenbolsa@gmail.com', 'tester', '$2y$10$scP1Rbtw0t6RaZ5Mg4ekC.DBs1ydOMJQkjpJ3DM5N8L6Pz0eZiO6G', NULL, NULL, '', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `i18n`
--

CREATE TABLE `i18n` (
  `id` int(11) NOT NULL,
  `locale` varchar(6) NOT NULL,
  `model` varchar(255) NOT NULL,
  `foreign_key` int(10) NOT NULL,
  `field` varchar(255) NOT NULL,
  `content` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `income_details`
--

CREATE TABLE `income_details` (
  `id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `period_income_id` int(11) NOT NULL,
  `Income_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `income_types`
--

CREATE TABLE `income_types` (
  `id` int(11) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `value` varchar(45) DEFAULT NULL,
  `imponible` varchar(45) DEFAULT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `off_days`
--

CREATE TABLE `off_days` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(45) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `type` enum('HORA','ADMINISTRATIVO','LEGAL','MEDIO-ADMIN') NOT NULL,
  `days` float DEFAULT NULL,
  `date_created` datetime DEFAULT CURRENT_TIMESTAMP,
  `period_summary_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `off_days`
--

INSERT INTO `off_days` (`id`, `employee_id`, `start_date`, `end_date`, `type`, `days`, `date_created`, `period_summary_id`) VALUES
(53, '1', '2017-07-26 00:00:00', '2017-07-26 00:00:00', 'MEDIO-ADMIN', 0.5, '2017-07-25 16:38:35', 0),
(54, '1', '2017-07-28 00:00:00', '2017-07-28 00:00:00', 'ADMINISTRATIVO', NULL, '2017-07-25 16:38:41', 0),
(55, '1', '2017-07-28 16:39:00', '2017-07-28 16:39:00', 'HORA', NULL, '2017-07-25 16:39:08', 0),
(56, '1', '2017-07-28 16:39:00', '2017-07-28 16:39:00', 'HORA', NULL, '2017-07-25 16:39:12', 0),
(57, '1', '2017-07-26 00:00:00', '2017-07-31 00:00:00', 'LEGAL', 5, '2017-07-25 16:41:15', 0),
(58, '1', '2017-07-26 00:00:00', '2017-07-31 00:00:00', 'LEGAL', 5, '2017-07-25 16:41:18', 0),
(59, '1', '2017-07-28 16:41:00', '2017-07-28 16:41:00', 'HORA', NULL, '2017-07-25 16:41:41', 0),
(60, '1', '2017-07-31 19:00:00', '2017-07-31 19:00:00', 'HORA', NULL, '2017-07-30 19:45:46', 0),
(61, '4', '2017-08-02 00:00:00', '2017-08-02 00:00:00', 'MEDIO-ADMIN', NULL, '2017-08-01 14:44:44', 0),
(62, '4', '2017-09-13 00:00:00', '2017-09-19 00:00:00', 'LEGAL', 6, '2017-09-01 15:45:34', 0),
(63, '4', '2017-09-13 00:00:00', '2017-09-19 00:00:00', 'LEGAL', 6, '2017-09-01 16:07:54', 0),
(64, '4', '2017-09-11 00:00:00', '2017-09-22 00:00:00', 'LEGAL', 11, '2017-09-01 16:08:33', 0),
(65, '15', '2018-03-22 00:00:00', '2018-03-22 00:00:00', 'MEDIO-ADMIN', NULL, '2018-03-20 13:00:22', 0),
(66, '15', '2018-03-23 14:00:00', '2018-03-23 14:00:00', 'HORA', NULL, '2018-03-20 13:00:58', 0),
(67, '15', '2018-03-23 13:00:00', '2018-03-23 13:00:00', 'HORA', NULL, '2018-03-20 13:01:53', 0),
(68, '15', '2018-03-23 13:00:00', '2018-03-23 13:00:00', 'HORA', NULL, '2018-03-20 13:02:00', 0),
(69, '15', '2018-03-21 12:00:00', '2018-03-21 12:00:00', 'HORA', NULL, '2018-03-20 13:02:19', 0),
(70, '15', '2018-03-26 00:00:00', '2018-03-30 00:00:00', 'LEGAL', NULL, '2018-03-20 13:02:51', 0),
(71, '15', '2018-03-26 00:00:00', '2018-03-30 00:00:00', 'LEGAL', NULL, '2018-03-20 13:02:54', 0),
(72, '15', '2018-03-26 00:00:00', '2018-03-30 00:00:00', 'LEGAL', NULL, '2018-03-20 13:03:17', 0),
(73, '15', '2018-03-21 00:00:00', '2018-03-30 00:00:00', 'LEGAL', NULL, '2018-03-20 13:03:34', 0),
(74, '32', '2019-02-08 00:00:00', '2019-02-08 00:00:00', 'MEDIO-ADMIN', NULL, '2019-02-01 15:41:41', 0),
(75, '32', '2018-03-22 15:40:00', '2018-03-22 15:40:00', 'HORA', NULL, '2019-02-01 15:41:58', 0),
(76, '32', '2018-03-22 15:40:00', '2018-03-22 15:40:00', 'HORA', NULL, '2019-02-01 15:42:06', 0),
(77, '32', '2019-02-12 00:00:00', '2019-02-12 00:00:00', 'MEDIO-ADMIN', NULL, '2019-02-11 15:14:50', 0),
(78, '32', '2019-02-19 00:00:00', '2019-02-19 00:00:00', 'MEDIO-ADMIN', NULL, '2019-02-11 15:35:23', 0),
(79, '32', '2019-02-12 15:35:00', '2019-02-12 15:35:00', 'HORA', NULL, '2019-02-11 15:36:05', 0),
(80, '32', '2019-02-12 15:35:00', '2019-02-12 15:35:00', 'HORA', NULL, '2019-02-11 15:36:12', 0),
(81, '32', '2019-02-19 03:27:00', '2019-02-19 03:27:00', 'HORA', NULL, '2019-02-11 15:37:05', 0),
(82, '32', '2019-02-13 00:00:00', '2019-02-14 00:00:00', 'LEGAL', NULL, '2019-02-11 15:37:26', 0),
(83, '32', '2019-02-13 00:00:00', '2019-02-14 00:00:00', 'LEGAL', NULL, '2019-02-11 15:37:27', 0),
(84, '32', '2019-02-12 00:00:00', '2019-02-14 00:00:00', 'LEGAL', NULL, '2019-02-11 15:37:42', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `on_days`
--

CREATE TABLE `on_days` (
  `id` int(11) NOT NULL,
  `employee_id` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `period_summary_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `outcomes`
--

CREATE TABLE `outcomes` (
  `id` int(11) NOT NULL,
  `item` varchar(255) DEFAULT NULL,
  `subitem` varchar(255) DEFAULT NULL,
  `document_type` varchar(45) DEFAULT NULL,
  `document_id` varchar(45) DEFAULT NULL,
  `document_date` date DEFAULT NULL,
  `detail` text,
  `amount` int(11) DEFAULT NULL,
  `bo_id` varchar(45) DEFAULT NULL,
  `cc_id` int(11) NOT NULL,
  `provider_id` int(11) NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `outcomes`
--

INSERT INTO `outcomes` (`id`, `item`, `subitem`, `document_type`, `document_id`, `document_date`, `detail`, `amount`, `bo_id`, `cc_id`, `provider_id`, `employee_id`, `date_created`) VALUES
(5, 'materiales-fungibles', 'mat-oficina-comp', 'bho-3ro', '2342432', '2017-10-07', 'sdfsdfsdf', 234234234, NULL, 2, 3, 1, '2017-07-30 21:15:11'),
(6, 'perfeccionamiento-capacitacion', 'cursos-seminarios', 'factura-exec-elec', '12', '2017-11-07', 'Test', 1199050, NULL, 4, 2, 1, '2017-07-31 12:45:24'),
(7, 'materiales-fungibles', 'mat-ferreteria-cons', 'pas-peaj-ticket', '3213645', '2017-07-06', 'Fiting de riego', 5300, NULL, 2, 2, 4, '2017-08-01 14:34:15'),
(8, 'gastos-operacion', 'pasajes-peajes-mov', NULL, '444587', '2017-02-08', 'Peaje', 600, NULL, 2, 3, 4, '2017-08-01 14:34:15'),
(9, 'perfeccionamiento-capacitacion', 'cursos-seminarios', 'factura', '12457', '2017-05-09', 'Dfajfjakfads d fdjf ñer jñlker j', 1500, NULL, 3, 2, 4, '2017-09-01 15:15:48'),
(10, '', 'valor1', NULL, '4444', '2017-02-04', 'wajajaja', 1300, NULL, 1, 3, 4, '2017-09-01 15:15:48'),
(11, 'materiales-fungibles', 'insumos-lab', 'boleta-elec', '454554', '2019-01-02', '1000 lts agua destilada', 103200, NULL, 5, 2, 32, '2019-02-01 15:35:56'),
(12, 'materiales-fungibles', 'insumos-lab', NULL, '44879879898', '2019-01-02', 'Sodimac', 16600, NULL, 5, 3, 32, '2019-02-01 15:35:56'),
(13, 'gastos-operacion', 'mantencion-vehiculos', 'solicitud', '111', '2019-01-02', 'Test', 999999, NULL, 1, 2, 32, '2019-02-01 16:09:16'),
(14, 'materiales-fungibles', 'insumos-lab', 'bho', '1', '2019-05-02', 'Test', 12, NULL, 1, 2, 32, '2019-02-11 13:35:59'),
(15, 'materiales-fungibles', 'mat-oficina-comp', 'factura-exec-elec', '1', '2019-05-02', '123', 12, NULL, 1, 2, 32, '2019-02-11 15:24:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `outcome_details`
--

CREATE TABLE `outcome_details` (
  `id` int(11) NOT NULL,
  `outcome_id` int(11) NOT NULL,
  `cost_center_id` int(11) NOT NULL,
  `related_budget_item` varchar(255) NOT NULL,
  `detail` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `outcome_releases`
--

CREATE TABLE `outcome_releases` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `cost_center_id` int(11) NOT NULL,
  `amount` int(10) NOT NULL,
  `estimated_release` date NOT NULL,
  `justification` text NOT NULL,
  `employee_id` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `outcome_releases`
--

INSERT INTO `outcome_releases` (`id`, `name`, `date`, `cost_center_id`, `amount`, `estimated_release`, `justification`, `employee_id`, `date_created`) VALUES
(1, 'Alejandro', '2016-09-02', 3, 50000, '2016-09-01', 'sadsadasd', 1, '2016-09-28 03:52:47'),
(2, 'Alejandro', '2016-09-02', 3, 50000, '2016-09-01', 'sadsadasd', 1, '2016-09-28 03:53:51'),
(3, 'Alejandro', '2016-09-02', 3, 50000, '2016-09-01', 'sadsadasd', 1, '2016-09-28 03:54:38'),
(4, 'Alejandro', '2016-09-02', 3, 50000, '2016-09-01', 'sadsadasd', 1, '2016-09-28 03:54:58'),
(5, 'Alejandro Sanchez', '2016-09-29', 3, 50000, '2016-09-29', 'asdasdasd', 1, '2016-09-28 03:55:56'),
(6, 'Alejandro', '2016-09-28', 4, 50000, '2016-09-26', 'asdasdasd', 1, '2016-09-28 03:58:02'),
(7, 'Alejandro', '2016-09-28', 3, 50000, '2016-09-20', 'asdsadasd', 1, '2016-09-28 03:59:15'),
(8, 'Alejandro', '2016-09-28', 3, 50000, '2016-09-29', 'asdasdasdasd', 1, '2016-09-28 04:14:14'),
(9, 'Alejandro', '2016-09-28', 2, 50000, '2016-09-21', 'NEcesito los fondos debido a que...no se ', 1, '2016-09-28 04:28:29'),
(10, 'Felipe Acuña', '2016-09-14', 2, 1233223, '2016-09-15', 'Esto es una prueba', 1, '2016-09-29 03:59:56'),
(11, 'Raúl', '2016-09-30', 1, 20000, '2016-10-03', 'Bla', 1, '2016-09-29 21:46:36'),
(13, 'Felipe Acuña', '2023-10-16', 1, 1266, '1970-01-01', 'asdf', 1, '2016-11-23 16:19:46'),
(14, '123', '2016-12-08', 1, 12, '2016-12-06', '12', 1, '2016-12-12 00:39:20'),
(15, 'Raúl Orellana', '2016-12-23', 5, 200000, '2016-12-30', 'Compras varias', 1, '2016-12-22 13:47:19'),
(16, 'Felipe Acuña', '2016-12-22', 3, 23, '2016-12-16', 'asdfasd', 1, '2016-12-22 20:38:43'),
(17, 'Felipe Acuña', '2016-12-13', 1, 123, '1970-01-01', '121', 1, '2016-12-23 19:50:39'),
(39, 'Asdf', '2016-12-15', 3, 123, '1970-01-01', 'asdasdasdasd', 1, '2016-12-23 20:14:41'),
(68, 'Felipe Acuña ', '2016-12-30', 3, 1500, '2016-12-31', 'Para Probar', 1, '2016-12-30 17:34:56'),
(69, 'Felipe', '2017-01-13', 4, 0, '2017-01-19', 'asdf', 1, '2017-01-01 13:42:18'),
(70, 'Donde Esta Esto', '2017-01-12', 2, 123, '1970-01-01', 'asdasf', 1, '2017-01-15 04:31:56'),
(71, 'Felipe Acuña', '2017-07-20', 1, 1500, '2017-07-29', 'Testear esto', 1, '2017-07-25 16:27:36'),
(72, 'Felipe Acuña', '2017-07-19', 2, 0, '2017-07-28', 'Testear esto', 1, '2017-07-25 16:28:05'),
(73, 'Felipe Acuña', '2017-07-18', 4, 0, '2017-07-17', 'Testear ', 1, '2017-07-25 18:18:45'),
(74, 'Nombre', '2017-07-11', 1, 15000, '2017-07-19', 'Testear', 1, '2017-07-25 18:19:12'),
(75, 'Test', '2017-07-19', 3, 123, '2017-07-12', 'Asdf', 1, '2017-07-26 16:41:30'),
(76, 'Raúl Orellana Moraga', '2017-08-02', 1, 20000, '2017-08-04', 'Cualquier cosa, sólo es una prueba.', 4, '2017-08-01 14:26:26'),
(77, 'Raúl Orellana Moraga', '2017-08-02', 2, 20000, '2017-08-04', 'DAFDASfadñsla fjlñ  dfa', 4, '2017-08-01 14:32:25'),
(78, 'Felipe Acuña', '2017-08-16', 2, 123, '2017-08-07', 'adasd', 1, '2017-08-01 15:28:21'),
(79, 'Test', '2017-08-14', 3, 0, '2017-08-08', 'asd', 1, '2017-08-01 15:30:07'),
(80, 'Asdf', '2017-08-16', 3, 0, '2017-08-16', 'asd', 1, '2017-08-01 16:06:06'),
(81, 'asd', '2017-08-07', 4, 123, '2017-08-15', 'sdf', 1, '2017-08-01 16:11:14'),
(82, 'Tester tester', '2019-02-01', 1, 120000, '2019-02-04', 'Compra X weás', 32, '2019-02-01 15:33:59'),
(83, 'Tester tester', '2019-02-13', 2, 150, '2019-02-20', 'Testear', 32, '2019-02-11 12:30:07'),
(84, 'Tester tester', '2019-02-05', 1, 150, '2019-02-12', 'asdf', 32, '2019-02-11 13:23:35'),
(85, 'Tester tester', '2019-02-13', 1, 12, '2019-02-13', 'qasd', 32, '2019-02-11 13:36:11'),
(86, 'Tester tester', '2019-02-12', 4, 150, '2019-02-13', 'asdf', 32, '2019-02-11 15:24:26');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `period_discounts`
--

CREATE TABLE `period_discounts` (
  `id` int(11) NOT NULL,
  `month` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `period_incomes`
--

CREATE TABLE `period_incomes` (
  `id` int(11) NOT NULL,
  `month` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  `Employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `period_summaries`
--

CREATE TABLE `period_summaries` (
  `id` int(11) NOT NULL,
  `month` varchar(45) DEFAULT NULL,
  `year` varchar(45) DEFAULT NULL,
  `income_total` varchar(45) DEFAULT NULL,
  `employee_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `providers`
--

CREATE TABLE `providers` (
  `id` int(11) NOT NULL,
  `rut` int(11) NOT NULL,
  `names` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `entry_date` date DEFAULT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `providers`
--

INSERT INTO `providers` (`id`, `rut`, `names`, `address`, `entry_date`, `city_id`) VALUES
(2, 1232, 'Maderas González', 'Alameda #1052', NULL, 1),
(3, 16203220, 'Probando las Ñ', 'Alcalde Aldea #1320', NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regions`
--

CREATE TABLE `regions` (
  `id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `travel_provisions`
--

CREATE TABLE `travel_provisions` (
  `id` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `employee_id` int(11) NOT NULL,
  `cc_id` int(11) NOT NULL,
  `budget_id` int(11) DEFAULT NULL,
  `days_nosleep` int(2) NOT NULL,
  `days_sleep` int(2) NOT NULL,
  `date_created` datetime NOT NULL,
  `transport_type` varchar(255) NOT NULL,
  `amount` int(5) NOT NULL,
  `project` varchar(255) DEFAULT NULL,
  `motive` text NOT NULL,
  `objective` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `cities` text NOT NULL,
  `places` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `travel_provisions`
--

INSERT INTO `travel_provisions` (`id`, `start_date`, `end_date`, `employee_id`, `cc_id`, `budget_id`, `days_nosleep`, `days_sleep`, `date_created`, `transport_type`, `amount`, `project`, `motive`, `objective`, `country_id`, `cities`, `places`) VALUES
(135, '2017-11-07', '1970-01-01', 1, 0, NULL, 3, 2, '2017-07-25 18:09:04', '3', 117000, 'Test', 'Motivo 1', 'Objetivo', 43, 'Test,Test', 'Test,Test'),
(136, '2017-01-08', '1970-01-01', 14, 0, NULL, 11, 6, '2017-07-31 14:45:05', '0', 369000, 'Test', '', 'Test', 28, 'Rancagua,', 'Testear,'),
(137, '2017-04-07', '1970-01-01', 14, 0, NULL, 3, 9, '2017-07-31 14:46:32', '2', 432000, 'Test', '', 'Test', 43, 'Test,', 'Test,'),
(138, '2017-08-08', '2017-10-08', 1, 0, NULL, 2, 1, '2017-07-31 15:10:11', '0', 63000, 'Test', '', 'Test', 43, 'Test,Test', 'Test,'),
(139, '2017-08-03', '2017-08-03', 4, 0, NULL, 1, 176, '2017-08-01 14:40:42', '0', 7929000, 'R08i1001', '', 'Compra macetas plásticas para ensayo de la línea Fisiología', 43, 'Macul,', 'Plásticos Hadad,'),
(140, '2017-08-07', '1970-01-01', 1, 0, NULL, 4, 37, '2017-08-04 02:25:58', '2', 1701000, 'Proyecto1', '', '1312', 43, 'Asd,', 'adas,'),
(141, '2017-08-18', '2017-08-21', 1, 3, NULL, 3, 1, '2017-08-16 16:45:35', '1', 72000, NULL, 'Testear el motivo', 'Testear el objetivo', 43, 'Rancagua,', 'Casa,Test'),
(142, '2017-08-16', '2017-08-24', 1, 0, NULL, 5, 4, '2017-08-16 16:47:40', '2', 225000, 'Asdf', 'ASd', 'asd', 43, 'Test,Test', 'Test,'),
(143, '2017-08-23', '2017-08-25', 1, 1, NULL, 2, 1, '2017-08-16 16:51:34', '0', 63000, NULL, '123', '123', 43, 'Test,', 'Test,'),
(144, '1970-01-01', '1970-01-01', 1, 1, NULL, 0, 0, '2017-08-16 16:52:01', '0', 0, NULL, '', '', 43, ',', ','),
(145, '1970-01-01', '1970-01-01', 1, 1, NULL, 0, 0, '2017-08-16 16:52:43', '0', 0, NULL, '', '', 43, ',', ','),
(146, '1970-01-01', '1970-01-01', 1, 1, NULL, 0, 0, '2017-08-16 16:53:07', '0', 0, NULL, '', '', 43, ',', ','),
(147, '1970-01-01', '1970-01-01', 1, 1, NULL, 0, 0, '2017-08-16 16:54:26', '0', 0, NULL, '', '', 43, ',', ','),
(148, '2017-08-16', '2017-08-18', 1, 1, NULL, 0, 0, '2017-08-16 17:29:54', '0', 0, NULL, '', '', 43, ',', ','),
(149, '2017-08-17', '2017-08-19', 1, 1, NULL, 2, 1, '2017-08-16 18:58:35', '2', 63000, NULL, 'Asd', 'asd', 43, 'Test,Test', 'ASd,'),
(150, '2017-08-18', '2017-08-19', 1, 1, NULL, 1, 1, '2017-08-16 18:59:01', '0', 54000, NULL, 'asd', 'asd', 43, 'asd,', 'asd,'),
(151, '2017-08-16', '2017-08-25', 1, 4, NULL, 3, 7, '2017-08-16 19:00:08', '2', 342000, NULL, '123', '232', 43, '123,2', '12,'),
(152, '2017-08-17', '2017-08-26', 1, 3, NULL, 3, 7, '2017-08-16 19:00:33', '2', 342000, NULL, 'ASdf', 'Asd', 43, 'Test,Test', 'Test,'),
(153, '2017-09-01', '2017-09-03', 1, 3, NULL, 3, 0, '2017-09-01 03:41:35', '2', 27000, NULL, 'Investigacion', 'Investigacion', 43, 'Santiago,', 'Intendencia,'),
(154, '2017-09-07', '2017-09-09', 1, 2, NULL, 3, 0, '2017-09-01 03:48:43', '2', 27000, NULL, 'sdasd', 'dsaasdasd', 43, 'Santiago,', 'Intendencia,'),
(155, '2017-09-01', '2017-09-01', 1, 1, NULL, 1, 0, '2017-09-01 03:50:04', '0', 9000, NULL, 'sadasdasdasd', 'asdasdasd', 43, 'asdasd,', 'asdasd,'),
(156, '2017-09-04', '2017-09-04', 4, 1, NULL, 1, 0, '2017-09-01 14:55:51', '0', 9000, NULL, 'wajajajajaj,jejejejejejejej', 'reir', 43, 'Rancagua,', 'Mi casa,'),
(157, '2019-02-07', '2019-02-15', 32, 1, NULL, 1, 8, '2019-02-01 14:28:36', '1', 369000, NULL, 'Testear', 'Testear', 43, 'Rancagua,Rancagua', 'Casa,'),
(158, '2019-02-13', '2019-02-13', 32, 1, NULL, 1, 0, '2019-02-11 12:49:44', '0', 9000, NULL, 'Test', 'Test', 43, 'Rancagua,Rancagua', 'Casa,casa'),
(159, '2019-02-12', '2019-02-13', 32, 1, NULL, 1, 1, '2019-02-11 15:26:52', '1', 54000, NULL, 'a', 'b', 43, 'Rancagua,Rancagua', 'a,b');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `budgets`
--
ALTER TABLE `budgets`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `buy_orders`
--
ALTER TABLE `buy_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `buy_orders_details`
--
ALTER TABLE `buy_orders_details`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_City_Region1_idx` (`region_id`);

--
-- Indices de la tabla `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rut_UNIQUE` (`rut`),
  ADD KEY `fk_Contact_Provider1_idx` (`provider_id`);

--
-- Indices de la tabla `cost_centers`
--
ALTER TABLE `cost_centers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `countriesasdasd`
--
ALTER TABLE `countriesasdasd`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `discounts_details`
--
ALTER TABLE `discounts_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Discount_detail_Discount_type1_idx` (`discount_type_id`);

--
-- Indices de la tabla `discount_types`
--
ALTER TABLE `discount_types`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `i18n`
--
ALTER TABLE `i18n`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `I18N_LOCALE_FIELD` (`locale`,`model`,`foreign_key`,`field`),
  ADD KEY `I18N_FIELD` (`model`,`foreign_key`,`field`);

--
-- Indices de la tabla `income_details`
--
ALTER TABLE `income_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Income_detail_Period_income1_idx` (`period_income_id`),
  ADD KEY `fk_Income_detail_Income_type1_idx` (`Income_type_id`);

--
-- Indices de la tabla `income_types`
--
ALTER TABLE `income_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Income_type_Employee1_idx` (`employee_id`);

--
-- Indices de la tabla `off_days`
--
ALTER TABLE `off_days`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_off_days_period_summary1_idx` (`period_summary_id`);

--
-- Indices de la tabla `on_days`
--
ALTER TABLE `on_days`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_on_days_period_summary1_idx` (`period_summary_id`),
  ADD KEY `period_summary_id` (`period_summary_id`);

--
-- Indices de la tabla `outcomes`
--
ALTER TABLE `outcomes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Outcome_Provider1_idx` (`provider_id`);

--
-- Indices de la tabla `outcome_details`
--
ALTER TABLE `outcome_details`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `outcome_releases`
--
ALTER TABLE `outcome_releases`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `period_discounts`
--
ALTER TABLE `period_discounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Legal_discount_Employee1_idx` (`employee_id`);

--
-- Indices de la tabla `period_incomes`
--
ALTER TABLE `period_incomes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Period_income_Employee1_idx` (`Employee_id`);

--
-- Indices de la tabla `period_summaries`
--
ALTER TABLE `period_summaries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_period_summary_Employee1_idx` (`employee_id`);

--
-- Indices de la tabla `providers`
--
ALTER TABLE `providers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `city_id` (`city_id`),
  ADD UNIQUE KEY `city_id_2` (`city_id`),
  ADD KEY `fk_Proveedor_City1_idx` (`city_id`);

--
-- Indices de la tabla `regions`
--
ALTER TABLE `regions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_Region_Country_idx` (`country_id`);

--
-- Indices de la tabla `travel_provisions`
--
ALTER TABLE `travel_provisions`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `budgets`
--
ALTER TABLE `budgets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `buy_orders`
--
ALTER TABLE `buy_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `buy_orders_details`
--
ALTER TABLE `buy_orders_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `cost_centers`
--
ALTER TABLE `cost_centers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=240;
--
-- AUTO_INCREMENT de la tabla `countriesasdasd`
--
ALTER TABLE `countriesasdasd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `discounts_details`
--
ALTER TABLE `discounts_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `discount_types`
--
ALTER TABLE `discount_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT de la tabla `i18n`
--
ALTER TABLE `i18n`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `income_details`
--
ALTER TABLE `income_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `income_types`
--
ALTER TABLE `income_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `off_days`
--
ALTER TABLE `off_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
--
-- AUTO_INCREMENT de la tabla `on_days`
--
ALTER TABLE `on_days`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `outcomes`
--
ALTER TABLE `outcomes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT de la tabla `outcome_details`
--
ALTER TABLE `outcome_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `outcome_releases`
--
ALTER TABLE `outcome_releases`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT de la tabla `period_discounts`
--
ALTER TABLE `period_discounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `period_incomes`
--
ALTER TABLE `period_incomes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `period_summaries`
--
ALTER TABLE `period_summaries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `providers`
--
ALTER TABLE `providers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `travel_provisions`
--
ALTER TABLE `travel_provisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
