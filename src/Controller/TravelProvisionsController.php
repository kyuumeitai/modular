<?php
namespace App\Controller;

use Cake\Event\Event;
use App\Controller\AppController;

/**
 * TravelProvisions Controller
 *
 * @property \App\Model\Table\TravelProvisionsTable $TravelProvisions
 */
class TravelProvisionsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
	public function isAuthorized($user)
	{
		$rol = $user['role'];
	    if ($rol === 'USUARIO' && in_array($this->request->action, ['downloadInforme', 'downloadAnexo', 'saveViatico'])) {
	        return true;
	    }
		return parent::isAuthorized($user);
	}

	public function index()
    {
        $this->paginate = [
            'contain' => ['Employees', 'Countries'],
            'order' => ['date_created DESC']
        ];
        $travelProvisions = $this->paginate($this->TravelProvisions);
		$ttypes = [0 => 'Vehículo CEAF', 1 => 'Vehículo Propio', 2 => 'Locomoción Colectiva', 3 => 'Avión', 4 => 'Otro'];
        $this->set(compact('travelProvisions', 'ttypes'));
        $this->set('_serialize', ['travelProvisions']);
    }

    /**
     * View method
     *
     * @param string|null $id Travel Provision id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $travelProvision = $this->TravelProvisions->get($id, [
            'contain' => ['Employees', 'Budgets']
        ]);

        $this->set('travelProvision', $travelProvision);
        $this->set('_serialize', ['travelProvision']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $travelProvision = $this->TravelProvisions->newEntity();
        if ($this->request->is('post')) {
            $travelProvision = $this->TravelProvisions->patchEntity($travelProvision, $this->request->data);
            if ($this->TravelProvisions->save($travelProvision)) {
                $this->Flash->success(__('The travel provision has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The travel provision could not be saved. Please, try again.'));
            }
        }
        $employees = $this->TravelProvisions->Employees->find('list', ['limit' => 200]);
        $budgets = $this->TravelProvisions->Budgets->find('list', ['limit' => 200]);
        $this->set(compact('travelProvision', 'employees', 'budgets'));
        $this->set('_serialize', ['travelProvision']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Travel Provision id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $travelProvision = $this->TravelProvisions->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $travelProvision = $this->TravelProvisions->patchEntity($travelProvision, $this->request->data);
            if ($this->TravelProvisions->save($travelProvision)) {
                $this->Flash->success(__('The travel provision has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The travel provision could not be saved. Please, try again.'));
            }
        }
        $employees = $this->TravelProvisions->Employees->find('list', ['limit' => 200]);
        $budgets = $this->TravelProvisions->Budgets->find('list', ['limit' => 200]);
        $this->set(compact('travelProvision', 'employees', 'budgets'));
        $this->set('_serialize', ['travelProvision']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Travel Provision id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $travelProvision = $this->TravelProvisions->get($id);
        if ($this->TravelProvisions->delete($travelProvision)) {
            $this->Flash->success(__('The travel provision has been deleted.'));
        } else {
            $this->Flash->error(__('The travel provision could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	public function saveViatico()
	{
		$this->autoRender = false;
		$this->loadModel('TravelProvisions');
		$data = $this->request->data;
		$data['start_date']	= date('Y-m-d', strtotime(str_replace('/', '-', $data['start_date'])));
		$data['end_date']	= date('Y-m-d', strtotime(str_replace('/', '-', $data['end_date'])));
		$data['date_created'] = date('Y-m-d H:i:s');
		$data['employee_id'] = $this->Auth->user('id');
		$data['cities'] = implode(',', $data['cities']);
		$data['places'] = implode(',', $data['places']);
		$data['motive'] = implode(',', $data['motive']);
		$data['objective'] = implode(',', $data['objective']);
		$data['days'] = $data['days_nosleep'] + $data['days_sleep'];
    $tp = $this->TravelProvisions->newEntity();
		$tp = $this->TravelProvisions->patchEntity($tp, $data);
		$resultado = $this->TravelProvisions->save($tp);
		if($resultado){
			$res = [
				'success' => true,
				'msg' => 'Se ha guardado el viatico en la base de datos',
				'id_viatico' => $tp->id
			];
		}else{
			$res = [
				'dbg' => $resultado,
				'success' => false,
				'msg' => 'Ha ocurrido un error al guardar en la base de datos',
			];
		}
		echo json_encode($res);
	}

	public function downloadAnexo($id = null)
	{
		$this->autoRender = false;
		$transportes = ['' => 'Vehículo CEAF', 0 => 'Vehículo CEAF', 1 => 'Vehículo Propio', 2 => 'Locomoción Colectiva', 3 => 'Avión', 4 => 'Otro'];
		$this->loadModel('TravelProvisions');
		$tp = $this->TravelProvisions->get($id, [
			'contain' => ['Employees', 'Countries', 'CostCenters']
		]);
    $this->set(compact('tp', 'transportes'));
		$this->set('_serialize', ['tp']);
		$this->render('/Element/Modales/viaticos/anexo_pdf', 'ajax');
	}

	public function downloadInforme($id = null)
	{
		$this->autoRender = false;
		$transportes = ['' => 'Vehículo CEAF', 0 => 'Vehículo CEAF', 1 => 'Vehículo Propio', 2 => 'Locomoción Colectiva', 3 => 'Avión', 4 => 'Otro'];
		$this->loadModel('TravelProvisions');
		$tp = $this->TravelProvisions->get($id, [
			'contain' => ['Employees', 'Countries', 'CostCenters']
		]);
		$this->set(compact('tp', 'transportes'));
		$this->set('_serialize', ['tp']);
		$this->render('/Element/Modales/viaticos/informe_pdf', 'ajax');
	}

	public function viewAnexo($id = null)
	{
		$this->autoRender = false;
		$transportes = ['' => 'Vehículo CEAF', 0 => 'Vehículo CEAF', 1 => 'Vehículo Propio', 2 => 'Locomoción Colectiva', 3 => 'Avión', 4 => 'Otro'];
		$this->loadModel('TravelProvisions');
		$tp = $this->TravelProvisions->get($id, [
			'contain' => ['Employees', 'Countries']
		]);
		$this->set(compact('tp', 'transportes'));
		$this->set('_serialize', ['tp']);
		$this->render('/Element/Modales/viaticos/anexo_pdf', 'ajax');
	}
}
