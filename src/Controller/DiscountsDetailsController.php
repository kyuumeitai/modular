<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * DiscountsDetails Controller
 *
 * @property \App\Model\Table\DiscountsDetailsTable $DiscountsDetails
 */
class DiscountsDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['LegalDiscounts', 'DiscountTypes']
        ];
        $discountsDetails = $this->paginate($this->DiscountsDetails);

        $this->set(compact('discountsDetails'));
        $this->set('_serialize', ['discountsDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Discounts Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $discountsDetail = $this->DiscountsDetails->get($id, [
            'contain' => ['LegalDiscounts', 'DiscountTypes']
        ]);

        $this->set('discountsDetail', $discountsDetail);
        $this->set('_serialize', ['discountsDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $discountsDetail = $this->DiscountsDetails->newEntity();
        if ($this->request->is('post')) {
            $discountsDetail = $this->DiscountsDetails->patchEntity($discountsDetail, $this->request->data);
            if ($this->DiscountsDetails->save($discountsDetail)) {
                $this->Flash->success(__('The discounts detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The discounts detail could not be saved. Please, try again.'));
            }
        }
        $legalDiscounts = $this->DiscountsDetails->LegalDiscounts->find('list', ['limit' => 200]);
        $discountTypes = $this->DiscountsDetails->DiscountTypes->find('list', ['limit' => 200]);
        $this->set(compact('discountsDetail', 'legalDiscounts', 'discountTypes'));
        $this->set('_serialize', ['discountsDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Discounts Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $discountsDetail = $this->DiscountsDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $discountsDetail = $this->DiscountsDetails->patchEntity($discountsDetail, $this->request->data);
            if ($this->DiscountsDetails->save($discountsDetail)) {
                $this->Flash->success(__('The discounts detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The discounts detail could not be saved. Please, try again.'));
            }
        }
        $legalDiscounts = $this->DiscountsDetails->LegalDiscounts->find('list', ['limit' => 200]);
        $discountTypes = $this->DiscountsDetails->DiscountTypes->find('list', ['limit' => 200]);
        $this->set(compact('discountsDetail', 'legalDiscounts', 'discountTypes'));
        $this->set('_serialize', ['discountsDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Discounts Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $discountsDetail = $this->DiscountsDetails->get($id);
        if ($this->DiscountsDetails->delete($discountsDetail)) {
            $this->Flash->success(__('The discounts detail has been deleted.'));
        } else {
            $this->Flash->error(__('The discounts detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
