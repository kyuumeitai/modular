<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OnDays Controller
 *
 * @property \App\Model\Table\OnDaysTable $OnDays
 */
class OnDaysController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            
        ];
        $onDays = $this->paginate($this->OnDays);

        $this->set(compact('onDays'));
        $this->set('_serialize', ['onDays']);


{

            $this->loadModel('OffDays');
            $this->paginate = [];
            $OffDays = $this->paginate($this->OffDays);

            $this->set(compact('OffDays'));
            $this->set('_serialize', ['OffDays']);



 }
}

    /**
     * View method
     *
     * @param string|null $id On Day id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $onDay = $this->OnDays->get($id, [
            'contain' => ['Employees', 'PeriodSummaries', 'PeriodSummaryEmployees']
        ]);

        $this->set('onDay', $onDay);
        $this->set('_serialize', ['onDay']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $onDay = $this->OnDays->newEntity();
        if ($this->request->is('post')) {
            $onDay = $this->OnDays->patchEntity($onDay, $this->request->data);
            if ($this->OnDays->save($onDay)) {
                $this->Flash->success(__('The on day has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The on day could not be saved. Please, try again.'));
            }
        }
        $employees = $this->OnDays->Employees->find('list', ['limit' => 200]);
        $periodSummaries = $this->OnDays->PeriodSummaries->find('list', ['limit' => 200]);
        $periodSummaryEmployees = $this->OnDays->PeriodSummaryEmployees->find('list', ['limit' => 200]);
        $this->set(compact('onDay', 'employees', 'periodSummaries', 'periodSummaryEmployees'));
        $this->set('_serialize', ['onDay']);
    }

    /**
     * Edit method
     *
     * @param string|null $id On Day id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $onDay = $this->OnDays->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $onDay = $this->OnDays->patchEntity($onDay, $this->request->data);
            if ($this->OnDays->save($onDay)) {
                $this->Flash->success(__('The on day has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The on day could not be saved. Please, try again.'));
            }
        }
        $employees = $this->OnDays->Employees->find('list', ['limit' => 200]);
        $periodSummaries = $this->OnDays->PeriodSummaries->find('list', ['limit' => 200]);
        $periodSummaryEmployees = $this->OnDays->PeriodSummaryEmployees->find('list', ['limit' => 200]);
        $this->set(compact('onDay', 'employees', 'periodSummaries', 'periodSummaryEmployees'));
        $this->set('_serialize', ['onDay']);
    }

    /**
     * Delete method
     *
     * @param string|null $id On Day id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $onDay = $this->OnDays->get($id);
        if ($this->OnDays->delete($onDay)) {
            $this->Flash->success(__('The on day has been deleted.'));
        } else {
            $this->Flash->error(__('The on day could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}


        
