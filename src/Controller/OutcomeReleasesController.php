<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OutcomeReleases Controller
 *
 * @property \App\Model\Table\OutcomeReleasesTable $OutcomeReleases
 */
class OutcomeReleasesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Employees', 'CostCenters'],
            'order' => ['date_created DESC']
        ];
        $outcomeReleases = $this->paginate($this->OutcomeReleases);

        $this->set(compact('outcomeReleases'));
        $this->set('_serialize', ['outcomeReleases']);
    }

    /**
     * View method
     *
     * @param string|null $id Outcome Release id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $outcomeRelease = $this->OutcomeReleases->get($id, [
            'contain' => ['Employees', 'CostCenters']
        ]);

        $this->set('outcomeRelease', $outcomeRelease);
        $this->set('_serialize', ['outcomeRelease']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $outcomeRelease = $this->OutcomeReleases->newEntity();
        if ($this->request->is('post')) {
            $outcomeRelease = $this->OutcomeReleases->patchEntity($outcomeRelease, $this->request->data);
            if ($this->OutcomeReleases->save($outcomeRelease)) {
                $this->Flash->success(__('The outcome release has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The outcome release could not be saved. Please, try again.'));
            }
        }
        $employees = $this->OutcomeReleases->Employees->find('list', ['limit' => 200]);
        $costCenters = $this->OutcomeReleases->CostCenters->find('list', ['limit' => 200]);
        $this->set(compact('outcomeRelease', 'employees', 'costCenters'));
        $this->set('_serialize', ['outcomeRelease']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Outcome Release id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $outcomeRelease = $this->OutcomeReleases->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $outcomeRelease = $this->OutcomeReleases->patchEntity($outcomeRelease, $this->request->data);
            if ($this->OutcomeReleases->save($outcomeRelease)) {
                $this->Flash->success(__('The outcome release has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The outcome release could not be saved. Please, try again.'));
            }
        }
        $employees = $this->OutcomeReleases->Employees->find('list', ['limit' => 200]);
        $costCenters = $this->OutcomeReleases->CostCenters->find('list', ['limit' => 200]);
        $this->set(compact('outcomeRelease', 'employees', 'costCenters'));
        $this->set('_serialize', ['outcomeRelease']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Outcome Release id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $outcomeRelease = $this->OutcomeReleases->get($id);
        if ($this->OutcomeReleases->delete($outcomeRelease)) {
            $this->Flash->success(__('The outcome release has been deleted.'));
        } else {
            $this->Flash->error(__('The outcome release could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
