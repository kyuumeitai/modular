<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PeriodIncomes Controller
 *
 * @property \App\Model\Table\PeriodIncomesTable $PeriodIncomes
 */
class PeriodIncomesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Employees']
        ];
        $periodIncomes = $this->paginate($this->PeriodIncomes);

        $this->set(compact('periodIncomes'));
        $this->set('_serialize', ['periodIncomes']);
    }

    /**
     * View method
     *
     * @param string|null $id Period Income id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $periodIncome = $this->PeriodIncomes->get($id, [
            'contain' => ['Employees']
        ]);

        $this->set('periodIncome', $periodIncome);
        $this->set('_serialize', ['periodIncome']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $periodIncome = $this->PeriodIncomes->newEntity();
        if ($this->request->is('post')) {
            $periodIncome = $this->PeriodIncomes->patchEntity($periodIncome, $this->request->data);
            if ($this->PeriodIncomes->save($periodIncome)) {
                $this->Flash->success(__('The period income has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The period income could not be saved. Please, try again.'));
            }
        }
        $employees = $this->PeriodIncomes->Employees->find('list', ['limit' => 200]);
        $this->set(compact('periodIncome', 'employees'));
        $this->set('_serialize', ['periodIncome']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Period Income id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $periodIncome = $this->PeriodIncomes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $periodIncome = $this->PeriodIncomes->patchEntity($periodIncome, $this->request->data);
            if ($this->PeriodIncomes->save($periodIncome)) {
                $this->Flash->success(__('The period income has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The period income could not be saved. Please, try again.'));
            }
        }
        $employees = $this->PeriodIncomes->Employees->find('list', ['limit' => 200]);
        $this->set(compact('periodIncome', 'employees'));
        $this->set('_serialize', ['periodIncome']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Period Income id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $periodIncome = $this->PeriodIncomes->get($id);
        if ($this->PeriodIncomes->delete($periodIncome)) {
            $this->Flash->success(__('The period income has been deleted.'));
        } else {
            $this->Flash->error(__('The period income could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
