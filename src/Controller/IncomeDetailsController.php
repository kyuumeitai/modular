<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * IncomeDetails Controller
 *
 * @property \App\Model\Table\IncomeDetailsTable $IncomeDetails
 */
class IncomeDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['PeriodIncomes', 'IncomeTypes']
        ];
        $incomeDetails = $this->paginate($this->IncomeDetails);

        $this->set(compact('incomeDetails'));
        $this->set('_serialize', ['incomeDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Income Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $incomeDetail = $this->IncomeDetails->get($id, [
            'contain' => ['PeriodIncomes', 'IncomeTypes']
        ]);

        $this->set('incomeDetail', $incomeDetail);
        $this->set('_serialize', ['incomeDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $incomeDetail = $this->IncomeDetails->newEntity();
        if ($this->request->is('post')) {
            $incomeDetail = $this->IncomeDetails->patchEntity($incomeDetail, $this->request->data);
            if ($this->IncomeDetails->save($incomeDetail)) {
                $this->Flash->success(__('The income detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The income detail could not be saved. Please, try again.'));
            }
        }
        $periodIncomes = $this->IncomeDetails->PeriodIncomes->find('list', ['limit' => 200]);
        $incomeTypes = $this->IncomeDetails->IncomeTypes->find('list', ['limit' => 200]);
        $this->set(compact('incomeDetail', 'periodIncomes', 'incomeTypes'));
        $this->set('_serialize', ['incomeDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Income Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $incomeDetail = $this->IncomeDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incomeDetail = $this->IncomeDetails->patchEntity($incomeDetail, $this->request->data);
            if ($this->IncomeDetails->save($incomeDetail)) {
                $this->Flash->success(__('The income detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The income detail could not be saved. Please, try again.'));
            }
        }
        $periodIncomes = $this->IncomeDetails->PeriodIncomes->find('list', ['limit' => 200]);
        $incomeTypes = $this->IncomeDetails->IncomeTypes->find('list', ['limit' => 200]);
        $this->set(compact('incomeDetail', 'periodIncomes', 'incomeTypes'));
        $this->set('_serialize', ['incomeDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Income Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incomeDetail = $this->IncomeDetails->get($id);
        if ($this->IncomeDetails->delete($incomeDetail)) {
            $this->Flash->success(__('The income detail has been deleted.'));
        } else {
            $this->Flash->error(__('The income detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
