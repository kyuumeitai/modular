<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OffDays Controller
 *
 * @property \App\Model\Table\OffDaysTable $OffDays
 */
class OffDaysController extends AppController
{

	public function isAuthorized($user)
	{
		$rol = $user['role'];
	    if ($rol === 'USUARIO' && in_array($this->request->action, ['checkAdminDays', 'checkVacationDays', 'checkHourOffs', 'vacationSummary'])) {
	        return true;
	    }
		return parent::isAuthorized($user);
	}
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
        	'contain' => ['Employees'],
        	'order' => ['OffDays.id DESC']
        ];
        $offDays = $this->paginate($this->OffDays);
		$tipos = [
			'HORA' => 'Solicitud por horas', 
			'ADMINISTRATIVO' => 'Dias administrativos',
			'MEDIO-ADMIN' => 'Media día administrativo', 
			'LEGAL' => 'Feriado legal'
		];
        $this->set(compact('offDays', 'tipos'));
        $this->set('_serialize', ['offDays']);
    }

    /**
     * View method
     *
     * @param string|null $id Off Day id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $offDay = $this->OffDays->get($id, [
            'contain' => ['Employees']
        ]);

        $this->set('offDay', $offDay);
        $this->set('_serialize', ['offDay']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $offDay = $this->OffDays->newEntity();
        if ($this->request->is('post')) {
            $offDay = $this->OffDays->patchEntity($offDay, $this->request->data);
            if ($this->OffDays->save($offDay)) {
                $this->Flash->success(__('The off day has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The off day could not be saved. Please, try again.'));
            }
        }
        $employees = $this->OffDays->Employees->find('list', ['limit' => 200]);
        $periodSummaries = $this->OffDays->PeriodSummaries->find('list', ['limit' => 200]);
        $periodSummaryEmployees = $this->OffDays->PeriodSummaryEmployees->find('list', ['limit' => 200]);
        $this->set(compact('offDay', 'employees', 'periodSummaries', 'periodSummaryEmployees'));
        $this->set('_serialize', ['offDay']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Off Day id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $offDay = $this->OffDays->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $offDay = $this->OffDays->patchEntity($offDay, $this->request->data);
            if ($this->OffDays->save($offDay)) {
                $this->Flash->success(__('The off day has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The off day could not be saved. Please, try again.'));
            }
        }
        $employees = $this->OffDays->Employees->find('list', ['limit' => 200]);
        $periodSummaries = $this->OffDays->PeriodSummaries->find('list', ['limit' => 200]);
        $periodSummaryEmployees = $this->OffDays->PeriodSummaryEmployees->find('list', ['limit' => 200]);
        $this->set(compact('offDay', 'employees', 'periodSummaries', 'periodSummaryEmployees'));
        $this->set('_serialize', ['offDay']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Off Day id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $offDay = $this->OffDays->get($id);
        if ($this->OffDays->delete($offDay)) {
            $this->Flash->success(__('The off day has been deleted.'));
        } else {
            $this->Flash->error(__('The off day could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
	
	public function checkAdminDays()
	{
		$this->autoRender = false;
		$this->request->allowMethod(['post']);
		$employee_id = $this->Auth->user('id');
		$fecha = implode('-', array_reverse(explode('/', $this->request->data['off_date'])));
		$periodo = date('Y', strtotime($fecha));
		$solicitados = 0;
		$dias = $this->OffDays->find();
		$dias = $dias->select([
					'total' => 'count(*)', 
					'period' => 'DATE_FORMAT(start_date, "%x")'
			])
			->where([
				'AND' => [
					'DATE_FORMAT(start_date, "%x") like' => $periodo,
					'employee_id' => $employee_id,
					'type' => 'ADMINISTRATIVO'
				]
			])
			->group('DATE_FORMAT(start_date, "%Y")');
		$res = $dias->toArray();
		if(!empty($res)){
			$solicitados = (int)$res[0]->total;
		}
		$medios = $this->OffDays->find();
		$medios = $medios->select([
					'total' => 'count(*)',
					'period' => 'DATE_FORMAT(start_date, "%x")'
			])
			->where([
				'AND' => [
					'DATE_FORMAT(start_date, "%x") like' => $periodo,
					'employee_id' => $employee_id,
					'type' => 'MEDIO-ADMIN'
				]
			])
			->group('DATE_FORMAT(start_date, "%Y")');
		$res = $medios->toArray();	
		if(!empty($res)){
			$solicitados += (float)($res[0]->total / 2);
		}
		if($solicitados > 6){
			$res = [
				'success' => false,
				'msg' => 'No puede solicitar dias administrativos para este periodo, ya se ha alcanzado el maximo permitido'
			];
		}else{
			$offDay = $this->OffDays->newEntity();
			$offDay = $this->OffDays->patchEntity($offDay, 
				[
					'employee_id' => $employee_id,
					'start_date' => $fecha,
					'end_date' => $fecha,
					'type' => $this->request->data['type'],
					
				]
			);
			if($this->OffDays->save($offDay)){
				$res = [
					'success' => true,
					'msg' => 'Se ha guardado la solicitud de dia administrativo, previamente ha solicitado '.$solicitados.' dias para el año '.$fecha.''
				];
			}else{
				$res = [
					'success' => false,
					'msg' => 'Ha ocurrido un error con la solicitud de dia administrativo'
				];
			}
		}
		echo json_encode($res);
	}

	public function checkVacationDays()
	{
		$this->autoRender = false;
		$this->request->allowMethod(['post']);
		$employee_id = $this->Auth->user('id');
		$fecha = implode('-', array_reverse(explode('/', $this->request->data['start_date'])));
		$fecha_fin = implode('-', array_reverse(explode('/', $this->request->data['end_date'])));
		$periodo = date('Y', strtotime($fecha));
		$dias = $this->OffDays->find();
		$dias = $dias->select([
								'total' => 'COUNT(*)', 
								'period' => 'DATE_FORMAT(start_date, "%x")'
						])
						->where([
							'AND' => [
								'DATE_FORMAT(start_date, "%x") like' => $periodo,
								'type' => 'LEGAL',
								'employee_id' => $employee_id
								
							]
						])
						->group('DATE_FORMAT(start_date, "%Y")');
		$res = $dias->toArray();
		if(!empty($res) && $res[0]->total > 15){
			$res = [
				'success' => false,
				'msg' => 'No puede solicitar dias de feriado legal para este periodo, ya se ha alcanzado el maximo permitido'
			];
		}else{
			$offDay = $this->OffDays->newEntity();
			$offDay = $this->OffDays->patchEntity($offDay, 
				[
					'employee_id' => $employee_id,
					'start_date' => $fecha,
					'end_date' => $fecha_fin,
					'type' => 'LEGAL',
					
				]
			);
			if($this->OffDays->save($offDay)){
				$res = [
					'success' => true,
					'msg' => 'Se ha guardado la solicitud de feriado legal, previamente ha solicitado '.$res[0]->total.' dias para el año '.$fecha.''
				];
			}else{
				$res = [
					'success' => false,
					'msg' => 'Ha ocurrido un error con la solicitud de feriado legal'
				];
			}
		}
		echo json_encode($res);
	}

	public function checkHourOffs()
	{
		$this->autoRender = false;
		$this->request->allowMethod(['post']);
		$employee_id = $this->Auth->user('id');
		$fecha = implode('-', array_reverse(explode('/', $this->request->data['off_date'])));
		$periodo = date('Y-m', strtotime($fecha));
		$dias = $this->OffDays->find();
		$dias = $dias->select([
								'total' => 'COUNT(*)', 
								'period' => 'DATE_FORMAT(start_date, "%x-%m")'
						])
						->where([
							'AND' => [
								'DATE_FORMAT(start_date, "%x-%m") like' => $periodo,
								'type' => 'HORA',
								'employee_id' => $employee_id
							]
						])
						->group('DATE_FORMAT(start_date, "%x-%m")');
		$res = $dias->toArray();
		if(!empty($res) && $res[0]->total > 3){
			$res = [
				'success' => false,
				'msg' => 'No puede solicitar más permisos por hora este mes, llego al limite de 3'
			];
		}else{
			$res[0]->total = 0;
			$offDay = $this->OffDays->newEntity();
			$offDay = $this->OffDays->patchEntity($offDay, 
				[
					'employee_id' => $employee_id,
					'start_date' => $fecha.' '.implode(':', $this->request->data['hour_desde']).':00',
					'end_date' => $fecha.' '.implode(':', $this->request->data['hour_desde']).':00',
					'type' => 'HORA'
				]
			);
			if($this->OffDays->save($offDay)){
				$res = [
					'success' => true,
					'msg' => 'Se ha guardado la solicitud de permiso por horas, previamente ha solicitado '.$res[0]->total.' dias para este mes'
				];
			}else{
				$res = [
					'success' => false,
					'msg' => 'Ha ocurrido un error con la solicitud de permiso por horas'
				];
			}
		}
		echo json_encode($res);
	}

	public function vacationSummary()
	{
		
	}
	
}
