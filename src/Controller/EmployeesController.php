<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Employees Controller
 *
 * @property \App\Model\Table\EmployeesTable $Employees
 */
class EmployeesController extends AppController
{

	public function isAuthorized($user)
	{
		$rol = $user['role'];
	    if ($rol === 'USUARIO' && in_array($this->request->action, ['index', 'home', 'login', 'logout'])) {
	        return true;
	    }
		return parent::isAuthorized($user);
	}

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $employees = $this->paginate($this->Employees);

        $this->set(compact('employees'));
        $this->set('_serialize', ['employees']);
    }

    /**
     * View method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => ['OffDays', 'OnDays', 'PeriodDiscounts', 'PeriodSummaries']
        ]);

        $this->set('employee', $employee);
        $this->set('_serialize', ['employee']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $employee = $this->Employees->newEntity();
        if ($this->request->is('post')) {
            $employee = $this->Employees->patchEntity($employee, $this->request->data);
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The employee has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The employee could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('employee'));
        $this->set('_serialize', ['employee']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $employee = $this->Employees->patchEntity($employee, $this->request->data);
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('The employee has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The employee could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('employee'));
        $this->set('_serialize', ['employee']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employee = $this->Employees->get($id);
        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('The employee has been deleted.'));
        } else {
            $this->Flash->error(__('The employee could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	public function home(){
		$this->viewBuilder()->layout('userview');
	}

	public function login(){
		if ($this->request->is('post')) {
			$user = $this->Auth->identify();
			if ($user) {
				$this->Auth->setUser($user);
				return $this->redirect($this->Auth->redirectUrl());
			} else {
				$this->Flash->error(__('Username or password is incorrect'), [
					'key' => 'auth'
				]);
			}
		}
		$this->viewBuilder()->layout('anon');
	}

	public function logout(){
		return $this->redirect($this->Auth->logout());
	}

	public function editProfile()
    {
    	$id = $this->request->data['id'];
        $employee = $this->Employees->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $employee = $this->Employees->patchEntity($employee, $this->request->data);
            if ($this->Employees->save($employee)) {
				if ($this->Auth->user('id') === $employee->id) {
					$data = $employee->toArray();
					unset($data['password']);
					$this->Auth->setUser($data);
				}
            	echo json_encode(['exito' => true, 'msg' => 'Perfil editado correctamente']);
				exit();
            } else {
                echo json_encode(['exito' => false, 'msg' => 'Ha ocurrido un error']);
				exit();
            }
        }
    }
    public function changePassword(){
      if ($this->request->is(['patch', 'post', 'put'])) {
          $data = $this->request->data;
          if($data['new_password2'] != $data['new_password']){
            echo json_encode(['exito' => false, 'msg' => 'La contraseña nueva no coincide']);
            exit();
          }
          $data['password'] = $data['new_password'];
          $employee = $this->Employees->get($data['id'], [
              'contain' => []
          ]);
          $employee = $this->Employees->patchEntity($employee, $data);
          if ($this->Employees->save($employee)) {
            if ($this->Auth->user('id') === $employee->id) {
              $data = $employee->toArray();
              unset($data['password']);
              $this->Auth->setUser($data);
            }
            echo json_encode(['exito' => true, 'msg' => 'Se ha cambiado la contraseña correctamente']);
            exit();
          } else {
            $errores = "";
            foreach($employee->errors() as $ix=>$error){
              foreach($error as $causa=>$msg){
                $errores .= $msg."<br>";
              }
            }
            echo json_encode(['exito' => false, 'msg' => $errores]);
            exit();
          }
        }
    }

}
