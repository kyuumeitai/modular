<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PeriodDiscounts Controller
 *
 * @property \App\Model\Table\PeriodDiscountsTable $PeriodDiscounts
 */
class PeriodDiscountsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Employees']
        ];
        $periodDiscounts = $this->paginate($this->PeriodDiscounts);

        $this->set(compact('periodDiscounts'));
        $this->set('_serialize', ['periodDiscounts']);
    }

    /**
     * View method
     *
     * @param string|null $id Period Discount id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $periodDiscount = $this->PeriodDiscounts->get($id, [
            'contain' => ['Employees']
        ]);

        $this->set('periodDiscount', $periodDiscount);
        $this->set('_serialize', ['periodDiscount']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $periodDiscount = $this->PeriodDiscounts->newEntity();
        if ($this->request->is('post')) {
            $periodDiscount = $this->PeriodDiscounts->patchEntity($periodDiscount, $this->request->data);
            if ($this->PeriodDiscounts->save($periodDiscount)) {
                $this->Flash->success(__('The period discount has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The period discount could not be saved. Please, try again.'));
            }
        }
        $employees = $this->PeriodDiscounts->Employees->find('list', ['limit' => 200]);
        $this->set(compact('periodDiscount', 'employees'));
        $this->set('_serialize', ['periodDiscount']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Period Discount id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $periodDiscount = $this->PeriodDiscounts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $periodDiscount = $this->PeriodDiscounts->patchEntity($periodDiscount, $this->request->data);
            if ($this->PeriodDiscounts->save($periodDiscount)) {
                $this->Flash->success(__('The period discount has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The period discount could not be saved. Please, try again.'));
            }
        }
        $employees = $this->PeriodDiscounts->Employees->find('list', ['limit' => 200]);
        $this->set(compact('periodDiscount', 'employees'));
        $this->set('_serialize', ['periodDiscount']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Period Discount id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $periodDiscount = $this->PeriodDiscounts->get($id);
        if ($this->PeriodDiscounts->delete($periodDiscount)) {
            $this->Flash->success(__('The period discount has been deleted.'));
        } else {
            $this->Flash->error(__('The period discount could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
