<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * OutcomeDetails Controller
 *
 * @property \App\Model\Table\OutcomeDetailsTable $OutcomeDetails
 */
class OutcomeDetailsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Outcomes', 'CostCenters']
        ];
        $outcomeDetails = $this->paginate($this->OutcomeDetails);

        $this->set(compact('outcomeDetails'));
        $this->set('_serialize', ['outcomeDetails']);
    }

    /**
     * View method
     *
     * @param string|null $id Outcome Detail id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $outcomeDetail = $this->OutcomeDetails->get($id, [
            'contain' => ['Outcomes', 'CostCenters']
        ]);

        $this->set('outcomeDetail', $outcomeDetail);
        $this->set('_serialize', ['outcomeDetail']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $outcomeDetail = $this->OutcomeDetails->newEntity();
        if ($this->request->is('post')) {
            $outcomeDetail = $this->OutcomeDetails->patchEntity($outcomeDetail, $this->request->data);
            if ($this->OutcomeDetails->save($outcomeDetail)) {
                $this->Flash->success(__('The outcome detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The outcome detail could not be saved. Please, try again.'));
            }
        }
        $outcomes = $this->OutcomeDetails->Outcomes->find('list', ['limit' => 200]);
        $costCenters = $this->OutcomeDetails->CostCenters->find('list', ['limit' => 200]);
        $this->set(compact('outcomeDetail', 'outcomes', 'costCenters'));
        $this->set('_serialize', ['outcomeDetail']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Outcome Detail id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $outcomeDetail = $this->OutcomeDetails->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $outcomeDetail = $this->OutcomeDetails->patchEntity($outcomeDetail, $this->request->data);
            if ($this->OutcomeDetails->save($outcomeDetail)) {
                $this->Flash->success(__('The outcome detail has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The outcome detail could not be saved. Please, try again.'));
            }
        }
        $outcomes = $this->OutcomeDetails->Outcomes->find('list', ['limit' => 200]);
        $costCenters = $this->OutcomeDetails->CostCenters->find('list', ['limit' => 200]);
        $this->set(compact('outcomeDetail', 'outcomes', 'costCenters'));
        $this->set('_serialize', ['outcomeDetail']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Outcome Detail id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $outcomeDetail = $this->OutcomeDetails->get($id);
        if ($this->OutcomeDetails->delete($outcomeDetail)) {
            $this->Flash->success(__('The outcome detail has been deleted.'));
        } else {
            $this->Flash->error(__('The outcome detail could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
