<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * IncomeTypes Controller
 *
 * @property \App\Model\Table\IncomeTypesTable $IncomeTypes
 */
class IncomeTypesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Employees']
        ];
        $incomeTypes = $this->paginate($this->IncomeTypes);

        $this->set(compact('incomeTypes'));
        $this->set('_serialize', ['incomeTypes']);
    }

    /**
     * View method
     *
     * @param string|null $id Income Type id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $incomeType = $this->IncomeTypes->get($id, [
            'contain' => ['Employees']
        ]);

        $this->set('incomeType', $incomeType);
        $this->set('_serialize', ['incomeType']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $incomeType = $this->IncomeTypes->newEntity();
        if ($this->request->is('post')) {
            $incomeType = $this->IncomeTypes->patchEntity($incomeType, $this->request->data);
            if ($this->IncomeTypes->save($incomeType)) {
                $this->Flash->success(__('The income type has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The income type could not be saved. Please, try again.'));
            }
        }
        $employees = $this->IncomeTypes->Employees->find('list', ['limit' => 200]);
        $this->set(compact('incomeType', 'employees'));
        $this->set('_serialize', ['incomeType']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Income Type id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $incomeType = $this->IncomeTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $incomeType = $this->IncomeTypes->patchEntity($incomeType, $this->request->data);
            if ($this->IncomeTypes->save($incomeType)) {
                $this->Flash->success(__('The income type has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The income type could not be saved. Please, try again.'));
            }
        }
        $employees = $this->IncomeTypes->Employees->find('list', ['limit' => 200]);
        $this->set(compact('incomeType', 'employees'));
        $this->set('_serialize', ['incomeType']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Income Type id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $incomeType = $this->IncomeTypes->get($id);
        if ($this->IncomeTypes->delete($incomeType)) {
            $this->Flash->success(__('The income type has been deleted.'));
        } else {
            $this->Flash->error(__('The income type could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
