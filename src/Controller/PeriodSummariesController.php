<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PeriodSummaries Controller
 *
 * @property \App\Model\Table\PeriodSummariesTable $PeriodSummaries
 */
class PeriodSummariesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Employees']
        ];
        $periodSummaries = $this->paginate($this->PeriodSummaries);

        $this->set(compact('periodSummaries'));
        $this->set('_serialize', ['periodSummaries']);
    }

    /**
     * View method
     *
     * @param string|null $id Period Summary id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $periodSummary = $this->PeriodSummaries->get($id, [
            'contain' => ['Employees']
        ]);

        $this->set('periodSummary', $periodSummary);
        $this->set('_serialize', ['periodSummary']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $periodSummary = $this->PeriodSummaries->newEntity();
        if ($this->request->is('post')) {
            $periodSummary = $this->PeriodSummaries->patchEntity($periodSummary, $this->request->data);
            if ($this->PeriodSummaries->save($periodSummary)) {
                $this->Flash->success(__('The period summary has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The period summary could not be saved. Please, try again.'));
            }
        }
        $employees = $this->PeriodSummaries->Employees->find('list', ['limit' => 200]);
        $this->set(compact('periodSummary', 'employees'));
        $this->set('_serialize', ['periodSummary']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Period Summary id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $periodSummary = $this->PeriodSummaries->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $periodSummary = $this->PeriodSummaries->patchEntity($periodSummary, $this->request->data);
            if ($this->PeriodSummaries->save($periodSummary)) {
                $this->Flash->success(__('The period summary has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The period summary could not be saved. Please, try again.'));
            }
        }
        $employees = $this->PeriodSummaries->Employees->find('list', ['limit' => 200]);
        $this->set(compact('periodSummary', 'employees'));
        $this->set('_serialize', ['periodSummary']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Period Summary id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $periodSummary = $this->PeriodSummaries->get($id);
        if ($this->PeriodSummaries->delete($periodSummary)) {
            $this->Flash->success(__('The period summary has been deleted.'));
        } else {
            $this->Flash->error(__('The period summary could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
