<?php
namespace App\Controller;

use App\Controller\AppController;

class ModalesController extends AppController
{

	public function isAuthorized($user)
	{
		$rol = $user['role'];
	    if ($rol === 'USUARIO') {
	        return true;
	    }
		return parent::isAuthorized($user);
	}

    public function index()
    {

    }

	public function loadViaticos()
	{
		$this->loadModel('Countries');
    $this->loadModel('CostCenters');
    $paises = $this->Countries->find('list');
    $cc = $this->CostCenters->find('list');
		$this->set(compact('paises', 'cc'));
		$this->render('/Element/Modales/viaticos', 'ajax');
	}

	public function loadRendiciones()
	{
		$this->render('/Element/Modales/rendiciones', 'ajax');
	}

	public function loadCompras()
	{
		$this->render('/Element/Modales/compras', 'ajax');
	}

	public function loadPermisos()
	{
		$this->render('/Element/Modales/permisos', 'ajax');
	}

	public function loadSolicitudPermisos()
	{
		$this->render('/Element/Modales/permisos/solicitar', 'ajax');
	}

	public function loadRevisarInfo()
	{
		$this->loadModel('OffDays');
		$employee_id = $this->Auth->user('id');
		$periodo = date('Y');
		$dias = $this->OffDays->find();
		$dias = $dias->select([
						'total' => 'COUNT(*)',
						'period' => 'DATE_FORMAT(start_date, "%x")'
				])
				->where([
					'AND' => [
						'DATE_FORMAT(start_date, "%x") like' => $periodo,
						'employee_id' => $employee_id,
						'type' => 'LEGAL'
					]
				])
				->group('DATE_FORMAT(start_date, "%Y")');
		$res = $dias->toArray();
		$diasLegales['totales'] = 15;
		if(!empty($res)){
			$diasLegales['disponibles'] = $diasLegales['totales'] - (int)$res[0]->total;
			$diasLegales['usados'] = (int)$res[0]->total;
		}else{
			$diasLegales['disponibles'] = $diasLegales['totales'];
			$diasLegales['usados'] = 0;
		}
		$dias = $this->OffDays->find();
		$dias = $dias->select([
					'total' => 'count(*)',
					'period' => 'DATE_FORMAT(start_date, "%x")'
			])
			->where([
				'AND' => [
					'DATE_FORMAT(start_date, "%x") like' => $periodo,
					'employee_id' => $employee_id,
					'type' => 'ADMINISTRATIVO'
				]
			])
			->group('DATE_FORMAT(start_date, "%Y")');
		$res = $dias->toArray();
		$diasAdministrativos['totales'] = 6;
		if(!empty($res)){
			$diasAdministrativos['usados'] = (int)$res[0]->total;
			$diasAdministrativos['disponibles'] = $diasAdministrativos['totales'] - $diasAdministrativos['usados'];
		}else{
			$diasAdministrativos['usados'] = 0;
			$diasAdministrativos['disponibles'] = $diasAdministrativos['totales'];
		}
		$medios = $this->OffDays->find();
		$medios = $medios->select([
					'total' => 'count(*)',
					'period' => 'DATE_FORMAT(start_date, "%x")'
			])
			->where([
				'AND' => [
					'DATE_FORMAT(start_date, "%x") like' => $periodo,
					'employee_id' => $employee_id,
					'type' => 'MEDIO-ADMIN'
				]
			])
			->group('DATE_FORMAT(start_date, "%Y")');
		$res = $medios->toArray();
		if(!empty($res)){
			$diasAdministrativos['usados'] = (float)$diasAdministrativos['usados'] + ($res[0]->total / 2);
			$diasAdministrativos['disponibles'] = $diasAdministrativos['totales'] - $diasAdministrativos['usados'];

		}
		$dias = $this->OffDays->find();
		$dias = $dias->select([
								'total' => 'COUNT(*)',
								'period' => 'DATE_FORMAT(start_date, "%x-%m")'
						])
						->where([
							'AND' => [
								'DATE_FORMAT(start_date, "%x-%m") like' => $periodo,
								'type' => 'HORA',
								'employee_id' => $employee_id
							]
						])
						->group('DATE_FORMAT(start_date, "%x-%m")');
		$res = $dias->toArray();
		$permisoHoras['totales'] = 3;
		if(!empty($res)){
			$permisoHoras['usados'] = (int)$res[0]->total;
			$permisoHoras['disponibles'] = $permisoHoras['totales'] - (int)$res[0]->total;
		}else{
			$permisoHoras['usados'] = 0;
			$permisoHoras['disponibles'] = $permisoHoras['totales'];
		}
		$this->set('permisos', [
			'porHoras' => $permisoHoras,
			'administrativos' => $diasAdministrativos,
			'legales' => $diasLegales,
		]);
		$this->render('/Element/Modales/permisos/revisarInformacion', 'ajax');
	}

	public function loadFormOc()
	{
		$this->loadModel('CostCenters');
		$this->loadModel('Providers');
		$prov = $this->Providers->find('all', ['recursive' => -1]);
		$prov_json = json_encode($prov);
		$cc = $this->CostCenters->find('list');
		$this->set(compact('cc', 'prov_json'));
		$this->render('/Element/Modales/compras/formOc', 'ajax');
	}

	public function loadViewOc($id = null)
	{
		$this->loadModel('BuyOrders');
		$oc = $this->BuyOrders->get($id);
		$detalles = $this->BuyOrders->BuyOrdersDetails->find('all', ['conditions' => ['buy_order_id' => $id]]);
		$oc->BuyOrderDetails = $detalles;
		$this->set(compact('oc'));
		$this->set('_serialize', ['oc']);
		$this->render('/Element/Modales/compras/viewOc', 'ajax');
	}

	public function loadViewOcPdf($id = null)
	{
		$this->loadModel('BuyOrders');
		$oc = $this->BuyOrders->get($id);
		$detalles = $this->BuyOrders->BuyOrdersDetails->find('all', ['conditions' => ['buy_order_id' => $id]]);
		$oc->BuyOrderDetails = $detalles;
		$this->set(compact('oc'));
		$this->set('_serialize', ['oc']);
		$this->render('/Element/Modales/compras/viewOc_pdf', 'ajax');
	}

	public function loadIngresoCompra()
	{
		$this->render('/Element/Modales/compras/ingresoCompra', 'ajax');
	}

	public function loadFormRendicion()
	{
		$this->loadModel('CostCenters');
		$cc = $this->CostCenters->find('list');
    $user = $this->Auth->user();
		$this->set(compact('cc', 'user'));
		$this->render('/Element/Modales/rendiciones/form', 'ajax');
	}

	public function loadViewRendicion($id = null)
	{
		$this->loadModel('OutcomeReleases');
		$this->loadModel('CostCenters');
		$cc = $this->CostCenters->find('list');
		$release = $this->OutcomeReleases->get($id);
        $this->set(compact('cc', 'release'));
		$this->set('_serialize', ['release']);
		$this->render('/Element/Modales/rendiciones/view', 'ajax');
	}

	public function loadViewRendicionPdf($id = null)
	{
		$this->loadModel('OutcomeReleases');
		$this->loadModel('CostCenters');
		$cc = $this->CostCenters->find('list');
		$release = $this->OutcomeReleases->get($id, [
      'contain' => ['CostCenters']
    ]);
    debug($release);
    $this->set(compact('cc', 'release'));
		$this->set('_serialize', ['release']);
		$this->render('/Element/Modales/rendiciones/view_pdf', 'ajax');
	}

	public function loadFormRendir()
	{
		$this->loadModel('CostCenters');
		$documentos = [
			'factura' => 'Factura',
			'factura-elec' => 'Factura Electrónica',
			'factura-exec' => 'Factura Exenta',
			'factura-exec-elec' => 'Factura Exenta Electrónica',
			'bho' => 'BHO',
			'bho-3ro' => 'BHO 3ros',
			'boleta' => 'Boleta',
			'boleta-elec' => 'Boleta Electr',
			'comprobante' => 'Comprobante',
			'anexo-3' => 'Anexo 3',
			'solicitud' => 'Solicitud',
			'pas-peaj-ticket' => 'Pasajes/Peajes/Tickets'
		];
		$items = [
			'' => 'Seleccione item',
			'perfeccionamiento-capacitacion' => 'Perfeccionamiento y Capacitación',
			'materiales-fungibles' => 'Materiales Fungibles',
			'pasajes-viaticos' => 'Pasajes y Viáticos',
			'publicaciones-seminarios' => 'Publicaciones y Seminarios',
			'gastos-operacion' => 'Gastos de Operación',
			'equipos' => 'Equipos'
		];
		$subitems = [
			'' => [
				'' => 'Debe seleccionar un item'
			],
			'perfeccionamiento-capacitacion' => [
				'traida-expertos' => 'Traída de Expertos',
				'cursos-seminarios' => 'Cursos, Seminarios y Similares'
			],
			'materiales-fungibles' => [
				'insumos-lab' =>'Insumos para Laboratorio y Campo',
				'mat-ferreteria-cons' =>'Materiales de Ferretería, Construcción y Similares',
				'mat-oficina-comp' =>'Materiales de Oficina y Computación',
				'mat-aseo-similares' =>'Materiales de Aseo y Similares',
				'mat-sum-otros' =>'Otros Materiales y Suministros'
			],
			'pasajes-viaticos' => [
				'viaticos-nacionales' =>'Viáticos Nacionales',
				'viaticos-internacionales' =>'Viáticos Internacionales',
				'pasajes-nacionales' =>'Pasajes Nacionales',
				'pasajes-aereos' =>'Pasajes Aéreos (Nacionales e Internacionales)'
			],
			'publicaciones-seminarios' => [
				'pub-cientificas' =>'Publicaciones Científicas',
				'exp-congresos' =>'Exposiciones en Congresos, Seminarios y Similares',
				'exp-congresos' =>'Organización de Congresos, Seminarios y Similares'
			],
			'gastos-operacion' => [
				'electricidad' => 'Electricidad',
				'agua-potable' => 'Agua Potable',
				'derechos-agua' => 'Derechos de Agua',
				'telefonia-internet' => 'Telefonía e Internet',
				'gas' => 'Gas',
				'comb-lubr-vehi' => 'Combustible y Lubricantes para Vehículos',
				'comb-lubr-maq-otros' => 'Combustible y Lubricantes para Maq., Equipos y Otros',
				'mantencion-edificios' => 'Mant. y Rep. de Edificios',
				'mantencion-lab-campo' => 'Mant. y Rep. de Equipos de Laboratorio y Campo',
				'mantencion-eq-oficina' => 'Mant. y Rep. de Equipos de Oficina y Computación',
				'mantencion-vehiculos' => 'Mant. y Rep. de Vehículos',
				'analisis-lab-ext' => 'Análisis o Estudios en Laboratorios Externos',
				'impr-publ-prens' => 'Impresión, Publicidad y Prensa',
				'transp-pasajeros' => 'Transportes de Pasajeros',
				'poliza-seguro' => 'Pólizas de Seguro',
				'poliza-garantia' => 'Pólizas de Garantía',
				'aseo-vigilancia' => 'Aseo y Vigilancia',
				'asesoria-externa' => 'Asesorías Externas',
				'mantencion-vehiculos' => 'Otros Servicios Externos N.C.P',
				'arriendo-bienes' => 'Arriendo de Bienes Inmuebles',
				'arriendo-vehiculos' => 'Arriendo de Vehículos',
				'arriendo-maq-equipo' => 'Arriendo de Maquinarias, Equipos y Similares',
				'pasajes-peajes-mov' => 'Pasajes, Peajes, Estacionamientos, Taxi, BIP!',
				'despacho-fletes-bodega' => 'Gastos de Despacho, Fletes y Bodegajes',
				'elem-proteccion-pers' => 'Elementos de Protección Personal',
				'impl-seguridad' => 'Implementos de Seguridad',
				'alimentacion' => 'Alimentación (Reunión/Almuerzo - Coffee Break)'
			],
			'equipos' => [
				'equipos-lab' =>'Equipos de Laboratorio',
				'equipos-camp' =>'Equipos de Campo',
				'equipos-ofi' =>'Equipos de Oficina'
			]
		];
		$cc = $this->CostCenters->find('list');
		$this->set(compact('cc', 'items', 'subitems', 'documentos'));
		$this->set('_serialize', ['cc']);
		$this->render('/Element/Modales/rendiciones/form-rendir', 'ajax');
	}

	public function loadFormAdministrativo()
	{
		$this->render('/Element/Modales/permisos/formAdministrativo', 'ajax');
	}

	public function loadFormVacaciones()
	{
		$this->loadModel('OffDays');
		$employee_id = $this->Auth->user('id');
		$periodo = date('Y');
		$dias = $this->OffDays->find();
		$dias = $dias->select([
						'total' => 'COUNT(*)',
						'period' => 'DATE_FORMAT(start_date, "%x")'
				])
				->where([
					'AND' => [
						'DATE_FORMAT(start_date, "%x") like' => $periodo,
						'employee_id' => $employee_id,
						'type' => 'LEGAL'
					]
				])
				->group('DATE_FORMAT(start_date, "%Y")');
		$res = $dias->toArray();
		$diasLegales = 15;
		if(!empty($res)){
			$diasLegales = $diasLegales - (int)$res[0]->total;
		}
		$this->set(compact('diasLegales'));
		$this->render('/Element/Modales/permisos/formLegal', 'ajax');
	}

	public function loadFormHoras()
	{
		$this->render('/Element/Modales/permisos/formHoras', 'ajax');
	}

	public function perfil()
	{
		$this->set('usuario', $this->Auth->user());
		$this->render('/Element/Modales/perfil/form', 'ajax');
	}

	public function changePassword()
	{
		$this->set('usuario', $this->Auth->user());
		$this->render('/Element/Modales/perfil/changePassword', 'ajax');
	}

}
