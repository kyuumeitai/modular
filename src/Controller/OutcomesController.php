<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Outcomes Controller
 *
 * @property \App\Model\Table\OutcomesTable $Outcomes
 */
class OutcomesController extends AppController
{

	public function isAuthorized($user)
	{
		$rol = $user['role'];
	    if ($rol === 'USUARIO' && in_array($this->request->action, ['saveRendicion', 'outcomeRSave', 'createOc', '_insertProveedor', '_checkProveedor'])) {
	        return true;
	    }
		return parent::isAuthorized($user);
	}
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [];
        $outcomes = $this->paginate($this->Outcomes);

        $this->set(compact('outcomes'));
        $this->set('_serialize', ['outcomes']);
    }

    /**
     * View method
     *
     * @param string|null $id Outcome id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $outcome = $this->Outcomes->get($id);

        $this->set('outcome', $outcome);
        $this->set('_serialize', ['outcome']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $outcome = $this->Outcomes->newEntity();
        if ($this->request->is('post')) {
            $outcome = $this->Outcomes->patchEntity($outcome, $this->request->data);
            if ($this->Outcomes->save($outcome)) {
                $this->Flash->success(__('The outcome has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The outcome could not be saved. Please, try again.'));
            }
        }
        $bos = $this->Outcomes->BuyOrders->find('list', ['limit' => 200]);
        $providers = $this->Outcomes->Providers->find('list', ['limit' => 200]);
        $this->set(compact('outcome', 'bos', 'providers'));
        $this->set('_serialize', ['outcome']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Outcome id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $outcome = $this->Outcomes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $outcome = $this->Outcomes->patchEntity($outcome, $this->request->data);
            if ($this->Outcomes->save($outcome)) {
                $this->Flash->success(__('The outcome has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The outcome could not be saved. Please, try again.'));
            }
        }
        $bos = $this->Outcomes->BuyOrders->find('list', ['limit' => 200]);
        $providers = $this->Outcomes->Providers->find('list', ['limit' => 200]);
        $this->set(compact('outcome', 'bos', 'providers'));
        $this->set('_serialize', ['outcome']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Outcome id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $outcome = $this->Outcomes->get($id);
        if ($this->Outcomes->delete($outcome)) {
            $this->Flash->success(__('The outcome has been deleted.'));
        } else {
            $this->Flash->error(__('The outcome could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

	public function saveRendicion(){
		$this->autoRender = false;
		$employee_id = $this->Auth->user('id');
		$msg = "";
		$guardados = 0;
		foreach($this->request->data['detalle'] as $ix=>$rendicion){
			$data = $rendicion;
			$prov = $this->_checkProveedor($data['provider_rut']);
			if($prov != false){
				$data['provider_id'] = $prov->id;
			}else{
				$proveedor = [
					'rut' => $data['provider_rut'],
					'names' => $data['provider_name'],
					'address' => 'Sin dirección',
					'city_id' => 0
				];
				$prov = $this->_insertProveedor($data['provider_rut']);
				if($prov != false){
					$data['provider_id'] = $prov->id;
				}else{
					echo json_encode(['exito' => false, 'msg' => 'Error al agregar proveedor']);
					exit();
				}
			}

			$data['document_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['document_date'])));
			$data['employee_id'] = $this->Auth->user('id');
			$outcome = $this->Outcomes->newEntity();
			$outcome = $this->Outcomes->patchEntity($outcome, $data);
			$resultado = $this->Outcomes->save($outcome);

			if($resultado){
				$msg .= "El documento número ".$outcome['document_id']." se ha guardado como rendición";
				$guardados++;
			}else{
				$msg .= "Hubo un error al guardar el documento número ".$outcome['document_id']."<br>";
			}
		}
		if($guardados > 0){
			$res = [
				'total' => $guardados,
				'success' => true,
				'msg' => $msg,
			];
		}else{
			$res = [
				'dbg' => $resultado,
				'success' => false,
				'msg' => 'Ha ocurrido un error al guardar en la base de datos',
			];
		}
		echo json_encode($res);
		exit();
	}

	public function outcomeRSave(){
		$this->autoRender = false;
		$this->loadModel('OutcomeReleases');
		$data = $this->request->data;
		$data['date'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['date'])));
		$data['estimated_release'] = date('Y-m-d', strtotime(str_replace('/', '-', $data['estimated_release'])));
		$data['employee_id'] = $this->Auth->user('id');
		$or = $this->OutcomeReleases->newEntity();
		$or = $this->OutcomeReleases->patchEntity($or, $data);
		$resultado = $this->OutcomeReleases->save($or);
		if($resultado){
			$res = [
				'dbg' => $resultado,
				'id_rendicion' => $resultado->id,
				'success' => true,
				'msg' => 'Se ha guardado la rendición en la base de datos'
			];
		}else{
			$res = [
				'dbg' => $resultado,
				'success' => false,
				'msg' => 'Ha ocurrido un error al guardar en la base de datos',
			];
		}
		echo json_encode($res);
	}

	function createOc(){
		$this->autoRender = false;
		$this->loadModel('BuyOrders');
		$data = $this->request->data;
		$prov = $this->_checkProveedor($data['rut_provider']);
		$ins = [];
		if($prov != false){
			$ins['provider_id'] = $prov->id;
		}else{
			$proveedor = [
				'rut' => $data['rut_provider'],
				'names' => $data['name_provider'],
				'address' => 'Sin dirección',
				'city_id' => 0,
				'Contact' => [
					'names' => $data['contact_provider'],
					'lastnames' => $data['contact_provider'],
					'rut' => '1-9',
					'address' => 'Sin dirección',
					'provider_id' => 0,
				]
			];
			$prov = $this->_insertProveedor($data['rut_provider']);
			if($prov != false){
				$ins['provider_id'] = $prov->id;
			}else{
				echo json_encode(['exito' => false, 'msg' => 'Error al agregar proveedor']);
				exit();
			}
		}
		$items = [];
		$ins['cotizacion']		= ($data['cotizacion'] != '') ? $data['cotizacion'] : 0;
		$ins['create_date']		= date("Y-m-d H:i:s");
		$ins['buy_date']		= date("Y-m-d", strtotime(str_replace("/", "-", $data['buy_date'])));
		$ins['name_buyer']		= $data['name_buyer'];
		$ins['rut_buyer']		= $data['rut_buyer']."-".$data['dv'];
		$ins['cost_center_id']	= $data['cost_center_id'];
		$oc = $this->BuyOrders->newEntity();
		$oc = $this->BuyOrders->patchEntity($oc, $ins);
		if($this->BuyOrders->save($oc)){
			$oc_id = $oc->id;
			foreach($data['detalle'] as $ix=>$item){
				$item['buy_order_id'] = $oc_id;
				$detalle = $this->BuyOrders->BuyOrdersDetails->newEntity();
				$detalle = $this->BuyOrders->BuyOrdersDetails->patchEntity($detalle, $item);
				$this->BuyOrders->BuyOrdersDetails->save($detalle);
			}
			return $this->redirect(['action' => 'loadViewOc', 'controller' => 'modales', $oc_id]);
		}else{
			echo json_encode(['exito' => false, 'msg' => 'Error al agregar proveedor']);
			exit();
		}

	}

	function insertOutcome(){
		debug($this->request->data);
	}

	private function _insertProveedor($datos = null){
		$this->loadModel('Providers');
		if($datos = null)
			return;
		$contacto = [];
		if(isset($datos['Contact'])){
			$contacto = $datos['Contact'];
			unset($datos['Contact']);
		}
		$provider = $this->Providers->newEntity();
        $provider = $this->Providers->patchEntity($provider, $datos);
		if($this->Providers->save($provider)) {
			if(!empty($contacto)){
				$contacto['provider_id'] = $provider->id;
				$contact = $this->Providers->Contacts->newEntity();
				$contact = $this->Providers->Contacts->patchEntity($contact, $contacto);
				$this->Providers->Contacts->save($contact);
			}
			return $provider;
		}else{
			return false;
		}
	}

	private function _checkProveedor($rut_proveedor = null){
		$this->loadModel('Providers');
		$prov = $this->Providers->find('all', [
										'conditions' => [
											'rut LIKE' => $rut_proveedor
										]
									]);
		return (!empty($prov)) ? $prov->first() : false;
	}

}

		///$pernoctando = (isset($data['days_sleep']) && $data['days_sleep'] != '') ? $data['days_sleep'] : 0;		$no_pernoctando = (isset($data['days_nosleep']) && $data['days_nosleep'] != '') ? $data['days_nosleep'] : 0;		$amount = ($pernoctando * 45000) + ($no_pernoctando * 9000);		if($data['amount'] != $amount){			$data['amount'] = $amount; }//
