<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OutcomeDetail Entity
 *
 * @property int $id
 * @property int $outcome_id
 * @property int $cost_center_id
 * @property string $related_budget_item
 * @property string $detail
 * @property \Cake\I18n\Time $date_created
 *
 * @property \App\Model\Entity\Outcome $outcome
 * @property \App\Model\Entity\CostCenter $cost_center
 */
class OutcomeDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
