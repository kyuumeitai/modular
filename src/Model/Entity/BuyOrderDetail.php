<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class BuyOrderDetail extends Entity
{

    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
