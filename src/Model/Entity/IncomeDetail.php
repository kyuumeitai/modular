<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * IncomeDetail Entity
 *
 * @property int $id
 * @property string $description
 * @property string $value
 * @property int $Period_income_id
 * @property int $Income_type_id
 *
 * @property \App\Model\Entity\PeriodIncome $period_income
 * @property \App\Model\Entity\IncomeType $income_type
 */
class IncomeDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'Period_income_id' => false,
        'Income_type_id' => false
    ];
}
