<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DiscountsDetail Entity
 *
 * @property int $id
 * @property string $legal_discount_id
 * @property string $value
 * @property int $Legal_discounts_id
 * @property int $Discount_type_id
 *
 * @property \App\Model\Entity\LegalDiscount $legal_discount
 * @property \App\Model\Entity\DiscountType $discount_type
 */
class DiscountsDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'Legal_discounts_id' => false,
        'Discount_type_id' => false
    ];
}
