<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Budget Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $buy_order_id
 * @property int $cost_center_id
 * @property int $outcome_id
 * @property int $period_month
 * @property int $period_year
 * @property \Cake\I18n\Time $date_created
 *
 * @property \App\Model\Entity\BuyOrder $buy_order
 * @property \App\Model\Entity\CostCenter $cost_center
 * @property \App\Model\Entity\Outcome $outcome
 */
class Budget extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
