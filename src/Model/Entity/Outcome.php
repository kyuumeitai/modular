<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Outcome Entity
 *
 * @property int $id
 * @property string $rut_proveedor
 * @property string $dv
 * @property string $document_type
 * @property string $document_id
 * @property string $bo_id
 * @property int $provider_id
 *
 * @property \App\Model\Entity\Document $document
 * @property \App\Model\Entity\Bo $bo
 * @property \App\Model\Entity\Provider $provider
 */
class Outcome extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'provider_id' => true
    ];
}
