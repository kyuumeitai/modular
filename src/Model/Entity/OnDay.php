<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OnDay Entity
 *
 * @property int $id
 * @property string $employee_id
 * @property string $date
 * @property int $period_summary_id
 * @property int $period_summary_employee_id
 *
 * @property \App\Model\Entity\Employee $employee
 * @property \App\Model\Entity\PeriodSummary $period_summary
 * @property \App\Model\Entity\PeriodSummaryEmployee $period_summary_employee
 */
class OnDay extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
        'period_summary_id' => false,
        'period_summary_employee_id' => false
    ];
}
