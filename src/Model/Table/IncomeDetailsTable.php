<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * IncomeDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $PeriodIncomes
 * @property \Cake\ORM\Association\BelongsTo $IncomeTypes
 *
 * @method \App\Model\Entity\IncomeDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\IncomeDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\IncomeDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\IncomeDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\IncomeDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\IncomeDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\IncomeDetail findOrCreate($search, callable $callback = null)
 */
class IncomeDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('income_details');
        $this->displayField('id');
        $this->primaryKey(['id', 'Period_income_id', 'Income_type_id']);

        $this->belongsTo('PeriodIncomes', [
            'foreignKey' => 'Period_income_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('IncomeTypes', [
            'foreignKey' => 'Income_type_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('description');

        $validator
            ->allowEmpty('value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['Period_income_id'], 'PeriodIncomes'));
        $rules->add($rules->existsIn(['Income_type_id'], 'IncomeTypes'));

        return $rules;
    }
}
