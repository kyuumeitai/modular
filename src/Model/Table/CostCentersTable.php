<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * CostCenters Model
 *
 * @property \Cake\ORM\Association\HasMany $Budgets
 * @property \Cake\ORM\Association\HasMany $OutcomeDetails
 *
 * @method \App\Model\Entity\CostCenter get($primaryKey, $options = [])
 * @method \App\Model\Entity\CostCenter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\CostCenter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\CostCenter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\CostCenter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\CostCenter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\CostCenter findOrCreate($search, callable $callback = null)
 */
class CostCentersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('cost_centers');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->hasMany('Budgets', [
            'foreignKey' => 'cost_center_id'
        ]);
        $this->hasMany('OutcomeDetails', [
            'foreignKey' => 'cost_center_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('description');

        return $validator;
    }
}
