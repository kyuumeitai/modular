<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Budgets Model
 *
 * @property \Cake\ORM\Association\BelongsTo $BuyOrders
 * @property \Cake\ORM\Association\BelongsTo $CostCenters
 * @property \Cake\ORM\Association\BelongsTo $Outcomes
 *
 * @method \App\Model\Entity\Budget get($primaryKey, $options = [])
 * @method \App\Model\Entity\Budget newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Budget[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Budget|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Budget patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Budget[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Budget findOrCreate($search, callable $callback = null)
 */
class BudgetsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('budgets');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->belongsTo('BuyOrders', [
            'foreignKey' => 'buy_order_id'
        ]);
        $this->belongsTo('CostCenters', [
            'foreignKey' => 'cost_center_id'
        ]);
        $this->belongsTo('Outcomes', [
            'foreignKey' => 'outcome_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->integer('period_month')
            ->requirePresence('period_month', 'create')
            ->notEmpty('period_month');

        $validator
            ->integer('period_year')
            ->requirePresence('period_year', 'create')
            ->notEmpty('period_year');

        $validator
            ->dateTime('date_created')
            ->allowEmpty('date_created');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['buy_order_id'], 'BuyOrders'));
        $rules->add($rules->existsIn(['cost_center_id'], 'CostCenters'));
        $rules->add($rules->existsIn(['outcome_id'], 'Outcomes'));
        return $rules;
    }
}
