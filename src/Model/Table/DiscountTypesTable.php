<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DiscountTypes Model
 *
 * @method \App\Model\Entity\DiscountType get($primaryKey, $options = [])
 * @method \App\Model\Entity\DiscountType newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DiscountType[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DiscountType|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DiscountType patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DiscountType[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DiscountType findOrCreate($search, callable $callback = null)
 */
class DiscountTypesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('discount_types');
        $this->displayField('title');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('title');

        $validator
            ->allowEmpty('discount_value');

        $validator
            ->allowEmpty('description');

        return $validator;
    }
}
