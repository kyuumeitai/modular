<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Outcomes Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Documents
 * @property \Cake\ORM\Association\BelongsTo $Bos
 * @property \Cake\ORM\Association\BelongsTo $Providers
 *
 * @method \App\Model\Entity\Outcome get($primaryKey, $options = [])
 * @method \App\Model\Entity\Outcome newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Outcome[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Outcome|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Outcome patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Outcome[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Outcome findOrCreate($search, callable $callback = null)
 */
class OutcomesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('outcomes');
        $this->displayField('id');

        $this->belongsTo('Providers', [
            'foreignKey' => 'provider_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        //$rules->add($rules->existsIn(['document_id'], 'Documents'));
        //$rules->add($rules->existsIn(['provider_id'], 'Providers'));

        return $rules;
    }
}
