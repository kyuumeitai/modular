<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OffDays Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Employees
 * @property \Cake\ORM\Association\BelongsTo $PeriodSummaries
 * @property \Cake\ORM\Association\BelongsTo $PeriodSummaryEmployees
 *
 * @method \App\Model\Entity\OffDay get($primaryKey, $options = [])
 * @method \App\Model\Entity\OffDay newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OffDay[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OffDay|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OffDay patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OffDay[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OffDay findOrCreate($search, callable $callback = null)
 */
class OffDaysTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('off_days');
        $this->displayField('id');
        $this->primaryKey(['id', 'employee_id']);

        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id'
        ]);
        /*$this->belongsTo('PeriodSummaries', [
            'foreignKey' => 'period_summary_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('PeriodSummaryEmployees', [
            'foreignKey' => 'period_summary_employee_id',
            'joinType' => 'INNER'
        ]);*/
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['employee_id'], 'Employees'));
        return $rules;
    }
}
