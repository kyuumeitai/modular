<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * DiscountsDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $LegalDiscounts
 * @property \Cake\ORM\Association\BelongsTo $LegalDiscounts
 * @property \Cake\ORM\Association\BelongsTo $DiscountTypes
 *
 * @method \App\Model\Entity\DiscountsDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\DiscountsDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\DiscountsDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\DiscountsDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\DiscountsDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\DiscountsDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\DiscountsDetail findOrCreate($search, callable $callback = null)
 */
class DiscountsDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('discounts_details');
        $this->displayField('id');
        $this->primaryKey(['id', 'Legal_discounts_id', 'Discount_type_id']);

        $this->belongsTo('LegalDiscounts', [
            'foreignKey' => 'legal_discount_id'
        ]);
        $this->belongsTo('DiscountTypes', [
            'foreignKey' => 'Discount_type_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('value');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['legal_discount_id'], 'LegalDiscounts'));
        $rules->add($rules->existsIn(['Discount_type_id'], 'DiscountTypes'));

        return $rules;
    }
}
