<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * OutcomeDetails Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Outcomes
 * @property \Cake\ORM\Association\BelongsTo $CostCenters
 *
 * @method \App\Model\Entity\OutcomeDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\OutcomeDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OutcomeDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OutcomeDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OutcomeDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OutcomeDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OutcomeDetail findOrCreate($search, callable $callback = null)
 */
class OutcomeDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('outcome_details');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Outcomes', [
            'foreignKey' => 'outcome_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('CostCenters', [
            'foreignKey' => 'cost_center_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('related_budget_item', 'create')
            ->notEmpty('related_budget_item');

        $validator
            ->requirePresence('detail', 'create')
            ->notEmpty('detail');

        $validator
            ->dateTime('date_created')
            ->requirePresence('date_created', 'create')
            ->notEmpty('date_created');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['outcome_id'], 'Outcomes'));
        $rules->add($rules->existsIn(['cost_center_id'], 'CostCenters'));

        return $rules;
    }
}
