<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * BuyOrders Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Providers
 *
 * @method \App\Model\Entity\BuyOrder get($primaryKey, $options = [])
 * @method \App\Model\Entity\BuyOrder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\BuyOrder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\BuyOrder|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\BuyOrder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\BuyOrder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\BuyOrder findOrCreate($search, callable $callback = null)
 */
class BuyOrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('buy_orders');
        //$this->displayField('provider_id');
        $this->primaryKey(['id']);

        $this->belongsTo('Providers', [
            'foreignKey' => 'provider_id',
        ]);
		
		$this->hasMany('BuyOrdersDetails', [
            'foreignKey' => 'buy_order_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('cotizacion');

        $validator
            ->allowEmpty('create_date');

        $validator
            ->allowEmpty('autodate');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['provider_id'], 'Providers'));

        return $rules;
    }
}
