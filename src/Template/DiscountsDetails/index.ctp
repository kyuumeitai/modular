<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Discounts Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Discount Types'), ['controller' => 'DiscountTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Discount Type'), ['controller' => 'DiscountTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="discountsDetails index large-9 medium-8 columns content">
    <h3><?= __('Discounts Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('legal_discount_id') ?></th>
                <th><?= $this->Paginator->sort('value') ?></th>
                <th><?= $this->Paginator->sort('Legal_discounts_id') ?></th>
                <th><?= $this->Paginator->sort('Discount_type_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($discountsDetails as $discountsDetail): ?>
            <tr>
                <td><?= $this->Number->format($discountsDetail->id) ?></td>
                <td><?= h($discountsDetail->legal_discount_id) ?></td>
                <td><?= h($discountsDetail->value) ?></td>
                <td><?= $this->Number->format($discountsDetail->Legal_discounts_id) ?></td>
                <td><?= $discountsDetail->has('discount_type') ? $this->Html->link($discountsDetail->discount_type->title, ['controller' => 'DiscountTypes', 'action' => 'view', $discountsDetail->discount_type->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $discountsDetail->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $discountsDetail->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $discountsDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $discountsDetail->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
