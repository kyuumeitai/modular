<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Discounts Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Discount Types'), ['controller' => 'DiscountTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Discount Type'), ['controller' => 'DiscountTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="discountsDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($discountsDetail) ?>
    <fieldset>
        <legend><?= __('Add Discounts Detail') ?></legend>
        <?php
            echo $this->Form->input('legal_discount_id');
            echo $this->Form->input('value');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
