<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Discounts Detail'), ['action' => 'edit', $discountsDetail->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Discounts Detail'), ['action' => 'delete', $discountsDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $discountsDetail->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Discounts Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Discounts Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Discount Types'), ['controller' => 'DiscountTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Discount Type'), ['controller' => 'DiscountTypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="discountsDetails view large-9 medium-8 columns content">
    <h3><?= h($discountsDetail->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Legal Discount Id') ?></th>
            <td><?= h($discountsDetail->legal_discount_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Value') ?></th>
            <td><?= h($discountsDetail->value) ?></td>
        </tr>
        <tr>
            <th><?= __('Discount Type') ?></th>
            <td><?= $discountsDetail->has('discount_type') ? $this->Html->link($discountsDetail->discount_type->title, ['controller' => 'DiscountTypes', 'action' => 'view', $discountsDetail->discount_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($discountsDetail->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Legal Discounts Id') ?></th>
            <td><?= $this->Number->format($discountsDetail->Legal_discounts_id) ?></td>
        </tr>
    </table>
</div>
