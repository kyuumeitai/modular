<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Cost Center'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Budgets'), ['controller' => 'Budgets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Budget'), ['controller' => 'Budgets', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Outcome Details'), ['controller' => 'OutcomeDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Outcome Detail'), ['controller' => 'OutcomeDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="costCenters index large-9 medium-8 columns content">
    <h3><?= __('Cost Centers') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($costCenters as $costCenter): ?>
            <tr>
                <td><?= $this->Number->format($costCenter->id) ?></td>
                <td><?= h($costCenter->title) ?></td>
                <td><?= h($costCenter->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $costCenter->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $costCenter->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $costCenter->id], ['confirm' => __('Are you sure you want to delete # {0}?', $costCenter->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
