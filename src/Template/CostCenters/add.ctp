<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Budgets'), ['controller' => 'Budgets', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Budget'), ['controller' => 'Budgets', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Outcome Details'), ['controller' => 'OutcomeDetails', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Outcome Detail'), ['controller' => 'OutcomeDetails', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="costCenters form large-9 medium-8 columns content">
    <?= $this->Form->create($costCenter) ?>
    <fieldset>
        <legend><?= __('Add Cost Center') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
