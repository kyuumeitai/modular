<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Cost Center'), ['action' => 'edit', $costCenter->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Cost Center'), ['action' => 'delete', $costCenter->id], ['confirm' => __('Are you sure you want to delete # {0}?', $costCenter->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cost Center'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Budgets'), ['controller' => 'Budgets', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Budget'), ['controller' => 'Budgets', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Outcome Details'), ['controller' => 'OutcomeDetails', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Outcome Detail'), ['controller' => 'OutcomeDetails', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="costCenters view large-9 medium-8 columns content">
    <h3><?= h($costCenter->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($costCenter->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($costCenter->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($costCenter->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Budgets') ?></h4>
        <?php if (!empty($costCenter->budgets)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Title') ?></th>
                <th><?= __('Description') ?></th>
                <th><?= __('Buy Order Id') ?></th>
                <th><?= __('Cost Center Id') ?></th>
                <th><?= __('Outcome Id') ?></th>
                <th><?= __('Period Month') ?></th>
                <th><?= __('Period Year') ?></th>
                <th><?= __('Date Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($costCenter->budgets as $budgets): ?>
            <tr>
                <td><?= h($budgets->id) ?></td>
                <td><?= h($budgets->title) ?></td>
                <td><?= h($budgets->description) ?></td>
                <td><?= h($budgets->buy_order_id) ?></td>
                <td><?= h($budgets->cost_center_id) ?></td>
                <td><?= h($budgets->outcome_id) ?></td>
                <td><?= h($budgets->period_month) ?></td>
                <td><?= h($budgets->period_year) ?></td>
                <td><?= h($budgets->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Budgets', 'action' => 'view', $budgets->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Budgets', 'action' => 'edit', $budgets->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Budgets', 'action' => 'delete', $budgets->id], ['confirm' => __('Are you sure you want to delete # {0}?', $budgets->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Outcome Details') ?></h4>
        <?php if (!empty($costCenter->outcome_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Outcome Id') ?></th>
                <th><?= __('Cost Center Id') ?></th>
                <th><?= __('Related Budget Item') ?></th>
                <th><?= __('Detail') ?></th>
                <th><?= __('Date Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($costCenter->outcome_details as $outcomeDetails): ?>
            <tr>
                <td><?= h($outcomeDetails->id) ?></td>
                <td><?= h($outcomeDetails->outcome_id) ?></td>
                <td><?= h($outcomeDetails->cost_center_id) ?></td>
                <td><?= h($outcomeDetails->related_budget_item) ?></td>
                <td><?= h($outcomeDetails->detail) ?></td>
                <td><?= h($outcomeDetails->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OutcomeDetails', 'action' => 'view', $outcomeDetails->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OutcomeDetails', 'action' => 'edit', $outcomeDetails->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OutcomeDetails', 'action' => 'delete', $outcomeDetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $outcomeDetails->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
