<div class="providers index large-10 medium-9 columns content">

<script>
    $(".proveedores-nav").addClass('active');
</script>

    <h3><?= __('Proveedores') ?>
    <button class="agregar"><?= $this->Html->link(__('Agregar Proveedor'), ['action' => 'add']) ?></button>
    </h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('rut') ?></th>
                <th><?= $this->Paginator->sort('Nombres') ?></th>
                <th><?= $this->Paginator->sort('Dirección') ?></th>
                <th><?= $this->Paginator->sort('Ciudad') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($providers as $provider): ?>
            <tr>
                <td><?= $this->Number->format($provider->id) ?></td>
                <td><?= $this->Number->format($provider->rut) ?></td>
                <td><?= h($provider->names) ?></td>
                <td><?= h($provider->address) ?></td>
                <td><?= $provider->has('city') ? $this->Html->link($provider->city->name, ['controller' => 'Cities', 'action' => 'view', $provider->city->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $provider->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $provider->id]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $provider->id], ['confirm' => __('Are you sure you want to delete a {0}?', $provider->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
