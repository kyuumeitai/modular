<div class="providers form large-10 medium-9 columns content">

<script>
    $(".proveedores-nav").addClass('active');
</script>

    <?= $this->Form->create($provider) ?>
    <fieldset>
        <legend><?= __('Agregar Proveedor') ?></legend>
        <?php
            echo $this->Form->input('rut');
            echo $this->Form->input('names');
            echo $this->Form->input('address');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
