<div class="providers view large-10 medium-9 columns content">

<script>
    $(".proveedores-nav").addClass('active');
</script>

    <h3><?= h($provider->names) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Names') ?></th>
            <td><?= h($provider->names) ?></td>
        </tr>
        <tr>
            <th><?= __('Address') ?></th>
            <td><?= h($provider->address) ?></td>
        </tr>
        <tr>
            <th><?= __('City') ?></th>
            <td><?= $provider->has('city') ? $this->Html->link($provider->city->name, ['controller' => 'Cities', 'action' => 'view', $provider->city->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($provider->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Rut') ?></th>
            <td><?= $this->Number->format($provider->rut) ?></td>
        </tr>
    </table>
</div>
