<div class="offDays index large-10 medium-8 columns content">
	<script>
		$(".permisos-nav").addClass('active');
	</script>
    <h3>Solicitudes de dias libres</h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('employee_id', 'Empleado') ?></th>
                <th><?= $this->Paginator->sort('type', 'Tipo de permiso') ?></th>
                <th><?= $this->Paginator->sort('start_date', 'Fecha Inicio') ?></th>
                <th><?= $this->Paginator->sort('end_date', 'Fecha término') ?></th>
                <!--<th class="actions"><?= __('Actions') ?></th>-->
            </tr>
        </thead>
        <tbody>
            <?php foreach ($offDays as $offDay): ?>
            <tr>
                <td><?= $this->Number->format($offDay->id) ?></td>
                <td><?= $offDay->has('employee') ? $offDay->employee->firstname." ".$offDay->employee->lastname : '' ?></td>
                <td><?= h($tipos[$offDay->type]) ?></td>
                <td><?= h($offDay->start_date->nice('America/Santiago', 'es-ES')) ?></td>
                <td><?= h($offDay->end_date->nice('America/Santiago', 'es-ES')) ?></td>
               <!-- <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $offDay->id]) ?>
                </td>-->
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
