

<script>
		$( "#fechaviaticosdesde" ).datepicker({
			inline: true,
			dateFormat: "dd/mm/yy",
			defaultDate: new Date(),
            minDate: new Date(),
            onSelect: function(dateStr)
            {
		 		//$("#fechaviaticoshasta").datepicker("destroy");
		        $("#fechaviaticoshasta").val(dateStr);
		        //$("#fechaviaticoshasta").datepicker({ minDate: new Date(dateStr)})
            }


		});


		$( "#fechaviaticoshasta" ).datepicker({
			inline: true,
			dateFormat: "dd/mm/yy",
			defaultDate: new Date(),
			minDate: new Date(),
            onSelect: function(dateStr) {
            toDate = new Date(dateStr);

                // Converts date objects to appropriate strings
             /*   fromDate = ConvertDateToShortDateString(fromDate);
                toDate = ConvertDateToShortDateString(toDate);*/
            }

		});

		function add_comuna(){
			var divtest = document.createElement("div");
			divtest.innerHTML = '<input class="col-md-6 col-xs-12 borrar seleccion" type="text" id="Comuna'+comuna+'" name="cities[]" placeholder="Comuna Visitada">';

			$('.boton_comuna').click(function() {
				var divcomuna = $(this).parents(':eq(1)').attr('id');
				var objTo = document.getElementById(divcomuna);
				objTo.appendChild(divtest)
			});
		};


		function del_comuna(r) {
			var i = r.parentNode.parentNode.rowIndex;
			document.getElementById("tablacomunas").deleteRow(i);
		}

	</script>



	<?php
		echo $this->Form->create('TravelProvision', [
			'class' => 'formViatico',
			'url' => ['controller' => 'TravelProvisions', 'action' => 'saveViatico']
		]);
	?>

	<h1 class="titulo"> Solicitud de Viáticos <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>


	<div id="contenedorpais" class="col-md-6 col-xs-12">

			<!--País y Regiones, Comunas y Lugares-->

				<h1 class="titulo2 separado">País, Regiones y Comunas</h1>


			<div id="paisregiones" class="wrapper col-md-12">
				<div id="paisregion">
					<h3 class="tituloboton">País
						<input type="button" class="nopadding boton-mas2 pull-right" id="agregar_pais" onclick="add_pais();" value="+" />
					</h3>

						<?= $this->Form->input('country_id', ['options' => $paises, 'id' => 'countries', 'label' => false, 'div' => false, 'class' => 'col-md-12 col-xs-12']); ?>

				</div><!--Paisregion-->


				<div id="ciudades">
					<h3 class="tituloboton">Ciudades
						<input type="button" class="nopadding boton-mas2 pull-right" id="agregar_ciudad" onclick="add_ciudad();" value="+" />
					</h3>

					<input class="col-md-6 col-xs-12" type="text" name="cities[]" placeholder="Ciudad Visitada" required>
					<input class="col-md-6 col-xs-12 borrar" type="text" name="cities[]" placeholder="Ciudad Visitada">

				</div>


				<div id="lugaresvisitados">
					<h3 class="tituloboton">Lugares Visitados
						<input type="button" class="nopadding boton-mas2 boton_lugar pull-right" id="agregar_lugar" onclick="add_lugar();" value="+" />
					</h3>

					<input class="col-md-6 col-xs-12 " type="text" name="places[]" placeholder="Lugar Visitado" required>
					<input class="col-md-6 col-xs-12 borrar " type="text" name="places[]" placeholder="Lugar Visitado">

				</div> <!--lugaresvisitados-->
			</div><!--paisregiones-->

		</div>



	<div id="contenedorperiodo" class="col-md-6 col-xs-12 nopadding ">
		<!--/País Regiones, Comunas y lugares-->

		<div id="tabladatos" class="wrapper">
				<div id="periodo" class="col-md-12">
					<h3 class="titulo">Periodo desde/hasta</h3>

					<td class="col-md-6 col-xs-12">
						<?= $this->Form->input('employee_id', ['type' => 'hidden', 'value' => ""]); ?>
						<?= $this->Form->input('start_date', ['label' => false, 'id' => 'fechaviaticosdesde', 'required' =>'required', 'class' => 'col-md-6 col-xs-12 dateField']); ?>
					</td>

					<td class="col-md-6 col-xs-12">
						<?= $this->Form->input('end_date', ['label' => false, 'id' => 'fechaviaticoshasta', 'required' =>'required', 'class' => 'col-md-6 col-xs-12 dateField']); ?>
					</td>

				</div><!--/Periodo-->

					<div class="col-md-12 col-xs-12">
						<div class="col-md-6 col-xs-12 nopadding">
							<h3 class="titulo">Tipo de Locomoción</h3>
							<?= $this->Form->input('transport_type', ['type' => 'select', 'options' => [0 => 'Vehículo CEAF', 1 => 'Vehículo Propio', 2 => 'Locomoción Colectiva', 3 => 'Avión', 4 => 'Otro'], 'label' => false, 'required' =>'required', 'class' => 'col-md-12 col-xs-12 ', ]); ?>
						</div>

						<div class="col-md-6 col-xs-12 nopadding">
							<h3 class="titulo">Centro de costos</h3>
							<?= $this->Form->input('cc_id', ['options' => $cc, 'id' => 'CentroCosto', 'label' => false, 'div' => false, 'class' => 'col-md-12 col-xs-12']); ?>
            </div>

					</div><!--/locomoción, proyecto-->
			          

					</div>
					</div>

	<div id="contenedorDOS" class="col-md-6 col-xs-12">

	<div id="tabladatos" class="wrapper">

				<div id="motivodiv" class="col-md-6 col-xs-12 nopadding">
					<h3 class="tituloboton">Motivo(s) del Viaje
						<input type="button" class="nopadding boton-mas2 pull-right" id="agregar_motivo" onclick="add_motivo();" value="+" />
					</h3>

						<input class="col-md-12 col-xs-12" type="text" name="motive[]" placeholder="Motivo" required>

				</div><!--/motivo-->

				<div id="objetivodiv" class="col-md-6 col-xs-12 nopadding">
					<h3 class="tituloboton">Objetivos del Viaje
						<input type="button" class="nopadding boton-mas2 boton_objetivo pull-right" id="agregar_objetivo" onclick="add_objetivo();" value="+" />
					</h3>

					<input class="col-md-12 col-xs-12" type="text" name="objective[]" placeholder="Objetivos del Viaje" required>

				</div><!--/objetivodiv-->

				<div class="col-md-6 col-xs-12 nopadding">

					<h3 class="titulo">Total de Días</h3>
					<?php
						echo $this->Form->input('days_nosleep', ['type' => 'select', 'id' => 'sinpernoctar', 'label' => false, 'required' =>'required', 'class' => 'col-md-6 col-xs-12', 'options' => ['' => 'Sin Pernoctar']]);
						echo $this->Form->input('days_sleep', ['type' => 'number', 'id' => 'pernoctando', 'label' => false, 'required' =>'required', 'class' => 'col-md-6 col-xs-12', 'placeholder' => 'Pernoctando', 'ReadOnly' => 'true']);
					?>
				</div>

				<div class="col-md-6 col-xs-12 nopadding">
					<h3 class="titulo">Monto total del Viático</h3>
					<?= $this->Form->input('amount', ['type' => 'number', 'id' => 'monto', 'label' => false, 'required' =>'required', 'class' => 'col-md-12 col-xs-12', 'placeholder' => 'Monto']); ?>
				</div>

				<div class="col-md-12 col-xs-12 nopadding separado">
					<?= $this->Form->button("Ingresar Solicitud", ['class' => 'botongrande verde col-md-12 col-xs-12', 'id' => 'ingresar']) ?>
				</div>

		</div><!--/tabladatos-->
</div>
<div id="invi">
</div>


	</div>
	<?php echo $this->Form->end() ?>
	<script>
		var d = new Date();
	</script>
</div><!--Contenedor-->
<hr class="dashed">
<div id="impresora" class="hidden">
</div>


<script>
function add_ciudad(){
     var row = $("<input class='col-md-6 col-xs-12 borrar' type='text' name='cities[]' placeholder='Ciudad Visitada'>");
      $("#ciudades").append(row);
}

function add_lugar(){
     var row = $("<input class='col-md-6 col-xs-12 borrar' type='text' name='places[]' placeholder='Lugar Visitado'>");
      $("#lugaresvisitados").append(row);
}

function add_objetivo(){
     var row = $("<input class='col-md-12 borrar' type='text' name='objective[]' placeholder='Objetivos del Viaje'>");
      $("#objetivodiv").append(row);
}

function add_pais(){
     var row = $('<?= $this->Form->input("country_id", ["options" => $paises, "id" => "countries", "label" => false, "div" => false, "class" => "col-md-12 borrar"]); ?>');
      $("#paisregion").append(row);
}

function add_motivo(){
     var row = $('<input class="col-md-12 col-xs-12" type="text" name="motive[]" placeholder="Motivo" required>');
      $("#motivodiv").append(row);
}

</script>



<script>
jQuery(function($) {


  // /////
  // Botón X
  function tog(v){return v?'addClass':'removeClass';}
  $(document).on('input', '.borrar', function(){
    $('this')[tog(this.value)]('borrar');
  }).on('mousemove', '.borrar', function( e ){
    $('.borrar')[tog(this.offsetWidth-47 < e.clientX-this.getBoundingClientRect().left)]('onBorrar');
  }).on('touchstart click', '.onBorrar', function( ev ){
    $(this).remove(); x--;
  });


});
$(document).ready(function(){
	var diferencia = 0;
	$('body').on('focusout', '.dateField', function(){
		if($('.dateField[name="end_date"]').val() != '' && $('.dateField[name="start_date"]').val() != ''){
			var sd_parse = $('.dateField[name="start_date"]').val().split("/");
			var ed_parse = $('.dateField[name="end_date"]').val().split("/");
			var start_date = new Date(sd_parse[2], sd_parse[1], sd_parse[0]);
			var end_date = new Date(ed_parse[2], ed_parse[1], ed_parse[0]);
			var html = '<option value="">Sin Pernoctar</option>';
			diferencia = parseInt((end_date - start_date) / (1000 * 60 * 60 * 24)) + 1;
			for(var i = 1; i <= diferencia; i++){
				html += "<option value=" + i + ">" + i + "</option>";
			}
			$('#sinpernoctar').html(html);
			if (diferencia <= 2){
				$("#sinpernoctar option[value='2']").val("2").remove();
				$("#sinpernoctar").val("1").trigger("change");
			}
		}
	});
	$('body').on('change', '#sinpernoctar', function(e){
		var sin_pernoctar = $(this).val();
		var html = '<option value="">Pernoctando</option>';
		var pernoctando = diferencia - sin_pernoctar;
		var monto = (parseInt(sin_pernoctar) * 9000) + (parseInt(pernoctando) * 45000);
		if(sin_pernoctar == ""){
			$('#pernoctando').val(0);
			$('input[name="amount"]').val(0);
		}else{
			$('#pernoctando').val(pernoctando);
			$('input[name="amount"]').val(monto);
		}
	});

	$('body').on('submit', '.formViatico', function(e){
		e.preventDefault();
		var form = $(this);
		var url = form.attr('action');
		var data = form.serialize();
		$.post(url, data, function(res){
			var op = $.parseJSON(res);
			if(op.success){
				form[0].reset();
				var url_anexo = "<?= $this->Url->build(['controller' => 'TravelProvisions', 'action' => 'downloadAnexo']); ?>" + "/" + op.id_viatico;
				var url_informe = "<?= $this->Url->build(['controller' => 'TravelProvisions', 'action' => 'downloadInforme']); ?>" + "/" + op.id_viatico;
				console.log("La Url:", url);
				$("#invi").load(url_anexo)
				$("#invi").load(url_informe)
				setTimeout(function(){
					$("#invi").empty();
				}, 5000);
			}
		});
	});

});
/**Select Chile**/
$('#countries option[value="43"]').attr("selected", "selected");

</script>
