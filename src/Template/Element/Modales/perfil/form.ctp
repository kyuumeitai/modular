<?= $this->Form->create('Employee', [
		'class' => 'form-perfil',
		'url' => ['controller' => 'Employees', 'action' => 'editProfile']
]); ?>
	<h1 class="titulo">Mi Perfil <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
	<div class="col-md-12 contenedor wrapper">
		<div id="perfil1" class="perfilcont text-center">
			<br>
			<h1 class="grande textoverde"><?= $usuario['firstname']." ".$usuario['lastname'] ?></h1>        
			<h2> <?= $usuario['position'] ?> | <?= $usuario['section'] ?></h2>
			<h2>RUT: <?= $usuario['rut'] ?></h2>
		</div>
		<hr>
		<div id="perfil2" class="perfilcont text-center col-md-8 col-md-offset-2">
			<table class="table table-bordered"> 
				<?= $this->Form->input('id', [
					'type' => 'hidden', 
					'value' => $usuario['id'], 
					'label' => false, 
					'id' => 'id'
				]); ?>
				<tr>
					<th class="tdperfil col-md-3"> Correo:</th>
					<td class="col-md-9"> 
						<?= $this->Form->input('email', [
							'value' => $usuario['email'],
							'type' => 'email', 
							'label' => false,
							'id' => 'email',
							'required' => true,
							'div' => false,
							'placeholder' => 'Email',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
				<tr>
					<th class="tdperfil"> Dirección:</th>
					<td>
						<?= $this->Form->input('address', [
							'value' => $usuario['address'], 
							'label' => false, 
							'id' => 'address', 
							'required' => true, 
							'div' => false, 
							'placeholder' => 'Dirección',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
				<tr>
					<th class="tdperfil"> Teléfono:</th>
					<td>
						<?= $this->Form->input('telephone', [
							'type' => 'tel',
							'value' => $usuario['telephone'], 
							'label' => false, 
							'id' => 'telephone', 
							'required' => true, 
							'div' => false, 
							'placeholder' => 'Teléfono',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
			</table>
			<table class="table table-bordered">
				<tr>
					<th class="tdperfil col-md-3"> Teléfono para Emergencias:</th>
					<td class="col-md-9">
						<?= $this->Form->input('emergency_tel', [
							'value' => $usuario['emergency_tel'], 
							'label' => false, 
							'id' => 'emergency_tel', 
							'required' => true, 
							'div' => false, 
							'placeholder' => 'Teléfono de emergencia',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
				<tr>
					<th class="tdperfil"> Contacto Emergencias:</th>
					<td>
						<?= $this->Form->input('emergency_contact', [
							'value' => $usuario['emergency_contact'], 
							'label' => false, 
							'id' => 'emergency_contact', 
							'required' => true, 
							'div' => false, 
							'placeholder' => 'Contacto de emergencias',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
				<tr>
					<th class="tdperfil"> Relación con el contacto:</th>
					<td>
						<?= $this->Form->input('emergency_relation', [
							'value' => $usuario['emergency_relation'], 
							'label' => false, 
							'id' => 'emergency_relation', 
							'required' => true, 
							'div' => false, 
							'placeholder' => 'Relación con el contacto',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
			</table>
			<table class="table table-bordered">
				<?= $this->Form->submit('Editar Perfil', ['class' => 'botongrande verde col-md-8 col-xs-12 col-md-offset-2', 'div' => false]); ?>
			</table>
			<table class="table table-bordered">
				<div id="ProfileSuccessMsg" class="alert alert-success" style="display: none;">
					<p>Perfil actualizado correctamente</p>
				</div>
			</table>
		</div>
	</div>
<?= $this->Form->end(); ?>