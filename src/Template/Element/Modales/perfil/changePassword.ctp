<?= $this->Form->create('Employee', [
		'class' => 'form-password',
		'url' => ['controller' => 'Employees', 'action' => 'changePassword']
]); ?>
	<h1 class="titulo">Mi Perfil <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
	<div class="col-md-12 contenedor wrapper">
		<div id="perfil1" class="perfilcont text-center">
			<br>
			<h1 class="grande textoverde"><?= $usuario['firstname']." ".$usuario['lastname'] ?></h1>
			<h2> <?= $usuario['position'] ?> | <?= $usuario['section'] ?></h2>
			<h2>RUT: <?= $usuario['rut'] ?></h2>
		</div>
		<hr>
		<div id="perfil2" class="perfilcont text-center col-md-8 col-md-offset-2">
			<table class="table table-bordered">
			<?= $this->Form->input('id', [
				'type' => 'hidden',
				'value' => $usuario['id'],
				'label' => false,
				'id' => 'id'
			]); ?>
			<h1 class="titulo"> Cambiar Contraseña:</h1>
				<tr>
					<th> Contraseña Actual </th>
					<td>
						<?= $this->Form->input('current_password', [
							'type' => 'password',
							'label' => false,
							'id' => 'current_password',
							'required' => true,
							'div' => false,
							'placeholder' => 'Contraseña actual',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
				<tr>
					<th>Contraseña Nueva </th>
					<td>
						<?= $this->Form->input('new_password', [
							'type' => 'password',
							'label' => false,
							'id' => 'new_password',
							'required' => true,
							'div' => false,
							'placeholder' => 'Contraseña nueva',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
				<tr>
					<th> Confirme Contraseña Nueva </th>
					<td>
						<?= $this->Form->input('new_password2', [
							'type' => 'password',
							'label' => false,
							'id' => 'new_password2',
							'required' => true,
							'div' => false,
							'placeholder' => 'Confirme contraseña nueva',
							'class' => 'text-center'
						]); ?>
					</td>
				</tr>
			</table>
			<table class="table table-bordered">
				<?= $this->Form->submit('Cambiar contraseña', ['class' => 'botongrande verde col-md-8 col-xs-12 col-md-offset-2', 'div' => false]); ?>
			</table>
			<div id="MensajeSubmit" role="alert" style="display:none;">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<p></p>
			</div>
		</div>
	</div>
<?= $this->Form->end(); ?>
<script>
$(document).ready(function(){
	$('body').on('submit', '.form-password', function(e){
		e.preventDefault();
		var url = $(this).attr("action");
		var data = $(this).serialize();
		$.post(url, data, function(res){
			res = $.parseJSON(res);
			if(res.exito){
				$('#MensajeSubmit').removeClass().addClass('alert alert-dismissible alert-success');
			}else{
				$('#MensajeSubmit').removeClass().addClass('alert alert-dismissible alert-warning');
			}
			$('#MensajeSubmit p').text(res.msg);
			$('#MensajeSubmit').show(200, function(){
				setTimeout(function(){
					$('#MensajeSubmit').hide(200);
				}, 5000)
			});
		});
	});
})
</script>
