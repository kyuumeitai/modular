<div id="container" class="rendicion nopadding nomargin">
	<h1 class="titulo"> Vista de Rendición <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
	<div id="imprimir" class="col-md-12 col-xs-12 wrapper nomargin">
		<div class="col-md-12 table-responsive vista">

				<div id="cabeceravista" class="col-md-12">

						<div id="logovista" class="col-md-2"></div>
						
						<div id="titulovista" class="text-center col-md-8">
							Solicitud de
							<br>
							Fondos Por Rendir
						</div>
						
						<div id="codigovista" class="text-right col-md-2 pull-right">
							<table>
								<tr>
									<td>Código:</td>
									<td class="text-right">R1-ADQ</td>
								</tr>
								<tr>
									<td>Versión:</td>
									<td class="text-right">1</td>
								</tr>
								<tr>
									<td>Fecha:</td>
									<td class="text-right"> <?php echo date("d/m/y"); ?> </td>
								</tr>
							</table>
						</div>
				</div>





				<div class="col-md-3 nopadding">
					<table class="table table-bordered">
						<thead>
							<th>Nombre</th>					
						</thead>
						<tbody>
							<td id="nombre_v"><?= $release->name ?></td>
						</tbody>
						</table>
				</div>

				<div class="col-md-2 nopadding">
						<table class="table table-bordered">

						<thead>
							<th>Fecha</th>
						</thead>
						
						<tbody>
							<td id="fecha_rendicion_v"><?= date('d/m/Y', strtotime($release->date)) ?></td>
						</tbody>
						</table>
				</div>

				<div class="col-md-2 nopadding">
					<table class="table table-bordered">
						<thead>
							<th>Centro de Costos</th>
						</thead>
					
						<tbody>
							<td id="centro_v"></td>
						</tbody>
					</table>
				</div>
					
				<div class="col-md-2 nopadding">
					<table class="table table-bordered">

						<thead>
							<th>Cantidad Solicitada</th>
							
						</thead>
						
						<tbody>
							<td id="cantidad_v">$<?= number_format($release->amount, 0, ',', '.') ?></td>
							
						</tbody>
					</table>
				</div>
			

				<div class="col-md-3 nopadding">
					<table class="table table-bordered">
						<thead>
							<th>Fecha de Devolución Estimada</th>
						</thead>
					
						<tbody>
							<td id="fecha_devolucion_v"><?= date('d/m/Y', strtotime($release->estimated_release)) ?></td>
						</tbody>
					</table>
				</div>


	<table class="table table-bordered">
			

				<table class="table table-bordered">
					<thead>
						<th>Justificación Fondo</th>
					</thead>
					<tbody>
						<td id="justificacion_v"><?= $release->justification ?></td>
					</tbody>
				</table>
				<p class="text-center">
					Esta solicitud se considerará válida sólo si se envía al correo electrónico compras@ceaf.cl, con copia al correo del líder del Centro de Costos al que se cargará.
				</p>

				<p class="text-center hidden-md">
				<br>
					Las proporciones visuales han sido alteradas para optimizar la vista en su dispositivo, el archivo PDF final es distinto.
				</p>




			<hr class="espaciador">
			<input type="button" class="botongrande verde col-md-5 col-xs-12" id="enviarcorreo" onclick="" value="Enviar por Correo" />

			<input type="button" class="botongrande verde col-md-5 col-xs-12 pull-right" id="descargar" onclick="" value="Descargar" />

		</div>

	</div><!--/tabladatos .Wrapper-->
</div><!--/datos-->

<div id="invi"></div>

<script>
	$("#descargar").click(function() {
		$("#invi").load("<?=$this->Url->build(['controller' => 'modales', 'action' => 'loadViewRendicionPdf', $release->id])?>")

	});

	$("#enviarcorreo").click(function() {
		$("#invi").load("mod_rendiciones/rendicion_enviar.php")

	});

</script>

</body>
</html>