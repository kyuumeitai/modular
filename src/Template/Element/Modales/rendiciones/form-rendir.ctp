<?php echo $this->Html->script('/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min'); ?>
<script>
	var d = new Date();
  var detail_row = 0;
	var month = d.getMonth()+1;
	var day = d.getDate();
	var output = ((''+day).length<2 ? '0' : '') + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' + d.getFullYear();
	var items = [<?=json_encode($items)?>];
	var itemsHtml = "";
	var docsHtml = "";
	var subitems = [<?=json_encode($subitems)?>];
	$("#hoy").text(output)
	
	$("textarea").removeAttr('cols');
  $("textarea").width('100%');
  $.each(items[0], function(key, value){
		itemsHtml += '<option value="' + key + '">' + value + '</option>';
	});

  $('body').on('change', '.item-selector', function(){
    var elitem = $(this).val();
    var lalinea = $(this).data('linea');
    var lositems = subitems[0][elitem];
    var elhtml = '<select name="detalle[' + lalinea + '][subitem]" id="Detalle' + lalinea + 'subitem" required="required" class="item-input subitem-selector" data-linea="' + lalinea + '">';
    $.each(lositems, function(key, value){
      elhtml += '<option value="' + key + '">' + value + '</option>';
    });
    elhtml += '</option>';
    $('select[name="detalle[' + lalinea + '][subitem]"]').html(elhtml);
  });

  $( ".fechadoc").datepicker({
    inline: true
  });
  $('body').on('submit', '.formRendir', function(e){
    var form = $(this);
    var url = form.attr('action');
    var data = form.serialize();
    $.post(url, data, function(res){
      var op = $.parseJSON(res);
      if(op.success){
        form[0].reset();
      }
      $(".alert .msg").text(op.msg);
      $("#alert-rendicion").show(0, function(){
        setTimeout(function(){
          $("#alert-permiso").hide();
        }, 6000);
      });
    });
    e.preventDefault();
  });

  var options = {
    url: window.location.pathname + "providers.json",
    categories: [
      {
        listLocation: "providers",
        header: "Proveedores"
      }
    ],
    getValue: function(element) {
      return element.rut + " - " + element.names;
    },

    list: {
      onChooseEvent : function(){
        var elegido = $("#Detalle0rutp").getSelectedItemData();
        $("#Detalle0ProviderId").val(elegido.id);
        $("#Detalle0rutp").val(elegido.rut);
        $("#Detalle0nombrep").val(elegido.names);
      }
    }
  };
  $("#Detalle0rutp").easyAutocomplete(options);
  $( "#Detalle0fechadoc").datepicker({
	inline: true
});
  function add_item_rendiciondos(){
  		detail_row++;
  		var table = document.getElementById("tablarendirfondos");
  		var row = table.insertRow(-1);
  		var cell1 = row.insertCell(0);
  		var cell2 = row.insertCell(1);
  		var cell3 = row.insertCell(2);
  		var cell4 = row.insertCell(3);
  		var cell5 = row.insertCell(4);
  		var cell6 = row.insertCell(5);
  		var cell7 = row.insertCell(6);
  		var cell8 = row.insertCell(7);
  		var cell9 = row.insertCell(8);
  		var cell10 = row.insertCell(9);
  		var cell11 = row.insertCell(10);

  		cell1.innerHTML = "<select name='detalle[" + detail_row + "][item]' id='Detalle" + detail_row + "item' class='item-input item-selector' data-linea='" + detail_row  + "'>" + itemsHtml + "</select>";
  		cell2.innerHTML = "<select id='subitems' name='detalle[" + detail_row + "][subitem]' id='Detalle" + detail_row + "subitems' class='item-input  ' data-linea='" + detail_row  + "'><option value='valor1' selected>SubItem 1</option><option value='2'>Subitem2</option></select>";
  		cell3.innerHTML = "<td id='centrodecostos'><select  name='detalle[" + detail_row + "][cc_id]' id='Detalle" + detail_row + "centrodecostos' class='item-input  ' data-linea='" + detail_row  + "'><option value='1'> Gestión </option><option value='2'> Mejoramiento Genético </option><option value='3'> Genómica </option><option value='4'> Fisiología del Estrés </option><option value='5'> Agronomía </option></select></td>";
  		cell4.innerHTML = "<td id='fechadoc> <input type='text" + detail_row + "'> <input type='text' name='detalle[" + detail_row + "][document_date]' id='Detalle" + detail_row + "fechadoc' class='item-input fechadoc' data-linea='" + detail_row  + "'</td>";
  		cell5.innerHTML = "<td id='ndoc'><input type='number' name='detalle[" + detail_row + "][document_id]' id='Detalle" + detail_row + "ndoc' class='item-input  ' data-linea='" + detail_row  + "'></td>";
  		cell6.innerHTML = '<td id="tipodoc"><select name="detalle[0][document_type]" id="Detalle' + detail_row + 'tipodoc" required="required" class="item-input" data-linea="0"><option value="factura">Factura</option><option value="factura-elec">Factura Electrónica</option><option value="factura-exec">Factura Exenta</option><option value="factura-exec-elec">Factura Exenta Electrónica</option><option value="bho">BHO</option><option value="bho-3ro">BHO 3ros</option><option value="boleta">Boleta</option><option value="boleta-elec">Boleta Electr</option><option value="comprobante">Comprobante</option><option value="anexo-3">Anexo 3</option><option value="solicitud">Solicitud</option><option value="pas-peaj-ticket">Pasajes/Peajes/Tickets</option></select></td>';
  		cell7.innerHTML = "<td id='total'> <input type='number' name='detalle[" + detail_row + "][amount]' id='Detalle" + detail_row + "total' class='item-input  ' data-linea='" + detail_row  + "'> </td>";
  		cell8.innerHTML = "<td id='rutp'> <input type='text' name='detalle[" + detail_row + "][provider_rut]' id='Detalle" + detail_row + "rutp' class='item-input  ' data-linea='" + detail_row  + "'><input type='hidden' name='detalle["+ detail_row +"][provider_id]' id='Detalle"+ detail_row +"ProviderId' value=''></td>";
  		cell9.innerHTML = "<td id='nombrep'> <input type='text' name='detalle[" + detail_row + "][provider_name]' id='Detalle" + detail_row + "nombrep' class='item-input  ' data-linea='" + detail_row  + "'></td>";
  		cell10.innerHTML = "<td id='detalle'> <input type='text' name='detalle[" + detail_row + "][detail]' id='Detalle" + detail_row + "detalle' class='item-input  ' data-linea='" + detail_row  + "'></td>";
  	  cell11.innerHTML = "<td id='eliminar'><button class='borraroc' onclick='del_item(this)'></button></td>";
      var options = {
        url: window.location.pathname + "providers.json",
        categories: [
          {
            listLocation: "providers",
            header: "Proveedores"
          }
        ],
        getValue: function(element) {
          return element.rut + " - " + element.names;
        },

        list: {
          onChooseEvent : function(){
            var elegido = $("#Detalle" + detail_row + "rutp").getSelectedItemData();
            $("#Detalle" + detail_row + "ProviderId").val(elegido.id);
            $("#Detalle" + detail_row + "rutp").val(elegido.rut);
            $("#Detalle" + detail_row + "nombrep").val(elegido.names);
          }
        }
      };

		$("#Detalle" + detail_row + "rutp").easyAutocomplete(options);
		$( "#Detalle" + detail_row + "fechadoc").datepicker({
			inline: true
		});
  }
  function del_item(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tablarendirfondos").deleteRow(i);
  }
</script>
<div id="container" class="rendicion">
	<h1 class="titulo"> Rendir Fondos
		<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button>
	</h1>
	<div class="col-md-12 col-xs-12 wrapper">
		<div id="alert-rendicion" class="alert alert-info" style="display:none;"><span class="msg"></span></div>
		<h1 class="titulo2 gris separado"> Items
			<input type="button" class="nopadding boton-mas2 pull-right" id="agregar_item" onclick="add_item_rendiciondos();" value="+" />
		</h1>
		<?php
			echo $this->Form->create('TravelProvision', [
				'class' => 'formRendir',
				'url' => ['controller' => 'Outcomes', 'action' => 'saveRendicion']
			]);
		?>
		<div id="rendicion" class="col-md-12">
			<table id="tablarendirfondos" class="table table-bordered">
				<thead class="textocentro">
					<th>Ítem</th>
					<th>Subítem</th>
					<th>Centro de Costos</th>
					<th>Fecha Doc</th>
					<th>Nº Doc</th>
					<th>Tipo Documento </th>
					<th> Total </th>
					<th> Rut Proveedor</th>
					<th> Nombre Proveedor</th>
					<th> Detalle</th>
					<th id="eliminar"></th>
				</thead>
				<tbody>
					<tr>
						<td id="compra_cantidad_0">
							<?= $this->Form->input('detalle[0][item]', ['type' => 'select', 'options' => $items, 'id' => 'Detalle0Item', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input item-selector', 'data-linea' => 0]); ?>
						</td>
						<td id="subitem" >
							<?= $this->Form->input('detalle[0][subitem]', ['type' => 'select', 'options' => ['Debe seleccionar un item'], 'id' => 'Detalle0subitem', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input subitem-selector', 'data-linea' => 0]); ?>
						</td>
						<td id="centrodecostos">
							<select name="detalle[0][cc_id]" id="Detalle0ccostos" class="item-input  " data-linea="0" required  >
								<option value="1"> Gestión </option>
								<option value="2"> Mejoramiento Genético </option>
								<option value="3"> Genómica </option>
								<option value="4"> Fisiología del Estrés </option>
								<option value="5"> Agronomía </option>
							</select>
						</td>
						<td id="fecha">
							<?= $this->Form->input('detalle[0][document_date]', ['type' => 'text', 'id' => 'Detalle0fechadoc', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input fechadoc', 'data-linea' => 0]); ?>
						</td>
						<td id="ndoc">
							<?= $this->Form->input('detalle[0][document_id]', ['type' => 'number', 'id' => 'Detalle0ndoc', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input', 'data-linea' => 0]); ?>
						</td>
						<td id="tipodoc">
							<?= $this->Form->input('detalle[0][document_type]', ['type' => 'select', 'options' => $documentos, 'id' => 'Detalle0tipodoc', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input', 'data-linea' => 0]); ?>
						</td>
						<td id="total">
							<?= $this->Form->input('detalle[0][amount]', ['type' => 'number', 'id' => 'Detalle0total', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input', 'data-linea' => 0]); ?>
						</td>
						<td id="rutp">
							<?= $this->Form->input('detalle[0][provider_rut]', ['type' => 'text', 'id' => 'Detalle0rutp', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input', 'data-linea' => 0]); ?>
							<?= $this->Form->input('detalle[0][provider_id]', ['type' => 'hidden', 'value' => '', 'id' => 'Detalle0ProviderId']); ?>
						</td>
						<td id="nombrep">
							<?= $this->Form->input('detalle[0][provider_name]', ['type' => 'text', 'id' => 'Detalle0nombrep', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input', 'data-linea' => 0]); ?>
						</td>
						<td id="detalle">
							<?= $this->Form->input('detalle[0][detail]', ['type' => 'text', 'id' => 'Detalle0detalle', 'label' => false, 'div' => false, 'required' =>'required', 'class' => 'item-input', 'data-linea' => 0]); ?>
						</td>
						<td></td>
					</tr>
				</tbody>
			</table>
			<hr class="espaciador">
			<?= $this->Form->button("Guardar", ['class' => 'botongrande verde col-md-12 col-xs-12']) ?>
		</div><!--/tabladatos .Wrapper-->
		<?= $this->Form->end() ?>
	</div><!--/datos-->
</div>
