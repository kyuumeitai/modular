<div id="container" class="rendicion nopadding nomargin">
	<h1 class="titulo"> Solicitud de Fondos por Rendir <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h3>
	<div class="col-md-12 col-xs-12 wrapper nomargin">
		<div id="forma" class="col-md-12 table-responsive ">
			<script>
				$("#fecha_rendicion").datepicker({
					inline : true,
					dateFormat : 'dd/mm/yy'
				});
				$("#fecha_devolucion").datepicker({
					inline : true,
					dateFormat : 'dd/mm/yy'
				});
			</script>
			<?=
				$this->Form->create('OutcomeRelease', [
						'class' => 'formRendicion',
						'url' => ['controller' => 'Outcomes', 'action' => 'outcomeRSave']
				]);
			?>


					<div class="col-md-4 nopadding">
					<table class="table table-bordered">
						<thead>
							<th>Nombre</th>
						</thead>
						<tbody>
						<td>
							<?= $this->Form->input('name', ['label' => false, 'id' => 'nombre', 'required' => true, 'div' => false, 'readonly' => true, 'value' => $user['firstname']." ".$user['lastname']]); ?>
						</td>
						</tbody>
						</table>
					</div>

					<div class="col-md-4 nopadding">
						<table class="table table-bordered">

						<thead>
							<th>Fecha</th>
						</thead>

						<tbody>
							<td>
								<?= $this->Form->input('date', ['label' => false, 'id' => 'fecha_rendicion', 'required' => true, 'div' => false]); ?>
							</td>
						</tbody>
						</table>
					</div>

					<div class="col-md-4 nopadding">
					<table class="table table-bordered">
						<thead>
							<th>Centro de Costos</th>
						</thead>

						<tbody>
							<td>
								<?= $this->Form->input('cost_center_id', ['label' => false, 'id' => 'centro', 'options' => $cc, 'required' => true, 'div' => false]); ?>
							</td>
						</tbody>
					</table>
					</div>


				<table class="table table-bordered">
					<thead>
						<th>Cantidad Solicitada</th>
						<th>Fecha de Devolución Estimada</th>
					</thead>
					<tbody>
						<td>
							<?= $this->Form->input('amount', ['label' => false, 'id' => 'cantidad', 'required' => true, 'div' => false]); ?>
						</td>
						<td>
							<?= $this->Form->input('estimated_release', ['label' => false, 'id' => 'fecha_devolucion', 'required' => true, 'div' => false]); ?>
						</td>
					</tbody>
				</table>

				<table class="table table-bordered">
					<thead>
						<th>Justificación Fondo</th>
					</thead>
					<tbody>
						<td>
							<?= $this->Form->input('justification', ['type'=>'textarea', 'label' => false, 'id' => 'justificacion', 'required' => true, 'div' => false]); ?>
						</td>
					</tbody>
				</table>
				<p class="text-center">
					Esta solicitud se considerará válida sólo si se envía al correo electrónico compras@ceaf.cl, con copia al correo del líder del Centro de Costos al que se cargará.
				</p>
				<hr class="espaciador">
				<?= $this->Form->submit('Siguiente', ['class' => 'botongrande verde col-md-12 col-xs-12', 'div' => false]); ?>
				<?= $this->Form->end(); ?>
		</div>
	</div>
</div>
