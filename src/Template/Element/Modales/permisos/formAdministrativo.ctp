<title>Rendiciones</title>
<h1 class="titulo">Permiso Administrativo <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor nopadding nomargin">
	<script>
		
	</script>
    <div class="col-md-12 wrapper">
    			<div id="alert-permiso" class="alert alert-info" style="display:none;">
					<h2 class="msg"></h2>
				</div>

				
			<div class="" style="width:100%">
				<?= $this->Form->create('OutcomeRelease', [
						'class' => 'form-administrativo',
						'url' => ['controller' => 'OffDays', 'action' => 'checkAdminDays']
				]); ?>
				<table class="table">
					<tr class="col-md-12">
						<div class="seleccionmodulos centrar"><?= $this->Form->input('type', ['label' => false, 'type' => 'radio', 'div' => false, 'options' => ['MEDIO-ADMIN' => 'Medio dia', 'ADMINISTRATIVO' => 'Dia completo']]); ?></div>
					
					<tr class="col-md-4 col-md-offset-4">
						<td><label for="fechaadmin" class="col-md-6">Fecha</label></td>
						<td><?= $this->Form->input('off_date', ['label' => false, 'id' => 'cantidad', 'class' => 'col-md-6 date-input', 'required' => true, 'div' => false]); ?></td>
					</tr>
	            </table>
				<hr class="espaciador2">
				<?= $this->Form->submit('Ingresar Solicitud', ['class' => 'botongrande verde col-xs-12 col-md-12', 'div' => false]); ?>
				<hr class="espaciador2">
	
				<?= $this->Form->end(); ?>
			</div>
			
    </div>
    <script>
	    $(document).ready(function() {
	    	var hoy = new Date();
	    	$( ".date-input" ).datepicker({
				inline: true,
				dateFormat : "dd/mm/yy",
				minDate : hoy
			});
	        $('body').on('submit', '.form-administrativo', function(e){
	        	var form = $(this);
				var url = form.attr('action');
				var data = form.serialize();
				$.post(url, data, function(res){
					var op = $.parseJSON(res);
					if(op.success){
						form[0].reset();
					}
					$(".alert .msg").text(op.msg);
					$("#alert-permiso").show(0, function(){
						setTimeout(function(){
							$("#alert-permiso").hide();
						}, 6000);
					});
				});
				e.preventDefault();
	        });
    	});
    </script>

    <script>

        $('input#type-medio-admin[type="radio"]').click(function(){
               	$('.checked').removeClass("checked");
           		$(this).closest('label').addClass("checked");
        });
  
          $('input#type-administrativo[type="radio"]').click(function(){
               	$('.checked').removeClass("checked");
           		$(this).closest('label').addClass("checked");
        });

</script>


</div> <!--contenedor-->