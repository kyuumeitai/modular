<title>Rendiciones</title>
<h1 class="titulo">Ausencias<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor col-md-12 nopadding">  
    <div class="col-md-12 col-xs-12 nomargin wrapper">
        <div class="seleccionmodulos3">
            <?= $this->Html->link('<div class="permisoadmin"> </div>', [ 'controller' => 'modales', 'action' => 'loadFormAdministrativo'],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#permisoadmin']); ?>
            <?= $this->Html->link('<div class="permisohoras"> </div>', [ 'controller' => 'modales', 'action' => 'loadFormHoras'],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#permisohoras']); ?>
            <?= $this->Html->link('<div class="feriadolegal "> </div>', [ 'controller' => 'modales', 'action' => 'loadFormVacaciones'],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#feriadolegal']); ?>
        </div>
    </div>
</div> <!--contenedor-->

