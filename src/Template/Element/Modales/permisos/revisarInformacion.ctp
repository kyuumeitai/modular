<title>Datos</title>
	<h1 class="titulo">Revisar mi Información<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
	<div class="contenedor nopadding nomargin">  
	<div class="container">
				<div class="col-xs-12 col-md-4 ">
                <hr>

					<h2 class="modularh2 text-center">Días Administrativos</h2>
					<hr class="espaciador">

					<canvas id="chartdiasadmin"></canvas>
					<div id="leyendadiasadmin"></div>

                    
                <hr class="espaciador">

                     <?= $this->Html->link('Solicitar', [ 'controller' => 'modales', 'action' => 'loadFormAdministrativo'],['class' => 'botongrande verde col-md-12 col-xs-12 text-center', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#permisoadmin']); ?>


				</div>

               
				
				<div class="col-xs-12 col-md-4">
                <hr>
				<h2 class="modularh2 text-center"> Permisos por hora (Mes Actual)</h2>
				<hr class="espaciador">

					<canvas id="chartpermisoshora"></canvas>
					<div id="leyendapermisoshora"></div>

                    
                <hr class="espaciador">

                      <?= $this->Html->link('Solicitar', [ 'controller' => 'modales', 'action' => 'loadFormHoras'],['class' => 'botongrande verde col-md-12 col-xs-12 text-center', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#permisohoras']); ?>


				</div>


				<div class="col-xs-12 col-md-4">
                <hr>
					<h2 class="modularh2 text-center">Feriado Legal</h2>
					<hr class="espaciador">

					<canvas id="chartferiadolegal"></canvas>
					<div id="leyendaferiadolegal"></div>

                    
                <hr class="espaciador">
                    <?= $this->Html->link('Solicitar', [ 'controller' => 'modales', 'action' => 'loadFormVacaciones'],['class' => 'botongrande verde col-md-12 col-xs-12 text-center', 'escape' => false, 'data-toggle' => 'modal', 'data-target' => '#feriadolegal']); ?>


				</div>

                                


				

                
                




		<hr class="espaciador2">
	</div>


<script>

var datadiasadmin = {
    labels: [
        "Disponibles",
        "Ocupados",
    ],
    datasets: [
        {
            data: [<?= $permisos['administrativos']['disponibles'] ?>, <?= $permisos['administrativos']['usados'] ?>],
            backgroundColor: [
                "#0bc2ce",
                "#acacac",
            ],
            hoverBackgroundColor: [
                "#4ddae3",
                "#bcbcbc",
            ]
        }]
};

var datapermisoshora = {
    labels: [
        "Disponibles",
        "Ocupados",

    ],
    datasets: [
        {
            data: [<?= $permisos['porHoras']['disponibles'] ?>, <?= $permisos['porHoras']['usados'] ?>],
            backgroundColor: [
                "#96d343",
                "#acacac",

            ],
            hoverBackgroundColor: [
                "#a1e148",
                "#bcbcbc",

            ]
        }]
};

var dataferiadolegal = {
    labels: [
        "Disponibles",
        "Ocupados",
    ],
    datasets: [
        {
            data: [<?= $permisos['legales']['disponibles'] ?>, <?= $permisos['legales']['usados'] ?>],
            backgroundColor: [
                "#d31b65",
                "#acacac"
            ],
            hoverBackgroundColor: [
                "#f03b84",
                "#ababab",
            ]
        }]
};
</script>


</div> <!--contenedor-->