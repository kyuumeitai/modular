<title>Rendiciones</title>
	<h1 class="titulo">Feriado Legal<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
	<div class="contenedor nopadding nomargin">  
	<hr class="espaciador2">
    <div class="col-md-12 wrapper">
	    	<?= $this->Form->create('OutcomeRelease', [
					'class' => 'form-legal',
					'url' => ['controller' => 'OffDays', 'action' => 'checkVacationDays']
			]); ?>
            <div class="col-md-4 col-md-offset-4">
	            <h2> Usted dispone de <?= $diasLegales ?> días de feriado para el periodo <?= date('Y') ?></h2>
	            <table class="table">
	                 <tr class="">
	                    <td><label for="desdefe" class="col-md-6">Desde</label></td>
	                    <td><?= $this->Form->input('start_date', ['label' => false, 'id' => 'fechaInicio', 'class' => 'col-md-6 date-input', 'required' => true, 'div' => false]); ?></td>
	                </tr>
	                <br/>
	                <tr class="">
	            	    <td><label for="desdefe" class="col-md-6">Hasta</label></td>
	                    <td><?= $this->Form->input('end_date', ['label' => false, 'id' => 'fechaFin', 'class' => 'col-md-6 date-input', 'required' => true, 'div' => false]); ?></td>
	                </tr>
	            </table>
			</div>
			<div id="alert-permiso" class="alert alert-info" style="display:none;">
				<h2 class="msg"></h2>
			</div>
			<hr class="espaciador2">
			<?= $this->Form->submit('Ingresar Solicitud', ['class' => 'botongrande verde col-md-8 col-xs-12 col-md-offset-2', 'div' => false]); ?>
           	<?= $this->Form->end(); ?>
    </div>
    <script>
	    $(document).ready(function() {
	    	var hoy = new Date();
	    	$( ".date-input" ).datepicker({
				inline: true,
				dateFormat : "dd/mm/yy",
				minDate : hoy
			});
	        $('body').on('submit', '.form-legal', function(e){
	        	var form = $(this);
				var url = form.attr('action');
				var data = form.serialize();
				$.post(url, data, function(res){
					var op = $.parseJSON(res);
					if(op.success){
						form[0].reset();
					}
					$(".alert .msg").text(op.msg);
					$("#alert-permiso").show(0, function(){
						setTimeout(function(){
							$("#alert-permiso").hide();
						}, 6000);
					});
				});
				e.preventDefault();
	        });
	    });
    </script>
    
</div> <!--contenedor-->