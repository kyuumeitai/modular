	<title>Rendiciones</title>
	<h1 class="titulo">Permiso por horas<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
	<div class="contenedor nopadding nomargin">
		<?= $this->Form->create('OutcomeRelease', [
				'class' => 'form-hours',
				'url' => ['controller' => 'OffDays', 'action' => 'checkHourOffs']
		]); ?>
		<div class="col-md-12 wrapper">
			<div class="col-md-4 col-md-offset-4 ">
				<table class="table">
					<tr class="">
						<td><label for="fechapermisohoras" class="col-md-6">Fecha</label></td>
						<td><?= $this->Form->input('off_date', ['label' => false, 'id' => 'fechaPermiso', 'class' => 'col-md-6 date-input', 'required' => true, 'div' => false]); ?></td>
					</tr>
					<br/>
					<tr class="">
						<td><label for="desde" class="col-md-6"> Desde</label></td>
						<td><?= $this->Form->input('hour_desde', ['label' => false, 'id' => 'desde', 'class' => 'col-md-6', 'required' => true, 'div' => false, 'type' => 'time']); ?></td>
					</tr>
					<br/>
					<tr class="">
						<td><label for="hasta" class="col-md-6"> Hasta</label></td>
						<td><?= $this->Form->input('hour_hasta', ['label' => false, 'id' => 'hasta', 'class' => 'col-md-6', 'required' => true, 'div' => false, 'type' => 'time']); ?></td>
					</tr>
				</table>
	        </div>
	        <hr class="espaciador2">
	        <div id="alert-permiso" class="alert alert-info" style="display:none;">
				<h2 class="msg"></h2>
			</div>
			<hr class="espaciador2">
	        <?= $this->Form->submit('Ingresar Solicitud', ['class' => 'botongrande verde col-md-8 col-xs-12 col-md-offset-2', 'div' => false]); ?>
	       	<?= $this->Form->end(); ?>
		</div>
	    <script>
		    $(document).ready(function() {
		    	var hoy = new Date();
		    	$( ".date-input" ).datepicker({
					inline: true,
					dateFormat : "dd/mm/yy",
					minDate : hoy
				});
				
				$('body').on('submit', '.form-hours', function(e){
	        	var form = $(this);
				var url = form.attr('action');
				var data = form.serialize();
				$.post(url, data, function(res){
					var op = $.parseJSON(res);
					if(op.success){
						form[0].reset();
					}
					$(".alert .msg").text(op.msg);
					$("#alert-permiso").show(0, function(){
						setTimeout(function(){
							$("#alert-permiso").hide();
						}, 6000);
					});
				});
				e.preventDefault();
	        });
		    });
	    </script>
	</div> <!--contenedor-->