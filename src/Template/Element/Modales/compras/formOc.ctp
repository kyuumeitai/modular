<?php echo $this->Html->script('/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min'); ?>
<script>
	var detail_row = 0;
	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var output = ((''+day).length<2 ? '0' : '') + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' + d.getFullYear();
	$("#hoy").text(output);
	$( "#fechaoc" ).datepicker({
		inline: true
	});
	function add_item_oc(){
		detail_row++;
	    var table = document.getElementById("tabla_oc");
	    var row = table.insertRow(-1);
	    var cell1 = row.insertCell(0);
	    var cell2 = row.insertCell(1);
	    var cell3 = row.insertCell(2);
	    var cell4 = row.insertCell(3);
	    var cell5 = row.insertCell(4);
	    var cell6 = row.insertCell(5);
	    var cell7 = row.insertCell(6); 
		cell1.innerHTML = '<td id="compra_cantidad_'+ detail_row +'"><input type="number" name="detalle[' + detail_row + '][quantity]" id="Detalle' + detail_row + 'Quantity" class="item-input quantity-item" data-linea="' + detail_row  + '"></td>';
		cell2.innerHTML = '<td id="compra_item_'+ detail_row +'"><input type="text" name="detalle[' + detail_row + '][item]" id="Detalle' + detail_row + 'Item" class="item-input" data-linea="' + detail_row  + '"></td>';
		cell3.innerHTML = '<td id="compra_detalle_'+ detail_row +'"><input type="text" name="detalle[' + detail_row + '][detail]" id="Detalle' + detail_row + 'Detail" class="item-input" data-linea="' + detail_row  + '"></td>';
		cell4.innerHTML = '<td id="compra_codigo_'+ detail_row +'"><input type="text" name="detalle[' + detail_row + '][product_code]" id="Detalle' + detail_row + 'ProdCode" class="item-input" data-linea="' + detail_row  + '"></td>';
		cell5.innerHTML = '<td id="compra_unitario_'+ detail_row +'"><input type="number" name="detalle[' + detail_row + '][unit_price]" id="Detalle' + detail_row + 'UnitPrice" class="item-input unit-item" data-linea="' + detail_row  + '"></td>';
		cell6.innerHTML = '<td id="compra_neto_'+ detail_row +'"><input type="number" name="detalle[' + detail_row + '][total_price]" id="Detalle' + detail_row + 'TotalPrice" class="item-input total-item" data-linea="' + detail_row  + '" readonly></td>';
		cell7.innerHTML = "<td id='eliminar'><button class='borraroc' onclick='del_item(this)'></button></td>";
	}
	
	function del_item(r) {
	    var i = r.parentNode.parentNode.rowIndex;
	    document.getElementById("tabla_oc").deleteRow(i);
	    detail_row--;
	}
	
	var options = {
		url: "/modular/providers.json",
		categories: [
			{
				listLocation: "providers",
				header: "Proveedores"
			}
	    ],
		getValue: function(element) {
			return element.rut + " - " + element.names;
		},

		list: {
			onChooseEvent : function(){
				var elegido = $("#autocompletar").getSelectedItemData();
				$(".campos-proveedores[name='rut_provider']").val(elegido.rut);
				$(".campos-proveedores[name='name_provider']").val(elegido.names);
				$(".campos-proveedores[name='contact_provider']").val(elegido.contact.names + " " + elegido.contact.lastnames);
				$(".campos-proveedores[name='id_provider']").val(elegido.id);
			}
		}
};

$("#autocompletar").easyAutocomplete(options);

$("body").on("change", ".quantity-item", function(e){
	var valor_unitario = $(this).parents("tr").find(".unit-item").val();
	var unidades =  $(this).val();
	var total = parseInt(valor_unitario) * parseInt(unidades);
	$(this).parents("tr").find(".total-item").val(total);
});

$("body").on("change", ".unit-item", function(e){
	var valor_unitario = $(this).val();
	var unidades = $(this).parents("tr").find(".quantity-item").val();
	var total = parseInt(valor_unitario) * parseInt(unidades);
	$(this).parents("tr").find(".total-item").val(total);
});
</script>

<div id="container" class="rendicion nopadding nomargin">
	<h1 class="titulo"> Crear Orden de Compra <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button> </h1>
	<div class="col-md-12 col-xs-12 wrapper nomargin tablaoc">
		<?=
			$this->Form->create('BuyOrder', [
				'class' => 'formOc',
				'url' => ['controller' => 'Outcomes', 'action' => 'createOc']
			]);
		?>
		<div id="datos" class="col-md-12 pull-right">
        	<table class="table table-bordered nomargin">
				<th class="col-md-2 "> Rut Responsable </th>               
				<td id="rut" width="150px" >
					<?= $this->Form->input('rut_buyer', ['label' => false, 'id' => 'RUT', 'required' => true, 'div' => false, 'placeholder' => 'Rut']); ?>
				</td>
				<td width="60px">
					<?= $this->Form->input('dv', ['label' => false, 'id' => 'DV', 'required' => true, 'div' => false, 'placeholder' => 'DV']); ?>
				</td>
				<td width="300px">
					<?= $this->Form->input('name_buyer', ['label' => false, 'id' => 'nombre', 'required' => true, 'div' => false, 'placeholder' => 'Nombre']); ?>
				</td>
				<th width="150px"> Centro de Costos </th>
				<td id="centrocosto">
					<?= $this->Form->input('cost_center_id', ['label' => false, 'id' => 'centro', 'options' => $cc, 'required' => true, 'div' => false]); ?>
				</td>
			</table>
			<table class="table table-bordered nomargin">
				<?= $this->Form->input('id_provider', ['type' => 'hidden', 'value' => '']); ?>
				<th class="col-md-2"> Rut Proveedor </th>
				<td id="prorut" class="col-md-5">
					<?= $this->Form->input('rut_provider', ['label' => false, 'id' => 'autocompletar', 'class' => 'campos-proveedores', 'required' => true, 'div' => false, 'placeholder' => 'Rut Proveedor']); ?>
				</td>
				<td class="col-md-5">
					<?= $this->Form->input('name_provider', ['label' => false, 'id' => 'prorut', 'class' => 'pull-left campos-proveedores', 'required' => true, 'div' => false, 'placeholder' => 'Nombre Proveedor']); ?>
				</td>
			</table>
			<table class="table table-bordered nomargin">
				<th class="col-md-2"> Contacto Proveedor </th>
				<td id="procontacto">
					<?= $this->Form->input('contact_provider', ['label' => false, 'id' => 'procontacto', 'class' => 'campos-proveedores', 'required' => true, 'div' => false, 'placeholder' => 'Contacto Proveedor']); ?>
				</td>
			</table>
			<table class="table table-bordered nomargin">
				<th class="col-md-2"> ID Cotización </th>
				<td id="cotid">
					<?= $this->Form->input('cotizacion', ['class' => 'pull-left', 'label' => false, 'required' => true, 'div' => false, 'placeholder' => 'ID']); ?>
				</td>
				<td id="Fecha">
					<?= $this->Form->input('buy_date', ['class' => 'pull-left', 'label' => false, 'id' => 'fechaoc', 'required' => true, 'div' => false, 'placeholder' => 'Fecha']); ?>
				</td>
			</table>
			<h1 class="titulo2 gris separado"> Items
				<input type="button" class="nopadding boton-mas2 pull-right" id="agregar_item" onclick="add_item_oc();" value="+" />
			</h1>
			<table id="tabla_oc" class="table table-bordered">
				<thead>                    
					<th width="80px">Cant.</th>
					<th>Ítem</th>
					<th class="col-md-5">Detalle</th>
					<th class="col-md-1">Código Producto</th>
					<th class="col-md-1">$ Unitario</th>
					<th class="col-md-1">Total Neto </th>
					<th width="70px"> Eliminar </th>
				</thead>
				<tbody>
					<tr class="detalle">
						<td id="compra_cantidad_0"><input type="number" name="detalle[0][quantity]" id="Detalle0Quantity" class="item-input quantity-item" data-linea="0" required></td>
						<td id="compra_item_0"><input type="text" name="detalle[0][item]" id="Detalle0Item" class="item-input" data-linea="0" required></td>
						<td id="compra_detalle_0"><input type="text" name="detalle[0][detail]" id="Detalle0Detail" class="item-input" data-linea="0" required></td>
						<td id="compra_codigo_0"><input type="text" name="detalle[0][product_code]" id="Detalle0ProdCode" class="item-input" data-linea="0" required></td>
						<td id="compra_unitario_0"><input type="number" name="detalle[0][unit_price]" id="Detalle0UnitPrice" class="item-input unit-item" data-linea="0" required></td>
						<td id="compra_neto_0"><input type="number" name="detalle[0][total_price]" id="Detalle0TotalPrice" class="item-input total-item" data-linea="0" required readonly></td>
						<td id="eliminar"></td>
					</tr>
				</tbody>
			</table>
			<hr class="espaciador">
			<!--<a href="mod_compras/compras_oc_vista.php" class="" data-toggle="modal" data-target="#generaroc">
				<input type="button" class="botongrande verde col-md-5" value="Generar OC"/>
			</a>-->
			<?= $this->Form->submit('Generar', ['class' => 'botongrande verde col-md-5', 'div' => false]); ?>
			<input type="button" class="botongrande verde col-md-5 pull-right" id="comprometeroc"  onclick="compremeter_oc();" value="Comprometer"/>
        </div>
        <?= $this->Form->end(); ?>
	</div><!--/tabladatos .Wrapper-->
</div><!--/datos-->