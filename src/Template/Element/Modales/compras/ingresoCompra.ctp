<script>
function add_item(){
    var table = document.getElementById("tablaingresarcompra");
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);

    cell1.innerHTML = "<td id='compra_cantidad'><input type='number'></input></td>";
    cell2.innerHTML = "<td id='compra_item'><input type='text'></input></td>";
    cell3.innerHTML = "<td id='compra_detalle'><input type='text'></input></td>";
    cell4.innerHTML = "<td id='compra_codigo'><input type='text'></input></td>";
    cell5.innerHTML = "<td id='compra_unitario'><input type='number'></input></td>";
    cell6.innerHTML = "<td id='compra_neto'><input type='number'></input></td>";
    cell7.innerHTML = "<td id='eliminar'><button class='borraroc' onclick='del_item(this)'></button></td>";

    
}

function del_item(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tablaingresarcompra").deleteRow(i);
}
</script>
<div id="container" class="rendicion">
	<?=
		$this->Form->create('Outcome', [
			'class' => 'formInsertOutcome',
			'url' => ['controller' => 'Outcomes', 'action' => 'insertOutcome']
		]);
	?>
	<h1 class="titulo"> Ingresar Compra <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button> </h1>
	<div class="col-md-12 col-xs-12 wrapper">   
		<div id="rendicion">
			<h1 class="titulo2 separado gris">Datos Factura</h1>
			<table class="table table-bordered">
				<tr>
					<th class="col-md-2"> RUT Proveedor </th>
					<th class="col-md-1"> DV </th>
					<th class="col-md-9"> Razón Social </th>
				</tr>
				<tr>
					<td class="nopadding"><input type="text" class="nomargin"></input></td>
					<td class="nopadding"><input type="text" class="nomargin"></input></td>
					<td class="nopadding"><input type="text" class="nomargin"></input></td>
				</tr>
			</table>
			<div class="col-md-12 nopadding">
				<div class="col-md-5 nopadding">
					<table class="table table-bordered">
						<tr>
							<th class="col-md-2"> Tipo de Documento </th>
							<th class="col-md-1"> ID Documento </th>
							<th class="col-md-1"> Fecha Documento </th>
						</tr>
						<tr>
							<td class="nopadding"><input type="text" class="nomargin"></input></td>
							<td class="nopadding"><input type="text" class="nomargin"></input></td>
							<td class="nopadding"><input id="fechacom" type="text" class="nomargin"></input></td>
						</tr>
					</table>   
				</div>
				<div class="col-md-7">
					<span class="col-md-4"><input class="checkgrande" type="checkbox" id="relacionaroc" value="relacionar_oc"><label class="labelcheckgrande" for="relacionaroc">Relacionar a OC</label></span>
					<span class="col-md-2"><input class="checkgrande" type="checkbox" id="pagada" value="pagada"> <label class="labelcheckgrande" for="pagada">Pagada</label></span>
					<span class="col-md-3"><input class="checkgrande" type="checkbox" id="pagadaparcial" value="pagadaparcial"> <label class="labelcheckgrande" for="pagadaparcial">Pagada Parcial</label></span>
					<span class="col-md-3"><input type="text" class=""></span>
					<span class="col-md-4"><input class="checkgrande" type="checkbox" id="copiarccosto" value="copiar_cc"> <label class="labelcheckgrande" for="copiarccosto">Copiar C.Costo a Detalle</label></span>
					<span class="col-md-4"><input class="checkgrande" type="checkbox" id="porpagar" value="porpagar"><label class="labelcheckgrande" for="porpagar">Por Pagar</label></span>
				</div>
			</div>
			<hr class="separador">
			<div class="col-md-12 nopadding">
				<h1 class="titulo2 gris separado"> Items
					<input type="button" class="nopadding boton-mas2 pull-right" id="agregar_item" onclick="add_item();" value="+" />
				</h1>
			</div>
			<table id="tablaingresarcompra" class="table table-bordered">
				<thead>
					<th width="80px">Cant.</th>
					<th>Ítem</th>
					<th class="col-md-5">Detalle</th>
					<th class="col-md-1">Código Producto</th>
					<th class="col-md-1">$ Unitario</th>
					<th class="col-md-1"> Total Neto </th>
					<th width="70px"> Eliminar </th>
				</thead>
				<tbody>
					<tr>
						<td id="compra_cantidad"><input type="number" lang="nb" ></input></td>
						<td id="compra_item"><input type="text" lang="nb" ></input></td>
						<td id="compra_detalle"><input type="text" lang="nb" ></input></td>
						<td id="compra_codigo"><input type="text" lang="nb" ></input></td>
						<td id="compra_unitario"><input type="number" lang="nb" ></input></td>
						<td id="compra_neto"><input type="number" lang="nb" ></input></td>
						<td id="eliminar"></td>
					</tr>
				</tbody>
			</table>
			<hr class="espaciador">
			<?= $this->Form->submit('Ingresar Compra', ['class' => 'botongrande verde col-md-12', 'div' => false]); ?>
			
		</div>
	</div><!--/tabladatos .Wrapper-->
	<?= $this->Form->end(); ?>
</div><!--/datos-->