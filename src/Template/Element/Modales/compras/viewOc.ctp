<div id="container" class="rendicion nopadding nomargin">
	<h1 class="titulo"> Orden de compra
		<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button>
	</h1>
	<div id="imprimir" class="col-md-12 col-xs-12 wrapper nomargin">
		<div class="col-md-12 table-responsive vista">
			<table class="table table-no-bordered">
			    <thead>
			    <tr>
			        <td width="20%" id="logovista"></td>
			        <td id="titulovista" class="text-center">Orden de Compra</td>
			        <td width="20%" id="codigovista" class="text-right">
			            <table>
			                <tr>
			                    <td>Rut responsable:</td>
			                    <td class="text-right"><?=$oc->rut_buyer?></td>
			                </tr>
			                <tr>
			                    <td>Centro de costos</td>
			                    <td class="text-right">
			                        <?php echo date("d/m/y");?>                                        
			                    </td>
			                </tr>
			            </table>
			                
			        </td>
			
			    </tr>
			    </thead>
			</table>
			<h1 class="titulo2 gris  ">Detalle orden de compra</h1>
			<table class="table table-bordered table-bordered">
				<thead>
					<tr>
						<th>Cant.</th>
						<th>Item</th>
						<th>Detalle</th>
						<th>Código producto</th>
						<th>$ Unitario</th>
						<th>Total Neto</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($oc->BuyOrderDetails as $ix=>$detalle): ?>
					<tr>
						<td><?= $detalle->quantity ?></td>
						<td><?= $detalle->item ?></td>
						<td><?= $detalle->detail ?></td>
						<td><?= $detalle->product_code ?></td>
						<td><?= $detalle->unit_price ?></td>
						<td><?= $detalle->total_price ?></td>    
					</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<h1 class="titulo2 gris  ">Datos del Proveedor</h1>
				<table class="table table-bordered table-bordered">
					<thead>
						<tr>
							<th> Nombre</th>
							<th> Rut </th>
							<th> Atención a </th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="nombre_proveedor_v"></td>
							<td id="rut_proveedor_v"></td>
							<td id="atencion_proveedor_v"></td>
						</tr>
					</tbody>
				</table>

				<table class="table table-bordered nomargin">
					<thead>
						<tr>
							<th> Cotización ID</th>
							<th> Teléfono </th>
							<th> Fax </th>
							<th> Correo Electrónico </th>
						</tr>
					</thead>
					<tbody>
						
						<tr>
							<td id="cotización_id_v"></td>
							<td id="telefono_v"></td>
							<td id="fax_v"></td>
							<td id="correo_v"></td>
						</tr>
					</tbody>
				</table>
				<!--<h1 class="titulo2 gris  ">Clasificación de la Compra</h1>
				<table id="tabla" class="table table-bordered">
                    <thead>                    
                            <th width="80px">Cant.</th>
                            <th>Ítem</th>
                            <th class="col-md-5">Detalle</th>
                            <th class="col-md-1">Código Producto</th>
                            <th class="col-md-1">$ Unitario</th>
                            <th class="col-md-1"> Total Neto </th>
                            
                    </thead>
                    <tbody>
                        <tr>
                            <td id="compra_cantidad"></td>
                            <td id="compra_item"></td>
                            <td id="compra_detalle"></td>
                            <td id="compra_codigo"></td>
                            <td id="compra_unitario"></td>
                            <td id="compra_neto"></td>
                        </tr>
                    </tbody>
             </table> -->
		</div><!--/Nombrefechacentro -->
        <div id="aviso" class="col-md-12">
        	<hr class="espaciador">
        	<input type="button" class="botongrande verde col-md-5" id="enviarcorreo" onclick="" value="Enviar por Correo" />
			<input type="button" class="botongrande verde col-md-5 pull-right" id="descargar" onclick="" value="Descargar" />
        </div>
    </div><!--/tabladatos .Wrapper-->
</div><!--/datos-->
<div id="invi"></div>
<script>
	$("#descargar").click(function()
	{
		$("#invi").load("<?=$this->Url->build(['controller' => 'modales', 'action' => 'loadViewOcPdf', $release->id])?>")
	});
	
	$("#enviarcorreo").click(function()
	{
		$("#invi").load("mod_rendiciones/rendicion_enviar.php")
	});

</script>