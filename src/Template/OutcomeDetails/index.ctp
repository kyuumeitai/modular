<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Outcome Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Outcomes'), ['controller' => 'Outcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Outcome'), ['controller' => 'Outcomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['controller' => 'CostCenters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cost Center'), ['controller' => 'CostCenters', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="outcomeDetails index large-9 medium-8 columns content">
    <h3><?= __('Outcome Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('outcome_id') ?></th>
                <th><?= $this->Paginator->sort('cost_center_id') ?></th>
                <th><?= $this->Paginator->sort('related_budget_item') ?></th>
                <th><?= $this->Paginator->sort('detail') ?></th>
                <th><?= $this->Paginator->sort('date_created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($outcomeDetails as $outcomeDetail): ?>
            <tr>
                <td><?= $this->Number->format($outcomeDetail->id) ?></td>
                <td><?= $outcomeDetail->has('outcome') ? $this->Html->link($outcomeDetail->outcome->id, ['controller' => 'Outcomes', 'action' => 'view', $outcomeDetail->outcome->rut_proveedor]) : '' ?></td>
                <td><?= $outcomeDetail->has('cost_center') ? $this->Html->link($outcomeDetail->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $outcomeDetail->cost_center->id]) : '' ?></td>
                <td><?= h($outcomeDetail->related_budget_item) ?></td>
                <td><?= h($outcomeDetail->detail) ?></td>
                <td><?= h($outcomeDetail->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $outcomeDetail->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $outcomeDetail->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $outcomeDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $outcomeDetail->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
