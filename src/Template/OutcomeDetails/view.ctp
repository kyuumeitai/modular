<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Outcome Detail'), ['action' => 'edit', $outcomeDetail->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Outcome Detail'), ['action' => 'delete', $outcomeDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $outcomeDetail->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Outcome Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Outcome Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Outcomes'), ['controller' => 'Outcomes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Outcome'), ['controller' => 'Outcomes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['controller' => 'CostCenters', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cost Center'), ['controller' => 'CostCenters', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="outcomeDetails view large-9 medium-8 columns content">
    <h3><?= h($outcomeDetail->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Outcome') ?></th>
            <td><?= $outcomeDetail->has('outcome') ? $this->Html->link($outcomeDetail->outcome->id, ['controller' => 'Outcomes', 'action' => 'view', $outcomeDetail->outcome->rut_proveedor]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Cost Center') ?></th>
            <td><?= $outcomeDetail->has('cost_center') ? $this->Html->link($outcomeDetail->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $outcomeDetail->cost_center->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Related Budget Item') ?></th>
            <td><?= h($outcomeDetail->related_budget_item) ?></td>
        </tr>
        <tr>
            <th><?= __('Detail') ?></th>
            <td><?= h($outcomeDetail->detail) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($outcomeDetail->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Created') ?></th>
            <td><?= h($outcomeDetail->date_created) ?></td>
        </tr>
    </table>
</div>
