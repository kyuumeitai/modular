<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Outcome Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Outcomes'), ['controller' => 'Outcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Outcome'), ['controller' => 'Outcomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['controller' => 'CostCenters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cost Center'), ['controller' => 'CostCenters', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="outcomeDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($outcomeDetail) ?>
    <fieldset>
        <legend><?= __('Add Outcome Detail') ?></legend>
        <?php
            echo $this->Form->input('outcome_id', ['options' => $outcomes]);
            echo $this->Form->input('cost_center_id', ['options' => $costCenters]);
            echo $this->Form->input('related_budget_item');
            echo $this->Form->input('detail');
            echo $this->Form->input('date_created');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
