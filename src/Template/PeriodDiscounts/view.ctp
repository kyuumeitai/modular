<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Period Discount'), ['action' => 'edit', $periodDiscount->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Period Discount'), ['action' => 'delete', $periodDiscount->id], ['confirm' => __('Are you sure you want to delete # {0}?', $periodDiscount->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Period Discounts'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Period Discount'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="periodDiscounts view large-9 medium-8 columns content">
    <h3><?= h($periodDiscount->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Month') ?></th>
            <td><?= h($periodDiscount->month) ?></td>
        </tr>
        <tr>
            <th><?= __('Year') ?></th>
            <td><?= h($periodDiscount->year) ?></td>
        </tr>
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $periodDiscount->has('employee') ? $this->Html->link($periodDiscount->employee->id, ['controller' => 'Employees', 'action' => 'view', $periodDiscount->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($periodDiscount->id) ?></td>
        </tr>
    </table>
</div>
