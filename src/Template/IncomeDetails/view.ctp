<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Income Detail'), ['action' => 'edit', $incomeDetail->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Income Detail'), ['action' => 'delete', $incomeDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incomeDetail->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Income Details'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Income Detail'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Period Incomes'), ['controller' => 'PeriodIncomes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Period Income'), ['controller' => 'PeriodIncomes', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Income Types'), ['controller' => 'IncomeTypes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Income Type'), ['controller' => 'IncomeTypes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="incomeDetails view large-9 medium-8 columns content">
    <h3><?= h($incomeDetail->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($incomeDetail->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Value') ?></th>
            <td><?= h($incomeDetail->value) ?></td>
        </tr>
        <tr>
            <th><?= __('Period Income') ?></th>
            <td><?= $incomeDetail->has('period_income') ? $this->Html->link($incomeDetail->period_income->id, ['controller' => 'PeriodIncomes', 'action' => 'view', $incomeDetail->period_income->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Income Type') ?></th>
            <td><?= $incomeDetail->has('income_type') ? $this->Html->link($incomeDetail->income_type->id, ['controller' => 'IncomeTypes', 'action' => 'view', $incomeDetail->income_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($incomeDetail->id) ?></td>
        </tr>
    </table>
</div>
