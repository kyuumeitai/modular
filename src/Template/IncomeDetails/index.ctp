<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Income Detail'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Period Incomes'), ['controller' => 'PeriodIncomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Period Income'), ['controller' => 'PeriodIncomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Income Types'), ['controller' => 'IncomeTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Income Type'), ['controller' => 'IncomeTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incomeDetails index large-9 medium-8 columns content">
    <h3><?= __('Income Details') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('value') ?></th>
                <th><?= $this->Paginator->sort('Period_income_id') ?></th>
                <th><?= $this->Paginator->sort('Income_type_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($incomeDetails as $incomeDetail): ?>
            <tr>
                <td><?= $this->Number->format($incomeDetail->id) ?></td>
                <td><?= h($incomeDetail->description) ?></td>
                <td><?= h($incomeDetail->value) ?></td>
                <td><?= $incomeDetail->has('period_income') ? $this->Html->link($incomeDetail->period_income->id, ['controller' => 'PeriodIncomes', 'action' => 'view', $incomeDetail->period_income->id]) : '' ?></td>
                <td><?= $incomeDetail->has('income_type') ? $this->Html->link($incomeDetail->income_type->id, ['controller' => 'IncomeTypes', 'action' => 'view', $incomeDetail->income_type->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $incomeDetail->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $incomeDetail->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $incomeDetail->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incomeDetail->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
