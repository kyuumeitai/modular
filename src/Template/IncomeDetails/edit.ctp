<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $incomeDetail->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $incomeDetail->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Income Details'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Period Incomes'), ['controller' => 'PeriodIncomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Period Income'), ['controller' => 'PeriodIncomes', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Income Types'), ['controller' => 'IncomeTypes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Income Type'), ['controller' => 'IncomeTypes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incomeDetails form large-9 medium-8 columns content">
    <?= $this->Form->create($incomeDetail) ?>
    <fieldset>
        <legend><?= __('Edit Income Detail') ?></legend>
        <?php
            echo $this->Form->input('description');
            echo $this->Form->input('value');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
