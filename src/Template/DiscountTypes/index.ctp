<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Discount Type'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="discountTypes index large-9 medium-8 columns content">
    <h3><?= __('Discount Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('discount_value') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($discountTypes as $discountType): ?>
            <tr>
                <td><?= $this->Number->format($discountType->id) ?></td>
                <td><?= h($discountType->title) ?></td>
                <td><?= h($discountType->discount_value) ?></td>
                <td><?= h($discountType->description) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $discountType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $discountType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $discountType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $discountType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
