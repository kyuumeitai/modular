<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Discount Types'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="discountTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($discountType) ?>
    <fieldset>
        <legend><?= __('Add Discount Type') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('discount_value');
            echo $this->Form->input('description');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
