<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Discount Type'), ['action' => 'edit', $discountType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Discount Type'), ['action' => 'delete', $discountType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $discountType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Discount Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Discount Type'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="discountTypes view large-9 medium-8 columns content">
    <h3><?= h($discountType->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($discountType->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Discount Value') ?></th>
            <td><?= h($discountType->discount_value) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($discountType->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($discountType->id) ?></td>
        </tr>
    </table>
</div>
