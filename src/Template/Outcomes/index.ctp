<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Outcome'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Buy Orders'), ['controller' => 'BuyOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Buy Order'), ['controller' => 'BuyOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Providers'), ['controller' => 'Providers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Provider'), ['controller' => 'Providers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="outcomes index large-9 medium-8 columns content">
    <h3><?= __('Outcomes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('item') ?></th>
                <th><?= $this->Paginator->sort('subitem') ?></th>
                <th><?= $this->Paginator->sort('document_type') ?></th>
                <th><?= $this->Paginator->sort('document_id') ?></th>
                <th><?= $this->Paginator->sort('document_date') ?></th>
                <th><?= $this->Paginator->sort('amount') ?></th>
                <th><?= $this->Paginator->sort('bo_id') ?></th>
                <th><?= $this->Paginator->sort('cc_id') ?></th>
                <th><?= $this->Paginator->sort('provider_id') ?></th>
                <th><?= $this->Paginator->sort('employee_id') ?></th>
                <th><?= $this->Paginator->sort('date_created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($outcomes as $outcome): ?>
            <tr>
                <td><?= $this->Number->format($outcome->id) ?></td>
                <td><?= h($outcome->item) ?></td>
                <td><?= h($outcome->subitem) ?></td>
                <td><?= h($outcome->document_type) ?></td>
                <td><?= h($outcome->document_id) ?></td>
                <td><?= h($outcome->document_date) ?></td>
                <td><?= $this->Number->format($outcome->amount) ?></td>
                <td><?= $outcome->has('buy_order') ? $this->Html->link($outcome->buy_order->id, ['controller' => 'BuyOrders', 'action' => 'view', $outcome->buy_order->id]) : '' ?></td>
                <td><?= $this->Number->format($outcome->cc_id) ?></td>
                <td><?= $outcome->has('provider') ? $this->Html->link($outcome->provider->id, ['controller' => 'Providers', 'action' => 'view', $outcome->provider->id]) : '' ?></td>
                <td><?= $this->Number->format($outcome->employee_id) ?></td>
                <td><?= h($outcome->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $outcome->rut_proveedor]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $outcome->rut_proveedor]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $outcome->rut_proveedor], ['confirm' => __('Are you sure you want to delete # {0}?', $outcome->rut_proveedor)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
