<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Outcome'), ['action' => 'edit', $outcome->rut_proveedor]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Outcome'), ['action' => 'delete', $outcome->rut_proveedor], ['confirm' => __('Are you sure you want to delete # {0}?', $outcome->rut_proveedor)]) ?> </li>
        <li><?= $this->Html->link(__('List Outcomes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Outcome'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Buy Orders'), ['controller' => 'BuyOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Buy Order'), ['controller' => 'BuyOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Providers'), ['controller' => 'Providers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Provider'), ['controller' => 'Providers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="outcomes view large-9 medium-8 columns content">
    <h3><?= h($outcome->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Item') ?></th>
            <td><?= h($outcome->item) ?></td>
        </tr>
        <tr>
            <th><?= __('Subitem') ?></th>
            <td><?= h($outcome->subitem) ?></td>
        </tr>
        <tr>
            <th><?= __('Document Type') ?></th>
            <td><?= h($outcome->document_type) ?></td>
        </tr>
        <tr>
            <th><?= __('Document Id') ?></th>
            <td><?= h($outcome->document_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Buy Order') ?></th>
            <td><?= $outcome->has('buy_order') ? $this->Html->link($outcome->buy_order->id, ['controller' => 'BuyOrders', 'action' => 'view', $outcome->buy_order->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Provider') ?></th>
            <td><?= $outcome->has('provider') ? $this->Html->link($outcome->provider->id, ['controller' => 'Providers', 'action' => 'view', $outcome->provider->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($outcome->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Amount') ?></th>
            <td><?= $this->Number->format($outcome->amount) ?></td>
        </tr>
        <tr>
            <th><?= __('Cc Id') ?></th>
            <td><?= $this->Number->format($outcome->cc_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Employee Id') ?></th>
            <td><?= $this->Number->format($outcome->employee_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Document Date') ?></th>
            <td><?= h($outcome->document_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Created') ?></th>
            <td><?= h($outcome->date_created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Detail') ?></h4>
        <?= $this->Text->autoParagraph(h($outcome->detail)); ?>
    </div>
</div>
