<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Outcomes'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Buy Orders'), ['controller' => 'BuyOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Buy Order'), ['controller' => 'BuyOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Providers'), ['controller' => 'Providers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Provider'), ['controller' => 'Providers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="outcomes form large-9 medium-8 columns content">
    <?= $this->Form->create($outcome) ?>
    <fieldset>
        <legend><?= __('Add Outcome') ?></legend>
        <?php
            echo $this->Form->input('id');
            echo $this->Form->input('item');
            echo $this->Form->input('subitem');
            echo $this->Form->input('document_type');
            echo $this->Form->input('document_id');
            echo $this->Form->input('document_date', ['empty' => true]);
            echo $this->Form->input('detail');
            echo $this->Form->input('amount');
            echo $this->Form->input('bo_id', ['options' => $buyOrders, 'empty' => true]);
            echo $this->Form->input('cc_id');
            echo $this->Form->input('provider_id', ['options' => $providers]);
            echo $this->Form->input('employee_id');
            echo $this->Form->input('date_created');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
