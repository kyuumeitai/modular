
<div class="employees view large-10 medium-9 columns content">

<script>
    $(".usuarios-nav").addClass('active');
</script>


    <h3><?= h($employee->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nombre') ?></th>
            <td><?= h($employee->firstname) ?></td>
        </tr>
        <tr>
            <th><?= __('Apellido') ?></th>
            <td><?= h($employee->lastname) ?></td>
        </tr>
        <tr>
            <th><?= __('Posición') ?></th>
            <td><?= h($employee->position) ?></td>
        </tr>
        <tr>
            <th><?= __('Sección') ?></th>
            <td><?= h($employee->section) ?></td>
        </tr>
        <tr>
            <th><?= __('Teléfono') ?></th>
            <td><?= h($employee->telephone) ?></td>
        </tr>
        <tr>
            <th><?= __('Email') ?></th>
            <td><?= h($employee->email) ?></td>
        </tr>
        <tr>
            <th><?= __('Nombre de Usuario') ?></th>
            <td><?= h($employee->username) ?></td>
        </tr>
        <tr>
            <th><?= __('Vacaciones') ?></th>
            <td><?= h($employee->vacations) ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($employee->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Off Days') ?></h4>
        <?php if (!empty($employee->off_days)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Employee Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Period Summary Id') ?></th>
                <th><?= __('Period Summary Employee Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($employee->off_days as $offDays): ?>
            <tr>
                <td><?= h($offDays->id) ?></td>
                <td><?= h($offDays->employee_id) ?></td>
                <td><?= h($offDays->date) ?></td>
                <td><?= h($offDays->period_summary_id) ?></td>
                <td><?= h($offDays->period_summary_employee_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OffDays', 'action' => 'view', $offDays->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OffDays', 'action' => 'edit', $offDays->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OffDays', 'action' => 'delete', $offDays->id], ['confirm' => __('Are you sure you want to delete # {0}?', $offDays->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('On Days') ?></h4>
        <?php if (!empty($employee->on_days)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Employee Id') ?></th>
                <th><?= __('Date') ?></th>
                <th><?= __('Period Summary Id') ?></th>
                <th><?= __('Period Summary Employee Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($employee->on_days as $onDays): ?>
            <tr>
                <td><?= h($onDays->id) ?></td>
                <td><?= h($onDays->employee_id) ?></td>
                <td><?= h($onDays->date) ?></td>
                <td><?= h($onDays->period_summary_id) ?></td>
                <td><?= h($onDays->period_summary_employee_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'OnDays', 'action' => 'view', $onDays->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'OnDays', 'action' => 'edit', $onDays->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'OnDays', 'action' => 'delete', $onDays->id], ['confirm' => __('Are you sure you want to delete # {0}?', $onDays->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Descuentos de Periodo') ?></h4>
        <?php if (!empty($employee->period_discounts)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Month') ?></th>
                <th><?= __('Year') ?></th>
                <th><?= __('Employee Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($employee->period_discounts as $periodDiscounts): ?>
            <tr>
                <td><?= h($periodDiscounts->id) ?></td>
                <td><?= h($periodDiscounts->month) ?></td>
                <td><?= h($periodDiscounts->year) ?></td>
                <td><?= h($periodDiscounts->employee_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PeriodDiscounts', 'action' => 'view', $periodDiscounts->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PeriodDiscounts', 'action' => 'edit', $periodDiscounts->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PeriodDiscounts', 'action' => 'delete', $periodDiscounts->id], ['confirm' => __('Are you sure you want to delete # {0}?', $periodDiscounts->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Sumarios de Periodo') ?></h4>
        <?php if (!empty($employee->period_summaries)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Month') ?></th>
                <th><?= __('Year') ?></th>
                <th><?= __('Income Total') ?></th>
                <th><?= __('Employee Id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($employee->period_summaries as $periodSummaries): ?>
            <tr>
                <td><?= h($periodSummaries->id) ?></td>
                <td><?= h($periodSummaries->month) ?></td>
                <td><?= h($periodSummaries->year) ?></td>
                <td><?= h($periodSummaries->income_total) ?></td>
                <td><?= h($periodSummaries->employee_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PeriodSummaries', 'action' => 'view', $periodSummaries->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PeriodSummaries', 'action' => 'edit', $periodSummaries->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PeriodSummaries', 'action' => 'delete', $periodSummaries->id], ['confirm' => __('Are you sure you want to delete # {0}?', $periodSummaries->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
