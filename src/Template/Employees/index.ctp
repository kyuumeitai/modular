
<div class="employees index large-10 medium-9 columns content">

<script>
    $(".usuarios-nav").addClass('active');
</script>


    <h3><?= __('Empleados') ?>
        <button class="agregar"><?= $this->Html->link(__('Agregar Empleado'), ['action' => 'add']) ?></button>
    </h3>


    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('Nombre') ?></th>
                <th><?= $this->Paginator->sort('Apellido') ?></th>
                <th><?= $this->Paginator->sort('Posición') ?></th>
                <th><?= $this->Paginator->sort('Sección') ?></th>
                <th class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($employees as $employee): ?>
            <tr>
                <td><?= $this->Number->format($employee->id) ?></td>
                <td><?= h($employee->firstname) ?></td>
                <td><?= h($employee->lastname) ?></td>
                <td><?= h($employee->position) ?></td>
                <td><?= h($employee->section) ?></td>

                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $employee->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $employee->id]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $employee->id], ['confirm' => __('Estás seguro de que quieres eliminar a {0}?', $employee->firstname)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>

</div>


