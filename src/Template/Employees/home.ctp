<div id="pantalla1" class="pantalla vpw">
<div class="col-md-8 col-md-offset-2 vph">
    <hr class="espaciador">
    <div class="ceafblanco wow fadeInLeft"></div>
    <hr class="espaciador">
    <div class="logogrande wow fadeInLeft col-md-12 col-xs-12"></div>
    <hr class="espaciador">
    <br>
    <p class="blanco wow fadeInLeft">Beta</p>
    <hr class="espaciador">
    <h1 class="bienvenida col-md-6 nopadding wow fadeInLeft">¡Bienvenidos a Modular!, esta es una muestra beta del sistema.</h1>
    <hr class="espaciador">
    <h1 class="bienvenidasub col-md-6 nopadding wow fadeInLeft">

    </h1>
    
</div>

<div class="col-md-12 col-xs-12 bottom15 wow bounceInDown hidden-xs">
    <a data-scroll href="#pantalla2"><div class="botondown centrar infinite wow pulse"></div></a>
</div>
<div class="circulos col-md-pull-11 vph animated bounceInUp push-left">
    <ul class="contcirc centerv">
        <a data-scroll href="#pantalla1"><li class="circulonav"></li></a>
        <a data-scroll href="#pantalla2"><li class="circulonav"></li></a>
        <a data-scroll href="#pantalla3"><li class="circulonav"></li></a>
    </ul>
</div>


<!-- Pantalla 1 -->
<div id="pantalla2" class="vpw vph pantalla centercontentv1">
<hr class="espaciador2 visible-xs">
    <hr class="espaciador2 visible-xs">
        <div id="modulos" class="col-md-8 col-md-offset-2 centercontent1">
        <?=$this->Html->link('<div id="modulo1" class="col-md-8 modulo col-sm-8">
                <li class="moduloli">
                <p class="modtexto">Solicitud de Viáticos</p>
                </li>

            </div>', [ 'controller' => 'modales', 'action' => 'loadViaticos'],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#viaticos']); 
        ?>
        <?=$this->Html->link('<div id="modulo2" class="col-md-4 modulo col-sm-4">
                <li class="moduloli">
                <p class="modtexto">Rendiciones</p>
                </li>
            </div>', [ 'controller' => 'modales', 'action' => 'loadRendiciones'],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#rendicion']); 
        ?>
        
        <div class="hidden">
        

        <!--///////////////////Esto es Provisorio//////////////////////////////////////-->

        //<?=$this->Html->link('<div id="modulo3" class="col-md-3 modulo col-sm-3">
                <li class="moduloli">
                <p class="modtexto">Compras</p>
                </li>
            </div>', [ 'controller' => 'modales', 'action' => 'loadCompras'],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#compras']); 
        ?>

        <a href="mod_presupuesto/presupuesto.php" class="" data-toggle="modal" data-target="#presupuesto">
                <div id="modulo4" class="col-md-3 modulo col-sm-3">
                    <li class="moduloli">
                            <p class="modtexto">Presupuesto</p>
                    </li>
                </div>
            </a>
         
                
            <a href="mod_pagos/pagos1.php" class="" data-toggle="modal" data-target="#pagos1">
                <div id="modulo5" class="col-md-3 modulo col-sm-3">
                    <li class="moduloli">
                            <p class="modtexto">WIP | Pago a Colaboradores</p>
                            
                    </li>
                        </a>
    </div>




        <!--Esto es Provisorio-->
        </div>     

<?=$this->Html->link('<div id="modulo3" class="col-md-3 modulo col-sm-3">
                <li class="moduloli">
                <p class="modtexto">Compras </p>
                </li>
            </div>', [ 'controller' => 'modales', 'action' => 'loadCompras'],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#compras']); 
        ?>

        <div id="modulo4" class="col-md-3 modulo col-sm-3">
                    <li class="moduloli">
                            <p class="modtexto">Presupuesto</p>
                    </li>
                </div>
                     
                

        <div id="modulo5" class="col-md-3 modulo col-sm-3">
                    <li class="moduloli">
                            <p class="modtexto">Pago a Colaboradores</p>
                            
                    </li>
        
                </div>


           

            <?= $this->Html->link('<div id="modulo6" class="col-md-3 modulo">
                    <li class="moduloli"><p class="modtexto">Permisos</p>
                    </li>
                </div>', [ 'controller' => 'modales', 'action' => 'loadPermisos'],['escape' => false, 'data-toggle' => 'modal', 'data-target' => '#permisos']); 
            ?>

        </div>
    </div>
</div>
<!-- Pantalla 2 -->
<div id="pantalla3" class="vpw vph pantalla centercontentv2">
    
    <hr class="espaciador2 visible-xs">
    <hr class="espaciador2 visible-xs">

            <div id="modulos" class="col-md-8 col-md-offset-2 centercontent2">

             
                <div id="directoriomod" class="modulo2 col-md-6">
                    <li class="moduloli">
                        <p class="modtexto">Directorio</p>
                    </li>
                </div>
            

            <a href="mod_herramientas/herramientas.php" data-toggle="modal" data-target="#herramientas">
                <div id="herramientasmod" class="modulo2 col-md-6">
                    <li class="moduloli">
                        <p class="modtexto">Herramientas</p>
                    </li>
                </div>
            </a>


            </div> <!--Modulos-->

</div>


<!-- Pantalla 3 -->