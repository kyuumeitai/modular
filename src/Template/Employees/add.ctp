
<div class="employees form large-10 medium-9 columns content">
<script>
    $(".usuarios-nav").addClass('active');
</script>

    <?= $this->Form->create($employee) ?>
    <fieldset>
        <legend><?= __('Agregar Empleado') ?></legend>
        <?php
            echo $this->Form->input('firstname');
            echo $this->Form->input('lastname');
            echo $this->Form->input('position');
            echo $this->Form->input('section');
            echo $this->Form->input('telephone');
            echo $this->Form->input('email');
            echo $this->Form->input('username');
            echo $this->Form->input('password');
            echo $this->Form->input('vacations');
            echo $this->Form->input('address');
            echo $this->Form->input('emergency_contact');
            echo $this->Form->input('emergency_relation');
            echo $this->Form->input('emergency_tel');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
