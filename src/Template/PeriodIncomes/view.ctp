<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Period Income'), ['action' => 'edit', $periodIncome->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Period Income'), ['action' => 'delete', $periodIncome->id], ['confirm' => __('Are you sure you want to delete # {0}?', $periodIncome->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Period Incomes'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Period Income'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="periodIncomes view large-9 medium-8 columns content">
    <h3><?= h($periodIncome->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Month') ?></th>
            <td><?= h($periodIncome->month) ?></td>
        </tr>
        <tr>
            <th><?= __('Year') ?></th>
            <td><?= h($periodIncome->year) ?></td>
        </tr>
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $periodIncome->has('employee') ? $this->Html->link($periodIncome->employee->id, ['controller' => 'Employees', 'action' => 'view', $periodIncome->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($periodIncome->id) ?></td>
        </tr>
    </table>
</div>
