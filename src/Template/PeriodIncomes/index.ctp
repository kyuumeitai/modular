<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Period Income'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="periodIncomes index large-9 medium-8 columns content">
    <h3><?= __('Period Incomes') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('month') ?></th>
                <th><?= $this->Paginator->sort('year') ?></th>
                <th><?= $this->Paginator->sort('Employee_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($periodIncomes as $periodIncome): ?>
            <tr>
                <td><?= $this->Number->format($periodIncome->id) ?></td>
                <td><?= h($periodIncome->month) ?></td>
                <td><?= h($periodIncome->year) ?></td>
                <td><?= $periodIncome->has('employee') ? $this->Html->link($periodIncome->employee->id, ['controller' => 'Employees', 'action' => 'view', $periodIncome->employee->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $periodIncome->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $periodIncome->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $periodIncome->id], ['confirm' => __('Are you sure you want to delete # {0}?', $periodIncome->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
