<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New On Day'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Period Summaries'), ['controller' => 'PeriodSummaries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Period Summary'), ['controller' => 'PeriodSummaries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="onDays index large-9 medium-8 columns content">
    <h3><?= __('On Days') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('employee_id') ?></th>
                <th><?= $this->Paginator->sort('date') ?></th>
                <th><?= $this->Paginator->sort('period_summary_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($onDays as $onDay): ?>
            <tr>
                <td><?= $this->Number->format($onDay->id) ?></td>
                <td><?= $onDay->has('employee') ? $this->Html->link($onDay->employee->id, ['controller' => 'Employees', 'action' => 'view', $onDay->employee->id]) : '' ?></td>
                <td><?= h($onDay->date) ?></td>
                <td><?= $onDay->has('period_summary') ? $this->Html->link($onDay->period_summary->id, ['controller' => 'PeriodSummaries', 'action' => 'view', $onDay->period_summary->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $onDay->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $onDay->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $onDay->id], ['confirm' => __('Are you sure you want to delete # {0}?', $onDay->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
