<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $onDay->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $onDay->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List On Days'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Period Summaries'), ['controller' => 'PeriodSummaries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Period Summary'), ['controller' => 'PeriodSummaries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="onDays form large-9 medium-8 columns content">
    <?= $this->Form->create($onDay) ?>
    <fieldset>
        <legend><?= __('Edit On Day') ?></legend>
        <?php
            echo $this->Form->input('employee_id', ['options' => $employees, 'empty' => true]);
            echo $this->Form->input('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
