<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit On Day'), ['action' => 'edit', $onDay->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete On Day'), ['action' => 'delete', $onDay->id], ['confirm' => __('Are you sure you want to delete # {0}?', $onDay->id)]) ?> </li>
        <li><?= $this->Html->link(__('List On Days'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New On Day'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Period Summaries'), ['controller' => 'PeriodSummaries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Period Summary'), ['controller' => 'PeriodSummaries', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="onDays view large-9 medium-8 columns content">
    <h3><?= h($onDay->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $onDay->has('employee') ? $this->Html->link($onDay->employee->id, ['controller' => 'Employees', 'action' => 'view', $onDay->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Date') ?></th>
            <td><?= h($onDay->date) ?></td>
        </tr>
        <tr>
            <th><?= __('Period Summary') ?></th>
            <td><?= $onDay->has('period_summary') ? $this->Html->link($onDay->period_summary->id, ['controller' => 'PeriodSummaries', 'action' => 'view', $onDay->period_summary->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($onDay->id) ?></td>
        </tr>
    </table>
</div>
