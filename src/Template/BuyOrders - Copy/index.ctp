<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Buy Order'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Providers'), ['controller' => 'Providers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Provider'), ['controller' => 'Providers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="buyOrders index large-9 medium-8 columns content">
    <h3><?= __('Buy Orders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('cotizacion') ?></th>
                <th><?= $this->Paginator->sort('create_date') ?></th>
                <th><?= $this->Paginator->sort('autodate') ?></th>
                <th><?= $this->Paginator->sort('provider_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($buyOrders as $buyOrder): ?>
            <tr>
                <td><?= $this->Number->format($buyOrder->id) ?></td>
                <td><?= h($buyOrder->cotizacion) ?></td>
                <td><?= h($buyOrder->create_date) ?></td>
                <td><?= h($buyOrder->autodate) ?></td>
                <td><?= $buyOrder->has('provider') ? $this->Html->link($buyOrder->provider->id, ['controller' => 'Providers', 'action' => 'view', $buyOrder->provider->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $buyOrder->provider_id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $buyOrder->provider_id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $buyOrder->provider_id], ['confirm' => __('Are you sure you want to delete # {0}?', $buyOrder->provider_id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>
