
<div class="onDays index large-10 medium-9 columns content">

<script>
    $(".compras-nav").addClass('active');
</script>

    <h3><?= __('Órdenes de Compra') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('cotizacion') ?></th>
                <th><?= $this->Paginator->sort('fecha de Solicitud') ?></th>
                <th><?= $this->Paginator->sort('Fecha de Compra') ?></th>
                <th><?= $this->Paginator->sort('Proveedor') ?></th>
                <th><?= $this->Paginator->sort('RUT Comprador') ?></th>
                <th><?= $this->Paginator->sort('Centro de Costos') ?></th>
                <th class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($buyOrders as $buyOrder): ?>
            <tr>
                <td><?= $this->Number->format($buyOrder->id) ?></td>
                <td><?= $this->Number->format($buyOrder->cotizacion) ?></td>
                <td><?= h($buyOrder->create_date) ?></td>
                <td><?= h($buyOrder->buy_date) ?></td>
                <td><?= $buyOrder->has('provider') ? $this->Html->link($buyOrder->provider->id, ['controller' => 'Providers', 'action' => 'view', $buyOrder->provider->id]) : '' ?></td>
                <td><?= h($buyOrder->rut_buyer) ?></td>
                <td><?= $this->Number->format($buyOrder->cost_center_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $buyOrder->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $buyOrder->id]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $buyOrder->id], ['confirm' => __('Está seguro que quiere eliminar la Órden # {0}?', $buyOrder->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
