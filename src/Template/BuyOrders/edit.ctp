
<div class="onDays index large-10 medium-9 columns content">
    <?= $this->Form->create($buyOrder) ?>
    <fieldset>
        <legend><?= __('Edit Buy Order') ?></legend>
        <?php
            echo $this->Form->input('cotizacion');
            echo $this->Form->input('create_date', ['empty' => true]);
            echo $this->Form->input('buy_date', ['empty' => true]);
            echo $this->Form->input('provider_id', ['options' => $providers]);
            echo $this->Form->input('name_buyer');
            echo $this->Form->input('rut_buyer');
            echo $this->Form->input('cost_center_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
