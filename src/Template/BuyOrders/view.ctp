
<div class="onDays index large-10 medium-9 columns content">
    <h3>Órden de compra ID <?= h($buyOrder->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Proveedor') ?></th>
            <td><?= $buyOrder->has('provider') ? $this->Html->link($buyOrder->provider->names, ['controller' => 'Providers', 'action' => 'view', $buyOrder->provider->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('RUT del Comprador') ?></th>
            <td><?= h($buyOrder->rut_buyer) ?></td>
        </tr>
        <tr>
            <th><?= __('ID') ?></th>
            <td><?= $this->Number->format($buyOrder->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Cotizacion') ?></th>
            <td><?= $this->Number->format($buyOrder->cotizacion) ?></td>
        </tr>
        <tr>
            <th><?= __('Centro de Costos (ID)') ?></th>
            <td><?= $this->Number->format($buyOrder->cost_center_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha de Creación de la órden') ?></th>
            <td><?= h($buyOrder->create_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha de Compra') ?></th>
            <td><?= h($buyOrder->buy_date) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Nombre del Solicitante') ?></h4>
        <?= $this->Text->autoParagraph(h($buyOrder->name_buyer)); ?>
    </div>
    <div class="related">
        <h4><?= __('Detalles relacionados a la órden') ?></h4>
        <?php if (!empty($buyOrder->buy_orders_details)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Quantity') ?></th>
                <th><?= __('Item') ?></th>
                <th><?= __('Detail') ?></th>
                <th><?= __('Product Code') ?></th>
                <th><?= __('Unit Price') ?></th>
                <th><?= __('Total Price') ?></th>
                <th><?= __('Buy Order Id') ?></th>
                <th><?= __('Date Created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($buyOrder->buy_orders_details as $buyOrdersDetails): ?>
            <tr>
                <td><?= h($buyOrdersDetails->id) ?></td>
                <td><?= h($buyOrdersDetails->quantity) ?></td>
                <td><?= h($buyOrdersDetails->item) ?></td>
                <td><?= h($buyOrdersDetails->detail) ?></td>
                <td><?= h($buyOrdersDetails->product_code) ?></td>
                <td><?= h($buyOrdersDetails->unit_price) ?></td>
                <td><?= h($buyOrdersDetails->total_price) ?></td>
                <td><?= h($buyOrdersDetails->buy_order_id) ?></td>
                <td><?= h($buyOrdersDetails->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'BuyOrdersDetails', 'action' => 'view', $buyOrdersDetails->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'BuyOrdersDetails', 'action' => 'edit', $buyOrdersDetails->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'BuyOrdersDetails', 'action' => 'delete', $buyOrdersDetails->id], ['confirm' => __('Are you sure you want to delete # {0}?', $buyOrdersDetails->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
