<div class="budgets index large-10 medium-9 columns content">

<script>
    $(".presupuesto-nav").addClass('active');
</script>

    <h3><?= __('Presupuestos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('buy_order_id') ?></th>
                <th><?= $this->Paginator->sort('cost_center_id') ?></th>
                <th><?= $this->Paginator->sort('outcome_id') ?></th>
                <th><?= $this->Paginator->sort('period_month') ?></th>
                <th><?= $this->Paginator->sort('period_year') ?></th>
                <th><?= $this->Paginator->sort('date_created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($budgets as $budget): ?>
            <tr>
                <td><?= $this->Number->format($budget->id) ?></td>
                <td><?= h($budget->title) ?></td>
                <td><?= h($budget->description) ?></td>
                <td><?= $budget->has('buy_order') ? $this->Html->link($budget->buy_order->provider_id, ['controller' => 'BuyOrders', 'action' => 'view', $budget->buy_order->provider_id]) : '' ?></td>
                <td><?= $budget->has('cost_center') ? $this->Html->link($budget->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $budget->cost_center->id]) : '' ?></td>
                <td><?= $budget->has('outcome') ? $this->Html->link($budget->outcome->id, ['controller' => 'Outcomes', 'action' => 'view', $budget->outcome->id]) : '' ?></td>
                <td><?= $this->Number->format($budget->period_month) ?></td>
                <td><?= $this->Number->format($budget->period_year) ?></td>
                <td><?= h($budget->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $budget->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $budget->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $budget->id], ['confirm' => __('Are you sure you want to delete # {0}?', $budget->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
