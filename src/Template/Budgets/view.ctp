<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Budget'), ['action' => 'edit', $budget->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Budget'), ['action' => 'delete', $budget->id], ['confirm' => __('Are you sure you want to delete # {0}?', $budget->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Budgets'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Budget'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Buy Orders'), ['controller' => 'BuyOrders', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Buy Order'), ['controller' => 'BuyOrders', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['controller' => 'CostCenters', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cost Center'), ['controller' => 'CostCenters', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Outcomes'), ['controller' => 'Outcomes', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Outcome'), ['controller' => 'Outcomes', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="budgets view large-9 medium-8 columns content">
    <h3><?= h($budget->title) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Title') ?></th>
            <td><?= h($budget->title) ?></td>
        </tr>
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($budget->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Buy Order') ?></th>
            <td><?= $budget->has('buy_order') ? $this->Html->link($budget->buy_order->provider_id, ['controller' => 'BuyOrders', 'action' => 'view', $budget->buy_order->provider_id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Cost Center') ?></th>
            <td><?= $budget->has('cost_center') ? $this->Html->link($budget->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $budget->cost_center->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Outcome') ?></th>
            <td><?= $budget->has('outcome') ? $this->Html->link($budget->outcome->id, ['controller' => 'Outcomes', 'action' => 'view', $budget->outcome->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($budget->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Period Month') ?></th>
            <td><?= $this->Number->format($budget->period_month) ?></td>
        </tr>
        <tr>
            <th><?= __('Period Year') ?></th>
            <td><?= $this->Number->format($budget->period_year) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Created') ?></th>
            <td><?= h($budget->date_created) ?></td>
        </tr>
    </table>
</div>
