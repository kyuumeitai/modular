<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Budgets'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Buy Orders'), ['controller' => 'BuyOrders', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Buy Order'), ['controller' => 'BuyOrders', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['controller' => 'CostCenters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cost Center'), ['controller' => 'CostCenters', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Outcomes'), ['controller' => 'Outcomes', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Outcome'), ['controller' => 'Outcomes', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="budgets form large-9 medium-8 columns content">
    <?= $this->Form->create($budget) ?>
    <fieldset>
        <legend><?= __('Add Budget') ?></legend>
        <?php
            echo $this->Form->input('title');
            echo $this->Form->input('description');
            echo $this->Form->input('buy_order_id', ['options' => $buyOrders]);
            echo $this->Form->input('cost_center_id', ['options' => $costCenters]);
            echo $this->Form->input('outcome_id', ['options' => $outcomes]);
            echo $this->Form->input('period_month');
            echo $this->Form->input('period_year');
            echo $this->Form->input('date_created', ['empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
