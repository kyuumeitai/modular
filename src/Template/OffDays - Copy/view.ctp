<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Off Day'), ['action' => 'edit', $offDay->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Off Day'), ['action' => 'delete', $offDay->id], ['confirm' => __('Are you sure you want to delete # {0}?', $offDay->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Off Days'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Off Day'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Period Summaries'), ['controller' => 'PeriodSummaries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Period Summary'), ['controller' => 'PeriodSummaries', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="offDays view large-9 medium-8 columns content">
    <h3><?= h($offDay->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $offDay->has('employee') ? $this->Html->link($offDay->employee->id, ['controller' => 'Employees', 'action' => 'view', $offDay->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Date') ?></th>
            <td><?= h($offDay->date) ?></td>
        </tr>
        <tr>
            <th><?= __('Period Summary') ?></th>
            <td><?= $offDay->has('period_summary') ? $this->Html->link($offDay->period_summary->id, ['controller' => 'PeriodSummaries', 'action' => 'view', $offDay->period_summary->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($offDay->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Period Summary Employee Id') ?></th>
            <td><?= $this->Number->format($offDay->period_summary_employee_id) ?></td>
        </tr>
    </table>
</div>
