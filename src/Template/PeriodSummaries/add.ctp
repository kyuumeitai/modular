<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Period Summaries'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="periodSummaries form large-9 medium-8 columns content">
    <?= $this->Form->create($periodSummary) ?>
    <fieldset>
        <legend><?= __('Add Period Summary') ?></legend>
        <?php
            echo $this->Form->input('month');
            echo $this->Form->input('year');
            echo $this->Form->input('income_total');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
