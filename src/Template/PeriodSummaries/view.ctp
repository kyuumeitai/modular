<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Period Summary'), ['action' => 'edit', $periodSummary->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Period Summary'), ['action' => 'delete', $periodSummary->id], ['confirm' => __('Are you sure you want to delete # {0}?', $periodSummary->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Period Summaries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Period Summary'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="periodSummaries view large-9 medium-8 columns content">
    <h3><?= h($periodSummary->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Month') ?></th>
            <td><?= h($periodSummary->month) ?></td>
        </tr>
        <tr>
            <th><?= __('Year') ?></th>
            <td><?= h($periodSummary->year) ?></td>
        </tr>
        <tr>
            <th><?= __('Income Total') ?></th>
            <td><?= h($periodSummary->income_total) ?></td>
        </tr>
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $periodSummary->has('employee') ? $this->Html->link($periodSummary->employee->id, ['controller' => 'Employees', 'action' => 'view', $periodSummary->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($periodSummary->id) ?></td>
        </tr>
    </table>
</div>
