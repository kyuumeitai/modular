
<div class="contacts form large-10 medium-9 columns content">
    <?= $this->Form->create($contact) ?>
    <fieldset>
        <legend><?= __('Agregar Contacto') ?></legend>
        <?php
            echo $this->Form->input('names');
            echo $this->Form->input('lastnames');
            echo $this->Form->input('rut');
            echo $this->Form->input('address');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
