
<div class="contacts view large-10 medium-9 columns content">
    <h3><?= h($contact->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Names') ?></th>
            <td><?= h($contact->names) ?></td>
        </tr>
        <tr>
            <th><?= __('Lastnames') ?></th>
            <td><?= h($contact->lastnames) ?></td>
        </tr>
        <tr>
            <th><?= __('Rut') ?></th>
            <td><?= h($contact->rut) ?></td>
        </tr>
        <tr>
            <th><?= __('Address') ?></th>
            <td><?= h($contact->address) ?></td>
        </tr>
        <tr>
            <th><?= __('Provider') ?></th>
            <td><?= $contact->has('provider') ? $this->Html->link($contact->provider->id, ['controller' => 'Providers', 'action' => 'view', $contact->provider->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($contact->id) ?></td>
        </tr>
    </table>
</div>
