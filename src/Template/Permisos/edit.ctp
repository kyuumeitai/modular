<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $OffDay->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $OffDay->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Off Days'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="OffDays form large-9 medium-8 columns content">
    <?= $this->Form->create($OffDay) ?>
    <fieldset>
        <legend><?= __('Edit Off Day') ?></legend>
        <?php
            echo $this->Form->input('start_date', ['empty' => true]);
            echo $this->Form->input('end_date', ['empty' => true]);
            echo $this->Form->input('type');
            echo $this->Form->input('period_summary_id');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
