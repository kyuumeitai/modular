
<div class="onDays index large-10 medium-9 columns content">
<script>
    $(".permisos-nav").addClass('active');
</script>

	<h3><?= __('Días Libres solicitados') ?></h3>
    
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('ID') ?></th>
                <th><?= $this->Paginator->sort('Empleado *Falta*') ?></th>
                <th><?= $this->Paginator->sort('Fecha de Inicio') ?></th>
                <th><?= $this->Paginator->sort('Fecha de Retorno') ?></th>
                <th><?= $this->Paginator->sort('Periodo') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($OffDays as $OffDay): ?>
            <tr>
                <td><?= $this->Number->format($OffDay->id) ?></td>
                <td><?= $OffDay->has('employee') ? $this->Html->link($OffDay->employee->firstname, ['controller' => 'Employees', 'action' => 'view', $OffDay->employee->id]) : '' ?></td>
                <td><?= h($OffDay->start_date) ?></td>
                <td><?= h($OffDay->end_date) ?></td>
                <td><?= $this->Number->format($OffDay->period_summary_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $OffDay->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $OffDay->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $OffDay->id], ['confirm' => __('Are you sure you want to delete # {0}?', $OffDay->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


	<div class="paginator">
		<ul class="pagination">
			<?= $this->Paginator->prev('< ' . __('previous')) ?>
			<?= $this->Paginator->numbers() ?>
			<?= $this->Paginator->next(__('next') . ' >') ?>
		</ul>
		<p><?= $this->Paginator->counter() ?></p>
	</div>
</div>
