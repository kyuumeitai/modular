
<div class="outcomeReleases form large-9 medium-8 columns content">

<script>
    $(".rendiciones-nav").addClass('active');
</script>


    <?= $this->Form->create($outcomeRelease) ?>
    <fieldset>
        <legend><?= __('Edit Outcome Release') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('date');
            echo $this->Form->input('cost_center_id', ['options' => $costCenters]);
            echo $this->Form->input('amount');
            echo $this->Form->input('estimated_release');
            echo $this->Form->input('justification');
            echo $this->Form->input('employee_id', ['options' => $employees]);
            echo $this->Form->input('date_created');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
