<div class="outcomeReleases index large-10 medium-9 columns content">

<script>
    $(".rendiciones-nav").addClass('active');
</script>


    <h3>Rendición #<?= h($outcomeRelease->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Nombre') ?></th>
            <td><?= h($outcomeRelease->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Centro de Costos') ?></th>
            <td><?= $outcomeRelease->has('cost_center') ? $this->Html->link($outcomeRelease->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $outcomeRelease->cost_center->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Empleado') ?></th>
            <td><?= $outcomeRelease->has('employee') ? $this->Html->link($outcomeRelease->employee->id, ['controller' => 'Employees', 'action' => 'view', $outcomeRelease->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('ID') ?></th>
            <td><?= $this->Number->format($outcomeRelease->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Cantidad Solicitada') ?></th>
            <td><?= $this->Number->format($outcomeRelease->amount) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha') ?></th>
            <td><?= h($outcomeRelease->date) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha de Devolución Estimada') ?></th>
            <td><?= h($outcomeRelease->estimated_release) ?></td>
        </tr>
        <tr>
            <th><?= __('Fecha de Creación') ?></th>
            <td><?= h($outcomeRelease->date_created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Justificación') ?></h4>
        <?= $this->Text->autoParagraph(h($outcomeRelease->justification)); ?>
    </div>
</div>
