<div class="outcomeReleases index large-10 medium-9 columns content">

<script>
    $(".rendiciones-nav").addClass('active');
</script>

    <h3><?= __('Rendiciones') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th>Nombre</th>
                <th><?= $this->Paginator->sort('date', 'Fecha') ?></th>
                <th><?= $this->Paginator->sort('cost_center_id', 'Centro de Costos') ?></th>
                <th><?= $this->Paginator->sort('amount', 'Cantidad') ?></th>
                <th><?= $this->Paginator->sort('estimated_release','Entrega Estimada') ?></th>
                <th><?= $this->Paginator->sort('employee_id', 'Empleado Solicitante') ?></th>
                <th><?= $this->Paginator->sort('date_created','Fecha de Creación') ?></th>
                <th class="actions">Acciones</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($outcomeReleases as $outcomeRelease): ?>
            <tr>
                <td><?= $this->Number->format($outcomeRelease->id) ?></td>
                <td><?= h($outcomeRelease->name) ?></td>
                <td><?= h($outcomeRelease->date->nice('America/Santiago', 'es-Es')) ?></td>
                <td><?=
                	//$outcomeRelease->has('cost_center') ? $this->Html->link($outcomeRelease->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $outcomeRelease->cost_center->id]) : '' 
                	$outcomeRelease->has('cost_center') ? $outcomeRelease->cost_center->title : '' ?></td>
                <td><?= "$".$this->Number->format($outcomeRelease->amount) ?></td>
                <td><?= h($outcomeRelease->estimated_release->nice('America/Santiago', 'es-Es')) ?></td>
                <td><?= $outcomeRelease->has('employee') ? $this->Html->link([$outcomeRelease->employee->firstname, ' ' , $outcomeRelease->employee->lastname],   ['controller' => 'Employees', 'action' => 'view', $outcomeRelease->employee->id]) : '' ?></td>
                <td><?= h($outcomeRelease->date_created->nice('America/Santiago', 'es-Es')) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $outcomeRelease->id]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
