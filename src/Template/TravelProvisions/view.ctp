<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Travel Provision'), ['action' => 'edit', $travelProvision->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Travel Provision'), ['action' => 'delete', $travelProvision->id], ['confirm' => __('Are you sure you want to delete # {0}?', $travelProvision->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Travel Provisions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Travel Provision'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="travelProvisions view large-9 medium-8 columns content">
    <h3><?= h($travelProvision->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $travelProvision->has('employee') ? $this->Html->link($travelProvision->employee->id, ['controller' => 'Employees', 'action' => 'view', $travelProvision->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Transport Type') ?></th>
            <td><?= h($travelProvision->transport_type) ?></td>
        </tr>
        <tr>
            <th><?= __('Project') ?></th>
            <td><?= h($travelProvision->project) ?></td>
        </tr>
        <tr>
            <th><?= __('Country') ?></th>
            <td><?= $travelProvision->has('country') ? $this->Html->link($travelProvision->country->name, ['controller' => 'Countries', 'action' => 'view', $travelProvision->country->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($travelProvision->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Budget Id') ?></th>
            <td><?= $this->Number->format($travelProvision->budget_id) ?></td>
        </tr>
        <tr>
            <th><?= __('Days Nosleep') ?></th>
            <td><?= $this->Number->format($travelProvision->days_nosleep) ?></td>
        </tr>
        <tr>
            <th><?= __('Days Sleep') ?></th>
            <td><?= $this->Number->format($travelProvision->days_sleep) ?></td>
        </tr>
        <tr>
            <th><?= __('Amount') ?></th>
            <td><?= $this->Number->format($travelProvision->amount) ?></td>
        </tr>
        <tr>
            <th><?= __('Start Date') ?></th>
            <td><?= h($travelProvision->start_date) ?></td>
        </tr>
        <tr>
            <th><?= __('End Date') ?></th>
            <td><?= h($travelProvision->end_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Created') ?></th>
            <td><?= h($travelProvision->date_created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Motive') ?></h4>
        <?= $this->Text->autoParagraph(h($travelProvision->motive)); ?>
    </div>
    <div class="row">
        <h4><?= __('Objective') ?></h4>
        <?= $this->Text->autoParagraph(h($travelProvision->objective)); ?>
    </div>
    <div class="row">
        <h4><?= __('Cities') ?></h4>
        <?= $this->Text->autoParagraph(h($travelProvision->cities)); ?>
    </div>
    <div class="row">
        <h4><?= __('Places') ?></h4>
        <?= $this->Text->autoParagraph(h($travelProvision->places)); ?>
    </div>
</div>
