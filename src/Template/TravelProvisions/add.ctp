<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Travel Provisions'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Countries'), ['controller' => 'Countries', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Country'), ['controller' => 'Countries', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="travelProvisions form large-9 medium-8 columns content">
    <?= $this->Form->create($travelProvision) ?>
    <fieldset>
        <legend><?= __('Add Travel Provision') ?></legend>
        <?php
            echo $this->Form->input('start_date');
            echo $this->Form->input('end_date');
            echo $this->Form->input('employee_id', ['options' => $employees]);
            echo $this->Form->input('budget_id');
            echo $this->Form->input('days_nosleep');
            echo $this->Form->input('days_sleep');
            echo $this->Form->input('date_created');
            echo $this->Form->input('transport_type');
            echo $this->Form->input('amount');
            echo $this->Form->input('project');
            echo $this->Form->input('motive');
            echo $this->Form->input('objective');
            echo $this->Form->input('country_id', ['options' => $countries]);
            echo $this->Form->input('cities');
            echo $this->Form->input('places');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
