<div class="travelProvisions index large-10 medium-9 columns content">
	<script>
	    $(".viaticos-nav").addClass('active');
	</script>
    <h3>Viaticos</h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('start_date', 'Fecha de inicio') ?></th>
                <th><?= $this->Paginator->sort('end_date', 'Fecha de fin') ?></th>
                <th><?= $this->Paginator->sort('employee_id', 'Solicitante') ?></th>
                <th><?= $this->Paginator->sort('days_nosleep', 'Dias sin pernoctar') ?></th>
                <th><?= $this->Paginator->sort('days_sleep', 'Dias pernoctando') ?></th>
                <th><?= $this->Paginator->sort('transport_type', 'Tipo de transporte') ?></th>
                <th><?= $this->Paginator->sort('amount', 'Monto de Viatico') ?></th>
                <th><?= $this->Paginator->sort('project', 'Proyecto') ?></th>
                <th><?= $this->Paginator->sort('country_id', 'País') ?></th>
                <th><?= $this->Paginator->sort('date_created', 'Fecha de solicitud') ?></th>
                <th colspan="2" class="actions"><?= __('Acciones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($travelProvisions as $travelProvision): ?>
            <tr>
                <td><?= $this->Number->format($travelProvision->id) ?></td>
                <td><?= h($travelProvision->start_date->nice('America/Santiago', 'es-Es')) ?></td>
                <td><?= h($travelProvision->end_date->nice('America/Santiago', 'es-Es')) ?></td>
                <td><?= $travelProvision->has('employee') ? $travelProvision->employee->firstname." ".$travelProvision->employee->lastname : '' ?></td>
                <td><?= $this->Number->format($travelProvision->days_nosleep) ?></td>
                <td><?= $this->Number->format($travelProvision->days_sleep) ?></td>
                <td><?= h($ttypes[$travelProvision->transport_type]) ?></td>
                <td><?= "$".$this->Number->format($travelProvision->amount) ?></td>
                <td><?= h($travelProvision->project) ?></td>
                <td><?= $travelProvision->has('country') ? $this->Html->link($travelProvision->country->name, ['controller' => 'Countries', 'action' => 'view', $travelProvision->country->id]) : '' ?></td>
                <td><?= h($travelProvision->date_created->nice('America/Santiago', 'es-Es')) ?></td>
                <td><?= $this->Html->link('Ver Anexo PDF', ['action' => 'viewAnexo', $travelProvision->id], ['target' => '_blank']) ?></td>
                <td><?= $this->Html->link('Ver Informe PDF', ['action' => 'viewInforme', $travelProvision->id], ['target' => '_blank']) ?></td>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
