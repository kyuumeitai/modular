<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Income Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incomeTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($incomeType) ?>
    <fieldset>
        <legend><?= __('Add Income Type') ?></legend>
        <?php
            echo $this->Form->input('description');
            echo $this->Form->input('value');
            echo $this->Form->input('imponible');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
