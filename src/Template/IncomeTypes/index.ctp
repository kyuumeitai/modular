<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Income Type'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incomeTypes index large-9 medium-8 columns content">
    <h3><?= __('Income Types') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('value') ?></th>
                <th><?= $this->Paginator->sort('imponible') ?></th>
                <th><?= $this->Paginator->sort('Employee_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($incomeTypes as $incomeType): ?>
            <tr>
                <td><?= $this->Number->format($incomeType->id) ?></td>
                <td><?= h($incomeType->description) ?></td>
                <td><?= h($incomeType->value) ?></td>
                <td><?= h($incomeType->imponible) ?></td>
                <td><?= $incomeType->has('employee') ? $this->Html->link($incomeType->employee->id, ['controller' => 'Employees', 'action' => 'view', $incomeType->employee->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $incomeType->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $incomeType->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $incomeType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incomeType->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
