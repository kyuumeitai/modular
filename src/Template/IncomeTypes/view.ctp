<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Income Type'), ['action' => 'edit', $incomeType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Income Type'), ['action' => 'delete', $incomeType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $incomeType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Income Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Income Type'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="incomeTypes view large-9 medium-8 columns content">
    <h3><?= h($incomeType->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Description') ?></th>
            <td><?= h($incomeType->description) ?></td>
        </tr>
        <tr>
            <th><?= __('Value') ?></th>
            <td><?= h($incomeType->value) ?></td>
        </tr>
        <tr>
            <th><?= __('Imponible') ?></th>
            <td><?= h($incomeType->imponible) ?></td>
        </tr>
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $incomeType->has('employee') ? $this->Html->link($incomeType->employee->id, ['controller' => 'Employees', 'action' => 'view', $incomeType->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($incomeType->id) ?></td>
        </tr>
    </table>
</div>
