<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $incomeType->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $incomeType->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Income Types'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="incomeTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($incomeType) ?>
    <fieldset>
        <legend><?= __('Edit Income Type') ?></legend>
        <?php
            echo $this->Form->input('description');
            echo $this->Form->input('value');
            echo $this->Form->input('imponible');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
