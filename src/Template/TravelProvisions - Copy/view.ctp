<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Travel Provision'), ['action' => 'edit', $travelProvision->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Travel Provision'), ['action' => 'delete', $travelProvision->id], ['confirm' => __('Are you sure you want to delete # {0}?', $travelProvision->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Travel Provisions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Travel Provision'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Budgets'), ['controller' => 'Budgets', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Budget'), ['controller' => 'Budgets', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="travelProvisions view large-9 medium-8 columns content">
    <h3><?= h($travelProvision->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $travelProvision->has('employee') ? $this->Html->link($travelProvision->employee->id, ['controller' => 'Employees', 'action' => 'view', $travelProvision->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Budget') ?></th>
            <td><?= $travelProvision->has('budget') ? $this->Html->link($travelProvision->budget->title, ['controller' => 'Budgets', 'action' => 'view', $travelProvision->budget->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($travelProvision->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Days') ?></th>
            <td><?= $this->Number->format($travelProvision->days) ?></td>
        </tr>
        <tr>
            <th><?= __('Start Date') ?></th>
            <td><?= h($travelProvision->start_date) ?></td>
        </tr>
        <tr>
            <th><?= __('End Date') ?></th>
            <td><?= h($travelProvision->end_date) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Created') ?></th>
            <td><?= h($travelProvision->date_created) ?></td>
        </tr>
    </table>
</div>
