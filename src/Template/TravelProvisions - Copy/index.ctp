
<div class="travelProvisions index large-10 medium-9 columns content">
    <h3><?= __('Viáticos') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('start_date') ?></th>
                <th><?= $this->Paginator->sort('end_date') ?></th>
                <th><?= $this->Paginator->sort('employee_id') ?></th>
                <th><?= $this->Paginator->sort('budget_id') ?></th>
                <th><?= $this->Paginator->sort('days') ?></th>
                <th><?= $this->Paginator->sort('date_created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($travelProvisions as $travelProvision): ?>
            <tr>
                <td><?= $this->Number->format($travelProvision->id) ?></td>
                <td><?= h($travelProvision->start_date) ?></td>
                <td><?= h($travelProvision->end_date) ?></td>
                <td><?= $travelProvision->has('employee') ? $this->Html->link($travelProvision->employee->id, ['controller' => 'Employees', 'action' => 'view', $travelProvision->employee->id]) : '' ?></td>
                <td><?= $travelProvision->has('budget') ? $this->Html->link($travelProvision->budget->title, ['controller' => 'Budgets', 'action' => 'view', $travelProvision->budget->id]) : '' ?></td>
                <td><?= $this->Number->format($travelProvision->days) ?></td>
                <td><?= h($travelProvision->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $travelProvision->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $travelProvision->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $travelProvision->id], ['confirm' => __('Are you sure you want to delete # {0}?', $travelProvision->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
