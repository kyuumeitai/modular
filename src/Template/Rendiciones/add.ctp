<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Outcome Releases'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['controller' => 'CostCenters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cost Center'), ['controller' => 'CostCenters', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="outcomeReleases form large-9 medium-8 columns content">
    <?= $this->Form->create($outcomeRelease) ?>
    <fieldset>
        <legend><?= __('Add Outcome Release') ?></legend>
        <?php
            echo $this->Form->input('name');
            echo $this->Form->input('date');
            echo $this->Form->input('cost_center_id', ['options' => $costCenters]);
            echo $this->Form->input('amount');
            echo $this->Form->input('estimated_release');
            echo $this->Form->input('justification');
            echo $this->Form->input('employee_id', ['options' => $employees]);
            echo $this->Form->input('date_created');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
