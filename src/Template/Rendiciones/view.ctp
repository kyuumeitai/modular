<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Outcome Release'), ['action' => 'edit', $outcomeRelease->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Outcome Release'), ['action' => 'delete', $outcomeRelease->id], ['confirm' => __('Are you sure you want to delete # {0}?', $outcomeRelease->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Outcome Releases'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Outcome Release'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['controller' => 'CostCenters', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Cost Center'), ['controller' => 'CostCenters', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="outcomeReleases view large-9 medium-8 columns content">
    <h3><?= h($outcomeRelease->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Name') ?></th>
            <td><?= h($outcomeRelease->name) ?></td>
        </tr>
        <tr>
            <th><?= __('Cost Center') ?></th>
            <td><?= $outcomeRelease->has('cost_center') ? $this->Html->link($outcomeRelease->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $outcomeRelease->cost_center->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Employee') ?></th>
            <td><?= $outcomeRelease->has('employee') ? $this->Html->link($outcomeRelease->employee->id, ['controller' => 'Employees', 'action' => 'view', $outcomeRelease->employee->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($outcomeRelease->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Amount') ?></th>
            <td><?= $this->Number->format($outcomeRelease->amount) ?></td>
        </tr>
        <tr>
            <th><?= __('Date') ?></th>
            <td><?= h($outcomeRelease->date) ?></td>
        </tr>
        <tr>
            <th><?= __('Estimated Release') ?></th>
            <td><?= h($outcomeRelease->estimated_release) ?></td>
        </tr>
        <tr>
            <th><?= __('Date Created') ?></th>
            <td><?= h($outcomeRelease->date_created) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Justification') ?></h4>
        <?= $this->Text->autoParagraph(h($outcomeRelease->justification)); ?>
    </div>
</div>
