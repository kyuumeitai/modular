<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Outcome Release'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Employees'), ['controller' => 'Employees', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Employee'), ['controller' => 'Employees', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cost Centers'), ['controller' => 'CostCenters', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Cost Center'), ['controller' => 'CostCenters', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="outcomeReleases index large-9 medium-8 columns content">
    <h3><?= __('Outcome Releases') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('name') ?></th>
                <th><?= $this->Paginator->sort('date') ?></th>
                <th><?= $this->Paginator->sort('cost_center_id') ?></th>
                <th><?= $this->Paginator->sort('amount') ?></th>
                <th><?= $this->Paginator->sort('estimated_release') ?></th>
                <th><?= $this->Paginator->sort('employee_id') ?></th>
                <th><?= $this->Paginator->sort('date_created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($outcomeReleases as $outcomeRelease): ?>
            <tr>
                <td><?= $this->Number->format($outcomeRelease->id) ?></td>
                <td><?= h($outcomeRelease->name) ?></td>
                <td><?= h($outcomeRelease->date) ?></td>
                <td><?= $outcomeRelease->has('cost_center') ? $this->Html->link($outcomeRelease->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $outcomeRelease->cost_center->id]) : '' ?></td>
                <td><?= $this->Number->format($outcomeRelease->amount) ?></td>
                <td><?= h($outcomeRelease->estimated_release) ?></td>
                <td><?= $outcomeRelease->has('employee') ? $this->Html->link($outcomeRelease->employee->id, ['controller' => 'Employees', 'action' => 'view', $outcomeRelease->employee->id]) : '' ?></td>
                <td><?= h($outcomeRelease->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $outcomeRelease->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $outcomeRelease->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $outcomeRelease->id], ['confirm' => __('Are you sure you want to delete # {0}?', $outcomeRelease->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
