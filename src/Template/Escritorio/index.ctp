
<div class="escritorio index col-md-10 col-md-offset-2">



<script>
    $(".escritorio-nav").addClass('active');
</script>

    <div class="col-md-12">
    <h3><?= __('Escritorio') ?></h3>
    <h4 class="subtitulo">Últimos Movimientos</h4>

    <div id="escritorio_presupuesto" class=" ancho50 caja marginado">
        <div class="escritorio_mas">
            <?= $this->Html->link(__('+'), ['controller' => 'presupuesto', 'action' => 'index']) ?>
        </div>
        <h5 class="h5_titulo"> Presupuesto</h5>
        

<canvas id="chart_escritorio" height="450" width="550"></canvas>

<script>

// Any of the following formats may be used
var ctx = document.getElementById("chart_escritorio");
var ctx = document.getElementById("chart_escritorio").getContext("2d");
var ctx = $("#chart_escritorio");


var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Septiembre","Octubre", "Noviembre", "Diciembre", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto"],
        
        datasets: [

        {
            label: 'Meses',
            lineTension: 0.1,
            data: [15000000, 14300000, 13550000, 16126020, 13512956, 15005201, 12352112, 13546687, 16555963, 12364458, 26826550, 22385612],
            backgroundColor: 'rgba(151,187,205,0)',
            borderColor: 'rgba(71,192,189,1)',
            borderWidth: 3,
            pointBackgroundColor: 'rgba(71,192,189,1)'
        },
        {
            label: 'Gastos',
            lineTension: 0.1,
            data: [3132053, 4250212, 3256930, 2126020, 6132356, 3626987 , 5320159, 1335265, 1559623, 1223658, 6595213, 160023],
            backgroundColor: 'rgba(151,187,205,0)',
            borderColor: 'rgba(58,58,58,1)',
            borderWidth: 3,
            pointBackgroundColor: 'rgba(58,58,58,1)'
        }

        ]
    },
    options: {
        legend: {
            position: 'bottom',
            labels:{
                fontSize: 15,
            }
        }
      }
});

</script>



    </div>

    <div id="escritorio_permisos" class="ancho50 caja marginado float-right">
            <div class="escritorio_mas">
                <?= $this->Html->link(__('+'), ['controller' => 'permisos', 'action' => 'index']) ?>
            </div>
            <h5 class="h5_titulo"> Últimos Días Libres Solicitados </h5>


               <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('employee_id') ?></th>
                <th><?= $this->Paginator->sort('start_date') ?></th>
                <th><?= $this->Paginator->sort('end_date') ?></th>
                <th><?= $this->Paginator->sort('period_summary_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($OffDays as $OffDay): ?>
<tr>
                <td><?= $this->Number->format($OffDay->id) ?></td>
                <td><?= $OffDay->has('employee') ? $this->Html->link($OffDay->employee->id, ['controller' => 'Employees', 'action' => 'view', $OffDay->employee->id]) : '' ?></td>
                <td><?= h($OffDay->start_date) ?></td>
                <td><?= h($OffDay->end_date) ?></td>
                <td><?= $this->Number->format($OffDay->period_summary_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $OffDay->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $OffDay->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $OffDay->id], ['confirm' => __('Are you sure you want to delete # {0}?', $OffDay->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>


            
    </div>

    <div id="escritorio_compras" class="ancho50 caja marginado">
            <div class="escritorio_mas">
                <?= $this->Html->link(__('+'), ['controller' => 'buyOrders', 'action' => 'index']) ?>
            </div>
            <h5 class="h5_titulo"> Últimas órdenes de compra</h5>


<table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('cotizacion') ?></th>
                <th><?= $this->Paginator->sort('create_date') ?></th>
                <th><?= $this->Paginator->sort('provider_id') ?></th>
                
            </tr>
        </thead>
        <tbody>
            <?php foreach ($buyOrders as $buyOrder): ?>
            <tr>
                <td><?= $this->Number->format($buyOrder->id) ?></td>
                <td><?= $this->Number->format($buyOrder->cotizacion) ?></td>
                <td><?= h($buyOrder->create_date) ?></td>
                <td><?= $buyOrder->has('provider') ? $this->Html->link($buyOrder->provider->id, ['controller' => 'Providers', 'action' => 'view', $buyOrder->provider->id]) : '' ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>            

    
            
    </div>


</div>


</div>


