<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Buy Orders'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Providers'), ['controller' => 'Providers', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Provider'), ['controller' => 'Providers', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="buyOrders form large-9 medium-8 columns content">
    <?= $this->Form->create($buyOrder) ?>
    <fieldset>
        <legend><?= __('Add Buy Order') ?></legend>
        <?php
            echo $this->Form->input('cotizacion');
            echo $this->Form->input('create_date', ['empty' => true]);
            echo $this->Form->input('autodate');
            echo $this->Form->input('provider_id', ['options' => $providers]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
