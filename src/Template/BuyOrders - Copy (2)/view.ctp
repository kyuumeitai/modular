<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Buy Order'), ['action' => 'edit', $buyOrder->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Buy Order'), ['action' => 'delete', $buyOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $buyOrder->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Buy Orders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Buy Order'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Providers'), ['controller' => 'Providers', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Provider'), ['controller' => 'Providers', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="buyOrders view large-9 medium-8 columns content">
    <h3><?= h($buyOrder->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th><?= __('Autodate') ?></th>
            <td><?= h($buyOrder->autodate) ?></td>
        </tr>
        <tr>
            <th><?= __('Provider') ?></th>
            <td><?= $buyOrder->has('provider') ? $this->Html->link($buyOrder->provider->id, ['controller' => 'Providers', 'action' => 'view', $buyOrder->provider->id]) : '' ?></td>
        </tr>
        <tr>
            <th><?= __('Id') ?></th>
            <td><?= $this->Number->format($buyOrder->id) ?></td>
        </tr>
        <tr>
            <th><?= __('Cotizacion') ?></th>
            <td><?= $this->Number->format($buyOrder->cotizacion) ?></td>
        </tr>
        <tr>
            <th><?= __('Create Date') ?></th>
            <td><?= h($buyOrder->create_date) ?></td>
        </tr>
    </table>
</div>
