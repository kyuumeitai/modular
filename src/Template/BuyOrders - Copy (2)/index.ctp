<div class="buyOrders index large-10 medium-9 columns content">
    <h3><?= __('Buy Orders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('cotizacion') ?></th>
                <th><?= $this->Paginator->sort('create_date') ?></th>
                <th><?= $this->Paginator->sort('autodate') ?></th>
                <th><?= $this->Paginator->sort('provider_id') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($buyOrders as $buyOrder): ?>
            <tr>
                <td><?= $this->Number->format($buyOrder->id) ?></td>
                <td><?= $this->Number->format($buyOrder->cotizacion) ?></td>
                <td><?= h($buyOrder->create_date) ?></td>
                <td><?= h($buyOrder->autodate) ?></td>
                <td><?= $buyOrder->has('provider') ? $this->Html->link($buyOrder->provider->id, ['controller' => 'Providers', 'action' => 'view', $buyOrder->provider->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $buyOrder->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $buyOrder->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $buyOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $buyOrder->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
