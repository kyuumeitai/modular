<div class="budgets index large-10 medium-9 columns content">

<script>
    $(".presupuesto-nav").addClass('active');
</script>







    <h3><?= __('Presupuesto') ?></h3>



<div class="col-md-9 cajapresupuesto">
    <h4 class="sub text-center"> Gastos / Presupuesto a 12 meses</h4>
    <canvas id="myChart" height="150px"></canvas>
</div>

<div class="col-md-3 cajapresupuesto ">
    <h4 class="sub text-center"> Gastos por Área</h4>
    <canvas id="myChart2" height="450px"></canvas>
</div>

<script>

// Any of the following formats may be used
var ctx = document.getElementById("myChart");
var ctx = document.getElementById("myChart").getContext("2d");
var ctx = $("#myChart");


var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Septiembre","Octubre", "Noviembre", "Diciembre", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto"],
        
        datasets: [

        {
            label: 'Presupuesto Mensual',
            lineTension: 0.1,
            data: [15000000, 14300000, 13550000, 16126020, 13512956, 15005201, 12352112, 13546687, 16555963, 12364458, 26826550, 22385612],
            backgroundColor: 'rgba(151,187,205,0)',
            borderColor: 'rgba(71,192,189,1)',
            borderWidth: 3,
            pointBackgroundColor: 'rgba(71,192,189,1)'
        },
        {
            label: 'Gastos',
            lineTension: 0.1,
            data: [3132053, 4250212, 3256930, 2126020, 6132356, 3626987 , 5320159, 1335265, 1559623, 1223658, 6595213, 160023],
            backgroundColor: 'rgba(151,187,205,0)',
            borderColor: 'rgba(58,58,58,1)',
            borderWidth: 3,
            pointBackgroundColor: 'rgba(58,58,58,1)'
        }

        ]
    },
    options: {
        legend: {
            position: 'bottom',
            labels:{
                fontSize: 15,
            }
        }
      }
});

</script>


<script>

// Any of the following formats may be used
var ctx2 = document.getElementById("myChart2");
var ctx2 = document.getElementById("myChart2").getContext("2d");
var ctx2 = $("#myChart2");


// And for a doughnut chart
var myDoughnutChart = new Chart(ctx2, {
    type: 'doughnut',
    options: {
        legend: {
            position: 'bottom',
            labels:{
                fontSize: 15,
            }
        }
      },
    data: {
     labels: [
        "Gestión",
        "Mejoramiento Genético",
        "Genómica",
        "Fisiología del Estrés",
        "Agronomía"
    ],
    datasets: [
        {
            data: [150000, 230000, 1255000, 350000, 592650],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56",
                "#898989",
                "#86C440"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56",
                "#898989",
                "#86C440"
            ]
        }]



    }

});

var data2 = {
    labels: [
        "Red",
        "Blue",
        "Yellow"
    ],
    datasets: [
        {
            data: [300, 50, 100],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
        }]
};

</script>






    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= $this->Paginator->sort('id') ?></th>
                <th><?= $this->Paginator->sort('title') ?></th>
                <th><?= $this->Paginator->sort('description') ?></th>
                <th><?= $this->Paginator->sort('buy_order_id') ?></th>
                <th><?= $this->Paginator->sort('cost_center_id') ?></th>
                <th><?= $this->Paginator->sort('outcome_id') ?></th>
                <th><?= $this->Paginator->sort('period_month') ?></th>
                <th><?= $this->Paginator->sort('period_year') ?></th>
                <th><?= $this->Paginator->sort('date_created') ?></th>
                <th class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($budgets as $budget): ?>
            <tr>
                <td><?= $this->Number->format($budget->id) ?></td>
                <td><?= h($budget->title) ?></td>
                <td><?= h($budget->description) ?></td>
                <td><?= $budget->has('buy_order') ? $this->Html->link($budget->buy_order->provider_id, ['controller' => 'BuyOrders', 'action' => 'view', $budget->buy_order->provider_id]) : '' ?></td>
                <td><?= $budget->has('cost_center') ? $this->Html->link($budget->cost_center->title, ['controller' => 'CostCenters', 'action' => 'view', $budget->cost_center->id]) : '' ?></td>
                <td><?= $budget->has('outcome') ? $this->Html->link($budget->outcome->id, ['controller' => 'Outcomes', 'action' => 'view', $budget->outcome->id]) : '' ?></td>
                <td><?= $this->Number->format($budget->period_month) ?></td>
                <td><?= $this->Number->format($budget->period_year) ?></td>
                <td><?= h($budget->date_created) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $budget->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $budget->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $budget->id], ['confirm' => __('Are you sure you want to delete # {0}?', $budget->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
