<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Modular</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
		<?php
			echo $this->Html->css('style');
			echo $this->Html->css('jquery-ui.min');
			echo $this->Html->css('animate');
			echo $this->Html->css('/EasyAutocomplete-1.3.5/easy-autocomplete.min');
			echo $this->Html->css('/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min');
		?>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800' rel='stylesheet' type='text/css'>

	<script src="/modularwip/js/Chart.js"></script>


	</head>


	<body color="verde" class="verde1">

		<div class="navbar navbar-fixed-top">

<!-- ///////////////////////// MENÚ *//////////////////////////////////////////////-!-->

<!-- The overlay -->
<div id="myNav" class="overlayperfil">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>

  <!-- Overlay-->
  <div class="overlay-content">
  <div class="logochico"></div>
  <hr class="espaciador">

    <?= $this->Html->link('Mi Perfil', ['controller' => 'modales', 'action' => 'perfil'], ['data-toggle' => 'modal', 'data-target' => '#CambiarContrasena']);?>
    <?= $this->Html->link('Cambiar Contraseña', ['controller' => 'modales', 'action' => 'changePassword'], ['data-toggle' => 'modal', 'data-target' => '#MiPerfil']);?>
    <?= $this->Html->link('Cerrar Sesión', ['controller' => 'employees', 'action' => 'logout']);?>
    <?php if($this->request->session()->read('Auth.User.role') == 'ADMINISTRADOR'){ ?>
    	<?= $this->Html->link('Panel de Administración', ['controller' => 'escritorio', 'action' => 'index']);?>
	<?php } ?>
   </div>

</div>

<!-- Use any element to open/show the overlay navigation menu -->



<script>
/* Open when someone clicks on the span element */
function openNav() {
    document.getElementById("myNav").style.width = "100%";
}

/* Close when someone clicks on the "x" symbol inside the overlay */
function closeNav() {
    document.getElementById("myNav").style.width = "0%";
}
</script>


			<div onclick="openNav()" class="sandwich pull-right"></div>

<!-- ///////////////////////// FIN MENÚ //////////////////////////////////////////!-->




																																																																																																																																																																																																																																																																																																																																																								</div>
																																																																																																																																																																																																																																																																																																																																																								<?= $this->fetch('content') ?>
																																																																																																																																																																																																																																																																																																																																																								<!--Modal -->
																																																																																																																																																																																																																																																																																																																																																								<div class="modal fade nopadding" id="viaticos">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->
		<!--________________ Rendiciones ___________-->
		<!--Modal -->
		<div class="modal fade nopadding" id="rendicion">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="rendicion1">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->
		<!--Modal -->
		<div class="modal fade nopadding" id="rendicion2">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="rendicion_vista">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content">

			</div>
		  </div>
		</div>
		<!--/modal-->
		<!--________________ Compras  ___________-->
		<!--Modal -->
		<div class="modal fade nopadding" id="compras">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->
		<!--Modal -->
		<div class="modal fade nopadding" id="compras1">
		  <div class="modal-dialog modulomodal" nopadding>
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->
		<!--Modal -->
		<div class="modal fade nopadding" id="compras2">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->
		<!--Modal -->
		<div class="modal fade nopadding" id="generaroc">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="viewoc">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->


		<!--________________ Presupuesto  ___________-->

		<!--Modal -->
		<div class="modal fade nopadding" id="presupuesto">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>

		<!--/modal-->

		<!--________________ Pagos ______________-->

		<!--Modal -->
		<div class="modal fade nopadding" id="pagos1">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>

		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="honorarios">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>

		<!--/modal-->


		<!--Modal -->
		<div class="modal fade nopadding" id="crearinforme">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>

		<!--/modal-->


		<!--Modal -->
		<div class="modal fade nopadding" id="revisarmisinformes">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>

		<!--/modal-->


		<!--Modal -->
		<div class="modal fade nopadding" id="solicitaranticipohonorarios">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>

		<!--/modal-->


		<!--Modal -->
		<div class="modal fade nopadding" id="remuneraciones">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>

		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="remuneraciones2">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>

		<!--/modal-->

		<!--________________ Permisos  ___________-->




		<!--Modal -->
		<div class="modal fade nopadding" id="permisos">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="permisos1">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="revisarpermisos">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->


		<!--Modal -->
		<div class="modal fade nopadding" id="permisoadmin">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="permisohoras">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->



		<!--Modal -->
		<div class="modal fade nopadding" id="permisos2">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="feriadolegal">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--________________ Directorio______________-->


		<!--Modal -->
		<div class="modal fade nopadding" id="directorio">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->



		<!--________________ Herramientas ___________-->



		<!--Modal -->
		<div class="modal fade nopadding" id="herramientas">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="pendones">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<!--Modal -->
		<div class="modal fade nopadding" id="tarjetas">
		  <div class="modal-dialog modulomodal nopadding">
			<div class="modal-content ">

			</div>
		  </div>
		</div>
		<!--/modal-->

		<div class="modal fade nopadding" id="MiPerfil">
		  <div class="modal-dialog modulomodal ">
			<div class="modal-content miperfilcontent">

			</div>
		  </div>
		</div>

    <div class="modal fade nopadding" id="CambiarContrasena">
		  <div class="modal-dialog modulomodal ">
			<div class="modal-content miperfilcontent">

			</div>
		  </div>
		</div>



		<script src="/modularwip/js/jquery-3.0.0.min.js"></script>
		<script src="/modularwip/js/jquery-ui.js"></script>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script src="/modularwip/js/wow.js"></script>
		<script src="/modularwip/js/smooth-scroll.js"></script>

		<script type="text/javascript" src="/modularwip/js/jspdf.js"></script>
		<script type="text/javascript" src="/modularwip/js/jspdf.plugin.filesaver.js"></script>
		<script type="text/javascript" src="/modularwip/js/jspdf.plugin.autotable.js"></script>
		<script type="text/javascript" src="/modularwip/js/jspdf.plugin.from_html.js"></script>
		<script type="text/javascript" src="/modularwip/js/jspdf.plugin.split_text_to_size.js"></script>
		<script type="text/javascript" src="/modularwip/js/jspdf.plugin.standard_fonts_metrics.js"></script>

		<script type="text/javascript" src="/modularwip/js/jspdf.plugin.addimage.js"></script>
		<script type="text/javascript" src="/modularwip/js/jspdf.plugin.cell.js"></script>

		<!--<script type="text/javascript" src="http://files-stackablejs.netdna-ssl.com/stacktable.min.js"></script>-->


		<?php echo $this->Html->script('/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min'); ?>




		<script>


			$(document).ready(function(){
				resizeDiv();
				console.log("Ejecuto document ready");
				$('[data-toggle="modal1"]').click(function(e) {
					e.preventDefault();
					var url = $(this).attr('href');
					if (url.indexOf('#') == 0) {
						$(url).modal('open');
					} else {
						$.get(url, function(data) {
							$('<div class="modal hide fade">' + data + '</div>').modal();
						}).success(function() { $('input:text:visible:first').focus(); });
					}
				});
				$('body').on('submit', '.ajaxForm', function(e){
					e.preventDefault();
					console.log("El Submit");
					var form = $(this);
					var url = form.attr('action');
					var data = form.serialize();
					$.post(url, data, function(res){
						var op = $.parseJSON(res);
						if(op.exito){
							form.reset();
						}
					});
				});

				$('body').on('submit', '.formRendicion', function(e){
					e.preventDefault();
					var form = $(this);
					var url = form.attr('action');
					var data = form.serialize();
					$.post(url, data, function(res){
						var op = $.parseJSON(res);
						if(op.success){
							form[0].reset();
							var url = "<?= $this->Url->build(['controller' => 'modales', 'action' => 'loadViewRendicion']); ?>" + "/" + op.id_rendicion;
							console.log("La Url:", url);
							$('#rendicion1').find('#container').load(url);
						}
					});
				});

				$('body').on('submit', 'form.form-perfil', function(e){
					e.preventDefault();
					var form = $(this);
					var url = form.attr('action');
					var data = form.serialize();
					$.post(url, data, function(res){
						var op = $.parseJSON(res);
						if(op.exito){
							$('#ProfileSuccessMsg').show(200);
						}
					});
				});
			});
			smoothScroll.init({
				speed: 500, // Integer. How fast to complete the scroll in milliseconds
				easing: 'easeInOutCubic', // Easing pattern to use
				updateURL: false, // Boolean. Whether or not to update the URL with the anchor hash on scroll
				offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
				callback: function ( toggle, anchor ) {} // Function to run after scrolling
			});
			window.onresize = function(event) {
				resizeDiv();
			}

			function resizeDiv() {
				vpw = $(window).width();
				vph = $(window).height();
				vph5 = $(window).height() - vph/2;
				vph3 = $(window).height() - vph/2;

				$('.vph').css({'min-height': vph + 'px'});
				$('.vph2').css({'min-height': vph-(vph/10) + 'px'});
				$('.vph50').css({'min-height': vph-(vph/2) + 'px'});
				$('.vpw').css({'width': vpw + 'px'});
				$('.modulomodal').css({'width': vpw-(vpw/10) + 'px'});
				 $('.modulomodal').css({'max-height': vph-(vph/10) + 'px'});
				$('.centerh').css({'margin-left': (vpw/2)-([$('.centerh').width()]/2) + 'px'});
				$('.centerv').css({'margin-top': (vph/2)-([$('.centerv').height()]/2) + 'px'});

				$('.centercontentv1').css({'padding-top': (vph/2)-([$('.centercontent1').height()]/2) + 'px'});
				$('.centercontentv2').css({'padding-top': (vph/2)-([$('.centercontent2').height()]/2) + 'px'});

				$('.centerv2').css({'top': (vph/2)-([$('.centerv').height()]/2) + 'px'});

				$('#navadmin').remove();
			};
			new WOW().init();

			$.datepicker.regional['es'] = {
				closeText: 'Cerrar',
				prevText: '<Ant',
				nextText: 'Sig>',
				currentText: 'Hoy',
				monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
				monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
				dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
				dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
				dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
				weekHeader: 'Sm',
				dateFormat: 'yy/mm/dd',
				firstDay: 1,
				isRTL: false,
				showMonthAfterYear: false,
				yearSuffix: ''
			};
			$.datepicker.setDefaults($.datepicker.regional['es']);
			$(function () {
				$("#datepicker").datepicker();
			});
		</script>
		<?= $this->fetch('script'); ?>
	</body>


    <script>
$('#revisarpermisos').on('shown.bs.modal', function (event) {

		var ctxdiasadmin = $("#chartdiasadmin").get(0).getContext("2d");

        var myDoughnutChart1 = new Chart(ctxdiasadmin, {
			type: 'doughnut',
        	data: datadiasadmin,

        	options: {
        		responsive: true,
        			legend: {
        				display: true,
        				position: 'bottom'
        			}
        	}

        });


        var ctxpermisoshora = $("#chartpermisoshora").get(0).getContext("2d");

        var myDoughnutChart2 = new Chart(ctxpermisoshora, {
        	type: 'doughnut',
        	data: datapermisoshora,
        	        	options: {
        		responsive: true,
        			legend: {
        				display: true,
        				position: 'bottom'
        			}
        	}

        });


        var ctxferiadolegal = $("#chartferiadolegal").get(0).getContext("2d");

        var myDoughnutChart3 = new Chart(ctxferiadolegal, {
        	type: 'doughnut',
        	data: dataferiadolegal,
        	options: {
        		responsive: true,
        			legend: {
        				display: true,
        				position: 'bottom'
        			}
        	}


        });



});



    </script>

<link href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" />
<script src="http://code.jquery.com/jquery-1.12.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<link href="js/MonthPicker.css" rel="stylesheet" type="text/css" />


<script src="js/MonthPicker.js"></script>

		<script>
		  $( function() {
		    $( "#datepicker" ).datepicker();
		  } );
		</script>



</html>
