<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Modular';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('cake.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <script type="text/javascript" src="http://code.jquery.com/jquery-3.1.0.min.js"></script>
        <script src="js/Chart.js"></script> 

<script>
 $(document).ready(function(){
                resizeDiv();
            });

            window.onresize = function(event) {
                resizeDiv();
            }

            function resizeDiv() {
                vpw = $(window).width();
                vph = $(window).height();
                vph5 = $(window).height() - vph/2;
                       

                $('.vph').css({'min-height': vph + 'px'});
                           
                $('.vpw').css({'width': vpw + 'px'});

                $('.centerh').css({'margin-left': (vpw/2)-([$('.centerh').width()]/2) + 'px'});
                $('.centerv').css({'margin-top': (vph/2)-([$('.centerv').height()]/2) + 'px'});

   
    var ancho50 = $('.marginado').parent().width()/2 - 20;
    $('.ancho50').css({'width': ancho50 + 'px'});
   
};

        </script>




</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-3 medium-4 columns">
            <li class="brand">         
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <li class="notificacion_msj">99</li>
                <li class="sandwich"><a target="_blank"></a></li>
            </ul>
        </div>
    </nav>


    <?= $this->Flash->render() ?>
    <div class="container clearfix">

<nav id="navadmin" class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="escritorio-nav">
        <?= $this->Html->link(__('ESCRITORIO'), ['controller' => 'escritorio', 'action' => 'index']) ?>

        </li>
        <li class="presupuesto-nav">
            <?= $this->Html->link(__('PRESUPUESTO'), '#') ?>
        </li>
        <li class="viaticos-nav">
            <?= $this->Html->link(__('VIÁTICOS'), ['controller' => 'travel_provisions', 'action' => 'index']) ?>
        </li>
        <li class="rendiciones-nav"><?= $this->Html->link(__('RENDICIONES'), ['controller' => 'outcome-releases', 'action' => 'index']) ?>

        <li class="compras-nav">
            <?= $this->Html->link(__('COMPRAS'), ['controller' => 'buyOrders', 'action' => 'index']) ?>
        </li>

        <li class="pagos-nav"><a href="#">PAGOS</li></a> 
        <li class="permisos-nav">
            <?= $this->Html->link(__('PERMISOS'), ['controller' => 'OffDays', 'action' => 'index']) ?>
        </li>

        <hr class="hr-nav">

        <li class="usuarios-nav">
            <?= $this->Html->link(__('EMPLEADOS'), ['controller' => 'employees', 'action' => 'index']) ?>
            </li>

        <li class="proveedores-nav"><?= $this->Html->link(__('PROVEEDORES'), ['controller' => 'providers', 'action' => 'index']) ?></li>

        <li class="configuracion-nav"><a href="#">CONFIGURACIÓN</li></a>

        <hr class="hr-nav">

        <li class="utilidades-nav"><a href="#">UTILIDADES</li></a>

        <div class="modularicon col-md-12"></div>
    </ul>
</nav>
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
