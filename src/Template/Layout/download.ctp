<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Modular</title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
		<?php 
			echo $this->Html->css('style');
			echo $this->Html->css('jquery-ui.min');
			echo $this->Html->css('animate');
			echo $this->Html->css('/EasyAutocomplete-1.3.5/easy-autocomplete.min');
			echo $this->Html->css('/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min');
		?>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800' rel='stylesheet' type='text/css'>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="/modular/js/jspdf.js"></script>
		<script type="text/javascript" src="/modular/js/jspdf.plugin.filesaver.js"></script>
		<script type="text/javascript" src="/modular/js/jspdf.plugin.autotable.js"></script>
		<script type="text/javascript" src="/modular/js/jspdf.plugin.from_html.js"></script>
		<script type="text/javascript" src="/modular/js/jspdf.plugin.split_text_to_size.js"></script>
		<script type="text/javascript" src="/modular/js/jspdf.plugin.standard_fonts_metrics.js"></script>
		<script type="text/javascript" src="/modular/js/jspdf.plugin.addimage.js"></script>
		<script type="text/javascript" src="/modular/js/jspdf.plugin.cell.js"></script>
		<script type="text/javascript" src="https://files-stackablejs.netdna-ssl.com/stacktable.min.js"></script>
	</head>	
	<body color="verde" class="verde1">
		<?= $this->fetch('content') ?>
	</body>
</html>
