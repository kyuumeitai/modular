        <div id="pantalla1" class="col-md-12 vph">
            <hr class="espaciador">
            <div class="ceafblanco wow fadeInLeft"></div>
            <hr class="espaciador">
            <div class="logogrande wow fadeInLeft col-md-6a"></div>
            <hr class="espaciador">
            <p class="blanco wow fadeInLeft"> Versión 0.5</p>
            <hr class="espaciador">
            <h1 class="bienvenida col-md-6 nopadding wow fadeInLeft"> ¡Bienvenido!, esta es una muestra del sistema para CEAF</h1>
                <hr class="espaciador">
                <h1 class="bienvenidasub col-md-6 nopadding wow fadeInLeft">El nombre de proyecto es Modular, debido a su naturaleza que implementa todos los sistemas en módulos. Lo atractivo de esta forma, es que es una metodología aditiva, ¡sin límites!</h1>
       </div>
    <div class="col-md-12 bottom15 wow bounceInDown ">
        <a data-scroll href="#pantalla2"><div class="botondown centrar infinite wow pulse"></div></a>
    </div>
<div class="circulos col-md-pull-11 vph animated bounceInUp push-left">
    <ul class="centerv2 contcirc">
        <a data-scroll href="#pantalla1"><li class="circulonav"></li></a>
        <a data-scroll href="#pantalla2"><li class="circulonav"></li></a>
        <a data-scroll href="#pantalla3"><li class="circulonav"></li></a>
    </ul>
</div>


.<!-- Pantalla 1 -->
        <div id="pantalla2" class="col-md-9 centerh vph">
            <div id="modulos" class="col-md-12 centerv">
                <a href="mod_viaticos/viaticos.php" class="" data-toggle="modal" data-target="#viaticos">
                <div id="modulo1" class="col-md-8 modulo">
                    
                        <li class="moduloli">
                            <p class="modtexto">Solicitud de Viáticos</p>
                            
                        </li>
                        </a>
                    </div>
               <a href="mod_rendiciones/rendicionsel.php" class="" data-toggle="modal" data-target="#rendicion">
                <div id="modulo2" class="col-md-4 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Rendiciones</p>
                            
                    </li>
                        </a>
                </div>

                <a href="mod_compras/comprassel.php" class="" data-toggle="modal" data-target="#compras">
                <div id="modulo3" class="col-md-3 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Compras</p>
                            
                    </li>
                        </a>
                </div>

                
                <a href="mod_presupuesto/presupuesto.php" class="" data-toggle="modal" data-target="#presupuesto">
                <div id="modulo4" class="col-md-3 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Presupuesto</p>
                </li>
                        </a>
                </div>
                <a href="mod_pagos/pagos1.php" class="" data-toggle="modal" data-target="#pagos1">
                <div id="modulo5" class="col-md-3 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Pago a Colaboradores</p>
                            
                    </li>
                        </a>
                </div>

                <a href="mod_permisos/permisos1.php" class="" data-toggle="modal" data-target="#permisos">
                <div id="modulo6" class="col-md-3 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Permisos</p>
                            
                    </li>
                        </a>
                </div>

            </div>
        </div>
</div>
<!-- Pantalla 2 -->
    <div id="pantalla3" class="col-md-9 centerh vph">
    <hr class="espaciador2"><hr class="espaciador2">
                <div id="modulos" class="col-md-12 centerv">

                 <a href="mod_directorio/directorio.php" data-toggle="modal" data-target="#directorio">
                    <div id="directoriomod" class="modulo col-md-4">
                        <li class="moduloli">
                            <p class="modtexto">Directorio</p>
                        </li>
                    </div>
                </a>

                <a href="mod_herramientas/herramientas.php" data-toggle="modal" data-target="#herramientas">
                    <div id="herramientasmod" class="modulo col-md-4">
                        <li class="moduloli">
                            <p class="modtexto">Herramientas</p>
                        </li>
                    </div>
                </a>

                <a href="ajustesmod.php" class="" data-toggle="modal" data-target="#ajustes">
                    <div id="ajustesmod" class="modulo col-md-4">
                        <li class="moduloli">
                            <p class="modtexto">Ajustes</p>
                        </li>
                    </div>
                </a>

                </div> <!--Modulos-->

    </div>
<!-- Pantalla 3 -->
<script>
    smoothScroll.init({
    speed: 500, // Integer. How fast to complete the scroll in milliseconds
    easing: 'easeInOutCubic', // Easing pattern to use
    updateURL: false, // Boolean. Whether or not to update the URL with the anchor hash on scroll
    offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    callback: function ( toggle, anchor ) {} // Function to run after scrolling
});
</script>



<script>
$(document).ready(function() {
    
// Support for AJAX loaded modal window.
// Focuses on first input textbox after it loads the window.
$('[data-toggle="modal1"]').click(function(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    if (url.indexOf('#') == 0) {
        $(url).modal('open');
    } else {
        $.get(url, function(data) {
            $('<div class="modal hide fade">' + data + '</div>').modal();
        }).success(function() { $('input:text:visible:first').focus(); });
    }
});
    
});