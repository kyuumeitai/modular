<?php
session_start();
 // store session data
?>
<!doctype html>
<html><head>
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/jquery.json.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800' rel='stylesheet' type='text/css'>

    <script>
        
        var pais = 1;
        var comuna = 2;
        var lugar = 3;
        var objetivo = 3;

        window.onresize = function(event) {
        	resizeDiv();
        }

        function resizeDiv() {
        	vpw = $(window).width();
        	vph = $(window).height();
        	vph5 = $(window).height() - vph/2;
        	vph3 = $(window).height() - vph/2;

        	$('.vph').css({'min-height': vph + 'px'});
        	$('.vph2').css({'min-height': vph-(vph/10) + 'px'});
        	$('.vph50').css({'min-height': vph-(vph/2) + 'px'});
        	$('.vpw').css({'width': vpw + 'px'});
        	$('.centerh').css({'margin-left': (vpw/2)-([$('.centerh').width()]/2) + 'px'});
        	$('.centerv').css({'margin-top': (vph/2)-([$('.centerv').height()]/2) + 'px'});
        }
    </script>



<meta charset="utf-8">
<title>Viáticos</title>
<body color="verde">


<div id="container" class="col-md-12">

    <div class="col-md-10 col-md-offset-1 col-xs-12 wrapper">
            <div id="fecha" class="col-md-3 pull-right">
                <table class="table table-bordered table-bordered">
                    <thead>
                        <tr>
                            <th class ="th-verde"> Fecha </th>
                            <th class ="th-verde"> Folio nº </th>
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="fecha"> 19/08/15 </td>
                        <td id="folio"> 1 </td>
                    </tr>
                </tbody>
            </table>
           </div>
        <div id="nombrerut" class="col-md-12 table-responsive ">        
            <table class="table table-bordered table-bordered">
                    <thead>
                        <tr>
                            <th class ="th-verde"> Nombre Beneficiario </th>
                            <th class ="th-verde"> RUT </th>
                            <th class ="th-verde"> DV </th>
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="nombre"> Nombre </td>
                        <td id="rut"> Rut </td>
                        <td id="dv"> DV </td>
                    </tr>
                </tbody>
            </table>
        </div><!--/nombrerut -->

        <div id="destinotiempo" class="col-md-12">
            <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class ="th-verde"> Ciudad(es) Destino </th>
                                <th class ="th-verde"> Region(es) Destino </th>
                                <th class ="th-verde"> País(es) Destino</th>
                                <th class ="th-verde"> Desde </th>
                                <th class ="th-verde"> Hasta </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="ciudades">Ciudades Visitadas</td>
                            <td id="regiones">Regiones</td>
                            <td id="paises">Paises</td>
                            <td id="fechasalida">Fecha Salida</td>
                            <td id="fecharegreso">Fecha Regreso </td>
                        </tr>
                    </tbody>
                </table>
        </div><!--/destinotiempo -->


        <div id="Motivo" class="col-md-12">
                   <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class ="th-verde"> Motivo del Viaje </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="motivo"> Motivo </td>
                        </tr>
                    </tbody>
                </table>
        </div><!--/motivo -->
        

        <div id="lineacentro" class="col-md-12">
                   <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class ="th-verde"> Línea de Investigación </th>
                                <th class ="th-verde"> Código Centro </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="linea"> Gestión </td>
                            <td id="codigo"> CEAF Continuidad </td>
                        </tr>
                    </tbody>
                </table>
        </div><!--/lineacentro -->

        <div id="montos" class="col-md-12">
                   <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th class ="th-verde"> Montos Diarios </th>
                                <th class ="th-verde"> Nº Días </th>
                                <th class ="th-verde"> Subtotal </th>
                                <th class ="th-verde"> Total Viáticos Asignados </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="montosdiarios"> Sinpernoctar/Pernoctando </td>
                            <td id="días"> 1/4 </td>
                            <td id="Subtotal"> Cualquier de Plata </td>
                            <td id="totalviatico"> Mansa Cachaíta </td>
                        </tr>
                    </tbody>
                </table>
        </div><!--/lineacentro -->
</div><!--Wrapper-->

</body>
</html>