<?php
session_start();
 // store session data
?>
<!doctype html>
<html><head>
<script src="js/jquery-2.1.4.min.js"></script>
<script src="js/jquery.json.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800' rel='stylesheet' type='text/css'>

    <script>
        
        var pais = 1;
        var comuna = 2;
        var lugar = 3;
        var objetivo = 3;

        window.onresize = function(event) {
        	resizeDiv();
        }

        function resizeDiv() {
        	vpw = $(window).width();
        	vph = $(window).height();
        	vph5 = $(window).height() - vph/2;
        	vph3 = $(window).height() - vph/2;

        	$('.vph').css({'min-height': vph + 'px'});
        	$('.vph2').css({'min-height': vph-(vph/10) + 'px'});
        	$('.vph50').css({'min-height': vph-(vph/2) + 'px'});
        	$('.vpw').css({'width': vpw + 'px'});
        	$('.centerh').css({'margin-left': (vpw/2)-([$('.centerh').width()]/2) + 'px'});
        	$('.centerv').css({'margin-top': (vph/2)-([$('.centerv').height()]/2) + 'px'});
        }
    </script>



<meta charset="utf-8">
<title>Viáticos</title>
<body color="verde">


<div id="container" class="col-md-12">

    <div class="col-md-10 col-md-offset-1 col-xs-12 wrapper">
           
        <div id="presentado" class="col-md-12 table-responsive ">
        <h3 class="titulo verde"> Presentado Por </h3>
        
            <table class="table table-bordered table-bordered">
                    <thead>
                        <tr>
                            <th> Rut </th>
                            <th> DV </th>
                            <th> Nombre </th>
                            <th> Linea Asociada </th>
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="rut"> Rut </td>
                        <td id="dv"> DV </td>
                        <td id="nombre"> Nombre </td>
                        <td id="linea"> Línea </td>
                    </tr>
                </tbody>
            </table>
        </div><!--/Presentado -->

        <div id="identificacion" class="col-md-12">
            <h3 class="titulo verde"> Identificación del Centro / Proyecto </h3>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th> Fecha Salida </th>
                                <th> Fecha Regreso </th>
                                <th> Comuna(S) Visitada(s)</th>
                                <th> Region(es) Visitada(s)</th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="fechasalida">Fecha Salida</td>
                            <td id="fecharegreso">Fecha Regreso </td>
                            <td id="comunas">Comunas Visitadas</td>
                            <td id="regiones">Regiones</td>
                        </tr>
                    </tbody>
                </table>
        </div><!--/Identificación -->

        <div id="comision" class="col-md-12">
            <h3 class="titulo verde"> Comisión de Servicio Asociada </h3>
                   <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th> Centro / Proyecto </th>
                                <th> Código Centro / Proyecto </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="centroproyecto">Centro, Proyecto</td>
                            <td id="codigocentro"> Código Centro </td>
                        </tr>
                    </tbody>
                </table>
        </div><!--/Comision -->
        
        <div id="detalles" class="col-md-12">
            <h3 class="titulo verde"> Detalles Técnicos de la Comisión de Servicios </h3>
                   <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th colspan="3"> Lugar(es) Visitado(s) </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="lugares"> Lugares</td>
                            <td id="lugares"> Lugares</td>
                            <td id="lugares"> Lugares</td>
                        </tr>
                    </tbody>
                          <thead>
                            <tr>
                                <th colspan="3"> Motivo del Viaje </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td colspan="3" id="motivo"> Motivo</td>
                        </tr>
                    </tbody>
                     <thead>
                            <tr>
                                <th colspan="3"> Objetivos del Viaje </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="objetivos"> Objetivos del Viaje </td>
                            <td id="objetivos"> Objetivos del Viaje </td>
                            <td id="objetivos"> Objetivos del Viaje </td>
                        </tr>
                    </tbody>
                </table>

                      <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th> Observaciones </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="Observaciones"> Acá las Observaciones </td>
                        </tr>
                    </tbody>
        </div><!--/Comision -->

        </div><!--/tabladatos .Wrapper-->
    </div><!--/datos-->


</body>
</html>