<?php
require_once('tcpdf/config/tcpdf_config_alt.php');

global $l;
$l = Array();
// PAGE META DESCRIPTORS --------------------------------------

$l['a_meta_charset'] = 'UTF-8';
$l['a_meta_language'] = 'es';
$dpi="300";
// TRANSLATIONS --------------------------------------
$l['w_page'] = 'página';

// Include the main TCPDF library (search the library on the following directories).
$tcpdf_include_dirs = array(
	realpath('tcpdf/tcpdf.php')
);
foreach ($tcpdf_include_dirs as $tcpdf_include_path) {
	if (@file_exists($tcpdf_include_path)) {
		require_once($tcpdf_include_path);
		break;
	}
}
$ancho = $_GET['ancho'];
$alto = $_GET['alto'];
// set page format (read source code documentation for further information)
// MediaBox - width = urx - llx 210 (mm), height = ury - lly = 297 (mm) this is A4
$page_format = array(
    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => $ancho, 'ury' => $alto),
    //'CropBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 297),
    //'BleedBox' => array ('llx' => 5, 'lly' => 5, 'urx' => 205, 'ury' => 292),
    //'TrimBox' => array ('llx' => 10, 'lly' => 10, 'urx' => 200, 'ury' => 287),
    //'ArtBox' => array ('llx' => 15, 'lly' => 15, 'urx' => 195, 'ury' => 282),
    'Dur' => 3,
    'trans' => array(
        'M' => 'O'
		
    ),
    'Rotate' => 0,
    'PZ' => 1,
);

// Check the example n. 29 for viewer preferences

// add first page ---
$pdf = new TCPDF('P', PDF_UNIT, $page_format, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Alex Acuña Viera - Felipe Acuña Viera');
$pdf->SetTitle('Tarjeta de Presentación');

// set margins
$pdf->SetMargins(0, 0);

$pdf->SetAutoPageBreak(TRUE, 0);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
$pdf->setLanguageArray($l);

// ---------------------------------------------------------
$titulo1 = $_GET['titulo1'];
$titulo2 = $_GET['titulo2'];
$margenY = $_GET['margeny'];
$margenFooter = $_GET['margenfooter'];

$t1t = $_GET['t1t'];
$t2t = $_GET['t2t'];
$idiomat = $_GET['idiomat'];
$anchop = $_GET['anchop'];


$idioma =  $_GET['idioma'];
strip_tags($idioma, '<br><br/>');
$pendon = $_GET['pendon'];

$margenX = ($ancho - $anchop)/2;
$margenX2 = ($ancho - 600)/2;
function br2nl( $idioma ) {
 return preg_replace('/<br(\s+)?\/?>/i', "\n", $idioma);
}


// add a page
$pdf->AddPage();
	$pdf->ImageSVG($file=$pendon, $x=0, $y=0, $w=$ancho, $h=$alto, $link='#', $align='left', $palign='', $border=0, $fitonpage=true);
	$pdf->SetFont('bebasregular', '', $t1t);
	$pdf->SetY($margenY);
	$pdf->SetX($margenX);
	$txt = br2nl ($titulo1);
	$pdf->setCellHeightRatio(1);
	$pdf->MultiCell($anchop, 0, $txt, 0, 'C', 0, 1, '', '', false);
	$pdf->setCellHeightRatio(0.1);
	$pdf->MultiCell(1, 1, '', 0, 'C', 0, 1, '', '', false);
	$pdf->SetFont('bebasbook', '', $t2t);
	$pdf->SetX($margenX);
	$txt = br2nl ($titulo2);
	$pdf->setCellHeightRatio(1);
	$pdf->MultiCell($anchop, 0, $txt, 0, 'C', 0, 1, '', '', false);
	//idioma

	$txt = br2nl ($idioma);
	$pdf->SetFont('opensans', '', $idiomat);
	$pdf->SetY($margenFooter);
	$pdf->SetX($margenX2);
	$pdf->SetTextColor(255);
	$pdf->setCellHeightRatio(1.2);
	$pdf->MultiCell(600, 0, $txt, 0, 'C', 0, 1, '', '', true);
	
	

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('pendon.pdf', 'D');
//============================================================+
// END OF FILE
//============================================================+