<?php
session_start();
 // store session data
?>
<!doctype html>
<html><head>

<meta charset="utf-8">
<title>Viáticos</title>
<body color="gris">
   <form id="ViaticosAddForm" method="post" action="viaticos/add">

     <h1 class="titulo">Solicitud de Viáticos <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div id="contenedorpais" class="col-md-6 contenedor">


<!--País y Regiones, Comunas y Lugares-->

    <div class="col-md-12 nopadding">
        <h1 class="titulo2 separado">País, Regiones y Comunas</h1>
    </div>
 
    <div id="paisregiones" class="col-md-12 col-xs-12 wrapper noseparado">

        <div id="paisregion" class="col-md-12">
            <h3 class="titulo col-md-12">País y Región</h1>
            
            <input class="col-md-6 seleccion" type="text" id="pais" name="pais" placeholder="País">
            <input class="col-md-6 seleccion" type="text" id="region" name="region" placeholder="Región">

        </div><!--Paisregion-->
    
    <div id="comuna1" class="col-md-12">
        <h3 class="titulo">Comunas <input type="button" class="nopadding boton-mas2 boton_comuna pull-right" id="agregar_comuna" onclick="add_comuna();" value="+" /></h3>

        <input class="col-md-6 seleccion" type="text" id="comuna" name="comuna" placeholder="Comuna Visitada">
        <input class="col-md-6 seleccion borrar " type="text" id="comuna" name="comuna" placeholder="Comuna Visitada">
    </div> <!--Comuna1-->

    <div id="lugaresvisitados" class="col-md-12">
       <h3 class="titulo">Lugares Visitados <input type="button" class="nopadding boton-mas2 boton_lugar pull-right" id="agregar_lugar" onclick="add_lugar();" value="+" /></h3>
            <input class="col-md-6 seleccion" type="text" id="lugar" name="lugar" placeholder="Lugar Visitado" required>
            <input class="col-md-6 seleccion borrar" type="text" id="lugar" name="lugar" placeholder="Lugar Visitado">
            <input class="col-md-6 seleccion borrar" type="text" id="lugar" name="lugar" placeholder="Lugar Visitado">
            
    </div> <!--lugaresvisitados-->
</div><!--Wrapper-->
</div>

        
</div>

<div id="contenedorperiodo" class="col-md-6 nopadding contenedor ">
<!--/País Regiones, Comunas y lugares-->
    <div id="datos" class="col-md-12">
            
            <div id="tabladatos"class="col-md-12 wrapper">

            
                <div class="col-md-12">
                    <h3 class="titulo">Periodo desde/hasta</h3>

                    <input class="col-md-6 seleccion" type="date" id="periodo" name="desde" placeholder="Desde" required>
                    <input class="col-md-6 seleccion" type="date" id="periodo" name="hasta" placeholder="Hasta" required>
                </div>
                <div class="col-md-12">
                    <div class="col-md-6 nopadding">
                        <h3 class="titulo">Tipo de Locomoción</h3>

                        <input class="col-md-12 seleccion" type="text" id="locomocion" name="locomocion" placeholder="Tipo de Locomoción" required>
                    </div>
                    

                    <div class="col-md-6 nopadding">
                        <h3 class="titulo">Proyecto</h3>

                        <input class="col-md-12 seleccion" type="text" id="proyecto" name="proyecto" placeholder="Proyecto" required>
                    </div>
                </div>
                <div class="col-md-12">
                    <h3 class="titulo">Motivo del Viaje</h3>

                    <input class="col-md-12 seleccion" type="text" id="motivo" name="motivo" placeholder="Motivo del Viaje" required>
                </div>

                <div id="objetivodiv" class="col-md-12">
                    <h3 class="titulo">Objetivos del Viaje <input type="button" class="nopadding boton-mas2 boton_objetivo pull-right" id="agregar_objetivo" onclick="add_objetivo();" value="+" /></h3>


                    <input class="col-md-12 seleccion" type="text" id="objetivos" name="objetivos" placeholder="Objetivos del Viaje" required>
                    <input class="col-md-12 seleccion borrar" type="text" id="objetivos" name="objetivos" placeholder="Objetivos del Viaje">
                    <input class="col-md-12 seleccion borrar" type="text" id="objetivos" name="objetivos" placeholder="Objetivos del Viaje">
                </div>

                <div class="col-md-6">
                    <h3 class="titulo">Total de Días</h3>

                    <input class="col-md-6 seleccion" type="number" id="dias_sinper" name="dias_sinper" placeholder="Sin Pernoctar" required>
                    <input class="col-md-6 seleccion" type="number" id="dias_per" name="dias_per" placeholder="Pernoctando" required>
                </div>

                <div class="col-md-6">
                    <h3 class="titulo">Monto total del Viático</h3>

                    <input class="col-md-12 seleccion" type="number" id="monto" name="monto" placeholder="Monto" required>
                </div>

    </div><!--/tabladatos .Wrapper-->
 <input type="submit" class="btn btn-primary col-md-5" value="Ingresar Solicitud"/>
</div><!--/datos-->
</div>
<script>
</script>
<script>
jQuery(function($) {
 
  
  // /////
  // Botón X
  function tog(v){return v?'addClass':'removeClass';} 
  $(document).on('input', '.borrar', function(){
    $('this')[tog(this.value)]('borrar');
  }).on('mousemove', '.borrar', function( e ){
    $('.borrar')[tog(this.offsetWidth-47 < e.clientX-this.getBoundingClientRect().left)]('onBorrar');   
  }).on('touchstart click', '.onBorrar', function( ev ){
    $(this).remove(); x--;
  });
  
  
});
</script>
<script>
 $('h1, h2, h3, h4').addClass('gris');
function elegir_verde(){
        $('body').attr('color','verde');
        $('h1, h2, h3, h4').addClass('verde');
        $('h1, h2, h3, h4').removeClass('gris');
        $(':button').addClass('verde');
        $(':button').removeClass('gris');
        var color = "verde";

    };
function elegir_gris(){
        $('body').attr('color','gris');
        $('h1, h2, h3, h4').addClass('gris');
        $('h1, h2, h3, h4').removeClass('verde');
        $(':button').addClass('gris');
        $(':button').removeClass('verde');
        var color = "gris";
    };

    
    
</script>

</div><!--Contenedor-->

<hr class="dashed">
    
<script>
	function submitter(){
		document.getElementById("selecciones").submit();
		}
</script>


</div>
<div id="impresora" class="hidden">

</div>


</div>
</div> <!--wrapper 2-->

</form>
</body>
</html>