<?php
session_start();
 // store session data
?>

<title>Rendiciones</title>
<h1 class="titulo">Permiso por horas<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor nopadding nomargin">  

 <script>
$( "#fechapermisohoras" ).datepicker({
    inline: true
});
  </script>
  

    <div class="col-md-12 wrapper">
            <div class="col-md-4 col-md-offset-4 ">
            <table class="table">
                <tr class="">
                
                    <td><label for="fechapermisohoras" class="col-md-6">Fecha</label></td>
                    <td><input class="col-md-6" type="text" id="fechapermisohoras" name="fecha" required></td>
                
                </tr>
                
                <br/>

                <tr class="">
                    <td><label for="desde" class="col-md-6"> Desde</label></td>
                    <td><input class="col-md-6" type="time" id="desde" name="desde" required></td>
                
                </tr>
                
                <br/>

                <tr class="">
                    <td><label for="hasta" class="col-md-6"> Hasta</label></td>
                    <td><input class="col-md-6" type="time" id="hasta" name="hasta" required></td>
                
                </tr>
            </table>
            </div>
            <hr class="espaciador2">
            <hr class="espaciador2">
            <hr class="espaciador2">
            <hr class="espaciador2">
 <input type="submit" class="botongrande verde col-md-8 col-md-offset-2"  value="Ingresar Solicitud"/>
    </div>

    <script>
    $(document).ready(function() {
        $("input:radio:checked").next('label').addClass("checked");
    });
    </script>
    
</div> <!--contenedor-->