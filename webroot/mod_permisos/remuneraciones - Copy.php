<?php
session_start();
 // store session data
?>

	<script src="js/Chart.js"></script>	
	<script src="js/legend.js"></script>
	
		


  
<title>Datos</title>
<h1 class="titulo">Remuneraciones<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor nopadding nomargin">  
 
		
<div class="container">

 <table id ="tabla" class="table table-bordered table-bordered">
                    <thead>
                        <tr>
                            <th> RUT </th>
                            <th> DV </th>
                            <th> Nombres </th>
                            <th> Ap. Paterno </th>
                            <th> Ap. Materno </th>
                            <th> C.Costo </th>
                            <th> D/Mes </th>
                            <th> S.Base </th>
                            <th> AFP </th>
                            <th> Salud </th>
                            <th> Cesantía </th>
                            <th> IMPTO </th>
                            <th> DCTO1 </th>
                            <th> DCTO2 </th>
                            <th> DCTO3 </th>
                            <th> Liquido </th>
                            <th> SIS </th>
                            <th> Mutual </th>
                            <th> Cesantía </th>
                          
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="rut"></td>
                        <td id="dv"></td>
                        <td id="nombres"></td>
                        <td id="appaterno"></td>
                        <td id="apmaterno"></td>
                        <td id="ccosto"></td>
                        <td id="dmes"></td>
                        <td id="sbase"></td>
                        <td id="afp"></td>
                        <td id="salud"></td>
                        <td id="cesantia"></td>
                        <td id="impto"></td>
                        <td id="dcto1"></td>
                        <td id="dcto2"></td>
                        <td id="dcto3"></td>
                        <td id="liquido"></td>
                        <td id="sis"></td>
                        <td id="mutual"></td>
                        <td id="cesantia"></td>
                    </tr>


                 </tbody>
            </table>


				<div class="col-md-4">

					<h2 class="modularh2 text-center">Días Administrativos</h2>
					<hr class="espaciador">

					<canvas id="chartdiasadmin"></canvas>
					<div id="leyendadiasadmin"></div>
				</div>

				
				<div class="col-md-4">
				<h2 class="modularh2 text-center"> Permisos por hora (Mes Actual)</h2>
				<hr class="espaciador">

					<canvas id="chartpermisoshora"></canvas>
					<div id="leyendapermisoshora"></div>
				</div>




				<div class="col-md-4">
					<h2 class="modularh2 text-center">Feriado Legal</h2>
					<hr class="espaciador">

					<canvas id="chartferiadolegal"></canvas>
					<div id="leyendaferiadolegal"></div>
				</div>

				<hr class="espaciador">

				<a href="#" class="col-md-4">
					<input type="submit" class="botongrande verde col-md-12"  value="Solicitar"/>
				</a>

				<a href="#" class="col-md-4">
					<input type="submit" class="botongrande verde col-md-12"  value="Solicitar"/>
				</a>

				<a href="#" class="col-md-4">
					<input type="submit" class="botongrande verde col-md-12"  value="Solicitar"/>
				</a>

<hr class="espaciador2">




	 

	</div>

<script>
var datadiasadmin = [{	
	value: 2.5,
	color: "#8DC63F",
	highlight: "#96D343",
	label: "Disponibles"
}, {
	
	value: 3.5,
	color: "#757575",
	highlight: "#A5A5A5",
	label: "Ocupados"
}

];
var datapermisoshora = [{
	value: 3,
	color: "#F7464A",
	highlight: "#EA2828",
	label: "Disponibles"
}

];

var dataferiadolegal = [{
	value: 2,
	color: "#46BFBD",
	highlight: "#00FFF2",
	label: "Disponibles"
}, {
	
	value: 13,
	color: "#757575",
	highlight: "#A5A5A5",
	label: "Ocupados"
}

];
</script>
	

<!--Doughnut-->

	</body>
</html>

</div> <!--contenedor-->