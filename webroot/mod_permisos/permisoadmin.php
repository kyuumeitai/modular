<?php
session_start();
 // store session data
?>

<title>Rendiciones</title>
<h1 class="titulo">Permiso Administrativo<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor nopadding nomargin">  

 <script>
$( "#fechaadmin" ).datepicker({
    inline: true
});
  </script>
  

    <div class="col-md-12 wrapper">
            <div class="seleccionmodulos3 centrar">
           
           <form>
            
            <table class="table">
                <tr class="col-md-4 col-md-offset-4">
                    <td><label for="fechaadmin" class="col-md-6">Fecha</label></td>
                    <td><input class="col-md-6" type="text" id="fechaadmin" name="fechaadmin" required></td>
                </tr>
            </table>
            
            <div id="permisoselect" class="btn-group col-md-12 nopadding" data-toggle="buttons">

                <label id="permisomediama" class="permisomediama nopadding  btn">
                    <input type="radio" name="permiso" id="mediama" value="mediama" autocomplete="off" checked>
                </label>
                <label id="permisomediatar" class="permisomediatar nopadding btn">
                    <input type="radio" name="permiso" id="mediatar" value="mediatar" autocomplete="off">
                </label>
                <label id="permisodiacom" class="permisodiacom nopadding btn">
                    <input type="radio" name="permiso" id="diacom" value="diacom" autocomplete="off">
                </label>
    <hr class="espaciador2">
            <input type="submit" class="botongrande verde col-md-12"  value="Ingresar Solicitud"/>
          </div>



            </div>
        
            </form>
    </div>
    <script>
    $(document).ready(function() {
        $('input#type-medio-admin').attr('checked', true)
        $("input:radio:checked").next('label').addClass("checked");
    });
    </script>
    
</div> <!--contenedor-->