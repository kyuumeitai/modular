<?php
session_start();
 // store session data
?>

	<script src="js/Chart.js"></script>	
	<script src="js/legend.js"></script>
	
		


  
<title>Datos</title>
<h1 class="titulo">Remuneraciones<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor nomargin">  
 
		

<div class="col-md-12">
 <table id ="tablaremuneraciones" class="table table-bordered">
                    <thead>
                        <tr>
                            <th> RUT </th>
                            <th> DV </th>
                            <th> Nombres </th>
                            <th> Ap. Paterno </th>
                            <th> Ap. Materno </th>
                            <th> C.Costo </th>
                            <th> D/Mes </th>
                            <th> S.Base </th>
                            <th> AFP </th>
                            <th> Salud </th>
                            <th> Cesantía </th>
                            <th> IMPTO </th>
                            <th> DCTO1 </th>
                            <th> DCTO2 </th>
                            <th> DCTO3 </th>
                            <th> Liquido </th>
                            <th> SIS </th>
                            <th> Mutual </th>
                            <th> Cesantía </th>
                          
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="rut"></td>
                        <td id="dv"></td>
                        <td id="nombres"></td>
                        <td id="appaterno"></td>
                        <td id="apmaterno"></td>
                        <td id="ccosto"></td>
                        <td id="dmes"></td>
                        <td id="sbase"></td>
                        <td id="afp"></td>
                        <td id="salud"></td>
                        <td id="cesantia"></td>
                        <td id="impto"></td>
                        <td id="dcto1"></td>
                        <td id="dcto2"></td>
                        <td id="dcto3"></td>
                        <td id="liquido"></td>
                        <td id="sis"></td>
                        <td id="mutual"></td>
                        <td id="cesantia"></td>
                    </tr>
                    <tr>
                        <td id="rut"></td>
                        <td id="dv"></td>
                        <td id="nombres"></td>
                        <td id="appaterno"></td>
                        <td id="apmaterno"></td>
                        <td id="ccosto"></td>
                        <td id="dmes"></td>
                        <td id="sbase"></td>
                        <td id="afp"></td>
                        <td id="salud"></td>
                        <td id="cesantia"></td>
                        <td id="impto"></td>
                        <td id="dcto1"></td>
                        <td id="dcto2"></td>
                        <td id="dcto3"></td>
                        <td id="liquido"></td>
                        <td id="sis"></td>
                        <td id="mutual"></td>
                        <td id="cesantia"></td>
                    </tr>
                    <tr>
                        <td id="rut"></td>
                        <td id="dv"></td>
                        <td id="nombres"></td>
                        <td id="appaterno"></td>
                        <td id="apmaterno"></td>
                        <td id="ccosto"></td>
                        <td id="dmes"></td>
                        <td id="sbase"></td>
                        <td id="afp"></td>
                        <td id="salud"></td>
                        <td id="cesantia"></td>
                        <td id="impto"></td>
                        <td id="dcto1"></td>
                        <td id="dcto2"></td>
                        <td id="dcto3"></td>
                        <td id="liquido"></td>
                        <td id="sis"></td>
                        <td id="mutual"></td>
                        <td id="cesantia"></td>
                    </tr>

                    <tr>
                        <td id="rut"></td>
                        <td id="dv"></td>
                        <td id="nombres"></td>
                        <td id="appaterno"></td>
                        <td id="apmaterno"></td>
                        <td id="ccosto"></td>
                        <td id="dmes"></td>
                        <td id="sbase"></td>
                        <td id="afp"></td>
                        <td id="salud"></td>
                        <td id="cesantia"></td>
                        <td id="impto"></td>
                        <td id="dcto1"></td>
                        <td id="dcto2"></td>
                        <td id="dcto3"></td>
                        <td id="liquido"></td>
                        <td id="sis"></td>
                        <td id="mutual"></td>
                        <td id="cesantia"></td>
                    </tr>

                    <tr>
                        <td id="rut"></td>
                        <td id="dv"></td>
                        <td id="nombres"></td>
                        <td id="appaterno"></td>
                        <td id="apmaterno"></td>
                        <td id="ccosto"></td>
                        <td id="dmes"></td>
                        <td id="sbase"></td>
                        <td id="afp"></td>
                        <td id="salud"></td>
                        <td id="cesantia"></td>
                        <td id="impto"></td>
                        <td id="dcto1"></td>
                        <td id="dcto2"></td>
                        <td id="dcto3"></td>
                        <td id="liquido"></td>
                        <td id="sis"></td>
                        <td id="mutual"></td>
                        <td id="cesantia"></td>
                    </tr>

                    <tr>
                        <td id="rut"></td>
                        <td id="dv"></td>
                        <td id="nombres"></td>
                        <td id="appaterno"></td>
                        <td id="apmaterno"></td>
                        <td id="ccosto"></td>
                        <td id="dmes"></td>
                        <td id="sbase"></td>
                        <td id="afp"></td>
                        <td id="salud"></td>
                        <td id="cesantia"></td>
                        <td id="impto"></td>
                        <td id="dcto1"></td>
                        <td id="dcto2"></td>
                        <td id="dcto3"></td>
                        <td id="liquido"></td>
                        <td id="sis"></td>
                        <td id="mutual"></td>
                        <td id="cesantia"></td>
                    </tr>


                 </tbody>
            </table>
</div>


            <!--AFP-->
			<div class="col-md-3">

					<h2 class="modularh2 text-center">AFP</h2>
					<hr class="espaciador">

            <!--Habitat-->
                <h1 class="col-md-6"> Habitat </h1>
                        <div class="progress col-md-6 nopadding">
                            <div class="progress-bar progress-bar-ceaf" role="progressbar" aria-valuenow="72"
                            aria-valuemin="0" aria-valuemax="100" style="width:72%">
                            </div>     
                            <span>$724.324</span>
                        </div>
            <!--Plan Vital-->

             <h1 class="col-md-6"> Plan Vital </h1>
                       <div class="progress col-md-6 nopadding">
                            <div class="progress-bar progress-bar-ceaf" role="progressbar" aria-valuenow="39"
                            aria-valuemin="0" aria-valuemax="100" style="width:39%">

                            </div>
                            <span>$390.612</span>

                        </div>

            <!--Cuprum-->

            <h1 class="col-md-6"> Cuprum </h1>
                        <div class="progress col-md-6 nopadding">
                            <div class="progress-bar progress-bar-ceaf" role="progressbar" aria-valuenow="43"
                            aria-valuemin="0" aria-valuemax="100" style="width:43%">
                            </div>
                            <span>$434.600</span>
                        </div>
            <!--Modelo-->

            <h1 class="col-md-6"> Modelo </h1>
                        <div class="progress col-md-6 nopadding">
                            <div class="progress-bar progress-bar-ceaf" role="progressbar" aria-valuenow="10"
                            aria-valuemin="0" aria-valuemax="100" style="width:10%">
                            </div>
                            <span>$100.320</span>
                        </div>

            <!--Provida-->

            <h1 class="col-md-6"> Provida </h1>
                        <div class="progress col-md-6 nopadding">
                            <div class="progress-bar progress-bar-ceaf" role="progressbar" aria-valuenow="60"
                            aria-valuemin="0" aria-valuemax="100" style="width:60%">
                            </div>
                            <span>$600.036</span>
                        </div>

</div><!--AFP-->


				<div class="col-md-3">
				<h2 class="modularh2 text-center"> Fonasa - CCAF</h2>
				<hr class="espaciador">

					<canvas id="chartfonasa" class="col-md-2"></canvas>
					<div id="leyendafonasa"></div>

</div><!--Fonasa - CCAF-->


 <!--Isapres-->
            <div class="col-md-3">

                    <h2 class="modularh2 text-center">Isapres</h2>
                    <hr class="espaciador">

            <!--Habitat-->
                <h1 class="col-md-6"> Más Vida </h1>
                        <div class="progress col-md-6 nopadding">
                            <div class="progress-bar progress-bar-ceaf" role="progressbar" aria-valuenow="31"
                            aria-valuemin="0" aria-valuemax="100" style="width:31%">
                            </div>     
                            <span>$310.011</span>
                        </div>

            <!--Más Vida-->

            <h1 class="col-md-6"> Banmédica </h1>
                        <div class="progress col-md-6 nopadding">
                            <div class="progress-bar progress-bar-ceaf" role="progressbar" aria-valuenow="15"
                            aria-valuemin="0" aria-valuemax="100" style="width:15%">
                            </div>
                            <span>$151.200</span>
                        </div>
            <!--Banmédica-->

            <h1 class="col-md-6"> Vida Tres </h1>
                        <div class="progress col-md-6 nopadding">
                            <div class="progress-bar progress-bar-ceaf" role="progressbar" aria-valuenow="17"
                            aria-valuemin="0" aria-valuemax="100" style="width:17%">
                            </div>
                            <span>$175.698</span>
                        </div>
</div><!--Isapres-->
            <!--Vida Tres-->
   

                <div class="col-md-3 float-right">
                <h2 class="modularh2 text-center"> Mutual </h2>
                <hr class="espaciador">
                <h1 class="mutual text-center">+ $211.327</h1>

                

</div><!--Mutual-->




				<hr class="espaciador">

				
			
<hr class="espaciador2">




	 

	</div>
	</div>

<script>
var datafonasa = [{	
	value: 235.101,
	color: "#8DC63F",
	highlight: "#96D343",
	label: "Fonasa"
}, {
	
	value: 43.613,
	color: "#757575",
	highlight: "#A5A5A5",
	label: "CCAF"
}

];

</script>
	

<!--Doughnut-->

	</body>
</html>

</div> <!--contenedor-->