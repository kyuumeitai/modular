<?php
session_start();
 // store session data
?>

<title>Permisos</title>
<h1 class="titulo">Módulo Pago a Colaboradores<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor nopadding nomargin">  
 
    <div class="col-md-12 wrapper">
            <div class="seleccionmodulos centrar">
                <a href="mod_pagos/honorarios.php" class="" data-toggle="modal" data-target="#honorarios"><div class="honorarios nopadding"> </div></a>
                <a href="mod_pagos/remuneraciones.php" class="" data-toggle="modal" data-target="#remuneraciones"><div class="remuneraciones nopadding"> </div></a>
            </div>
    </div>

    <script>
$('#revisarpermisos').on('shown.bs.modal', function (event) {
		var ctxdiasadmin = $("#chartdiasadmin").get(0).getContext("2d");
		legend(document.getElementById("leyendadiasadmin"), datadiasadmin);

        var myDoughnutChart1 = new Chart(ctxdiasadmin).Doughnut(datadiasadmin, {
            responsive: true,
        });


        var ctxpermisoshora = $("#chartpermisoshora").get(0).getContext("2d");
        legend(document.getElementById("leyendapermisoshora"), datapermisoshora);
        var myDoughnutChart2 = new Chart(ctxpermisoshora).Doughnut(datapermisoshora, {
        	responsive: true,
        });
        var ctxferiadolegal = $("#chartferiadolegal").get(0).getContext("2d");
        legend(document.getElementById("leyendaferiadolegal"),dataferiadolegal);
        var myDoughnutChart3 = new Chart(ctxferiadolegal).Doughnut(dataferiadolegal, {
            responsive: true,
        });

});
    </script>

</div> <!--contenedor-->

