<?php
session_start();
 // store session data

?>

<!doctype html>
<html><head>
<script src="js/jquery-2.1.4.min.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<script src="js/wow.js"></script>
<script src="js/smooth-scroll.js"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800' rel='stylesheet' type='text/css'>

 <script>
$(document).ready(function(){
    resizeDiv();
});

window.onresize = function(event) {
    resizeDiv();
}

function resizeDiv() {
    vpw = $(window).width();
    vph = $(window).height();
    vph5 = $(window).height() - vph/2;
    vph3 = $(window).height() - vph/2;

    $('.vph').css({'min-height': vph + 'px'});
    $('.vph2').css({'min-height': vph-(vph/10) + 'px'});
    $('.vph50').css({'min-height': vph-(vph/2) + 'px'});
    $('.vpw').css({'width': vpw + 'px'});
    $('.modulomodal').css({'width': vpw-(vpw/10) + 'px'});
     $('.modulomodal').css({'max-height': vph-(vph/10) + 'px'});
    $('.centerh').css({'margin-left': (vpw/2)-([$('.centerh').width()]/2) + 'px'});
    $('.centerv').css({'margin-top': (vph/2)-([$('.centerv').height()]/2) + 'px'});
};

</script>

<script>
              new WOW().init();
</script>


<meta charset="utf-8">
<title>Modular</title>
<body color="verde" class="verde1">


<div id="container" class="col-md-12 ">
        <div id="pantalla1" class="col-md-10 col-md-offset-1 vph">
            <hr class="espaciador">
            <div class="ceafblanco wow fadeInLeft"></div>
            <hr class="espaciador">
            <div class="logogrande wow fadeInLeft col-md-6a"></div>
            <hr class="espaciador">
            <p class="blanco wow fadeInLeft"> Versión 0.1</p>
            <hr class="espaciador">
            <h1 class="bienvenida col-md-6 nopadding wow fadeInLeft"> ¡Bienvenido!, esta es la primera muestra del sistema para CEAF</h1>
                <hr class="espaciador">
                <h1 class="bienvenidasub col-md-6 nopadding wow fadeInLeft">El nombre de proyecto es Modular, debido a su naturaleza que implementa todos los sistemas en módulos. Lo atractivo de esta forma, es que es una metodología aditiva, ¡sin límites!</h1>
       </div>
    <div class="col-md-12 bottom15 wow bounceInDown ">
        <a data-scroll href="#pantalla2"><div class="botondown centrar infinite wow pulse"></div></a>
    </div>

</div>
        <div id="pantalla2" class="col-md-10 col-md-offset-1 vph">
            <div id="modulos" class="col-md-12 centerv">
                <a href="viaticos.php" class="" data-toggle="modal" data-target="#viaticos">
                <div id="modulo1" class="col-md-8 modulo">
                    
                        <li class="moduloli">
                            <p class="modtexto">Solicitud de Viáticos</p>
                            
                        </li>
                        </a>
                    </div>
               <a href="rendicionsel.php" class="" data-toggle="modal" data-target="#rendicion">
                <div id="modulo2" class="col-md-4 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Rendiciones</p>
                            
                    </li>
                        </a>
                </div>

                <a href="comprassel.php" class="" data-toggle="modal" data-target="#compras">
                <div id="modulo3" class="col-md-3 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Compras</p>
                            
                    </li>
                        </a>
                </div>

                <a href="#">
                <div id="modulo4" class="col-md-3 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Presupuesto</p>
                </li>
                        </a>
                </div>
                <a href="#">
                <div id="modulo5" class="col-md-3 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Pago al Personal</p>
                            
                    </li>
                        </a>
                </div>

                <a href="#">
                <div id="modulo6" class="col-md-3 modulo">
                    <li class="moduloli">
                            <p class="modtexto">Permisos</p>
                            
                    </li>
                        </a>
                </div>

            </div>
        </div>
<iframe src="https://www.google.com/maps/d/u/0/embed?mid=ze4V8G34d1AQ.kruBOir2r4bo" class="vph vpw"></iframe>
</div>
    <div class="vpw centerh bottomfloat wow bounceInDown ">
        <a data-scroll href="#pantalla1"><div class="botontop centrar infinite wow pulse"></div></a>
    </div>

<script>
    smoothScroll.init({
    speed: 500, // Integer. How fast to complete the scroll in milliseconds
    easing: 'easeInOutCubic', // Easing pattern to use
    updateURL: false, // Boolean. Whether or not to update the URL with the anchor hash on scroll
    offset: 0, // Integer. How far to offset the scrolling anchor location in pixels
    callback: function ( toggle, anchor ) {} // Function to run after scrolling
});
</script>
<!--Modal -->
        <div class="modal fade nopadding" id="viaticos">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->

<!--Modal -->
        <div class="modal fade nopadding" id="rendicion">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->

<!--Modal -->
        <div class="modal fade nopadding" id="rendicion1">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->
<!--Modal -->
        <div class="modal fade nopadding" id="rendicion2">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content">
      
            </div>
          </div>
        </div>
<!--/modal-->
<!--Modal -->
        <div class="modal fade nopadding" id="compras">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->
<!--Modal -->
        <div class="modal fade nopadding" id="compras1">
          <div class="modal-dialog modulomodal" nopadding>
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->
<!--Modal -->
        <div class="modal fade nopadding" id="compras2">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->
<!--Modal -->
        <div class="modal fade nopadding" id="modal1">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->
<!--Modal -->
        <div class="modal fade nopadding" id="modal2">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->
<!--Modal -->
        <div class="modal fade nopadding" id="modal3">
          <div class="modal-dialog modulomodal nopadding">
            <div class="modal-content nomargin">
      
            </div>
          </div>
        </div>
<!--/modal-->
<script>
$(document).ready(function() {
    
// Support for AJAX loaded modal window.
// Focuses on first input textbox after it loads the window.
$('[data-toggle="modal1"]').click(function(e) {
    e.preventDefault();
    var url = $(this).attr('href');
    if (url.indexOf('#') == 0) {
        $(url).modal('open');
    } else {
        $.get(url, function(data) {
            $('<div class="modal hide fade">' + data + '</div>').modal();
        }).success(function() { $('input:text:visible:first').focus(); });
    }
});
    
});
</script>
</body>
</html>