<?php
session_start();
 // store session data
?>

	<script src="js/Chart.js"></script>	
	<script src="js/legend.js"></script>
	
		<script src="js/jquery-2.1.4.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/jquery-ui.min.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<script src="js/wow.js"></script>
<script src="js/legend.js"></script>
<script src="js/smooth-scroll.js"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800' rel='stylesheet' type='text/css'>

	<script src="js/Chart.js"></script>	




  
<title>Datos</title>
<h1 class="titulo">Revisar mi Información<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor nopadding nomargin">  
 
		
<div class="container">

				<div class="col-md-12">

					<h2 class="modularh2 text-center">Días Administrativos</h2>
					<hr class="espaciador">

					<canvas id="chartdiasadmin"></canvas>
					<div id="leyendadiasadmin"></div>
				</div>

				
				<div class="col-md-12">
				<h2 class="modularh2 text-center"> Permisos por hora (Mes Actual)</h2>
				<hr class="espaciador">

					<canvas id="chartpermisoshora"></canvas>
					<div id="leyendapermisoshora"></div>
				</div>


				<div class="col-md-12">
					<h2 class="modularh2 text-center">Feriado Legal</h2>
					<hr class="espaciador">

					<canvas id="chartferiadolegal"></canvas>
					<div id="leyendaferiadolegal"></div>
				</div>




	 

	</div>

<script>
var datadiasadmin = [{	
	value: 40,
	color: "#F7464A",
	highlight: "#FF5A5E",
	label: "Gestión"
}, {
	value: 10,
	color: "#C185BD",
	highlight: "#C199BE",
	label: "Fisiología"
}, {

	value: 30,
	color: "#46BFBD",
	highlight: "#5AD3D1",
	label: "Genómica"
}, {
	value: 10,
	color: "#8DC63F",
	highlight: "#96D343",
	label: "Agronomía"
}, {
	
	value: 20,
	color: "#FDB45C",
	highlight: "#FFC870",
	label: "Mej. Genético"
}

];
var datapermisoshora = [
	{
		value: 300,
		color:"#F7464A",
		highlight: "#FF5A5E",
		label: "Gestión"
	},
	{
		value: 50,
		color: "#46BFBD",
		highlight: "#5AD3D1",
		label: "Genómica"
	},
	{
		value: 100,
		color: "#FDB45C",
		highlight: "#FFC870",
		label: "Mej. Genético"
	}
]

var dataferiadolegal = [
	{
		value: 600,
		color:"#F7464A",
		highlight: "#FF5A5E",
		label: "Gestión"
	},
	{
		value: 320,
		color: "#46BFBD",
		highlight: "#5AD3D1",
		label: "Genómica"
	},
	{
		value: 500,
		color: "#FDB45C",
		highlight: "#FFC870",
		label: "Mej. Genético"
	}
];
</script>
	
<script>

		var ctxdiasadmin = $("#chartdiasadmin").get(0).getContext("2d");
        legend(document.getElementById("leyendadiasadmin"), datadiasadmin);

        var myDoughnutChart1 = new Chart(ctxdiasadmin).Doughnut(datadiasadmin, {
            responsive: true,
        });


        var ctxpermisoshora = $("#chartpermisoshora").get(0).getContext("2d");
        legend(document.getElementById("leyendapermisoshora"), datapermisoshora);
        var myDoughnutChart2 = new Chart(ctxpermisoshora).Doughnut(datapermisoshora, {
        	responsive: true,
        });
        var ctxferiadolegal = $("#chartferiadolegal").get(0).getContext("2d");
        legend(document.getElementById("leyendaferiadolegal"),dataferiadolegal);
        var myDoughnutChart3 = new Chart(ctxferiadolegal).Doughnut(dataferiadolegal, {
            responsive: true,
        });


    </script>

<!--Doughnut-->

	</body>
</html>

</div> <!--contenedor-->