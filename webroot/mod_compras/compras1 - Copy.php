<?php
session_start();
 // store session data
?>
<!doctype html>
<html><head>
<body color="gris">
<script>
var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var output = ((''+day).length<2 ? '0' : '') + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' + d.getFullYear();

$("#hoy").text(output)
</script>

<div id="container" class="rendicion nopadding nomargin">
        <h1 class="titulo"> Crear Orden de Compra <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button> </h3>
    <div class="col-md-12 col-xs-12 wrapper nomargin">

        <div id="datos" class="col-md-12 pull-right">
            <table class="table table-bordered">
                   
                <tbody>

                    <tr>
                    <th> Rut Responsable </th>
                        <td id="rut">
                            <input type="number" placeholder="RUT" id="RUT"></input>
                            <input type="number"  placeholder="DV" id="DV"></input>
                            <input type="text"    id="nombre" placeholder="Nombre"></input>
                        </td>
                    </tr>

                    <tr>
                    <th> Centro de Costo </th>
                        <td id="centrocosto">
                            <select   >
                              <option value="gestion">Gestión</option>
                              <option value="mejoramiento_genetico">Mejoramiento Genético</option>
                              <option value="genomica">Genómica</option>
                              <option value="fisiologia">Fisiología del Estrés</option>
                              <option value="agronomia">Agronomía</option>
                            </select>
                        </td>
                    </tr>

                    <tr>
                    <th> Rut Proveedor </th>
                        <td id="prorut">
                            <input type="number"    placeholder="RUT Proveedor" id="prorut"></input>
                            <input type="number"     placeholder="DV" id="prodv"></input>
                            <input type="text"  class="pull-left seleccion30 " id="pronombre" placeholder="Nombre Proveedor"></input>
                        </td>
                    </tr>

                        <tr>
                    <th> Contacto Proveedor </th>
                        <td id="procontacto">
                            <input type="text"  class="pull-left seleccion30 " placeholder="Contacto Proveedor" id="procontacto"></input>
                        </td>
                    </tr>

                     <th> ID Cotización </th>
                        <td id="cotid">
                            <input type="text"     placeholder="ID" id="cotid"></input>
                            <input type="date"    placeholder="Fecha de Cotización" id="cotfecha"></input>
                        </td>
                    
                    </tr>
                </tbody>
            </table>
        </div><!--/datos -->

        <div id="datocompra" class="col-md-12">
                   <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Cant.</th>
                                <th>Ítem</th>
                                <th>Detalle</th>
                                <th>Código Producto</th>
                                <th>$ Unitario</th>
                                <th > Total Neto </th>
                            </tr>
                        </thead>
                    <tbody>
                        <tr>
                            <td id="compra_cantidad"><input type="number" lang="nb" class="seleccion30"></input></td>
                            <td id="compra_item"><input type="text" lang="nb" class="seleccion30"></input></td>
                            <td id="compra_detalle"><input type="text" lang="nb" class="seleccion30"></input></td>
                            <td id="compra_codigo"><input type="text" lang="nb" class="seleccion30"></input></td>
                            <td id="compra_unitario"><input type="number" lang="nb" class="seleccion30"></input></td>
                            <td id="compra_neto"><input type="number" lang="nb" class="seleccion30"></input></td>
                        </tr>
                    </tbody>
                </table>
        </div><!--/cantidad -->

        <hr class="espaciador">
        <input type="button" class="botongrande verde col-md-5" id="generaroc" onclick="generar_oc();" value="Generar OC"/>
        <input type="button" class="botongrande verde col-md-5 pull-right" id="comprometeroc" onclick="compremeter_oc();" value="Comprometer"/>
        </div>

        </div><!--/tabladatos .Wrapper-->
    </div><!--/datos-->


</body>
</html>