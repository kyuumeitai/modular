





<div id="container" class="rendicion nopadding nomargin">
        <h1 class="titulo"> Vista de Rendición
            <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button>
        </h1>
    <div id="imprimir" class="col-md-12 col-xs-12 wrapper nomargin">

           
        <div class="col-md-12 table-responsive vista">
 <script>
$( "#fecha_rendicion" ).datepicker({
    inline: true,
    dateFormat: 'dd-MM-yy'
});
$( "#fecha_rendicion_devolucion" ).datepicker({
    inline: true,
    dateFormat: 'dd-MM-yy',
});
  </script>
        <form action="post.php" method="get">
                <table class="table table-no-bordered">
                    <thead>
                    <tr>
                        <td width="20%" id="logovista"></td>
                        <td id="titulovista" class="text-center">Orden de Compra</td>
                        <td width="20%" id="codigovista" class="text-right">
                            <table>
                                <tr>
                                    <td>Nº de Orden:</td>
                                    <td class="text-right">1</td>
                                </tr>
                                <tr>
                                    <td>Fecha:</td>
                                    <td class="text-right">
                                        <?php echo date("d/m/y");?>                                        
                                    </td>
                                </tr>
                            </table>
                                
                        </td>

                    </tr>
                    </thead>
                </table>


<h1 class="titulo2 gris  ">Datos para Facturación</h1>

<table class="table table-bordered table-bordered">
                        <tr>
                            <th> Nombre </th>
                            <th> Rut </th>
                            <th> Giro </th>
                            <th> Teléfono/Fax </th>
                            <th> Dirección </th>
                        
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="nombre_v"></td>
                        <td id="rut_v"></td>
                        <td id="giro_v"></td>
                        <td id="telefono_v"></td>
                        <td id="direccion_v"></td>    
                 
                    </tr>
                </tbody>
            </table>

<h1 class="titulo2 gris  ">Datos del Proyecto / Programa Asociado</h1>

            <table class="table table-bordered table-bordered">
                <thead>
                    <tr>
                        <th> Centro de Costo </th>
                        <th> Código Proyecto </th>
                        <th> Responsable Solicitud </th>
                        <th> Línea Asociada </th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td id="centro_v"></td>
                        <td id="codigo_v"></td>
                        <td id="responsable_v"></td>
                        <td id="linea_v"></td>
                    </tr>
                </tbody>
            </table>


<h1 class="titulo2 gris  ">Datos del Proveedor</h1>

            <table class="table table-bordered table-bordered">
                <thead>
                    <tr>
                        <th> Nombre</th>
                        <th> Rut </th>
                        <th> Atención a </th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td id="nombre_proveedor_v"></td>
                        <td id="rut_proveedor_v"></td>
                        <td id="atencion_proveedor_v"></td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered nomargin">
                <thead>
                    <tr>
                        <th> Cotización ID</th>
                        <th> Teléfono </th>
                        <th> Fax </th>
                        <th> Correo Electrónico </th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td id="cotización_id_v"></td>
                        <td id="telefono_v"></td>
                        <td id="fax_v"></td>
                        <td id="correo_v"></td>
                    </tr>
                </tbody>
            </table>


        <p class="text-center">
        Estimado(s) Señor(es) <?php echo nombre_proveedor;?>, agradecemos sirvan enviar o entregar los siguientes artículos y/o servicios, cuyo valor deberá ser facturado acompañando la presente orden y la guía de despacho respectiva
        </p>

<h1 class="titulo2 gris  ">Clasificación de la Compra</h1>

<table id="tabla" class="table table-bordered">
                    <thead>                    
                            <th width="80px">Cant.</th>
                            <th>Ítem</th>
                            <th class="col-md-5">Detalle</th>
                            <th class="col-md-1">Código Producto</th>
                            <th class="col-md-1">$ Unitario</th>
                            <th class="col-md-1"> Total Neto </th>
                            
                    </thead>
                        
                    <tbody>
                        <tr>
                            <td id="compra_cantidad"></td>
                            <td id="compra_item"></td>
                            <td id="compra_detalle"></td>
                            <td id="compra_codigo"></td>
                            <td id="compra_unitario"></td>
                            <td id="compra_neto"></td>
                            
                        </tr>
                    </tbody>
                </table>




<script>
$( "#nombre" ).change(function() {
    var value = $( this ).val();
    $( "#nombre_v" ).text( value );
  })
  .change();

$( "#rut" )
  .change(function() {
    var value = $( this ).val();
    $( "#rut_v" ).text( value );
  })
  .change();

$( "#giro" )
  .change(function() {
    var value = $( this ).val();
    $( "#giro_v" ).text( value );
  })
  .change();

$( "#telefono" )
  .change(function() {
    var value = $( this ).val();
    $( "#telefono_v" ).text( value );
  })
  .change();

$( "#direccion" )
  .change(function() {
    var value = $( this ).val();
    $( "#direccion_v" ).text( value );
  })
  .change();




$( "#centro" )
  .change(function() {
    var value = $(this).val();
    $( "#centro_v" ).text( value );
  })
  .change();

$( "#codigo" )
  .change(function() {
    var value = $(this).val();
    $( "#codigo_v" ).text( value );
  })
  .change();

$( "#responsable" )
  .change(function() {
    var value = $(this).val();
    $( "#responsable_v" ).text( value );
  })
  .change();


$( "#linea" )
  .change(function() {
    var value = $(this).val();
    $( "#linea_v" ).text( value );
  })
  .change();



$( "#nombre_proveedor" )
  .change(function() {
    var value = $(this).val();
    $( "#nombre_proveedor_v" ).text( value );
  })
  .change();

$( "#rut_proveedor" )
  .change(function() {
    var value = $(this).val();
    $( "#rut_proveedor_v" ).text( value );
  })
  .change();

$( "#atencion_proveedor" )
  .change(function() {
    var value = $(this).val();
    $( "#atencion_proveedor_v" ).text( value );
  })
  .change();



$( "#cotizacion_id" )
  .change(function() {
    var value = $(this).val();
    $( "#cotización_id_v" ).text( value );
  })
  .change();

$( "#telefono" )
  .change(function() {
    var value = $(this).val();
    $( "#telefono_v" ).text( value );
  })
  .change();

$( "#fax" )
  .change(function() {
    var value = $(this).val();
    $( "#fax_v" ).text( value );
  })
  .change();

$( "#correo" )
  .change(function() {
    var value = $(this).val();
    $( "#correo_v" ).text( value );
  })
  .change();

</script>
 

        </div><!--/Nombrefechacentro -->

        <div id="aviso" class="col-md-12">
        <p class="text-center">
        Esta solicitud se considerará válida sólo si se envía al correo electrónico compras@ceaf.cl, con copia al correo del líder del Centro de Costos al que se cargará.
        </p>
        <hr class="espaciador">
        <input type="button" class="botongrande verde col-md-5" id="enviarcorreo" onclick="" value="Enviar por Correo" />
        
        <input type="button" class="botongrande verde col-md-5 pull-right" id="descargar" onclick="" value="Descargar" />
        
        </div>

        </div><!--/tabladatos .Wrapper-->
    </div><!--/datos-->

<div id="invi">
</div>
</form>

<script>

$("#descargar").click(function()
{
 $("#invi").load("mod_rendiciones/rendicion_pdf.php")

});

$("#enviarcorreo").click(function()
{
 $("#invi").load("mod_rendiciones/rendicion_enviar.php")

});

</script>

</body>
</html>