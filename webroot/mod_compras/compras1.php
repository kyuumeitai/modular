<?php
session_start();
 // store session data
?>
<!doctype html>
<html><head>
<body color="gris">
<script>
var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var output = ((''+day).length<2 ? '0' : '') + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' + d.getFullYear();

$("#hoy").text(output)
</script>


<script>
$( "#fechaoc" ).datepicker({
    inline: true
});
</script>

<div id="container" class="rendicion nopadding nomargin">
        <h1 class="titulo"> Crear Orden de Compra <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button> </h1>

    <div class="col-md-12 col-xs-12 wrapper nomargin tablaoc">

        <div id="datos" class="col-md-12 pull-right">
            <table class="table table-bordered nomargin">
                          
                    <th class="col-md-2 "> Rut Responsable </th>               

                    <td id="rut" width="150px" ><input type="number" placeholder="RUT" id="RUT"></input></td>
                    <td width="60px"><input type="number"  placeholder="DV" id="DV"></input></td>
                    <td width="300px"><input type="text" id="nombre" placeholder="Nombre"></input></td>


            <th width="150px"> Centro de Costos </th>
            <td id="centrocosto">
                            <select   >
                              <option value="gestion">Gestión</option>
                              <option value="mejoramiento_genetico">Mejoramiento Genético</option>
                              <option value="genomica">Genómica</option>
                              <option value="fisiologia">Fisiología del Estrés</option>
                              <option value="agronomia">Agronomía</option>
                            </select>
                        </td>
                  

                    
            </table>


    <table class="table table-bordered nomargin">
  

                    <th class="col-md-2"> Rut Proveedor </th>
                        <td id="prorut" class="col-md-4"><input type="number"    placeholder="RUT Proveedor" id="prorut"></input></td>
                        <td class="col-md-1"><input type="number"     placeholder="DV" id="prodv"></input></td>
                        <td class="col-md-5"><input type="text"  class="pull-left  " id="pronombre" placeholder="Nombre Proveedor"></input></td>
                        

    </table>

    <table class="table table-bordered nomargin">


                    <th class="col-md-2"> Contacto Proveedor </th>
                        <td id="procontacto">
                            <input type="text"  class="pull-left  " placeholder="Contacto Proveedor" id="procontacto"></input>
                        </td>

    </table>

    <table class="table table-bordered nomargin">
    
                     <th class="col-md-2"> ID Cotización </th>
                        <td id="cotid"><input type="text" placeholder="ID" id="cotid"></input></td>
                        <td id="Fecha"><input type="text" name="fechaoc" id="fechaoc" placeholder="Fecha"></input> </td>
    
                    
    
    
    </table>
    

<h1 class="titulo2 gris separado"> Items
    <input type="button" class="nopadding boton-mas2 pull-right" id="agregar_item" onclick="add_item_oc();" value="+" />
</h1>

<script>
function add_item_oc(){
    var table = document.getElementById("tabla_oc");
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);

    cell1.innerHTML = "<td id='compra_cantidad'><input type='number'></input></td>";
    cell2.innerHTML = "<td id='compra_item'><input type='text'></input></td>";
    cell3.innerHTML = "<td id='compra_detalle'><input type='text'></input></td>";
    cell4.innerHTML = "<td id='compra_codigo'><input type='text'></input></td>";
    cell5.innerHTML = "<td id='compra_unitario'><input type='number'></input></td>";
    cell6.innerHTML = "<td id='compra_neto'><input type='number'></input></td>";
    cell7.innerHTML = "<td id='eliminar'><button class='borraroc' onclick='del_item(this)'></button></td>";

    
}

function del_item(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tabla_oc").deleteRow(i);
}
</script>

        
                <table id="tabla_oc" class="table table-bordered">
                    <thead>                    
                            <th width="80px">Cant.</th>
                            <th>Ítem</th>
                            <th class="col-md-5">Detalle</th>
                            <th class="col-md-1">Código Producto</th>
                            <th class="col-md-1">$ Unitario</th>
                            <th class="col-md-1"> Total Neto </th>
                            <th width="70px"> Eliminar </th>
                    </thead>
                        
                    <tbody>
                        <tr>
                            <td id="compra_cantidad"><input type="number" lang="nb" ></input></td>
                            <td id="compra_item"><input type="text" lang="nb" ></input></td>
                            <td id="compra_detalle"><input type="text" lang="nb" ></input></td>
                            <td id="compra_codigo"><input type="text" lang="nb" ></input></td>
                            <td id="compra_unitario"><input type="number" lang="nb" ></input></td>
                            <td id="compra_neto"><input type="number" lang="nb" ></input></td>
                            <td id="eliminar"></td>
                        </tr>
                    </tbody>
                </table>


        <hr class="espaciador">
        <a href="mod_compras/compras_oc_vista.php" class="" data-toggle="modal" data-target="#generaroc">
            <input type="button" class="botongrande verde col-md-5" value="Generar OC"/>
        </a>

    
        <input type="button" class="botongrande verde col-md-5 pull-right" id="comprometeroc" onclick="compremeter_oc();" value="Comprometer"/>
        </div>

        </div><!--/tabladatos .Wrapper-->
    </div><!--/datos-->


</body>
</html>