<?php
require_once('tcpdf/config/tcpdf_config_alt.php');

global $l;
$l = Array();

// PAGE META DESCRIPTORS --------------------------------------

$l['a_meta_charset'] = 'UTF-8';
$l['a_meta_language'] = 'es';
$dpi="300";
// TRANSLATIONS --------------------------------------
$l['w_page'] = 'página';

// Include the main TCPDF library (search the library on the following directories).
$tcpdf_include_dirs = array(
	realpath('tcpdf/tcpdf.php')
);
foreach ($tcpdf_include_dirs as $tcpdf_include_path) {
	if (@file_exists($tcpdf_include_path)) {
		require_once($tcpdf_include_path);
		break;
	}
}

// set page format (read source code documentation for further information)
// MediaBox - width = urx - llx 210 (mm), height = ury - lly = 297 (mm) this is A4
$page_format = array(
    'MediaBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 100, 'ury' => 65),
    //'CropBox' => array ('llx' => 0, 'lly' => 0, 'urx' => 210, 'ury' => 297),
    //'BleedBox' => array ('llx' => 5, 'lly' => 5, 'urx' => 205, 'ury' => 292),
    //'TrimBox' => array ('llx' => 10, 'lly' => 10, 'urx' => 200, 'ury' => 287),
    //'ArtBox' => array ('llx' => 15, 'lly' => 15, 'urx' => 195, 'ury' => 282),
    'Dur' => 3,
    'trans' => array(
        'M' => 'O'
		
    ),
    'Rotate' => 0,
    'PZ' => 1,
);

// Check the example n. 29 for viewer preferences

// add first page ---
$pdf = new TCPDF('L', PDF_UNIT, $page_format, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Alex Acuña Viera - Felipe Acuña Viera');
$pdf->SetTitle('Tarjeta de Presentación');

// set margins
$pdf->SetMargins(0, 0);

$pdf->SetAutoPageBreak(TRUE, 0);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
$pdf->setLanguageArray($l);

// ---------------------------------------------------------

// set font
$pdf->SetFont('opensans', '', 10);

// add a page
$pdf->AddPage();
$pdf->ImageSVG($file='../svg/retiro1.svg', $x=0, $y=0, $w=100, $h=0, $link='#', $align='left', $palign='', $border=0, $fitonpage=false);

$pdf->AddPage();
	$pdf->ImageSVG($file='../svg/tiro1.svg', $x=0, $y=0, $w=100, $h=65, $link='#', $align='left', $palign='', $border=0, $fitonpage=false);
	$pdf->SetFont('opensans', '', 12);
	$pdf->SetY(22);
	$pdf->SetX(39);
	$txt = 'pepe';
	//titulo
	$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, $wadj='54', 0);
	$pdf->SetFont('opensans', '', 7);
	$pdf->SetY(28);
	$pdf->SetX(39);
	$txt = 'Título';
	$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, $wadj='54', 0);	//cargos
	$pdf->SetFont('opensans', '', 7);
	$pdf->SetY(31);
	$pdf->SetX(39);
	$txt = 'Cargo';
//telefono//
	$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, $wadj='54', 0);
	$pdf->SetFont('opensans', '', 7);
	$pdf->SetY(40);
	$pdf->SetX(39);
	$txt = '+tele-fono';
	$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, $wadj='54', 0);	//mail
	$pdf->SetFont('opensans', '', 7);
	$pdf->SetY(43);
	$pdf->SetX(39);
	$txt = 'example@ceaf.cl';
	$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, $wadj='54', 0);	//direccion inglesespanol
	$pdf->SetFont('opensans', '', 7);
	$pdf->SetY(51);
	$pdf->SetX(39);
	$txt = 'Camino Los Choapinos Road';
	$pdf->Write(0, $txt, '', 0, 'L', true, 0, false, false, 0);

// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('holi.pdf', 'D');
//============================================================+
// END OF FILE
//============================================================+