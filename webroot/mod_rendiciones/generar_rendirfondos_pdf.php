<script>


$nombre = $( "#nombre" ).val();
$rut = $( "#rut" ).val();
$centro = $( "#centro" ).val();

$cant = $( "#cant" ).val();
$item = $( "#item" ).val();
$detalle = $( "#detalle" ).val();
$codigoproducto = $( "#codigoproducto" ).val();
$preciounitario = $( "#preciounitario" ).val();
$totalneto = $( "#totalneto" ).val();

$cotizacion_p = $( "#cotizacion_p" ).val();
$telefono_p = $( "#telefono_p" ).val();
$fax_p = $( "#fax_p" ).val();
$correo_p = $( "#correo_p" ).val();









var columns = [
    {title: "Nombre", dataKey: "nombre"},
    {title: "Rut", dataKey: "rut"},
    {title: "Centro", dataKey: "centro"},
];
var rows = [
    {"id": 1, "nombre": $nombre, "rut": $rut, "centro": $centro},
];

var columnsB = [
    {title: "Item", dataKey:"item"},
    {title: "Subitem", dataKey:"subitem"},
    {title: "Centro de Costos", dataKey:"ccostos"},
    {title: "Fecha Doc", dataKey:"fechadoc"},
    {title: "Nº Doc", dataKey:"Ndoc"},
    {title: "Tipo Documento", dataKey:"Tipodoc"},
    {title: "Total", dataKey:"Total"},
    {title: "Rut Proveedor", dataKey:"RutP"},
    {title: "Nombre Proveedor", dataKey:"NombreP"},
    {title: "Detalle", dataKey:"Detalle"},

];

var rowsB = [
    {"id": 1, "centro_v" : $centro, "codigo_v" : $codigo, "responsable_v" : $responsable, "linea_v" : $linea}
];

var columnsC = [
    {title: "Nombre", dataKey:"nombre_p_v"},
    {title: "Rut", dataKey:"rut_p_v"},
    {title: "Atención A", dataKey:"atencion_p_v"},
];

var rowsC = [
    {"id": 1, "nombre_p_v" : $nombre_p, "rut_p_v" : $rut_p, "atencion_p_v" : $atencion_p}
];

var columnsD = [
    {title: "Cotización ID", dataKey:"cotizacionId_v"},
    {title: "Teléfono", dataKey:"telefono_v"},
    {title: "Fax", dataKey:"fax_v"},
    {title: "Correo Electrónico", dataKey:"correo_v"},
];

var rowsD = [
    {"id": 1, "cotizacionId_v" : $cotizacion_p, "telefono_v" : $telefono_p, "fax_v" : $fax_p, "correo_v" : $correo_p}
];





var columnsF = [
    {title: "Cant.", dataKey:"cant_v"},
    {title: "Ítem", dataKey:"item_v"},
    {title: "Detalle", dataKey:"detalle_v"},
    {title: "Código Producto", dataKey:"codigoproducto_v"},
    {title: "$ Unitario", dataKey:"preciounitario_v"},
    {title: "Total Neto", dataKey:"totalneto_v"}
];

var rowsF = [
    {"id": 1, "cant_v" : $cant, "item_v" : $item, "detalle_v" : $detalle, "codigoproducto_v" : $codigoproducto, "preciounitario_v" : $preciounitario, "totalneto_v" : $totalneto}
];





var datosTitulo = [
	{title: "Datos"},
];

var datosProyecto = [
	{title: "Datos del Proyecto / Programa Asociado"},
];

var datosProveedor = [
	{title: "Datos del Proveedor"},
];

var clasificacionCompra = [
	{title: "Clasificación de la Compra"},
];

// Only pt supported (not mm or in)

var imgData = 'data:image/jpeg;base64,/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABOAAD/4QMvaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzA2NyA3OS4xNTc3NDcsIDIwMTUvMDMvMzAtMjM6NDA6NDIgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE1IChXaW5kb3dzKSIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpCRUJDNTdDMTNBNDIxMUU2ODNBNTlDM0JFMzAxOEIzNCIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpCRUJDNTdDMjNBNDIxMUU2ODNBNTlDM0JFMzAxOEIzNCI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkJFQkM1N0JGM0E0MjExRTY4M0E1OUMzQkUzMDE4QjM0IiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkJFQkM1N0MwM0E0MjExRTY4M0E1OUMzQkUzMDE4QjM0Ii8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8+/+4ADkFkb2JlAGTAAAAAAf/bAIQAAwICAgICAwICAwQDAgMEBQMDAwMFBQQEBQQEBQcFBgYGBgUHBwgICQgIBwsLDAwLCwwMDAwMDg4ODg4ODg4ODgEDAwMFBQUKBwcKDwwKDA8SDg4ODhIRDg4ODg4REQ4ODg4ODhEODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4O/8AAEQgAggESAwERAAIRAQMRAf/EALcAAAEEAwEBAAAAAAAAAAAAAAAFBgcIAwQJAgEBAQACAwEBAQAAAAAAAAAAAAABAgMFBgQHCBAAAQMCAwQECQYKBgcJAAAAAQIDBAAFEQYHITESCEFRExRhcYEiMjM0NTdCUmKzFXWRobFygiNjdBY2wiRkRTgJweGSorR2F7LiQ1Nzk6NEGBEBAAECBAYABQQBBQEAAAAAAAERAhIDBAUhMUFxMwZRIjITNGGBobHhkcEUFhfR/9oADAMBAAIRAxEAPwDqnQFAUBQFAUBQFAUBQFAUBQFAUBQFAUBQFAUBQFAUFHufCDLhag2C+sqU2l+2d2bcQSk8Ud5azgR/6grmt6iYzLZ/RxHs9sxnW3fohrKGvOq+SXm12XMMsx0Ef1SWsyY5HVwO8QGPgrX5WtzsvldLTafdNRlT8t0rg6B82Fm1MkNZYzY03as3LHCwpJwiylDoRxbUq+iTW/0W525s4buEuv2vfbc+cF/C7+JWFrbOhFBgmzYluiuzp7yI8NhJceeeUEIQhIxJJOwAVEzERWVbrotis8lWdWed612eQ/ZdM4iLlJbJbVdpeIjBQ2Hs0DAr8ZIHjrSareIt4ZcV/Vy+v9kttnDlRX9eivN01l141LmKZau11lLcJwh2dLjSMD0cEcDEeOtTdq9Rmzzn9nPX7jrM+fqme3+FoeTGw57s1pzEvPEO4xH5D8dUb7WQ6ha0hKuIp7Xb01utpszLbbscT+7p/XcrOttu+5Exy5rJVuHSCgKAoCgKAoCgKAoCgKAoCgKAoCgKAoCgKAoCgKAoCgr/AM5mnMnOemqL9bWi7c8uumZwJGKjEWOF4DxYBXiFardtPN+VWOcNB7DpJzcjFHO3+nPquUfP2WLKkQpLUuI4pmUysONOtkpUlaTiCCNxBqYmYmsJtumJrDo5yw6xnVjIqU3RYOabPwxLj1uJw/VvYfTA2+EGuv27V/ey+POH0bZdw/5OTx+qOaY1KShJUo4JAxJO4AVsG4UI5qeYednm9yMi5VkqayfAcLMlxk4d8fQcCSRvQk7h07+quX3LXzmXYLfpj+XBb5u05185dk/LH8lDl55SX86xo+ctQw7Ey45g7DtqcW35KN4Us70IPR0mraDa8cYr+TJtOwzmxF+bwt6R8Vz8t5Oyvk+Ai2Zatka3w0AAIjNpQTh0qIGJPhNdFl5VlkUtijs8nT5eVFLIiILNZGYUBQFAUBQFAUBQFAUBQFAUBQFAUBQFAUBQFAUBQFAUBQeHmWpDK2H0BxlxJQtChilSVDAgg9dJiqJiJikqP8xXKZdMvzJWctN4y5mX3VKfl2tkcT8YnaS2kbVI8A2iua1+2TbM3Wcvg4jd9iusmb8qK2/D4KvuNuNLLbqShxJwUlQIII6wa0rmJhYvkZuFxj6rTIMbiVBk2x0ykjHhHZrQUKPiOzy1t9mun70x+jovWb7o1ExHKYWY5ptRXNPdKZzkFzs7xdyLVDUk4KT2wPaLGG3YgHb14VuNy1H28mac54Ol3vV/Z0805zwhUHla0mRqnqGl+7tl3LlmCZ9wCtqXV8X6to/nEYnwA1oNt0v3czjyhyOyaD/kZ/zfTbxl0baabYaQyykIabSEIQkYAJAwAArr4h9FiKPRIG07qJAUlXokHxbaD7QFAUGKTJjw2HJUt1DMZpJW466oJQlIGJJJ2ACgrlqjzwacZLfdteU2V5muzRKVOR1BqElQ2YdsQSr9EEeGrxZKs3IIvHP3q3NeUbXAtUCPj5iQ0665h4VKXgfwVbBCMTzaOfnV2G8k3KDap0fHz0lpxpZHgUhzAfgpggxJs0558NP8zSGrdnOC9lyW4QkSSrvMPiOzzlJAUnHwpw8NVmyUxcsrbLpbbzBauVpktTLe+kLZkR1pcbWk7iFJJBqizaoCg+KUlCSpRASBiSdgAoII1W5xNL9Nn3bTAcVmHMDRKVxbcpPYNrHQ48cUjycRq0WTKs3K8X//ADAdTJryhYLRbLdFJ80PJdkugfncSE/7tXwQjESYfPlrTGdC32rVJRjtbdjrAw8aHEmmCDElHIv+YNapTzcTUCwLhJUQlU61r7ZAx6S0vAgDwKNRNiYuWjyXn/KGodpResn3Ni4wVAcRZV56CfkrQcFJPgIqkwtU4agFAUBQJuYMyWLKlrevWYpzFvtjCeJ2RKWG0Dwbd5PUKCsGoXP7lKzyHYGQbS7enEEo79LV3aMSOlCcCtQ8fDV4sVm5ENw599Y5TpVEiWmI18lKGHVnDwlbhq2CEYm3Zef7VSG6n7YtdqnxsfOCEOsOkeBQWQP9mmCDEnnTDnb0yzu+za8yIcy1d3SEpMxQchqUegPADD9IAVWbJTFyw8eQxKZRJjOJdjuJC23GyFJUk7QQRsNUWeyARgdoPQaCOc7cvWkufpCpt9sTIuKziqXDKozqielRaICj4wa8edoMnMmsxxa7U7Tps6a3W8f04FHTjRzIGlTT6cn24RpEkASJTqlOvrSNw4lk4DwDZV9PpMvJ+mGTR7fk6euCKVVk5+7265essZeCiGWY781SegqdWEAnxcBrTb3f81trmfac2cdlv6VP7kktdvsGkdwzNMW2x32c66/JdIQlLEZCUjiUdmAPEa9Wz2xbkzdPWXv9bsts003z1kkav87Nqsbz9j0yjouc5slty7Sce6pUNh7NAwK/HsHjrHqt4i3hl8f1Ydw9ktsnDlRWfj0VwuuuGuWe5pSL9dHXXD5sa1FbCPEERwnGtRdrM/Mn6p/Zzl+5avOn6p/b/C0/JjHz8zaMxOZ5buaHXH45iKvAfBUnhVxcHbbx14Vu9ojMw3Y6/u6n12M6LbvuV6c1kq3DpBQad3u1usVsk3i7PojW2G0qRIfdPChDaBiSSaDnJzHc0WYdWLpIy/lx52BkJhwttMNEocl8JwDjxG3A7wndWa22jHMsWjXKFqFqmwze7jhYMru4KRMmIJfdQelpnYSPCSBSbogi1ZzL3Iho3a46U3hdwu0nD9Yt57sUE/RSyEkDxk1THK2GG1d+RnQ+4sKbhMT7c6R5rsaSpZB8TwWKY5MMK/atcjedcnRnr1kaT/EVpaBccihHZz0IG3YgEhzDwbfBVovRNpd5IYOtbWYne4qcj6bMrU3dGbmF9iXhs4Y6TgQ4Dvw2dfRUX0LV6qxrvK1obQpxxQS2kFSlK2AAbSTQUQ5p+bK4ZhnS9PdN5ao+XmFKj3K6R1FLkpYOCkNqG5sbiR6XirLbapMox0U5Ys/azuC5tj7LytxYO3aYlR49u0Mo2Fw/i8NTN1ERC3GU+RzRawx0C9sSr7MAHG7LeWy3xfRQwUYDwEmsc3ythOGfyg6ATo5Y/hpLCsMEusSJKFg9frMD5aYpThhCOqPIGGYz1z0sui3XUArFpuhTirpwbeSAMeoKHlq0XqzarVl3NGpWgmdVORDJs9+hOBEyDICktuoB2ocQdikqG4+UVekSjk6O6F64Zc1rysi6W5SY99jpSi62xSh2jLuG8dJQroNYZii8TVJlQkUDW1J1Ey/pflKbm7MbvBDip/VtDDtHnj6DaB0lRqYiqJlzQ1Y1kz7rtmkLmqeVDW72VpscTiU22FHBICE+ks9JwxNZYiikzVNmkPIfd75FYveqE1dqiugOItMPhVLKTtHaLVilB8GBPiqJvTFqw1o5QtBLTHSz/DiZbgGCnpj77i1HrI4wkeQVTFK2GGjmTkx0Jv0daItodtMlQwEi3yHQoH810rR+KkXyYYVb1q5Ms66bx38wZVdVmDLLOLjvZI4ZrCBtxW2MeIDrT+AVeL6qza1+W/mlzBpVc2Ms5qfdn5DeWGltukrdhFRw42idvCOlP4Km62pEui1rucC9W6PdrW+iTb5baX477RCkLbWMQQR1isK7aoCgorz5/Eey/dKfr3K5ne/LHZwvtHnt7Ickat5nVpvB0xgOmHYI7rsiX2KiFyVur4gF4Yeanq66186q/wC1GXHJp51+Z9iMqOFv9pa5e+U2dqDHYzdnouwMqrwXFiI8yRLT87E+gg9e89HXXu0O2TmfNfwhttp2Kc6IvzOFv9rpZT0/ybkeCm3ZWtEWBHSAklltPaKw6VrOKlHwk10eVkWZcUtijtMjS5WVFLLYg4AANgGA8FZXofaAoKYc+WsUhgxdJbI+UJcQmdfFNnAlJ2tMnDo+UR4qyWQpdJn8mvLxEz5PVqLnGP22Wbc72dvhujzJMpG0qUDvQjq6T4qm+4thf1tttltLTSQhpACUoSMAANgAArEu9UBQFBjYjsRkFuO2lpBUVlLYCRxKOJOA6SaDJQVq519ZpGQsmNZKsTxazBmJKkPOtnBbMFOxZGG4rPmjwY1eyFbpVU5YdEnNZs+Bq5JUMqWrhmXZ0YjjBPmMg9bhH4MavdNFYh02tdrt9lt8e1WqO3Ft8VtLLDDKQhCEIGAAArCyNqgKAoKx88WX9MH8hi9Zjdbh54aPBZHGEpMmQcfOaWnYS3htJPo/iN7Kq3KZ6J5k1Ay1qHa5Wm6HX8wuupYEJoFSJDaj5zbgGzhw3k7t9ZJ5Kw6v2x2a/boz1yZTHuC2kLksNq40odKQVJCsBiAemsDI2aDnVzr6tSM66iryXbnicvZbUY5Qg+a5OPrVEdPD6I8R66y2RwUulM/Jjy9Qsu2GPqlmuKlzMVyR2lpZeTj3aKrc4AfluDaD0J8dVvuTbC11UWFAUHxSUrSULAUhQwIO0EGgoVzo8vUTJ0wam5PjBmwz3ezusNkYIjyV7Q4kDclfT1Hx1lsuUug6+Q/WR+W3J0lvr5WphCptjU4cSGwf1rIx6seIeWovjqm2Vy6xrCgorz5/Eey/dKfr3K5ne/LHZwvtHnt7GLyw6SI1U1CaRc2yvLVoCZ1xx9FzBX6ton6Z3+AGvLt2l+7mceUPDsug/wCRncfpjjLpAwwzFYbjR0JbYaSG220DBKUpGAAA6q6+Io+jxERFIN3PWo2T9OLZ9q5tuDcNlWIZa9J51Q6EITiTXl1euytPbizJo2m17PqtfmYMi2Zn+I7yhr/9v6Zd97D7NuvdMeHvHZs/h4e0xwrRf9r01aYbqO0/8y3DBXHZX4Vn/wCJmyLqLlDUe1/auU7g3MYTgHmx5rzSj0LQrAit7pNblai3FlzVxe6bPqtBmYM+3DP8T2k5a9bWOR+rGZpGftU79f1rLnf7i4mPt4iGAvs2kjxIAFZ4jgxy6iaVZPi5D08sOVoqAgQYTSHuH5T6k8Tqjh0qWSawzPFkg7KgFAUBQFAUHNLnUvz9414u0Raypi1sRoLA6AnsUvKA/SWazWcmO7mtfyUZNj5Z0VhXbswm4X5524SF4edwJUWm049XCnHy1jvnitbyT9VVhQFBH+susuV9GcruX2+uBy4OBSLbbUEdtIew2ADoSOk9FTEVRMubWaM0ag8wWoaX30u3G/XF3sIEBjEtstE+ahCdyUpG8+U1mpSFK1X65cuXCx6L2RE+elubnmY2DOnEYhkHb2LOO5I6T01iuuqvEJrqqSZma6oseXbneXPVwYj0pXRsabK/9FByp0+sj+p+sFqtdwUXV3q7B2av5RbW72jx8fDjWeeEMcOscSKxBisw4yA3HYQlppCBglKEDAAAeCsDIzUBQFAUDa1JylDzzkW95VnISpm4Q3WUlQx4XeEltY8KVAEVMSS5haK3iZkvWrLctBLTsa7tQ3+g9k672DoP6KjWaeTHHN1hSQpIUNxGI8tYGR9oKK8+fxHsv3Sn69yuZ3vyx2cL7R57eyWuRnLzFu0vm33hHe7pcF8S+ktR0pQkeRRVXu2bLplTPxlt/WcqLdPN3WZWOcWG21OK3JBUfEBjW3maOkiKzRzO1o1EuepGfblepbylwG3lx7cySeBuM2opQAOs7z4a+T7prbtRn3XTy6dn6d9b2jL0Gisy7Y+albp+MmJWvb4/NFdRLnpvn623mI8pFvdeRGuLGJ4HIziglQI6xvHhrY7XrbtPn23RPDr2aH2TaMvX6K/Luj5oits/CYdKu+Nf/D3j9Gvq2J+Zftz/ADRx6sv9RzVC7/s7Cc12/F9B0cWNep5nYeIoKisqT6JbSRh1FIrzsrNQFAUBQFAUHLnmzYdj6/ZqQ6CFKeZcGPzVxm1D8RrNbyY55r88tL7EnQ3KLkfAoFvQ2cPnIJSr8YNYrua8ckm1CRQFBCepPKjkfVXMbuZs23i9PzFjgaZbfZSwy2NyGkFo8IFWi6iJgtaRcuOnOjMmVPyyy/JukoBBnXFSHX0Nj5DZSlISD07NtJumSIoh7me5p8+aR6iN5Tyk1b3YQgtSXzMaW44HnFLxGKVpGHCB0VNttUTKcdA88X7UfSqy5yzKhlu73EPrdRGSW2glt9baMAoqO1KQd9Vuikpgq6tNuPaXZsaZx7VdmnJRhv4jHXhSOaZc5+U95mNzA5WVJICC++2MfnrjuJT+M1lu5Mcc3UWsLIKAoCgKDw96lz80/koOTL4DWtbgb80IzEeHDowmbKz9GPq6yseob/MT+SsDIyUFFefP4j2X7pT9e5XM735Y7OF9o89vZO3Jn8ELf+9SvrTWz2nwQ3vrv4kd5Tg+32zDjXz0qR/tDCtlMVhv7ZpMS5bagZWn5MzldsuXFtTb8OU4hPEMOJsqJQseBSSCK+QazT3ZOddZPSX6q2nXWavS2Zts8Loj/JvV5mwOLT3K0/Omc7Tly3NqcfmSm0KKRjwNhQK1nwJSCTXq0Wnuzs62yOstdu+us0mkvzbp4RE/69HUDuJ6/wD63dv9dfXMH9Py193+6uWvMZkiRp7rHf7WWy3EelKuUA4eaWJR7VPD+aTw+SvVbNYeWXQnlz1Ei6k6T2S8IdC7jFYTbrkjHFSZMZIQoq6uIAKHgNYbopK8Sk2oSKAoCgKAoOf/AD9ZJetGotvzmy2e4XuIGXHANneonmkE+FBThWWyeClyXeQzUSNe9PZWQpLo+1LC+p1honaqHJUVggdOCyrHxiq3wm2Vo6osKAoCgwzJceBEenS3EtRWEKedcWQlKUIGJJJ3YCg5S6zZxf1a1fu99gBTrdwmiHbW0gkllshhnAdagAcOus8RSGOXTbS3KoyRp5l/K2ADtvgMsPYbi8EAuHyqJrDM8WSDhuUJq5W+Tb308TMlpbC0ncUuJKT+WoHJ19N00c1gJcSUz8tXjtADs40x3uIeRaR+Cs/OGPk6r5XzDbs2Zet+ZLS6HrdcY7cplafmuJBwPURuIrAyFSgKAoCg8PepX+afyUHJmZ8bHv8AmJX/ABlZ+jH1dZY/qG/zE/krAyMlBRXnz+I9l+6U/XuVzO9+WOzhfaPPb2TtyZ/BC3/vUr601s9p8EN767+JHeU6Vs29RbrJy/ZS1fZTKmFVvzGyngYucdIKinoS6k4caR48a1G57NlauKzwu+LqvXfbNTtc4bfmy552z/t8EAf/AIWzf33g/iCB3DH13A72vD18GGGP6Vc1/wBSza/XFH0L/wBQ0uDxXYv2p/r/AIWA0b5f8paQMrkwiq4ZifTwP3OQkBQT0pbSMeBJ8eNdLtmz5Wkiscbvi+e+xe2andJpd8uXHK2P9/ilGtu5VXHnJ0Le1KymjN+XWO0zZYUKUWmxiuRC9JaB1qSfOT5avZdRW6FTOXDXq5aHZsWJyXH8p3BSWbtCHpIKTgHUA/LR1dIq90VViaOlOVM3ZdzvZY+YMsTmp9rkpC23WVA4Y70qG9JHSDWKYZCxUAoNS6XW22SA9dLvJah26OguvyJCg22hCRiSSrZQNrTrVfI2qkKVNyZcUTEQ3lR5DZBQ6kpJAUUK28KgMQamYoiJPCoSjXmB0ljaw6dTct4JTeWf65aX1fIlNg8IJ6lglJ8dTbNJRMOcORM55w0G1IRdWWlxrva3lRLjAexSHWgrhcaWOo4bD17azTFYUjg6YaU6v5O1dy81e8sy0KkcI75b3FASY7hG1K0b9+47jWGYovEnxUJFBjffZjNLfkLS0wgFS3HCEpSBvJJ3UFJ+bXmqg3iFJ0y04ldtDdxavN2YPmLSNhYZUN4Pyj07qyW2qTJsclOhsnN2akalX+ORlqyOcUAODZInJ9EjHelvfj14VN8lsOgdYlxQUt56NEJLzzeruXI5cTwJjX5toYkcPmtvkDow81XkrJZKl0Gjyj8zjGny06eZ6fKcpyHMbfOWSRDdcO1K/wBmo7fAam62pEr8w5sS4xW5sB5EiI8kONPMqC0KSoYggjYQaxLs9AUBQeHvUr/NP5KDkzM+Nj3/ADEr/jKz9GPq6yx/UN/mJ/JWBkZKCivPn8R7L90p+vcrmd78sdnC+0ee3snbkz+CFv8A3qV9aa2e0+CG99d/EjvKdK2beigKAoCg+EAgg7QdhBoKmcyXJw3muRJzvpe23HvzmL0+z7G2pCztK2idiVnpG41e25WbVT8tZ41Z0IzA6xbJE2x3FpfDKt8pKgyspO5bTg4T48PFWSkSryT7lz/MNzHFjoazPleLOkAYKegvrig4dPCtLv5apgTibd6/zEbm7HUjL+UWY8kjzXZspTyAfChDaPy0wGJA+d9YdYtd7k3a7hJkzkOrwjWW1tqSxxHcA23iVEdasTVoiIRWZWk5SOWXN+nVz/j/ADnLdt855lTTFjjub0ODfJ4dhw3hPQapddVaIWwqiwoIB5juViy6wsrzHl9Tdtz20jAPEYMywkbEPYbj1Kq1t1ETFVE7jZdV9CM0YyET8vXyOohuSyVIbcSDvStPmrSfKKy8JU5Jiypz76p2eOiNmGDAvSUAAvrSqNIVh1ls8H+7VZshOIvz/wDMOzS4wU23K0JiRhsW++48gH81IR+WowGJDGovMdq9qvxW68XRxq2OnAWu1pLDCsehQRipf6RNWi2IRMn9oNyc5tz/ACo9/wA9su2TKAId7J4cEyUnHHhQg7UJPzj5Ki69MWugGXsvWbKlmi5fsEVEO0wmwzHjtDBKUp/KT0nprEuUqAoME6DDucN633BlEiFIQpl9l0BSFoWMClQO8EUFE+YTkvvWX5UnNelbC7hYFlTz9nR50qPjtPZDe4nqHpDw1ltvUm1EGnWv+rejb5ttonOiA0sh2z3RKnWAQdoCF4KR4eEipm2JREp2tH+YlcW2EoveUGn5OHnOxJamUE+BC21n8dVwJxHtpbztI1L1Fs+Sv4aFriXJxbS5i5XbqCktKWgBIbQNqgBvqJspCYuWmqizw96lf5p/JQcmZnxse/5iV/xlZ+jH1dZY/qG/zE/krAyMlBRXnz+I9l+6U/XuVzO9+WOzhfaPPb2TtyZ/BC3/AL1K+tNbPafBDe+u/iR3lOlbNvRQFAUBQFAUDYzppnkPUKL3TOFli3JGGCXHkAPIB+Y4nBafIamJoiiFLzyGaM3J9T0B+621JOIZjPoW2Mej9c2tX46tjlGGHqych2jFseS9cHbpdACCWpT6ENnDo/UoQr8dMcmGE0ZN0xyDp/HEfKFkiW4YcKnWWwXlAfOcVitXlNVmVqHRUAoCgKBMv+WMvZqgqtuY7dGuUFfpMzGkPJ8YCgcDQQxmHkp0Lvjyn49uk2txZxV9nyFpTj4Eu8YHkFWxyjDBHh8hWi8Z0OPP3eSgHHs3pDYSfB5jST+OpxyjCk/JGgGkmnq0P5ay7FbnIwKZkkGTIBHSFvFRT5KrN0ymiQgABgBgBuAqEvtAUBQFAUDFz1ofpbqPxLzXl+LJlqGHfG0liT4P1rXCo4eE1MTMIoiK48gujsx0uxJt4hA7mmn2VoH/ALjSj+OrY5RhgrZF5K9L8h5jgZpgzrtJulufRKjd4eaDYcbOIxDbSSR5aib5MKwNVWfFJC0lJ3EEfhoK5u8j+m7ualZtVdrsJyppuZbC2Oz7Uu9rhh2WOGPhq+NXCsWhIQhKBuSAkeSqLPVBRXnz+I9l+6U/XuVzO9+WOzhfaPPb2TtyZ/BC3/vUr601s9p8EN767+JHeU6Vs29FAUBQFAUBQFBjffZisOSZCw2w0kuOLUcEpSkYkk+AUDQyxqxlPNd2as9uMpqRLZXLtzkyM9HZmR2iAtyOtxICwOIHxHGpmEVfJ2q+XIWZpWVERrnMusFbTUswIEqSy0qQgOI43GkKSPNUDtNKFSnfs+5Zy1fbJly8ye73TMK3GbYhSVcK3GUhSklQGCT5wwx3mlE1ZIucrNOt91uMHtpLVnfehzG47Ljj3bxvTQhtIKlnbswG2lAk5P1Wy7ne4v2yzRbml6MpbUhyZAlRmW3W8CptTjqEpChiNhONJhFWtL1pyRAvEi0TFzGhFmptMmcYcgwW5bnDwtrkBBbSTxjeemlCpezXnSy5PixpFzLzr010RoMSG0uRJfdKSvhbbbBJwSCT0AUolmytmqz5wtQu9lcWqOHFx3W3kKZeafZUULbcbWApKkkbQaiYGTM2Y7XlKxTMx3pam7XAaL8haElaghPUlOJNAl5U1Gy1m+dItVuVJj3eM0mS7BuUZ+FI7Bw4JcSh9KSpJIwxFTMIq03tVsut5llZWYi3OXcYT7cOW5DgSpEZp15CXEhbzaCgeasE4nZShU8wcRjUJM296q5cseYX8sLjXKbdozbT8lu2QZM1LSJHEGytTKFAcXCd/VU0RUo5rzzZMnsQ13MSHZdxc7CDBhsuSJTzgQXFBLaAT5qQSSd1IhNXy257sl3yuvNltTKkQGi4h2O1HdVMS6ystuNlgJ4+NKgQRhSg0clap5dz7MkQ7HHuKTFLjb70yDJispdYWELbLjqEp40k7U440mEVbTuo2VmbTKva5Chb4dy+w319mvETDITF4cMMSO0UBjupRNXzN2olgyXJt8K6IlvTbmHVRI9vjPTHVJjhKnDwspUQAFCkQVbdrztly85YXnC2yu2sbbbrzjoSpK0CPj2iVIUApKklJBBGNRQfJ2d8u23KaM6zJBRYXGGpLTvApS1pkYdmlKACoqUVAAAY41NBjylnqyZx721bhIYnwFIRNgz2XI0lrtU8SCpDgBwUNoO6kwEpjV/J8jMCbA0qUSuau0IuHdnhAVPbx4mA/wAPDxAgjqx2Uoip71CRQUV58/iPZfulP17lczvfljs4X2jz29k7cmfwQt/71K+tNbPafBDe+u/iR3lOlbNvRQFAUBQFAUBQN/UGHLuGRcwQYCSqdItsplhKPSLi2VBIHlqYJMfTjPGUboxkrLtrjtXC9tWgqdeZCFuWxLDLbbiHflNlavNw2bqmYRBsx7pDtutede+Z2ayylcu2qFueMNPewITeOBkpKtu7zTTojqUNdMuPZpztk+DCVw3JuFdptufHyJsUR3mFY/npHkpaSUuW28rzDla+Xt1lcd2ZfpjzjLoKVocUEcSSD1HEUuTBY0gSpMjOvECMczTCMers2qiREWZrXfvs/O95+0ScrRc5xnLhY+xT/WW0rh8R7f0kgYg7OqrQhJ+pMuLZ896f5kuq0xsvRnpsZ+W8Qlll6TEwZK1HYnHhKQT0mohMs+iSky2s3XqKeK0XPMcuVbnk+rdYS20yXEHpSpaFYEb6iSGzr/8AB7NH7is/jFLeZJDy7eLVm7WiFdssSG59ttWWnIVxnRCHGBIkyGVtMlxOIKgG1HDHZU9DqQbHdYVu1czmiZndrL/He4qhZXjCSZie4xhsL6S5gr0fNP46dBPYIIxG0HcaqlAl5uMW2645oXLzo3lBDlutPCl7uYEvhMjEAy0q9HH5Pzqt0QdWry8uOS8qv3G6yLHcxIecs2ZWCz3WO/3c4pf7XFBS8nEAYbaQSU9FsxXHMuVZMu5JjOOMXKXEbuMBrsI89tl0pEpKMT6zpwJBNRJDW0QSpNszNxAjHNF4Ix2bO+LpJCMrhc4ByxfclF9AzXIz624zayR3pbS7s1KDgb38HZedxbsKsg/dSb1acu6o5Bud9lswLc1HuyXJEpaWmgSy1gCpRAxNRHJMk/I6Fv6T54vDLakWy8Tb1crZxJKOOK8VhDgScCAvAqHgNJ5kMOaW3G9EMkXFxJNvtj+X7hcthPDEjusqcUodSRtPgFOpJbyNcYGY9Xs2ZisDyJdhTbLbb1y4xC2HZban3VJSpOxRQhacerGk8gxYeYoGXcwxRk24iTHuWanIlyyRd22nZUaS7KUl+XGKPPbSNroxxTwnoqaIWNqiwoKK8+fxHsv3Sn69yuZ3vyx2cL7R57eyduTP4IW/96lfWmtntPghvfXfxI7ynStm3ooCgKAoNQXa1m5GzCWz9rBrvJh8ae3DOPDx8GOPDjsxoPBvtlENVwM6P3FDvdlyO0R2YeC+y4CrHDi4/Nw69lB7VdrWm4otCpbIurjZkNxCtPbKaBwKwjHEjHpoPJvVn7xLiGbH7zBQHZrXaI42W1AqCnBjikEDHE0CXIv+Q8uFE2TPtluNxT27by3GWO3Rv4wokcQ276mgySZGSpFv/imW5bnLYoJX9qOllTJGISk9qdm/YNtBt3S75etMZq7XiXEiRNiGJcpbbaP1gxAStRA84DoqB7tM2yTIRm2R6M7b3FKcL0RSFNFXyjxI2Y9dBgsmYcr3p2S1l64QpjrK8ZaILrbqkrOzFYQTgdnTQYWMwZMn3ORlyNcLe/dsSqTbm3WVv8Sd5W2DjiMOkVI37uuzsW152+GOi0tp4n1TOAMJSOlRXsAqBoWvNeTZcF92zXW3u263t8clUV5otMNYE4rKDgkYA76mg3TcbJOTHiqkRn0XFovRWitCw+yACVIGJ4kgKG0VA8SpeX8rQFS5jsW121KgFOuluO0FKOAxJwGJNAlqvWnM2I7mRUy0vQmXEtvXIrYU2l3YEhTuOAO0YbaniFSFmTL9xt7t1t9xiyLWwFF6Uw6hbKAgYq4lJOAwFRQYZgypLkMuThBdlSGVSI6ng0pa2GwCpaSraUp4htHXQaTWbdPMxOosjN2tVxecPCiEl5h9RKRjgEYncB1VNJCm1dsvxLUuezLis2aLxocfQttMdvslFCwVA8I4SMDUDDb8x5Uk2ty82y4wnLOFntZkd1osdoo7eJaThiSaDI4cuIuin3e6C8NMd5UtXZ94THxI4yT5wTs37qDHdbhlQ2xm93l+CbRglxibLU0WMHcCkpWvZ53RgaDFCzjkq5QpDtvu9uk2+Kgd6Ww+ytptCtg4yk4AHw1NAoqlWkK+y1usBfYF7upKMe7jzSrg+b0Y7qgJuXcw5JnqNtytcLc+pALio1udZUQMdp4Wz11I2UnLKb+qMnuYzKWe8KbHZ987DHh4yPT4cdmNQFWgKCivPn8R7L90p+vcrmd78sdnC+0ee3snbkz+CFv/AHqV9aa2e0+CG99d/EjvKdK2beigKAoCgq5AzUw5rYzqKGJ3ZTL+9lMS+7vCAq19iIrfDIKezOMtvHDHpq9OCpYfaUNRH9IhiWZObWc18B9H7OMfvysR1d6bw8tOgSL1mln/AK1nUJLE4t2y/RsqIliO8YCbaWlRX+J8J7Mf1p7dj8mlOB1edRHnst6h5/zq2tX2Y5GZy/dUgngDM+3DsHSN3mvBKcepZpBJ7XV+LN0tyVlODHjv5szDbYsCA8+2hxUWN3dBkycVDYG0bvpFIqOqWrrPa7DaMq5Q0ghxpTtilugyo9tZclPqg2xrtCShkFR4ni3icOmkfFEk1q8pzRpVpsm5oKpEXMcKz3BmUgpX2sFbkdYdQsYgngxIIp1Ge6J+xYutcTKqQxbGITTzTUTzWmpbsAmQUBOxKuEJJw6afALVvgWez6iafoy221HZlZcnNzu6gALitpjKaUvh34KJwJ6zTokhZVj27KFzy0i5woV8y5Ovkr+Hc025xbU9ubMU8SiW1gCsbVJJ4iNm1NShIXMJxf8AR3M/AAVdzOAVuJ4hvqtvNMm9mO2XqJoznUXu12e3F2xvho2Ja19onuy8e14mm8CMdm+p6hm6byH7XqXkrIU1alSMvwboIilkkrtkxqK9GVid4SCW/wBCplEJF5iG5L2TLa1DaZflLv8AaENMyiQwtZnNgJcICjwk79h2VW1MkLU63X+Bpi0xcbPaI10XmC0lmBa3FiG8DPYCQ6tTKSOI7D5p2VMcySFbo8uPbNZ2rlAj2i8m2oLlqtiu1hoaNvc7N1DnC3xKWceLzE7qn4IOW1FuRnrTFAIcQcrzytIwUOFSYQ2+A1CSjkWwWSNqtnxceBGbXGNuMdSGkJLZXFPFw4DZj04VE8iDEj8L2nWX4UvzrVL1AkMT0L2tra+1ZCkpWDsIKwnYat1QcWqUHJdtyZn5nK5Dd0LsB69RmQpLLbpU1wFIwCASgAnDy1Ec0tm/qC9Tb4UHiSnIoxIOIBL72H4adA6NM4lvuejOV4s9pqSybFDUpp4JcTimMkg4Kx3VE8yEZItkCHypz5MOM0zIdhOl51pCUrUESlYcRAxOAq3U6HlOcS5qyezUFAZLeJ4TiBxPpw3deFR0EbaUx7g1fdKJF3tMO1w122WLfcLe520ia53THhlAttcA4MVYYr84b6mUBjNTK9bW9ROwndm/mBzKIld3eEA2zse6owkcPZn+uJxwx6aU4HVaKqLCgorz5/Eey/dKfr3K5ne/LHZwvtHnt7J25M/ghb/3qV9aa2e0+CG99d/EjvKdK2beigKAoCgr/E/w42P2X3jB3fe6PR/a/wBKr9VehZe/xPsexfygd/t3tR3eD/XUdE9SU5/h6vHsvvSZ6X3wr0/2v9KnVHRh1E/l/Vv2D2S3+2+h7Ij1nh+b4cKR0Sj/AFW9Zp57V/K7fuT2jc16H0OvyVMIlKOmH835S9Z/LEz3p7b7a16P9LyVEpgy9Rv5Evfo/wA+u+7/AFu7/wAH9r1/SxqYRJ86HfCa9e7PWzPXen6v+8/2nzvo4VE80wZ/LD/Nlz3+7E+9PW+t/u7+x/6cKm5EEHTn42xd3va4ev8A5f3ue6/2/wA79Kk8jqnrXv4RZm9X7Er13obxvqtvNMoU05+HGoXvP3Mr3/6j1Lvo+Drq080QerP+IXKPsH8qK9D230v+x1eWo6HU99Zfcdm9V/MFo9fu9ub3fS6qiEy96x/yzb/Ve/LR6/0feDO7w9XhpBJFZ+IWovsfuaB7R6j1Uj2rwf0adBGvK7/OMv1vuv8AvX1nrU+6/wCyf92rXIhMWUviTnr1X93+h6z2Y+n/AKKrPJKObt/h+vnsXvqf6X3s56j9v8z6eFT1R0aWnHwNzv7P6Un+Y/b/AGdPvL6fzfo8NTPMjk1dAfcmdPX+7W/5h97eqd3/ANm/8vy0uIJvLJ79R7190r9o92bkep+j83wUuISjl3/DvJ9g92Tva/YPSc9Z9HrqJ5p6GFy2+nmb2j3az7/957nPVf2T5lTciDgtHubRz2b0XfU+n7sd9T9H53gqPil5H+HiB7J70j7t3vkbv2v9OnVHRPDXqkfmj8lVWe6CivPn8R7L90p+vcrmd78sdnC+0ee3snbkz+CFv/epX1prZ7T4Ib3138SO8p0rZt6KAoCgKD//2Q==';

var doc = new jsPDF('l', 'pt' );


doc.autoTable(datosTitulo, datosTitulo,{
	styles: {
		rowHeight: 0,
	},
     headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica",
            halign: 'center',
            fontSize: 18,
            rowHeight: 30,
    },
    margin: {top: 110},

});


doc.autoTable(columns, rows, {
    styles: {lineColor: 200,
            lineWidth: 0.1,
            fillStyle: 'S'},

    headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica"
    },
    margin: {top: 140},








    beforePageContent: function(data) {


        doc.addImage(imgData, 'JPEG', 55, 40, 128, 61);

        doc.setFontSize(30);
        doc.setFontType("bold");
        doc.setTextColor(93);

        doc.text("Orden de Compra", 294, 80);
        

    }

});

doc.autoTable(datosProyecto, datosProyecto,{
	styles: {
		rowHeight: 0,
	},
     headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica",
            halign: 'center',
            fontSize: 18,
            rowHeight: 30,
    },
    margin: {top: 190},

});



doc.autoTable(columnsB, rowsB, {
    styles: {lineColor: 200,
            lineWidth: 0.1,
            fillStyle: 'S',
            overflow: 'linebreak',
    },

    headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica",

    },
    margin: {top: 220},

});

doc.autoTable(datosProveedor,datosProveedor, {
     headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica",
            halign: 'center',
            fontSize: 18,
            rowHeight: 30,
    },
    margin: {top: 270},

});



doc.autoTable(columnsC, rowsC, {
    styles: {lineColor: 200,
            lineWidth: 0.1,
            fillStyle: 'S',
            overflow: 'linebreak',
    },

    headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica",

    },
    margin: {top: 300},

});


doc.autoTable(columnsD, rowsD, {
    styles: {lineColor: 200,
            lineWidth: 0.1,
            fillStyle: 'S',
            overflow: 'linebreak',
    },

    headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica",

    },
    margin: {top: 340},

});

		doc.setFontSize(12);
        doc.setTextColor(93);

		doc.text("Estimado(s) Señor(es) NOMBRE_PROVEEDOR, agradecemos sirvan enviar o entregar los siguientes artículos y/o servicios,", 88, 400)
		doc.text("cuyo valor deberá ser facturado acompañando la presente orden y la guía de despacho respectiva", 159, 415)





        doc.setFontSize(10);
        doc.setTextColor(93);

        doc.text("Esta solicitud se considerará válida sólo si se envía al correo electrónico compras@ceaf.cl, con copia al correo del líder del Centro de Costos al que se cargará.", 66, 500);


doc.save('table2.pdf');
</script>