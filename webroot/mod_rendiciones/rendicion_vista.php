





<div id="container" class="rendicion nopadding nomargin">
        <h1 class="titulo"> Vista de Rendición
            <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button>
        </h1>
    <div id="imprimir" class="col-md-12 col-xs-12 wrapper nomargin">

           
        <div class="col-md-12 table-responsive vista">
 <script>
$( "#fecha_rendicion" ).datepicker({
    inline: true,
    dateFormat: 'dd-MM-yy'
});
$( "#fecha_rendicion_devolucion" ).datepicker({
    inline: true,
    dateFormat: 'dd-MM-yy',
});
  </script>
        <form action="post.php" method="get">
                <table class="table table-no-bordered">
                    <thead>
                    <tr>
                        <td width="20%" id="logovista"></td>
                        <td id="titulovista" class="text-center">Solicitud de <br> Fondos Por Rendir </td>
                        <td width="20%" id="codigovista" class="text-right">
                            <table>
                                <tr>
                                    <td>Código:</td>
                                    <td class="text-right">R1-ADQ</td>
                                </tr>
                                <tr>
                                    <td>Versión:</td>
                                    <td class="text-right">1</td>
                                </tr>
                                <tr>
                                    <td>Fecha:</td>
                                    <td class="text-right">
                                        <?php echo date("d/m/y");?>                                        
                                    </td>
                                </tr>
                            </table>
                                
                        </td>

                    </tr>
                    </thead>
                </table>

<table class="table table-bordered table-bordered">
                        <tr>
                            <th> Nombre </th>
                            <th width="10%"> Fecha </th>
                            <th width="20%"> Centro de Costos </th>
                            <th width="20%"> Cantidad Solicitada </th>
                            <th width="20%"> Fecha de Devolución Estimada </th>
                        
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="nombre_v"></td>
                        <td id="fecha_rendicion_v"></td>
                        <td id="centro_v"></td>
                        <td id="cantidad_v"></td>
                        <td id="fecha_devolucion_v"></td>    
                        </td>
                    </tr>
                </tbody>
            </table>

            
            <table class="table table-bordered table-bordered">
                <thead>
                    <tr>
                        <th> Justificación Fondo </th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td id="justificacion_v">
                           
                        </td>
                    </tr>
                </tbody>
            </table>

<script>
$( "#nombre" ).change(function() {
    var value = $( this ).val();
    $( "#nombre_v" ).text( value );
  })
  .change();

$( "#fecha_rendicion" )
  .change(function() {
    var value = $( this ).val();
    $( "#fecha_rendicion_v" ).text( value );
  })
  .change();

$( "#centro" )
  .change(function() {
    var value = $( this ).val();
    $( "#centro_v" ).text( value );
  })
  .change();

$( "#cantidad" )
  .change(function() {
    var value = $( this ).val();
    $( "#cantidad_v" ).text( value );
  })
  .change();

$( "#justificacion" )
  .change(function() {
    var value = $( this ).val();
    $( "#justificacion_v" ).text( value );
  })
  .change();

$( "#fecha_devolucion" )
  .change(function() {
    var value = $("#fecha_devolucion").val();
    $( "#fecha_devolucion_v" ).text( value );
  })
  .change();

</script>
 

        </div><!--/Nombrefechacentro -->

        <div id="aviso" class="col-md-12">
        <p class="text-center">
        Esta solicitud se considerará válida sólo si se envía al correo electrónico compras@ceaf.cl, con copia al correo del líder del Centro de Costos al que se cargará.
        </p>
        <hr class="espaciador">
        <input type="button" class="botongrande verde col-md-5" id="enviarcorreo" onclick="" value="Enviar por Correo" />
        
        <input type="button" class="botongrande verde col-md-5 pull-right" id="descargar" onclick="" value="Descargar" />
        
        </div>

        </div><!--/tabladatos .Wrapper-->
    </div><!--/datos-->

<div id="invi">
</div>
</form>

<script>

$("#descargar").click(function()
{
 $("#invi").load("mod_rendiciones/rendicion_pdf.php")

});

$("#enviarcorreo").click(function()
{
 $("#invi").load("mod_rendiciones/rendicion_enviar.php")

});

</script>

</body>
</html>