<script>


$nombre = $( "#nombre" ).val();
$fecha = $( "#fecha_rendicion" ).val();
$centro = $( "#centro" ).val();
$cantidad = $( "#cantidad" ).val();
$fechaDevolucion = $( "#fecha_devolucion" ).val();
$justificacion = $( "#justificacion" ).val();



var columns = [
    {title: "Nombre", dataKey: "nombre_v"},
    {title: "Fecha", dataKey: "fecha_v"},
    {title: "Centro de Costos", dataKey: "centro_v"},
    {title: "Cantidad Solicitada", dataKey: "cantidad_v"},
    {title: "Fecha de Devolución Estimada", dataKey: "fecha_devolucion_v"},
];
var rows = [
    {"id": 1, "nombre_v": $nombre, "fecha_v": $fecha, "centro_v": $centro, "cantidad_v": $cantidad, "fecha_devolucion_v": $fechaDevolucion},
];
var columnsB = [
    {title: "Justificación Fondo", dataKey:"justificacion_v"}
];

var rowsB = [
    {"id": 1, "justificacion_v" : $justificacion}
];

// Only pt supported (not mm or in)

var imgData = '';

var doc = new jsPDF('l', 'pt' );





doc.autoTable(columns, rows, {
    styles: {lineColor: 200,
            lineWidth: 0.1,
            fillStyle: 'S'},

    headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica"
    },
    margin: {top: 150},








    beforePageContent: function(data) {


        doc.addImage(imgData, 'JPEG', 55, 40, 170, 81);

        doc.setFontSize(30);
        doc.setFontType("bold");
        doc.setTextColor(93);

        doc.text("Solicitud de", 336, 80);
        doc.text("Fondos por Rendir", 286, 110);

    }

});

doc.autoTable(columnsB, rowsB, {
    styles: {lineColor: 200,
            lineWidth: 0.1,
            fillStyle: 'S',
            overflow: 'linebreak',
    },

    headerStyles: {
            fillColor: 65,
            lineColor: 255,
            lineWidth: 0.1,
            fillStyle: 'DF',
            font: "helvetica",

    },
    margin: {top: 210},

});


        doc.setFontSize(10);
        doc.setTextColor(93);

        doc.text("Esta solicitud se considerará válida sólo si se envía al correo electrónico compras@ceaf.cl, con copia al correo del líder del Centro de Costos al que se cargará.", 66, 500);


doc.save('table2.pdf');
</script>