

<div id="container" class="rendicion nopadding nomargin">
        <h1 class="titulo"> Solicitud de Fondos por Rendir <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h3>
    <div class="col-md-12 col-xs-12 wrapper nomargin">

           
        <div id="forma" class="col-md-12 table-responsive ">
 <script>
$( "#fecha_rendicion" ).datepicker({
    inline: true,
    dateFormat: 'dd/mm/yy'
});
$( "#fecha_devolucion" ).datepicker({
    inline: true,
    dateFormat: 'dd/mm/yy'
});
  </script>

<form action="" class="rendicion" method="get" action="mod_rendiciones/rendicion_vista.php">
<table class="table table-bordered">
    <thead>
        <th>Nombre</th>
        <th>Fecha</th>
        <th>Centro de Costos</th>
    </thead>

    <tbody>
      <td><input type="text" id="nombre"></td>
      <td><input type="text" id="fecha_rendicion"></td>
        <td>
            <select id="centro">
                <option value="Gestión">Gestión</option>
                <option value="Mejoramiento Genético">Mejoramiento Genético</option>
                <option value="Genómica">Genómica</option>
                <option value="Fisiología">Fisiología del Estrés</option>
                <option value="Agronomía">Agronomía</option>
            </select>
      </td>
    </tbody>
    </table>

    <table class="table table-bordered">
    <thead>
        <th>Cantidad Solicitada</th>
        <th>Fecha de Devolución Estimada</th>
    </thead>
    <tbody>
        <td><input type="number" id="cantidad"></td>
        <td><input type="text" id="fecha_devolucion" id="fecha_devolucion"></td>
    </tbody>
    </table>

    <table class="table table-bordered">
    <thead>
        <th>Justificación Fondo</th>
    </thead>
    <tbody>
        <td><textarea id="justificacion"></textarea></td>
    </tbody>

    </table>

        <p class="text-center">
        Esta solicitud se considerará válida sólo si se envía al correo electrónico compras@ceaf.cl, con copia al correo del líder del Centro de Costos al que se cargará.
        </p>
        <hr class="espaciador">

  
   <a href="mod_rendiciones/rendicion_vista.php" class="" data-toggle="modal" data-target="#rendicion_vista" onclick="rendicionver();">
    <input type="button" value="Siguiente" class="botongrande verde col-md-12">
  </a
>
  
 
  
</form>



</body>
</html>