





<div id="container" class="rendicion nopadding nomargin">
        <h1 class="titulo"> Vista de Rendición
            <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button>
        </h1>
    <div id="imprimir" class="col-md-12 col-xs-12 wrapper nomargin">

           
        <div class="col-md-12 table-responsive vista">
 <script>
$( "#fecha_rendicion" ).datepicker({
    inline: true,
    dateFormat: 'dd-MM-yy'
});
$( "#fecha_rendicion_devolucion" ).datepicker({
    inline: true,
    dateFormat: 'dd-MM-yy',
});
  </script>
        <form action="post.php" method="get">
                <table class="table table-no-bordered">
                    <thead>
                    <tr>
                        <td width="20%" id="logovista"></td>
                        <td id="titulovista" class="text-center">Rendición de Fondos</td>
                        <td width="20%" id="codigovista" class="text-right">
                            <table>
                                <tr>
                                    <td>Nº:</td>
                                    <td class="text-right">ID ACÁ</td>
                                </tr>
                                <tr>
                                    <td>Fecha:</td>
                                    <td class="text-right">
                                        <?php echo date("d/m/y");?>                                        
                                    </td>
                                </tr>
                            </table>
                                
                        </td>

                    </tr>
                    </thead>
                </table>


<h1 class="titulo2 gris">Datos</h1>

<table class="table table-bordered table-bordered">
                        <tr>
                            <th> Nombre </th>
                            <th> Rut </th>
                            <th> Centro de Costos </th>
                                                    
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="nombre_v">Nombre del usuario</td>
                        <td id="rut_v"> Rut del usuario</td>
                        <td id="centro_v">Centro asociado al Usuario</td>
                                         
                    </tr>
                </tbody>
            </table>

<h1 class="titulo2 gris">Items</h1>

            <table class="table table-bordered table-bordered">
                <thead class="textocentro">                    
                            <th>Ítem</th>
                            <th>Subítem</th>
                            <th>Centro de Costos</th>
                            <th>Fecha Doc</th>
                            <th>Nº Doc</th>
                            <th>Tipo Documento </th>
                            <th> Total </th>
                            <th> Rut Proveedor</th>
                            <th> Nombre Proveedor</th>
                            <th> Detalle</th>
                            <th id="eliminar"></th>
                            
                    </thead>
                
                   <tbody>
                        <tr>
                            <td id="item">Acá va el Item 1</td>
                            <td id="subitem">Subitem 1</td>
                            <td id="centrodecostos">Centro de Costos</td>
                            <td id="fecha">Fecha 1</td>

                            <td id="ndoc">Ndoc 1</td>
                            
                            <td id="tipodoc">Tipo Doc 1</td>
                            
                            <td id="total">Total 1</td>
                            <td id="rutp">Rut 1</td>
                            <td id="nombrep">Nombre P</td>
                            <td id="detalle"> Detalle 1</td>  
                        </tr>

                       
                    </tbody>
            </table>

<script>

$( "#nombre" ).change(function() {
    var value = $( this ).val();
    $( "#nombre_v" ).text( value );
  })
  .change();

$( "#rut" )
  .change(function() {
    var value = $( this ).val();
    $( "#rut_v" ).text( value );
  })
  .change();

$( "#giro" )
  .change(function() {
    var value = $( this ).val();
    $( "#giro_v" ).text( value );
  })
  .change();

</script>
 

        </div><!--/Nombrefechacentro -->

        <div id="aviso" class="col-md-12">
        <p class="text-center">
        Esta solicitud se considerará válida sólo si se envía al correo electrónico compras@ceaf.cl, con copia al correo del líder del Centro de Costos al que se cargará.
        </p>
        <hr class="espaciador">
        <input type="button" class="botongrande verde col-md-5" id="enviarcorreo" onclick="" value="Enviar por Correo" />
        
        <input type="button" class="botongrande verde col-md-5 pull-right" id="descargar" onclick="" value="Descargar" />
        
        </div>

        </div><!--/tabladatos .Wrapper-->
    </div><!--/datos-->

<div id="invi">
</div>
</form>

<script>

$("#descargar").click(function()
{
 $("#invi").load("mod_rendiciones/generar_rendirfondos_pdf.php")

});

$("#enviarcorreo").click(function()
{
 $("#invi").load("mod_rendiciones/rendicion_enviar.php")

});

</script>

</body>
</html>