<?php
session_start();
 // store session data
?>
<!doctype html>
<html><head>
<body color="gris">
<script>
var detail_row = 0;
var d = new Date();

var month = d.getMonth()+1;
var day = d.getDate();

var output = ((''+day).length<2 ? '0' : '') + day + '/' + ((''+month).length<2 ? '0' : '') + month + '/' + d.getFullYear();

$("#hoy").text(output)
</script>

 <script>
$( ".fechadoc").datepicker({
    inline: true
});
  </script>


<!--F: Pongo una solución parche, después hay que verlo bien con el tema de la base-->
<script>

</script>



<div id="container" class="rendicion">
    <h1 class="titulo"> Rendir Fondos
            <button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button>
    </h1>

    <div class="col-md-12 col-xs-12 wrapper">

        <h1 class="titulo2 gris separado"> Items
                <input type="button" class="nopadding boton-mas2 pull-right" id="agregar_item" onclick="add_item_rendiciondos();" value="+" />
            </h1>

        <div id="rendicion" class="col-md-12">


            <table class="table table-bordered">
                   
      
                <table id="tablarendirfondos" class="table table-bordered">
                    <thead class="textocentro">                    
                            <th>Ítem</th>
                            <th>Subítem</th>
                            <th>Centro de Costos</th>
                            <th>Fecha Doc</th>
                            <th>Nº Doc</th>
                            <th>Tipo Documento </th>
                            <th> Total </th>
                            <th> Rut Proveedor</th>
                            <th> Nombre Proveedor</th>
                            <th> Detalle</th>
                            <th id="eliminar"></th>
                            
                    </thead>
                        
                    <tbody>
                        <tr>
                            <td id="compra_cantidad_0">

                                    <select id="items" name="detalle[0][item]" id="Detalle0Item" class="item-input  " data-linea="0" required >
                                        <option value="valor1" selected>Item 1</option>
                                        <option value="2">Gastos de Operación</option> <!--Esto funciona con 200 más abajo-->
                                        
                                    </select>
                            </td>
                            <td id="subitem" >
                                <select id="subitems" name="detalle[0][subitem]" id="Detalle0subitem" class="item-input  " data-linea="0" required  >
                                       
                                        <!-- estas son las opciones dependientes del Gastos de Operación, osea, 2-->
                 
                                </select>
                            </td>
                            <td id="centrodecostos">
                                <select name="detalle[0][ccostos]" id="Detalle0ccostos" class="item-input  " data-linea="0" required  >
                                    <option> Gestión </option>
                                    <option> Mejoramiento Genético </option>
                                    <option> Genómica </option>
                                    <option> Fisiología del Estrés </option>
                                    <option> Agronomía </option>
                                </select>
                            </td>


                            <td id="fecha">
                                <input type="text" id="fechadoc" name="detalle[0][fechadoc]" id="Detalle0fechadoc" class="item-input fechadoc" data-linea="0" required >
                            </td>

                            <td id="ndoc">
                                <input type="number" name="detalle[0][ndoc]" id="Detalle0ndoc" class="item-input  " data-linea="0" required  >
                            </td>
                            
                            <td id="tipodoc">
                                <input type="text" name="detalle[0][tipodoc]" id="Detalle0tipodoc" class="item-input  " data-linea="0" required  >
                            </td>
                            
                            <td id="total">
                                <input type="number" name="detalle[0][total]" id="Detalle0total" class="item-input  " data-linea="0" required  >
                            </td>

                            <td id="rutp">
                                <input type="number" name="detalle[0][rutp]" id="Detalle0rutp" class="item-input  " data-linea="0" required  >
                            </td>

                            <td id="nombrep">
                                <input type="text" name="detalle[0][nombrep]" id="Detalle0nombrep" class="item-input  " data-linea="0" required  >
                            </td>

                            <td id="detalle">
                                <input type="text" name="detalle[0][detalle]" id="Detalle0detalle" class="item-input  " data-linea="0" required  >
                            </td>  

                            <td></td>
                        </tr>

                       
                    </tbody>
                </table>
<script>
    $("textarea").removeAttr('cols');
    $("textarea").width('100%');
</script>


        <hr class="espaciador">
        <a href="mod_rendiciones/guardar_rendicion.php" class="" data-toggle="modal" data-target="#generaroc">
            <input type="button" class="botongrande verde col-md-12 col-xs-12" value="Guardar"/>
        </a>

    
<script>
function add_item_rendiciondos(){
    detail_row++;
    var table = document.getElementById("tablarendirfondos");
    var row = table.insertRow(-1);
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);
    var cell5 = row.insertCell(4);
    var cell6 = row.insertCell(5);
    var cell7 = row.insertCell(6);
    var cell8 = row.insertCell(7);
    var cell9 = row.insertCell(8);
    var cell10 = row.insertCell(9);
    var cell11 = row.insertCell(10);


    cell1.innerHTML = "<select name='detalle[" + detail_row + "][item]' id='Detalle" + detail_row + "item' class='item-input  ' data-linea='" + detail_row  + "'><option value='valor1' selected>Item 1</option><option value='2'>Gastos de Operación</option></select>";

    cell2.innerHTML = "<select id='subitems' name='detalle[" + detail_row + "][subitem]' id='Detalle" + detail_row + "subitems' class='item-input  ' data-linea='" + detail_row  + "'><option value='valor1' selected>SubItem 1</option><option value='2'>Subitem2</option></select>";

    cell3.innerHTML = "<td id='centrodecostos'><select  name='detalle[" + detail_row + "][centrodecostos]' id='Detalle" + detail_row + "centrodecostos' class='item-input  ' data-linea='" + detail_row  + "'><option> Gestión </option><option> Mejoramiento Genético </option><option> Genómica </option><option> Fisiología del Estrés </option><option> Agronomía </option></select></td>";

    cell4.innerHTML = "<td id='fechadoc> <input type='text" + detail_row + "'> <input type='text' name='detalle[" + detail_row + "][fechadoc]' id='Detalle" + detail_row + "Quantity' class='item-input  fechadoc' data-linea='" + detail_row  + "'</td>";

    cell5.innerHTML = "<td id='ndoc'><input type='number' name='detalle[" + detail_row + "][ndoc]' id='Detalle" + detail_row + "ndoc' class='item-input  ' data-linea='" + detail_row  + "'></td>";

    cell6.innerHTML = "<td id='tipodoc'><input type='text' name='detalle[" + detail_row + "][tipodoc]' id='Detalle" + detail_row + "tipodoc' class='item-input  ' data-linea='" + detail_row  + "'></td>";

    cell7.innerHTML = "<td id='total'> <input type='number' name='detalle[" + detail_row + "][total]' id='Detalle" + detail_row + "total' class='item-input  ' data-linea='" + detail_row  + "'> </td>";

    cell8.innerHTML = "<td id='rutp'> <input type='number' name='detalle[" + detail_row + "][rutp]' id='Detalle" + detail_row + "rutp' class='item-input  ' data-linea='" + detail_row  + "'></td>";

    cell9.innerHTML = "<td id='nombrep'> <input type='text' name='detalle[" + detail_row + "][nombrep]' id='Detalle" + detail_row + "nombrep' class='item-input  ' data-linea='" + detail_row  + "'></td>";

    cell10.innerHTML = "<td id='detalle'> <input type='text' name='detalle[" + detail_row + "][detalle]' id='Detalle" + detail_row + "detalle' class='item-input  ' data-linea='" + detail_row  + "'></td>";

    cell11.innerHTML = "<td id='eliminar'><button class='borraroc' onclick='del_item(this)'></button></td>";
    
    $( ".fechadoc").datepicker({
    inline: true
});
}

function del_item(r) {
    var i = r.parentNode.parentNode.rowIndex;
    document.getElementById("tablarendirfondos").deleteRow(i);
}
</script>

        </div><!--/tabladatos .Wrapper-->
    </div><!--/datos-->


</body>
</html>