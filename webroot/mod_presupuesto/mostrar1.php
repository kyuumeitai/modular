     
        <script src="js/jquery.tablesorter.min.js"></script>
        <div id="rendicion" class="col-md-12 table-responsive ">
        <hr>
    <script>
          $(document).ready(function() 
          { 
          $("#tabla").tablesorter(); 
          } 
          ); 
    </script>

<h2 class="modularh2 text-left">Presupuesto</h2>
<h1 class="modularh1 text-left">
              Últimas Compras Aprobadas
              </h1>
        <hr>
            <table id ="tabla" class="table table-bordered table-bordered">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <th> Nombre de Proveedor </th>
                            <th> Item </th>
                            <th> Área </th>
                            <th> Fecha </th>
                            <th> Total </th>
                            <th>   </th>
                          
                        </tr>
                    </thead>
                <tbody>
                    <tr>
                        <td id="subitem">#1005</td>
                        <td id="centrocosto">Proveedor de Ejemplo LTDA</td>
                        <td id="docfecha">Item de Ejemplo</td>
                        <td id="docfecha">Gestión</td>
                        <td id="docnum">02/12/2015</td>
                        <td id="doctipo">$150.000</td>
                        <td id="neto"><a href="#">Ver Más</td></a>
                    </tr>
                    <tr>
                        <td id="subitem">#1004</td>
                        <td id="centrocosto">Proveedor DOS</td>
                        <td id="docfecha">Otro Item</td>
                        <td id="docfecha">Genómica</td>
                        <td id="docnum">29/11/2015</td>
                        <td id="doctipo">$50.132</td>
                        <td id="neto"><a href="#">Ver Más</td></a>
                    </tr>
                    <tr>
                        <td id="subitem">#1003</td>
                        <td id="centrocosto">Proveedor de Ejemplo</td>
                        <td id="docfecha">Otros Items</td>
                        <td id="docfecha">Mej. Genético</td>
                        <td id="docnum">25/11/2015</td>
                        <td id="doctipo">$1.500</td>
                        <td id="neto"><a href="#">Ver Más</td></a>
                    </tr>
                    <tr>
                        <td id="subitem">#1002</td>
                        <td id="centrocosto">Proveedor de Ejemplo LTDA</td>
                        <td id="docfecha">Más ITEMS</td>
                        <td id="docfecha">Agronomía</td>
                        <td id="docnum">23/11/2015</td>
                        <td id="doctipo">$320.000</td>
                        <td id="neto"><a href="#">Ver Más</td></a>
                    </tr>
                    <tr>
                        <td id="subitem">#1001</td>
                        <td id="centrocosto">Proveedor</td>
                        <td id="docfecha">Item de Ejemplo</td>
                        <td id="docfecha">Fisiología</td>
                        <td id="docnum">20/11/2015</td>
                        <td id="doctipo">$99.990</td>
                        <td id="neto"><a href="#">Ver Más</td></a>
                    </tr>


                 </tbody>
            </table>
              <hr>
              <hr>
              <hr>
        </div><!--/rendicion -->
         