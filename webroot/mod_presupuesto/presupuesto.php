<?php
session_start();
 // store session data
?>




<script>
 $('#presupuesto').on('shown.bs.modal', function (event) {
        var randomScalingFactor = function(){ return Math.round(Math.random()*100)};

// Any of the following formats may be used
var ctx = document.getElementById("myChart");
var ctx = document.getElementById("myChart").getContext("2d");
var ctx = $("#myChart");


var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: ["Septiembre","Octubre", "Noviembre", "Diciembre", "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto"],
        
        datasets: [

        {
            label: 'Meses',
            lineTension: 0.1,
            data: [15000000, 14300000, 13550000, 16126020, 13512956, 15005201, 12352112, 13546687, 16555963, 12364458, 26826550, 22385612],
            backgroundColor: 'rgba(151,187,205,0)',
            borderColor: 'rgba(71,192,189,1)',
            borderWidth: 3,
            pointBackgroundColor: 'rgba(71,192,189,1)'
        },
        {
            label: 'Gastos',
            lineTension: 0.1,
            data: [3132053, 4250212, 3256930, 16126020, 13512956, 15005201, 12352112, 13546687, 16555963, 12364458, 26826550, 22385612],
            backgroundColor: 'rgba(151,187,205,0)',
            borderColor: 'rgba(212,20,90,1)',
            borderWidth: 3,
            pointBackgroundColor: 'rgba(71,192,189,1)'
        }

        ];
    },
    options: {
      }
});




        var ctx2 = $("#chart3").get(0).getContext("2d");
        legend(document.getElementById("leyendadona1"), data1);
        var myDoughnutChart = new Chart(ctx2).Doughnut(data1, {
            responsive: true,
        });
        var ctx3 = $("#chart4").get(0).getContext("2d");
        legend(document.getElementById("leyendadona2"), data2);
        var myDoughnutChart = new Chart(ctx3).Doughnut(data2, {
            responsive: true,
        });
        var ctx4 = $("#chart5").get(0).getContext("2d");
        legend(document.getElementById("leyendadona3"), data3);
        var myDoughnutChart = new Chart(ctx4).Doughnut(data3, {
            responsive: true,
        });


});
    </script>

<title>Datos</title>
<h1 class="titulo">Presupuesto<button type="button" class="close cerrarmodal nopadding" data-dismiss="modal" aria-hidden="true"></button></h1>
<div class="contenedor nopadding nomargin">  
 
   <!doctype html>
<html>
	<head>
		<title>Line Chart</title>
		<script src="js/jquery-2.1.4.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<link href="css/jquery-ui.min.css" rel="stylesheet">
<link href="css/animate.css" rel="stylesheet">
<script src="js/wow.js"></script>
<script src="js/legend.js"></script>
<script src="js/smooth-scroll.js"></script>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,700,600,800' rel='stylesheet' type='text/css'>

	<script src="js/Chart.js"></script>	
	</head>
	<body>
		
<div class="container">

  <ul class="nav nav-tabs pull-right">

	<li class="active"><a data-toggle="tab" href="#home">12 Meses</a></li>
	<li><a data-toggle="tab" href="#menu1">30 Días</a></li>
	<li><a data-toggle="tab" href="#menu2">7 Días</a></li>
	<li><a data-toggle="tab" href="#menu2">1 Día</a></li>
	<li><a data-toggle="tab" href="#menu2">Tiempo Real</a></li>
  </ul>

  <div class="tab-content">
	<div id="home" class="tab-pane fade in active">
<!-- Cabecera -->
	<h2 class="modularh2 text-left">Presupuesto</h2>
	<h1 class="modularh1 text-left">Gastos</h1>
	<hr class="" />
<!-- Fin Cabecera -->

		<div id= "grafico1" class="col-md-12">
			<canvas id="myChart" height="100"></canvas>
				
		</div>
			<h2 class="modularh2 text-left">Presupuesto</h2>
			<h1 class="modularh1">Gasto por Áreas</h1>
			<hr>

				<div class="col-md-4">

				<h2 class="modularh2 text-center">&nbsp &nbsp &nbsp12 Meses</h2>
				<hr class="espaciador">

				<canvas id="chart3"></canvas>
				<div id="leyendadona1"></div>
				</div>

				
				<div class="col-md-4">
				<h2 class="modularh2 text-center"> &nbsp &nbsp &nbsp30 Días</h2>
				<hr class="espaciador">

					<canvas id="chart4"></canvas>
					<div id="leyendadona2"></div>
				</div>


				<div class="col-md-4">
					<h2 class="modularh2 text-center">&nbsp &nbsp &nbsp1 día</h2>
					<hr class="espaciador">

					<canvas id="chart5"></canvas>
					<div id="leyendadona3"></div>
				</div>
   <?php include 'mostrar1.php';?>



	  <script>
var data1 = [{	
	value: 40,
	color: "#F7464A",
	highlight: "#FF5A5E",
	label: "Gestión"
}, {
	value: 10,
	color: "#C185BD",
	highlight: "#C199BE",
	label: "Fisiología"
}, {

	value: 30,
	color: "#46BFBD",
	highlight: "#5AD3D1",
	label: "Genómica"
}, {
	value: 10,
	color: "#8DC63F",
	highlight: "#96D343",
	label: "Agronomía"
}, {
	
	value: 20,
	color: "#FDB45C",
	highlight: "#FFC870",
	label: "Mej. Genético"
}

];
var data2 = [
	{
		value: 300,
		color:"#F7464A",
		highlight: "#FF5A5E",
		label: "Gestión"
	},
	{
		value: 50,
		color: "#46BFBD",
		highlight: "#5AD3D1",
		label: "Genómica"
	},
	{
		value: 100,
		color: "#FDB45C",
		highlight: "#FFC870",
		label: "Mej. Genético"
	}
]

var data3 = [
	{
		value: 600,
		color:"#F7464A",
		highlight: "#FF5A5E",
		label: "Gestión"
	},
	{
		value: 320,
		color: "#46BFBD",
		highlight: "#5AD3D1",
		label: "Genómica"
	},
	{
		value: 500,
		color: "#FDB45C",
		highlight: "#FFC870",
		label: "Mej. Genético"
	}
];



</script>

	</div>
	<div id="menu1" class="tab-pane fade">


	</div>
	<div id="menu2" class="tab-pane fade">
	  <h3>Menu 2</h3>
	  <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
	</div>
	<div id="menu3" class="tab-pane fade">
	  <h3>Menu 3</h3>
	  <p>Eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
	</div>
  </div>
</div>


	
<!--Doughnut-->

	</body>
</html>

</div> <!--contenedor-->